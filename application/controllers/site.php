<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller{
	function __construct(){
		parent::__construct();		
		// setlocale (LC_TIME, 'INDONESIA');
		date_default_timezone_set("Asia/Jakarta");
		
		$this->auth = unserialize(base64_decode($this->session->userdata('calon_mah')));
		$this->host	= $this->config->item('base_url');
		
		$this->db2 = $this->load->database('second', TRUE); 
		
		$this->smarty->assign('host',$this->host);
		$host = $this->host;
		
		//$this->session->unset_userdata("status");
	}
	
	function index() {
		if(!$this->auth){
			$status ="";
			$status = $this->session->userdata("status");
				
			$this->smarty->assign("status",$status);
			$this->smarty->display('login_soft.html');
		
		}else{
			header("Location: " .  $this->host."portal");
		}
	}

	function magister(){
		if(!$this->auth){
			$status ="";
			$status = $this->session->userdata("status");
				
			$this->smarty->assign("status",$status);
			$this->smarty->display('login_soft_mm.html');
		
		}else{
			header("Location: " .  $this->host."portal");
		}
	}

	function kelaskaryawan(){
		if(!$this->auth){
			$status ="";
			$status = $this->session->userdata("status");
				
			$this->smarty->assign("status",$status);
			$this->smarty->display('login_soft_kk.html');
		
		}else{
			header("Location: " .  $this->host."portal");
		}
	}
		
	function passwordUser($nomor = '18110012'){
		
		$this->load->library('encrypt');
		$sql = "SELECT A.*, LEFT(bukaSmb, 8) as periode, RIGHT(bukaSmb, 4) as prodi, E.jenjangType 
				FROM adis_smb_usr A "
				. "LEFT JOIN adis_smb_form B ON B.kode = A.kode "
				. "LEFT JOIN adis_periode_master E ON E.kode =  LEFT(B.bukaSmb, 8) "
				. "WHERE B.nomor = '$nomor'";
				
		$rs  = $this->db2->query($sql)->row();
		echo $this->encrypt->decode($rs->password);exit;
		
	}
		
	function login(){
		$this->load->library('encrypt');
		$username = $this->input->post("username");
		$pass = $this->input->post("password");
		$jenjang = $this->input->post("jenjang");

		$date = date('Y-m-d H:i:s');
		if (!$username OR !$pass)
                    {echo "<script>alert('Username dan Password tidak boleh kosong!');history.go(-1);</script>";}
		
		$sql = "SELECT A.*, LEFT(bukaSmb, 8) as periode, RIGHT(bukaSmb, 4) as prodi, E.jenjangType , B.nomor
				FROM adis_smb_usr A "
				. "LEFT JOIN adis_smb_form B ON B.kode = A.kode "
				. "LEFT JOIN adis_periode_master E ON E.kode =  LEFT(B.bukaSmb, 8) "
				. "WHERE A.kode = '$username'";
				
		$rs  = $this->db2->query($sql)->row();
		
		if ($rs){
			$row['password'] = $rs->password;
			$passdb = $this->encrypt->decode($row['password']);
			
			if($passdb != $pass){
				echo "<script>alert('Password Yang Anda Masukkan Salah!');history.go(-1);</script>";
				
			}else if($rs->validation_status == 0){
				$status ="notaktif";
				$this->session->set_userdata('status', $status);
				if($jenjang == 2){
					redirect ('/magister', 'refresh');
				}else{
					redirect ('/site', 'refresh');
				}
				
			}else{
				$row['kode'] 		=  $rs->kode;
				$row['username'] 	=  $rs->username;
				$row['validation_status'] =  $rs->validation_status;
				$row['jenjang'] =  $rs->jenjangType;
				$row['periode'] = $rs->periode;
				$row['prodi'] = $rs->prodi;
				$row['nomor'] = $rs->nomor;
                                
				if ($rs->nonaktif == 1){
					$this->db2->where('kode', $row['kode']);
					$this->db2->update('adis_smb_usr', array('nonaktif'=>0)); 
				}                                
				
				$this->db2->where('kode', $row['kode']);
				$this->db2->update('adis_smb_usr', array('last_login'=>$date)); 
                                
				$this->session->set_userdata('calon_mah', base64_encode(serialize($row)));
				$this->session->unset_userdata("status");
				header("Location: " . $this->host."portal");
				
			}
                }else {
                    echo "<script>alert('Password atau Username Yang Anda Masukkan Salah!');history.go(-1);</script>";
                }

        }
	
	function logout(){
        $this->session->unset_userdata("calon_mah");
        header("Location: " . $this->host."site");
    }

}
?>