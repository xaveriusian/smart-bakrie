<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "portal";
$route['404_override'] = '';
$route['trims'] = 'landpage/trims';
$route['pendaftaran'] = 'registrasi/registration';
$route['registerkk'] = 'registrasi/regKaryawan';
$route['magister'] = 'site/magister';
$route['kelaskaryawan'] = 'site/kelaskaryawan';
$route['detail/(:any)'] = 'SalesController/detail/$1';
// $route['test'] = 'SalesController/detail/awalia';

// $route['sales/detail'] = 'SalesController/detail';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
?>