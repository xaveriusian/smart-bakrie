<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller{
	function __construct(){
		parent::__construct();	
		date_default_timezone_set("Asia/Jakarta");
		$this->db2 = $this->load->database('second', TRUE);
		$this->auth = unserialize(base64_decode($this->session->userdata('cuti_parmad')));

		if($this->auth){
			$this->smarty->assign('USER',$this->auth);

		}
		
		$this->host	= $this->config->item('base_url');
		$this->smarty->assign('host',$this->host);
		$host = $this->host;
		
		
		$nama = $this->auth['name'];
		$level = $this->auth['level'];
		$this->smarty->assign('levelID',$level);
		$this->smarty->assign('nama',$nama);
		$this->smarty->assign('level',$level);
	}
	
	function index() {
			if(!$this->auth){
				$this->smarty->display('login.html');
			}else{
				header("Location: " .  $this->host."dashboard");
			}
		}

	function ubahGel(){
		// $qry = "SELECT *, substring(bukaSmb, 12, 2) as gel FROM tmp_ubah_gel ";
		// $data = $this->db->query($qry)->result_array();

		// foreach($data as $val){
		// 	$old = $val['bukaSmb'];
		// 	$new = substr_replace($val['bukaSmb'], '5', 9, 1);

		// 	echo $old.'---------------------------->'.$new.'<br>';
		// 	$this->db->where('kode', $val['kode']);
		// 	$this->db->update('adis_smb_form', array(
		// 		'bukaSmb'=>$new
		// 	));
		// }
		
		// echo "<pre>";
		// print_r($data);
	}
		
	function login(){
		$this->load->library('encrypt');
		$username = $this->input->post("username");
		$pass = $this->input->post("password");
		//$pass = $this->encrypt->encode($pass);
		if (!$username OR !$pass){echo "Salah";}
		
		// $sql = "SELECT * FROM tbl_usrm_users LEFT JOIN tbl_usrm_level ON tbl_usrm_users.tbl_usrm_level_id = tbl_usrm_level.id
				// WHERE tbl_usrm_users.username = '".$username."' limit 1";
			
		$sql = "SELECT * FROM adis_sys_usr u LEFT JOIN tbl_usrm_level l ON l.id = u.id_level WHERE u.kode = '$username' and erased = 0;";
		$rs  = $this->db2->query($sql)->row();
		
		if ($rs){
			$row['password'] = $rs->password;
			$passdb = $this->encrypt->decode($row['password']);

			//ini_set('display_errors', 'On');
			//ini_set('error_reporting', E_ALL);
			//print_r($passdb); die(x);
			
			if($passdb != $pass){
				echo "<script>alert('Password Yang Anda Masukkan Salah!');history.go(-1);</script>";
				}
			else 
				if($rs->status == 0){
				echo "<script>alert('User Tidak Aktif!');history.go(-1);</script>";
			}else{
				// $row['id'] 			=  $rs->id;
                // $row['username'] 	=  $rs->username;
				// $row['status'] 		=  $rs->aktif;
				// $row['name'] 		=  $rs->name;
                // $row['tbl_usrm_level_id'] 	=  $rs->tbl_usrm_level_id;
				// $row['id_pegawai'] 		=  $rs->id_pegawai;
				
				$row['id'] 			=  $rs->kode;
                $row['name'] 	=  $rs->username;
				$row['status'] 		=  $rs->status;
                $row['level'] 	=  $rs->id_level;
				$row['email'] 		=  $rs->email;
				$row['jenjang'] 		=  $rs->jenjang;
				
                $this->session->set_userdata('cuti_parmad', base64_encode(serialize($row)));
				header("Location: " . $this->host."dashboard");
				
				//redirect('/dashboard','refresh');
			}
        }else {
			echo "<script>alert('Password atau Username Yang Anda Masukkan Salah!');history.go(-1);</script>";
		}
		
	}
	
	function logout(){
        $this->session->unset_userdata("cuti_parmad");
        header("Location: " . $this->host."home");
    }
	
	/*
	function autoMail(){    
		if(!$this->input->is_cli_request()){
			echo "This script can only be accessed via the command line" . PHP_EOL;
			return;
		}  else{
			$query = "SELECT DATEDIFF(A.createTime,NOW()) as countDay, A.email as kode, 
				'rahmad.syalevi@paramadina.ac.id' as email , A.username
				FROM `adis_smb_usr` A
				LEFT JOIN adis_smb_form B ON A.email = B.kode
				WHERE (stsApplyPaid = 0 AND LEFT(B.bukaSmb,4) = (SELECT MAX(tahun) FROM adis_periode_master WHERE status = 1 ))
				AND DATEDIFF(A.createTime,NOW()) < -13 AND A.reminder = 0";
			$resQuer = $this->db2->query($query)->result_array();
			
			$this->load->model('msmb');
			
			$subject = "Friendly Reminder Admisi Universitas Bakrie";
			if (count($resQuer) > 0){
				foreach ($resQuer as $val){            
					$konten = array( "konten" =>
						  "<p>Dear ".$val['username']."</p>"
						. "<p>Terima kasih sudah membuat akun di admission.paramadina.ac.id, "
						. "silahkan segera melakukan pembayaran uang ujian Rp 300.000 ke No. Rekening Mandiri Universitas Paramadina 070.00000.43526</p>"
						. "<p>Bukti pembayaran diunggah ke admission.paramadina.ac.id sebelum hari kamis untuk mengikuti ujian terdekat.</p>"
						. "<p>Jika anda tidak segera melakukan pembayaran dan mengikuti ujian maka akun anda akan dinonaktifkan. "
						. "Otomatis aktif saat anda login kembali ke admisi</p>"
						. "<p>Untuk informasi lebih lanjut silahkan ke leo.ericton@paramadina.ac.id, WA 08159181190, PIN BB 28249c59 SMS 08159181188</p>"
						. "<br>"
						. "<a href='".$this->host."' class='btn' "
						. "style='border-radius: 6px;font-family: Arial;color: #ffffff;font-size: 13px;padding: 10px 20px 10px 20px;"
						. "text-decoration: none;background: #3498db;'>Login ke akun Admisi</a>"
						. "<br>"
						. "<br>"
						. "<br>"
						. "<br>Terima Kasih"
						. "<br>Best Regards"
						. "<br>"
						. "<br>"
						. "<br>Panitia SMB Universitas Paramadina"
						. "<br>"
						. "<br>"
						. "<br>"
						. "<br>Jl. Gatot Subroto Kav. 97 Mampang"
						. "<br>Jakarta 12790 Indonesia"
						. "<br>Office Ph : +62 21 7918 1188 Ext 213"
						. "<br>Office Fax : +62 21 799 3375"
						. "<br>E-mail : smb@paramadina.ac.id"
					);
					$this->msmb->autoMail($val['email'], $konten, $subject, $val['kode']);  
				}
			}
		}  
	}*/
	
	
	
	function laporan($p1 = '', $p2= ''){
		
		$this->load->model('madmisi');
		$post = $this->input->post();
				
		switch ($p1){
			case "bulan":
				$kode = $this->input->post('periode');
				$periode ="SELECT * FROM adis_periode_master WHERE kode = '$kode'";
				$periode = $this->db2->query($periode)->row_array();

				$begin = new DateTime( $periode['tanggalMulai'] );
				$end = new DateTime( $periode['tanggalSelesai'] );
				$interval = new DateInterval('P1M');

				$period = new DatePeriod($begin, $interval, $end);

				$listBulan = array();
				$listBulan['all'] = "Semua bulan";
				foreach ($period as $dt) {
					$listBulan[trim($dt->format('m-Y') . PHP_EOL)] = $dt->format('F Y') . PHP_EOL;
				}

				echo json_encode($listBulan);

				exit;
			break;
			case "select":		
				$site = "Select";	
				$lokasi = "Laporan";
				$pages = "seleksi";	
				
				$periode ="SELECT * FROM adis_periode_master WHERE erased = 0 ORDER by kode DESC";
				$periode = $this->db2->query($periode)->result();
				$this->smarty->assign('periode',$periode);
				
		
				$jalur = array(
					(object) array ( 'kode'=>'RAPOR', 'nama'=>'Jalur Rapor'), 
					(object) array  ('kode'=>'TULIS', 'nama'=>'Jalur Tes Tulis')
				);
				$this->smarty->assign('jalur',$jalur);
				
				$this->madmisi->mSelectProdi();

				$begin = new DateTime($periode[0]->tanggalMulai );
				$end = new DateTime( $periode[0]->tanggalSelesai);
				$interval = new DateInterval('P1M');

				$period = new DatePeriod($begin, $interval, $end);

				$listBulan = array();
				foreach ($period as $dt) {
					$listBulan[trim($dt->format('m-Y') . PHP_EOL)] = $dt->format('F Y') . PHP_EOL;
				}

				$this->smarty->assign('bulan',$listBulan);

			break;
			case 'seleksi':
				$periode = $post['periode'];
				$prodi = $post['prodi'];
				$jalur = $post['jalur'];
				$bulan = $post['bulan'];

				$jenjang = substr($periode, 5, 1);
				$this->smarty->assign('jenjang', $jenjang);
				$whereQry = '';
				
				
				$this->grafikCalonMahasiswa($periode);
				$this->potensiChart($periode);
				##filter all query
				if($periode){ $whereQry = " LEFT(A.bukaSmb, 8) = '$periode' "; }
				if ($jalur != 'all'){
					$whereQry = " LEFT(A.bukaSmb,8) = '$periode' AND B.jalur_penerimaan = '$jalur' ";
				}
				if ($prodi != 'all'){
					if ($jalur == 'all'){
						$whereQry = " LEFT(A.bukaSmb,8) = '$periode' AND RIGHT(A.bukaSmb, 4) = '$prodi' ";
					}else{
						$whereQry = " LEFT(A.bukaSmb,8) = '$periode' 
							AND B.jalur_penerimaan = '$jalur' AND RIGHT(A.bukaSmb, 4) = '$prodi' ";
					}
				}
				$whereQry .=  $bulan != 'all' ? " and date_format (A.createTime, '%m-%Y' ) ='".trim($bulan)."' " : "";

				$this->getfrekuenharian($periode, $whereQry);
				// echo $whereQry;exit;
				$arrayStep = array("Daftar Online", "Bayar Formulir", "Mengikuti Seleksi", "Lulus Seleksi", "Daftar Ulang", "Mengundurkan Diri");
				
				$qryDaftar = "SELECT COUNT(A.kode) as mhsDaftar 
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry";

				$qryDaftar = $this->db2->query($qryDaftar)->row_array();
				
				$qryPayForm = "SELECT COUNT(A.kode) as mhsDaftar 
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry AND A.stsApplyPaid = 1";
				$qryPayForm = $this->db2->query($qryPayForm)->row_array();
				
				$qryHadirUjian = "SELECT COUNT(A.kode) as mhsDaftar
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry  AND A.stsEventConfirm = 1";
				$qryHadirUjian = $this->db2->query($qryHadirUjian)->row_array();
				
				$qrylulusUjian = "SELECT COUNT(A.kode) as mhsDaftar
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry AND A.stsResultConfirm = 1";
				$qrylulusUjian = $this->db2->query($qrylulusUjian)->row_array();
				
				$qryDaftarUlang = "SELECT COUNT(A.kode) as mhsDaftar 
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry  AND A.stsReapplyPaid = 1";
				$qryDaftarUlang = $this->db2->query($qryDaftarUlang)->row_array();
				
				$qryUndur = "SELECT COUNT(A.kode) as mhsDaftar
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry AND A.stsMundurAfterReapply = 1";
				$qryUndur = $this->db2->query($qryUndur)->row_array();
					
				$data[0] = array('Tahap Seleksi CMB','Jumlah CMB');	
				$data[1] = array($arrayStep[0],(int)$qryDaftar['mhsDaftar']);
				$data[2] = array($arrayStep[1],(int)$qryPayForm['mhsDaftar']);
				$data[3] = array($arrayStep[2],(int)$qryHadirUjian['mhsDaftar']);
				$data[4] = array($arrayStep[3],(int)$qrylulusUjian['mhsDaftar']);
				$data[5] = array($arrayStep[4],(int)$qryDaftarUlang['mhsDaftar']);
				$data[6] = array($arrayStep[5],(int)$qryUndur['mhsDaftar']);


				// echo "<pre>";
				// print_r($data);exit;
				
				$totalReg = json_encode($data);
				$this->smarty->assign('totalReg',$totalReg);
				
				$arrIntBulan = '';
				$sql = "";
				
				$arrJalur = array(
					array('kode'=>'RAPOR', 'nama'=>'Jalur Rapor'),
					array('kode'=>'TULIS', 'nama'=>'Jalur Tes Tulis')
				);
				
				
				$this->smarty->display('laporan/kontenSeleksi.html');
				
			break;
		}
		if ($p2 == ''){
			
			$this->smarty->assign('modul',"laporan/");
			$this->smarty->assign('lokasi',$lokasi);
			$this->smarty->assign('pages',$pages);
			$this->smarty->assign('site',$site);
			$this->smarty->display('index.html');
		}
	}

	function getfrekuenharian($periode, $where){

		$periodeRow = $this->db2->query("SELECT * FROM adis_periode_master WHERE kode = '$periode'")->row_array();

		$arrJalur = array(
			array('kode'=>'RAPOR', 'nama'=>'Jalur Rapor'),
			array('kode'=>'TULIS', 'nama'=>'Jalur Tes Tulis'),
			array('kode'=>'ODS', 'nama'=>'One Day Service Online')
		);

		$period = new DatePeriod(
			new DateTime($periodeRow['tanggalMulai']),
			// new DateTime(date('Y-m-d',strtotime("-4 Months"))),
			new DateInterval('P1D'),
			new DateTime(date('Y-m-d'))
		);

		
		$data = array();
		foreach($arrJalur as $k => $v){

			$qry = "SELECT IF(COUNT(A.kode) = null, 0, COUNT(A.kode))  as mhsDaftar , DATE_FORMAT(A.createTime, '%d/%m/%y') as date
			FROM adis_smb_form A 
			LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
			WHERE LEFT(A.bukaSmb, 8) = '$periode' AND B.jalur_penerimaan = '".$v['kode']."' AND $where
			GROUP BY DATE_FORMAT(A.createTime, '%Y-%m%-%d')";

			$sql = $this->db2->query($qry)->result_array();
			$data_frek[0] = array('Tanggal','Rapor','Tes Tulis','ODS');
			$i = 1;
			foreach ($period as $key => $value) {
				$tanggal = $value->format('d/m/y');
				$data_frek[$i][0] = $tanggal;
				// $data_frek[$i][1] = 0;
				// $data_frek[$i][2] = 0;

				foreach($sql as $val ){
					if($tanggal == $val['date']){
						if($v['kode'] == 'RAPOR'){
							$data_frek[$i][1] = (int)$val['mhsDaftar'];
						}else if($v['kode'] == 'TULIS'){
							$data_frek[$i][2] = (int)$val['mhsDaftar'];
						}else if($v['kode'] == 'ODS'){
							$data_frek[$i][3] = (int)$val['mhsDaftar'];
						}  
					}
				}
				$i++;
			}

			// array_push($data, $data_frek);

		}

		foreach($data_frek as $k => $val){
			if(empty($val[1])){
				$data_frek[$k][1] = 0;
			}
			if(empty($val[2])){
				$data_frek[$k][2] = 0;
			}
			if(empty($val[3])){
				$data_frek[$k][3] = 0;
			}

			ksort($data_frek[$k]);
		}

		$totalRegFrek = json_encode($data_frek);
		$this->smarty->assign('totalRegFrek',$totalRegFrek);
		// echo "<pre>";
		// print_r($data_frek);exit;

		

		// $data = array();
		// foreach ($period as $key => $value) {
		// 	$data_frek[0] = $value->format('Y-m-d');  
			
		// 	foreach($sql as $k => $v ){
		// 		if($k == 'RAPOR'){
		// 			$data_frek[1] = (int)$v['mhsDaftar'];
		// 		}else{
		// 			$data_frek[2] = (int)$v['mhsDaftar'];
		// 		}  
		// 	}

		// 	array_push($data, $data_frek);
			
			  
		// }

	}

	function potensiChart($periode){
		$periodeRow = $this->db2->query("SELECT * FROM adis_periode_master WHERE kode = '$periode'")->row_array();

		$qry = "SELECT
			A.singkatan as prodi, COUNT(B.kode) as totalMhs
		FROM
			adis_prodi A
			LEFT JOIN adis_smb_form B ON A.kode = SUBSTR( B.bukaSmb,- 4 ) AND SUBSTR( B.bukaSmb, 1, 8 ) = '$periode' AND B.stsReapplyPaid =1 AND B.nim = ''  AND stsMundurAfterReapply = 0 AND stsMundurBeforeReapply = 0
			GROUP BY A.singkatan ";

		$du_only = $this->db->query($qry)->result_array();

		$qryNim = "SELECT
			A.singkatan as prodi, COUNT(B.kode) as totalMhs
		FROM
			adis_prodi A
			LEFT JOIN adis_smb_form B ON A.kode = SUBSTR( B.bukaSmb,- 4 ) AND SUBSTR( B.bukaSmb, 1, 8 ) = '$periode' AND B.stsReapplyPaid =1 
			AND B.nim != '' AND stsMundurAfterReapply = 0
			GROUP BY A.singkatan ";

		$get_nim = $this->db->query($qryNim)->result_array();

		$totalDU = 0;
		$totalNim = 0;
		$datas = array();
		$datas[0] = array("Program Studi", $totalDU." Belum Lunas", $totalNim." Mendapat NIM");
		$i = 1;
		foreach($get_nim as $k => $val){
			if($val['prodi'] == $du_only[$k]['prodi']){
				$totalDU += $du_only[$k]['totalMhs'];
				$totalNim += $val['totalMhs'];
				$datas[$i] = array($val['prodi'], (int)$du_only[$k]['totalMhs'], (int)$val['totalMhs']);
				// array_push($datas, $data);
			}
			$i++;
		}
		$datas[0] = array("Program Studi", $totalDU." Belum Lunas", $totalNim." Mendapat NIM");
		
		$json_data = json_encode($datas);
		// echo $json_data;exit;
		$this->smarty->assign('potensi', $json_data);


	}
	
	function grafikCalonMahasiswa($periode){
		$periodeRow = $this->db2->query("SELECT * FROM adis_periode_master WHERE kode = '$periode'")->row_array();
		$QryProdi = $this->db2->query('SELECT * FROM `adis_prodi` WHERE jenjang = "'.$periodeRow['jenjangType'].'" 
			AND erased = 0;')->result_array();
			
		$qryjalur = $this->db2->query("SELECT * FROM adis_jalur_smb 
			WHERE jenjang = '".$periodeRow['jenjangType']."' AND erased = 0;")->result_array();
		
		$arrJalur = array(
			array('kode'=>'RAPOR', 'nama'=>'Jalur Rapor'),
			array('kode'=>'TULIS', 'nama'=>'Jalur Tes Tulis'),
			array('kode'=>'ODS', 'nama'=>'ODS Online')
		);
		
		$totalreg = array();         
		if ($periode){       
		
			foreach($arrJalur as $i => $v){
				$qry[$i] = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs 
					FROM adis_smb_form A
					LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
					LEFT JOIN adis_smb_usr C ON A.kode = C.kode 
					WHERE (SUBSTR(bukaSmb, 1, 8) = '".$periode."' AND C.jalur_penerimaan = '".$v['kode']."')
					GROUP BY B.nama;";
				$total[$i] = $this->db2->query($qry[$i])->result_array();
			} 
			
			$data[0] = array('PROGRAM STUDI','Mahasiswa');	
			
			for($i = 0; $i < count($QryProdi); $i++){
				$data[$i+1] = array($QryProdi[$i]['singkatan']);
				$data[0] = array('Program Studi');
				
				
				foreach($arrJalur as $k => $v){
					$dot = strlen($v['nama']) > 15 ? "..." : "";
					$data[0][$k+1] = substr($v['nama'],0,15).$dot;
					$totalall[$k] = 0;
					
					
					foreach($total[$k] as $x => $val){
						if ($val['prodi'] == $QryProdi[$i]['singkatan']){
							$totalall[$k] = $val['totalMhs'];
						}
						
					}	
					
					$data[$i+1][$k+1] = (int)$totalall[$k];
				}
				
			}	
			
			$totalReg = json_encode($data);
			// echo $totalReg;
			$this->smarty->assign('totalPeserta',$totalReg);
		}
	}
	
	
	
	function test(){
		if(!$this->input->is_cli_request()){
			echo "This script can only be accessed via the command line" . PHP_EOL;
			return;
		}else{			
			echo "Hello Cron, can u help me?". PHP_EOL;
		}
	}

}
