
$('#data_diri,#orang_tua,#pendidikan,#prestasi').click(function(){
	id = $(this).attr('class');
	page = $(this).attr('id');
	url = "";
	$('#tab_1').fadeOut(30);
	if (page == 'data_diri'){url = 'alamat';}else if(page == 'orang_tua'){url = 'keluarga';}
	else if(page == 'pendidikan'){url = 'pendidikan';}else if(page == 'prestasi'){url = 'prestasi';}
	$.post(hostir+"portal/formulir/"+url, {'id':id},function(resp){
		$('#tab_1').html(resp).fadeIn(100);		
	});
});

$('#profil').click(function(){
	document.getElementById("profil").style.height="45px" ;
	$('#imgP').hide();
});

jQuery(document).ready( function (){
	idtab = $('#statusTab').val();
	datadetailcmb = JSON.parse(datacmb);
	
	status = datadetailcmb['statusAlamat'];
	statusKel = datadetailcmb['statusKeluarga'];
	statusSau = datadetailcmb['statusSaudara'];
	statusPend = datadetailcmb['statusPend'];
	statusPrib = datadetailcmb['stsPribadiConfirm'];
	jalur = datadetailcmb['jalur'];
	jenjang = datadetailcmb['jenjangType'];
	
	if (status == 1){
		uri = 'pendidikan';			
		if (statusPend == 0){
			$('.pendidikan').addClass(' active');
			id = "formPendidikan";	
			// // toastr.success('Selanjutnya, Silahkan Perbaharui riwayat Pendidikan Anda');
		}else if (statusPend == 1){
			uri = 'prestasi';					
			if (statusPrib == 0){
				$('.prestasi').addClass(' active');
				id="formPrestasi";
				// // toastr.success('Terakhir, silahkan lengkapi daftar PRESTASI & ORGANISASI Anda. Kemudian Submit formulir pendaftaran.');
			}else if (statusPrib == 1){
				id="formPrestasi";
			}
		}
		/*
		var uri = 'keluarga';	
		if (statusKel == 0){
			$('.orangtua').addClass('active');
			id = "formKeluarga";
			// // toastr.info('Lanjutkan dengan melengkapi DATA ORANG TUA/WALI, Silahkan pilih salah satu untuk diisi.');
		}else if (statusKel == 1){
			uri = "keluarga";
			if (statusSau == 0){
				if(jenjang == 1){
					if (jalur == 'KP' || jalur == 'JP'){
						id="saudara";
							$.post(hostir+"portal/formulir/keluarga/saudaraConf", {'id':id},
								function(resp){
								 window.location.href = hostir+"portal/formulir";
							});
					}else{			
						id="saudara";
						if (saudara_rows > 0){                                            
							$(".btnSauYakin").click();
						}else{
							$(".btnSauConfirm").click();
						}
					}
				}else{
					confirmSaudara('keluarga','saudara');
				}
			}else if (statusSau == 1){
				uri = 'pendidikan';			
				if (statusPend == 0){
					$('.pendidikan').addClass('active');
					id = "formPendidikan";	
					// // toastr.success('Selanjutnya, Silahkan Perbaharui riwayat Pendidikan Anda');
				}else if (statusPend == 1){
					uri = 'prestasi';					
					if (statusPrib == 0){
						$('.prestasi').addClass('active');
						id="formPrestasi";
						// // toastr.success('Terakhir, silahkan lengkapi daftar PRESTASI & ORGANISASI Anda. Kemudian Submit formulir pendaftaran.');
					}else if (statusPrib == 1){
						id="formPrestasi";
					}
				}
			}
		}	*/	
	}else{		
		var uri = 'alamat';
		id ="formAlamat";
		$('.datadiri').addClass(' active');
		// // toastr.info('Silahkan Lengkapi Formulir Pendaftaran dengan Mengisi DATA ALAMAT, ORANG TUA/WALI, PENDIDIKAN, PRESTASI & ORGANISASI ');
	
	}
			
		$('#tab_1').fadeOut(30);
		$.post(hostir+"portal/formulir/"+uri+"", {'id':id},function(resp){
			$('#tab_1').html(resp).fadeIn(500);	
		});
});


function confirmSaudara(uri, id){
	if (id == "detailKeluarga"){
		$.post(hostir+"portal/formulir/"+uri+"", {'id':id},function(resp){
				$('#tab_1').html(resp);
			});
	}   
	if (id == "saudara"){
		$.post(hostir+"portal/formulir/keluarga/saudaraConf", {'id':id},function(resp){
				window.location.href = hostir+"portal/formulir";
		});
	}
}


$('#foo').click(function() {
       if (confirm('Lengkapi Data Orang Tua atau Wali')) {
           id = "formKeluarga";
			
			$.post(hostir+"portal/formulir/keluarga", {'id':id},function(resp){
				$('#konten').html(resp);
			});
			document.getElementById("profil").style.height="42px" ;
			document.getElementById("imgP").style.display="none" ;
			return false;
       }
    });

jQuery(document).ready(function() {
// toastr.info('Silahkan Lengkapi DATA DIRI Anda');
    kabkota();
	
});

$("#propinsi").change(function(){
    kabkota();
});

$("#kabkota").change(function(){
    kodepos();
});

function kabkota(){
	$.post(hostir+"portal/selectKabKota",{'propinsi':$('#propinsi').val()},function(resp){
        $('select#kabkota').html(resp);
		kodepos();
	});
	
}



function kodepos(){
	$.post(hostir+"portal/selectKodePos",{
					'kabkota':$('#kabkota').val()				
				},function(resp){
                $('select#kodepos').html(resp);
            });
}



function kabkota2(){
	$.post(hostir+"portal/selectKabKota",{
					'propinsi':$('#propinsi2').val()				
				},function(resp){
                $('select#kabkota2').html(resp);
            });
	
	kodepos2();
}


function kodepos2(){
	$.post(hostir+"portal/selectKodePos",{
					'kabkota':$('#kabkota2').val()				
				},function(resp){
                $('select#kodepos2').html(resp);
            });
}


$('#sama').click(function() {	
	if ( $(this).is(':checked') ){
		$('#occupationsur').val($('#occupation').val());
		$('#telRumah2').val($('#telRumah').val());
		$('#propinsi2').val($('#propinsi').val());
		$('#kabkota2').val($('#kabkota').val());
		$('#kodepos2').val($('#kodepos').val());
    }else{
		$('#occupationsur').val("");
		$('#telRumah2').val("");
		$('#propinsi2').val("");
		$('#kabkota2').val("");
		$('#kodepos2').val("");
			
	}	
});
