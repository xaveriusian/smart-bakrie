
!function($) {
    "use strict";

    var SweetAlert = function() {};

    //examples 
    SweetAlert.prototype.init = function() {
        
    //Basic
    $('#sa-basic').click(function(){
        swal("Here's a message!");
    });

    //A title with a text under
    $('#sa-title').click(function(){
        swal("Mohon Maaf Bukti Bayar Belum Dapat di Verifikasi",  "Pesan Keuangan : Nama Pengirim tidak Terdaftar Silahkan Hubungi Admisi Universitas Bakrie di No 021 5261448 atau Whatsapp ke 0857 777 2010")
    });

    //Success Message
    $('#sa-success').click(function(){
        swal("Good job!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.", "success")
    });

    //Penolakan Message
    $('#sa-tolak').click(function(){
        pesanTolakPayment = pesanTolakPayment == '' ? 'Nama Pengirim tidak Terdaftar' : pesanTolakPayment;
        swal({   
            title: "Mohon Maaf Bukti Bayar Belum Dapat di Verifikasi",   
            text: "Pesan Keuangan : "+pesanTolakPayment+" Silahkan Hubungi Admisi Universitas Bakrie di No 021 5261448 atau Whatsapp ke 0857 777 2010",   
            type: "warning",   
            showCancelButton: false,   
            confirmButtonColor: "red",   
            confirmButtonText: "Tutup",   
            closeOnConfirm: true 
        }, function(){   
            swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
        });
    });

    //Penolakan Message
    $('#sa-tolak-kk').click(function(){
        pesanTolakPayment = (pesanTolakPayment == '' ? 'Nama Pengirim tidak Terdaftar' : pesanTolakPayment);
        swal({   
            title: "Mohon Maaf Bukti Bayar Belum Dapat di Verifikasi",   
            text: "Pesan Keuangan : "+pesanTolakPayment+" Silahkan Hubungi Admisi Universitas Bakrie di No 021 5261448 atau Whatsapp ke +62 813-1377-7900",   
            type: "warning",   
            showCancelButton: false,   
            confirmButtonColor: "red",   
            confirmButtonText: "Tutup",   
            closeOnConfirm: true 
        }, function(){   
            swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
        });
    });

    //Penolakan Message
    $('#sa-tolak-mm').click(function(){
        pesanTolakPayment = (pesanTolakPayment == '' ? 'Nama Pengirim tidak Terdaftar' : pesanTolakPayment);
        swal({   
            title: "Mohon Maaf Bukti Bayar Belum Dapat di Verifikasi",   
            text: "Pesan Keuangan : "+pesanTolakPayment+" Silahkan Hubungi Admisi Universitas Bakrie di No 021 5261448 atau Whatsapp ke +62 813-1377-7900",   
            type: "warning",   
            showCancelButton: false,   
            confirmButtonColor: "red",   
            confirmButtonText: "Tutup",   
            closeOnConfirm: true 
        }, function(){   
            swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
        });
    });

    //Penolakan Message
    $('#sa-forlap-tolak').click(function(){
        swal({   
            title: "Mohon Maaf Data Forlap Anda ditolak!",   
            text: "Pesan Admin : "+pesanTolakForlap+", Silahkan Hubungi Admisi Universitas Bakrie di No 021 5261448 atau Whatsapp ke +62 813-1377-7900",   
            type: "warning",   
            showCancelButton: false,   
            confirmButtonColor: "red",   
            confirmButtonText: "Tutup",   
            closeOnConfirm: true 
        }, function(){   
            swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
        });
    });

    //Warning Message
    $('#sa-warning').click(function(){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this imaginary file!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: false 
        }, function(){   
            swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
        });
    });

    //Parameter
    $('#sa-params').click(function(){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this imaginary file!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                swal("Deleted!", "Your imaginary file has been deleted.", "success");   
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");   
            } 
        });
    });

    //Custom Image
    $('#sa-image').click(function(){
        swal({   
            title: "Govinda!",   
            text: "Recently joined twitter",   
            imageUrl: "../plugins/images/users/govinda.jpg" 
        });
    });

    //Auto Close Timer
    $('#sa-close').click(function(){
         swal({   
            title: "Auto close alert!",   
            text: "I will close in 2 seconds.",   
            timer: 2000,   
            showConfirmButton: false 
        });
    });


    },
    //init
    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.SweetAlert.init()
}(window.jQuery);