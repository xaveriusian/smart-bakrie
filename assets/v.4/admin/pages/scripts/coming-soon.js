var ComingSoon = function () {

    return {
        //main function to initiate the module
        init: function (y,m,d) {
            var austDay = new Date();
            // austDay = new Date(austDay.getFullYear() + 1, 1 - 1, 26);
            austDay = new Date(y,m-1,d);
            $('#defaultCountdown').countdown({until: austDay});
            $('#year').text(austDay.getFullYear());
        }

    };

}();