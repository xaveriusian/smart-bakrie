<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Smb extends CUTI_Controller{
	function __construct(){
		parent::__construct();	
		//setlocale (LC_TIME, 'id_ID');
		setlocale (LC_TIME, 'INDONESIA');
		$this->auth = unserialize(base64_decode($this->session->userdata('cuti_parmad')));
		$this->host	= $this->config->item('base_url');
		
		if(! $this->auth) {header("Location: " . $this->host);}	
		
		$this->db2 = $this->load->database('second', TRUE); 

		$this->prodiTidakPromo = array("2141","1204");
		
		$modul = "smb/";
		$this->smarty->assign('modul',$modul);
		$this->smarty->assign('host',$this->host);
		$this->load->model('madmisi');
		$this->load->model('msmb');
		$this->load->model('mportal');
		$this->load->model('mregistrasi');
		$this->load->model('mintegrasi');
		
		$nama = $this->auth['name'];
		$this->level = $this->auth['level'];
		$this->smarty->assign('nama',$nama);
		$this->smarty->assign('level',$this->level);

		// print_r($this->auth);
		
		$this->db2->query("SET lc_time_names = 'id_ID'");
	}
	
	function index() {
	}

	function load_page($site = ''){
		$modul = "laporan/";
		$this->smarty->assign('modul',$modul);
		$this->smarty->assign('site',$site);
		$this->smarty->assign('lokasi',"Dashboard");
		$this->smarty->assign('url_load',$site);
		$this->smarty->display('index.html');
	}

	function change_nilai_bayar($p1){
		if($p1 == 'update'){
			$post = $this->input->post();
			if($post['kode_cmb'] && $post['noreg_cmb']){
				$this->db->where('smbUsr', $post['kode_cmb']);
				$this->db->update('adis_smb_usr_keu', array('totalBiayaDaftarUlang'=>$post['dibayarkan']));

				
				$this->db->where('kode', $post['kode_cmb']);
				$this->db->update('adis_smb_form', array('reapplyBankTransferAmount'=>$post['dibayarkan']));
			}
		}

		redirect ('/smb/buktiPembayaran/'.$post['noreg_cmb'], 'refresh');
	}

	function telemarketing(){
		$uri = $this->uri->segment(3);
		$jalur = $this->input->post('jalur');
		$periode = $this->input->post('periode');
		$gelombang = $this->input->post('gelombang');
		$prodi = $this->input->post('prodi');
		$this->load->model('madmisi');
		
		$andQuery = "";
		
		if ($prodi != 'all'){
			$andQuery = "AND substr(f.bukaSmb, -4) = '$prodi'";
		}
		
		$this->madmisi->mSelectPeriode();
		
		if ($jalur != '' && $uri == "table"){
			
			if ($jalur == 'all'){
				$wrJalur = "AND SUBSTR(f.bukaSmb, 1, 8) = '$periode'";
			}else{
				// $periode = implode(".",array($periode,$jalur));
				$wrJalur = "AND SUBSTR(f.bukaSmb, 1, 8) = '$periode'";				
				$wrJalur .= "AND u.jalur_penerimaan = '$jalur'";				
			}
			
			if($gelombang != 'all'){
				$wrJalur .=  " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
			}
			
			$sql ="SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, up.nama as nama_cm, up.statusSaudara, p.singkatan as progdi, j.nama as n_jalur, f.`event` as event, up.adaSaudara, up.confirmSaudara,
			f.stsEventInterviewPresent as hadirWwc, f.stsEventUsmPresent hadirUsm, up.rumahCell,
			f.resultUsm as hasilUsm, f.resultInterview as hasilWwc, f.resultPept, f.stsResultGrade as hasilAkhir,
			f.stsResultPass as lulusTidak, f.stsResultRecommended as recomended, f.stsResultKet as ket, f.stsResultConfirm as konfirm, j.jenjang, u.jalur_penerimaan,
			T.id, T.followup, T.edu, odsK.followup as ods, T.date_updated as tele_date_update,
			if(f.applyBankTransferType = '', '' , if(f.applyBankTransferType = '04.5', 'Voucher', 'Transfer')) as 'status_pay', f.createTime as tglDaftar
			-- inv.cgts
			FROM adis_smb_form f 
				INNER JOIN adis_smb_usr u ON u.kode = f.kode
				INNER JOIN adis_periode_master c ON LEFT(f.bukaSmb,8) = c.kode
				INNER JOIN adis_prodi p ON RIGHT(f.bukaSmb, 4) = p.kode 
				INNER JOIN adis_jalur_smb j ON SUBSTR(f.bukaSmb, 12, 2) = j.kode
				INNER JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
				LEFT JOIN (select id, kode, ods, date_updated, followup, edu from smart_telemarketing ) as  T on f.kode = T.kode
				LEFT JOIN idx_followup odsK ON T.ods = odsK.id
				-- left join tbl_invent inv on f.kode = inv.email 
			WHERE 1=1 $wrJalur $andQuery
			ORDER BY f.createTime DESC, T.date_updated DESC
			";			

			// echo $sql;exit;
			$sql = $this->db2->query($sql)->result();	
			$this->smarty->assign('sql',$sql);
			
			$sql1 = $this->db2->query("SELECT COUNT(*) as total 
				FROM adis_smb_form f 
				INNER JOIN adis_smb_usr u ON u.kode = f.kode
				WHERE stsApplyPaidConfirm = 1 
				$wrJalur $andQuery")->row();
			$this->smarty->assign('total',$sql1);
			
			$this->smarty->display('tele/tblTelemarketing.html');
			
		}else if($uri == 'form'){
			$p = $this->input->post();

			$sales = $this->db2->query("SELECT * FROM tbl_sales ORDER BY nama")->result_array();
			$followup = $this->db2->query("SELECT * FROM idx_followup ORDER BY id ASC")->result_array();
			$attempt = $this->db2->query("SELECT * FROM idx_attempt ORDER BY id ASC")->result_array();
			$edu = $this->db2->query("SELECT kode, username FROM adis_sys_usr WHERE id_level = 44 AND status = 0 AND erased = 0 ORDER BY kode ASC")->result_array();

			$sql = "Select * FROM smart_telemarketing A WHERE A.kode = '".$p['kode']."' ";
			$query = $this->db2->query($sql);

			$data = "";
			if($query->num_rows() > 0){
				$data = $query->row_array();
			}
			
			$this->smarty->assign('data',$data);
			$this->smarty->assign('sales',$sales);
			$this->smarty->assign('followup',$followup);
			$this->smarty->assign('attempt',$attempt);
			$this->smarty->assign('edu',$edu);
			$this->smarty->assign('opt',$p['opt']);
			$this->smarty->assign('kode',$p['kode']);
			$this->smarty->display("tele/formTele.html");
		}else if($uri == 'save'){
			$p = $this->input->post();
			$sql = "Select * FROM smart_telemarketing A WHERE A.kode = '".$p['kode']."' ";
			$query = $this->db2->query($sql);

			if($p['submit'] == 'submit'){

				if($query->num_rows() > 0){
					$this->db2->where('kode', $p['kode']);
					$this->db2->update('smart_telemarketing',array(
						'followup'=>$p['sales'],
						'edu'=>$p['edu'],
						'ods'=>$p['ods'],
						'ket_ods'=>$p['ket_ods'],
						'oh'=>$p['oh'],
						'ket_oh'=>$p['ket_oh'],
						'pelunasan'=>$p['pelunasan'],
						'ket_pelunasan'=>$p['ket_pelunasan'],
						'sejuta'=>isset($p['sejuta'])?1:0,
						'islunas'=>isset($p['lunas'])?1:0,
						'date_updated'=>date("Y:m:d H:i:s")
					));
				}else{
					
					$this->db2->insert('smart_telemarketing',array(
						'kode'=>$p['kode'],
						'followup'=>$p['sales'],
						'edu'=>$p['edu'],
						'ods'=>$p['ods'],
						'ket_ods'=>$p['ket_ods'],
						'oh'=>$p['oh'],
						'ket_oh'=>$p['ket_oh'],
						'pelunasan'=>$p['pelunasan'],
						'ket_pelunasan'=>$p['ket_pelunasan'],
						'sejuta'=>isset($p['sejuta'])?1:0,
						'islunas'=>isset($p['lunas'])?1:0,
						'date_created'=>date("Y:m:d H:i:s"),
						'date_updated'=>date("Y:m:d H:i:s")
					));
				}
			}

		}else{
			$site = "Telemarketing";			
			$this->smarty->assign('site',$site);
			$this->smarty->assign('lokasi',"Telemarketing");
			$this->smarty->display('index.html');
		}
	}

	function updateErrorPeriode(){
		$sql = "UPDATE adis_smb_form A 
		SET bukaSmb = CONCAT(lEFT(A.bukaSmb,9),2,RIGHT(LEFT(A.bukaSmb,17), 8))
		WHERE bukaSmb LIKE '2020.1.1..%';";

		$this->db2->query($sql);
	}

	function sendWa(){
		$phone = $this->input->post('hp');
		$count = $this->input->post('count_wa');
		$this->db->where('hp',$phone);
		$this->db->update('tbl_invent', array(
			'send_wa'=>$count,
			'send_date'=>date('Y-m-d H:i:s'))
		);
		echo 1;

	}

	function sendWaMagister(){
		$phone = $this->input->post('hp');
		$count = $this->input->post('count_wa');
		$this->db->where('hp',$phone);
		$this->db->update('tbl_invent_mm', array(
			'send_wa'=>$count,
			'send_date'=>date('Y-m-d H:i:s'))
		);
		echo 1;

	}

	function simpan_invent(){
		$p = $this->input->post();
		$uri = $this->uri->segment(3);

		if($uri == 'save' && !empty($p['id'])){
			$this->db->where('id',$p['id']);
			$this->db->update('tbl_invent', array(
				'status'=>$p['status'],
				'cgts'=>$p['cgts'],
				'edu'=>$p['edu'],
				'date_updated'=>date('Y-m-d H:i:s')
			));

			echo 1;
		}
	}
		
	function getDisplay($type="", $p1="", $p2="", $p3=""){
		$site = "";
		switch($type){
			case 'setting_tarif_kk':
				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE jenjangType='3'  order by status desc")->result_array();
				$this->smarty->assign('periode', $periode_aktif);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Setting Tarif Kelas Karyawan";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'hasilSeleksiKK':
				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '1' ")->result();		
				$this->smarty->assign('prodi',$prodi);	

				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='3'  ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Hasil Seleksi";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'modulPrivilege':
				$content = "smb/calonmahasiswa/main.html";
				$site = "User Modul Privilege";
				$this->smarty->assign('tipe',$p1);
				break;
			case 'masterProdi':
				$content = "smb/calonmahasiswa/main.html";
				$site = "Master Program Studi";
				$this->smarty->assign('tipe',$p1);
				break;
			case 'masterAgama':
				$content = "smb/calonmahasiswa/main.html";
				$site = "Master Data Agama";
				$this->smarty->assign('tipe',$p1);
				break;
			case 'ver_dok_kk':
				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '1' ")->result();		
				$this->smarty->assign('prodi',$prodi);	

				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='3'  ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Verfikasi Ijazah Kelas Karyawan";
				$this->smarty->assign('tipe',$p1);
			break;
			case "cmbkk":
				// $this->madmisi->mSelectPeriode();
				$this->madmisi->mSelectJalur();
				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '1' ")->result();		
				$this->smarty->assign('prodi',$prodi);	

				$periode ="SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='3' AND erased = 0 ORDER by kode DESC";
				$periode = $this->db2->query($periode)->result();			
					
				$this->smarty->assign('periode',$periode);	
				
				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='3'  ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);
				
				$content = "smb/calonmahasiswa/main.html";
				$site = "Daftar Calon Mahasiswa Baru Kelas Karyawan";
				$this->smarty->assign('tipe',$p1);
			 break;	
			case 'man_edu':
				$content = "smb/calonmahasiswa/main.html";
				$site = "Management Edu Consultant";
				$this->smarty->assign('tipe',$p1);
				break;
			case 'mahasiswaKK':
				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '3' ")->result();		
				$this->smarty->assign('prodi',$prodi);	

				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='3'  ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Mahasiswa Magister";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'mahasiswaS2':
				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '2' ")->result();		
				$this->smarty->assign('prodi',$prodi);	

				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='2'  ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Mahasiswa Magister";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'daftarulangKK':
				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '3' ")->result();		
				$this->smarty->assign('prodi',$prodi);	

				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='1'  ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Pembayaran Daftar Ulang";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'daftarulangMag':
				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '2' ")->result();		
				$this->smarty->assign('prodi',$prodi);	

				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='2'  ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Pembayaran Daftar Ulang";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'beasiswaMag':
				$content = "smb/calonmahasiswa/main.html";
				$site = "Beasiswa Magister";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'setPotonganMag':
				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE jenjangType='2'  order by status desc")->result_array();
				$this->smarty->assign('periode', $periode_aktif);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Seting Potongan Biaya Magister";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'skemaBiaya':
				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE jenjangType='2'  order by status desc")->result_array();
				$this->smarty->assign('periode', $periode_aktif);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Skema Biaya Magister";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'setting_tarif_magister':
				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE jenjangType='2'  order by status desc")->result_array();
				$this->smarty->assign('periode', $periode_aktif);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Setting Tarif Magister";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'hasilSeleksi':
				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '2' ")->result();		
				$this->smarty->assign('prodi',$prodi);	

				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='2'  ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Hasil Seleksi";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'tesTPA':
				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '1' ")->result();		
				$this->smarty->assign('prodi',$prodi);	

				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='3'  ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Jadwal Tes Potensi Akademik";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'tesWawancara':
				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '2' ")->result();		
				$this->smarty->assign('prodi',$prodi);	

				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='2'  ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Jadwal Tes & Wawancara";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'payment_reg_kk':
				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '1' ")->result();		
				$this->smarty->assign('prodi',$prodi);	

				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='3'  ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Pembayaran Formulir Kelas Karyawan";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'payment_reg':
				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '2' ")->result();		
				$this->smarty->assign('prodi',$prodi);	

				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='2'  ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Pembayaran Formulir S2";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'ver_dok':
				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '2' ")->result();		
				$this->smarty->assign('prodi',$prodi);	

				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='2'  ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);

				$content = "smb/calonmahasiswa/main.html";
				$site = "Verfikasi Ijazah";
				$this->smarty->assign('tipe',$p1);
			break;
			case 'telemarketing_kk':
				
				$periode ="SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='3' AND erased = 0 ORDER by kode DESC";
				$periode = $this->db2->query($periode)->result();			
					
				$this->smarty->assign('periode',$periode);	

				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '1' ")->result();		
				$this->smarty->assign('prodi',$prodi);	
				
				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='3' ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);

				$gel_aktif = "";
				foreach($gelombang as $val){
					if($val->status == 1){
						$gel_aktif = $val->kode;
					}
				}

				$content = "smb/calonmahasiswa/main.html";
				$site = "Telemarketing";
				$this->smarty->assign('tipe',$p1);
				$this->smarty->assign('id_where',$periode_aktif['kode']);
				$this->smarty->assign('id_where2',$gel_aktif);
			break;
			case 'telemarketing':
				
				$periode ="SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='2' AND erased = 0 ORDER by kode DESC";
				$periode = $this->db2->query($periode)->result();			
					
				$this->smarty->assign('periode',$periode);	

				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '2' ")->result();		
				$this->smarty->assign('prodi',$prodi);	
				
				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='2' ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);

				$gel_aktif = "";
				foreach($gelombang as $val){
					if($val->status == 1){
						$gel_aktif = $val->kode;
					}
				}

				$content = "smb/calonmahasiswa/main.html";
				$site = "Telemarketing";
				$this->smarty->assign('tipe',$p1);
				$this->smarty->assign('id_where',$periode_aktif['kode']);
				$this->smarty->assign('id_where2',$gel_aktif);
			break;
			case 'telemarketing_s1':
				
				$periode ="SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='1' AND erased = 0 ORDER by kode DESC";
				$periode = $this->db2->query($periode)->result();			
					
				$this->smarty->assign('periode',$periode);	

				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '1' ")->result();		
				$this->smarty->assign('prodi',$prodi);	
				
				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='1' ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);

				$gel_aktif = "";
				foreach($gelombang as $val){
					if($val->status == 1){
						$gel_aktif = $val->kode;
					}
				}

				$content = "smb/calonmahasiswa/main.html";
				$site = "Telemarketing";
				$this->smarty->assign('tipe',$p1);
				$this->smarty->assign('id_where',$periode_aktif['kode']);
				$this->smarty->assign('id_where2',$gel_aktif);
			break;
			case 'form_invent_kk':
				$id = $this->input->post('id');
				$invent = $this->db->get_where('tbl_invent_kk', array('id'=>$id))->row_array();
				$this->smarty->assign('data', $invent);

				
				$edu = $this->db->get_where('adis_sys_usr', array('id_level'=>'44', 'erased'=>0))->result_array();
				$this->smarty->assign('edu', $edu);

				
				$cgts = $this->db->get_where('adis_sys_usr', array('id_level'=>'55', 'erased'=>0))->result_array();
				$this->smarty->assign('cgts', $cgts);

				$sts_contact = $this->db->get('idx_status_invent')->result_array();
				$this->smarty->assign('status_contact', $sts_contact);
				$content = "kk/form_invent.html";
			break;
			case 'form_invent_mm':
				$id = $this->input->post('id');
				$invent = $this->db->get_where('tbl_invent_mm', array('id'=>$id))->row_array();
				$this->smarty->assign('data', $invent);

				
				$edu = $this->db->get_where('adis_sys_usr', array('id_level'=>'44', 'erased'=>0))->result_array();
				$this->smarty->assign('edu', $edu);

				
				$cgts = $this->db->get_where('adis_sys_usr', array('id_level'=>'55', 'erased'=>0))->result_array();
				$this->smarty->assign('cgts', $cgts);

				$sts_contact = $this->db->get('idx_status_invent')->result_array();
				$this->smarty->assign('status_contact', $sts_contact);
				$content = "magister/form_invent.html";
			break;
			case 'form_invent':
				$id = $this->input->post('id');
				$invent = $this->db->get_where('tbl_invent', array('id'=>$id))->row_array();
				$this->smarty->assign('data', $invent);

				
				$edu = $this->db->get_where('adis_sys_usr', array('id_level'=>'44', 'erased'=>0))->result_array();
				$this->smarty->assign('edu', $edu);

				
				$cgts = $this->db->get_where('adis_sys_usr', array('id_level'=>'55', 'erased'=>0))->result_array();
				$this->smarty->assign('cgts', $cgts);

				$sts_contact = $this->db->get('idx_status_invent')->result_array();
				$this->smarty->assign('status_contact', $sts_contact);
				$content = "laporan/form_invent.html";
			break;
			case 'invent_kk':
			case 'invent_mm':
			case 'invent':
			   $content = "smb/calonmahasiswa/main.html";
			   $site = "Data Invent";
			   $this->smarty->assign('tipe',$p1);
			break;
			case "cmbs2":
				// $this->madmisi->mSelectPeriode();
				$this->madmisi->mSelectJalur();
				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '2' ")->result();		
				$this->smarty->assign('prodi',$prodi);	

				$periode ="SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='2' AND erased = 0 ORDER by kode DESC";
				$periode = $this->db2->query($periode)->result();			
					
				$this->smarty->assign('periode',$periode);	
				
				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='2'  ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);
				
				$content = "smb/calonmahasiswa/main.html";
				$site = "Daftar Calon Mahasiswa Baru";
				$this->smarty->assign('tipe',$p1);
			 break;			 
			case "cmb":
				// $this->madmisi->mSelectPeriode();
				$this->madmisi->mSelectJalur();
				$prodi = $this->db2->query("select * from adis_prodi where 1=1 and jenjang = '1' ")->result();		
				$this->smarty->assign('prodi',$prodi);	

				$periode ="SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='1' AND erased = 0 ORDER by kode DESC";
				$periode = $this->db2->query($periode)->result();			
					
				$this->smarty->assign('periode',$periode);	
				
				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 and jenjangType='1'  ")->row_array();
				$this->smarty->assign('periode_aktif', $periode_aktif['kode']);
				
				$gelombang = $this->db2->query("SELECT * FROM adis_periode WHERE idPeriodeMaster = '".$periode_aktif['kode']."' ")->result();
				$this->smarty->assign('gelombang', $gelombang);
				
				$content = "smb/calonmahasiswa/main.html";
				$site = "Daftar Calon Mahasiswa Baru";
				$this->smarty->assign('tipe',$p1);
			 break;			 
			 case "cmbPrestasi":
				$this->madmisi->mSelectPeriode();
				$this->madmisi->mSelectProdi();
				
				$jalur ="SELECT * FROM adis_jalur_smb WHERE kode= 'JP' AND erased = 0";
				$jalur = $this->db2->query($jalur)->result();
				$this->smarty->assign('jalur',$jalur);	
				
				$content = "smb/calonmahasiswa/main.html";
				$site = "Daftar Calon Mahasiswa Baru";
				$this->smarty->assign('tipe',$p1);
			 break;
		}
		 
		$this->smarty->assign('lokasi', $site);
		$this->smarty->assign('type', $type);
		$this->smarty->display($content);		
	}

	function getDisplayView($type="", $p1="", $p2="", $p3=""){
		$site = "";
		$content = "";

		switch($type){
			case 'formHasilSeleksiKK':

				// $beasiswa = $this->db->get('smart_ref_beasiswa')->result_array();
				$beasiswa = array();
				$this->smarty->assign('beasiswa', $beasiswa);
				
				$id = $this->input->post('id');
				$sql = "select a.*, b.kode_potongan
					from adis_smb_form a
					left JOIN adis_smb_usr_keu b ON a.kode = b.smbUsr
					where a.kode ='$id'";
				$data = $this->db->query($sql)->row_array();
				$this->smarty->assign('data', $data);
				$content = "kk/formPenilaian.html";
			break;
			case 'select_sub_menu':
				$id = $this->input->post('id');
				$submenu = $this->db->get_where('smart_menu', array('parent_id'=>$id))->result_array();
				$this->smarty->assign('submenu', $submenu);

				$html = '
					{foreach from=$submenu item=row}
					<option value="{$row.id}">{$row.nama_menu}</option>
					{/foreach}
				';
				$this->smarty->display('string:'.$html);
				exit;

			break;
			case 'reload_list_menu':
				$method = $this->input->post('editstatus');
				$data = "";
				$id = $this->input->post('id');
				$data = $this->db->get_where('smart_menu_privilage', array('id'=>$id))->row_array();
				$listMenu = "";
				$menu = str_replace(']','',str_replace('[', '', $data['privilage']));
				
				$listMenu = $this->db->query("select a.id, a.parent_id, a.nama_menu ,
					CASE WHEN b.id IS NULL THEN a.nama_menu ELSE b.nama_menu END AS sort1,
					CASE WHEN b.id IS NULL THEN NULL ELSE a.nama_menu END AS sort2
					from smart_menu a
					left join smart_menu b on a.parent_id = b.id
					where a.id in ($menu) 
					ORDER BY sort1, sort2;")->result_array();
				$this->smarty->assign('listMenu',$listMenu);

				$html = '
				{if isset($listMenu)}
				{foreach from=$listMenu item=row}
				{if $row.parent_id eq 0}
				<a class="btn btn-sm btn-primary" id="modId_{$row.id}" style="margin-bottom: 4px;min-width: 220px;text-align:left;" onclick="removeModul(\'{$row.id}\')">{$row.nama_menu} <i class="icon-remove-sign pull-right"></i></a><br>
				{else}
				<a class="btn btn-xs btn-info" id="modId_{$row.id}" style="margin-bottom: 4px;min-width: 220px;margin-left: 16px;text-align:left;" onclick="removeModul(\'{$row.id}\')">{$row.nama_menu} <i class="icon-remove-sign pull-right"></i></a><br>
				{/if}
				{/foreach}
				{/if}
				';
				$this->smarty->display('string:'.$html);
				exit;
				break;
			case 'form_mod_priv':
				$method = $this->input->post('editstatus');
				$data = "";
				if($method == 'edit'){
					$id = $this->input->post('id');
					$data = $this->db->get_where('smart_menu_privilage', array('id'=>$id))->row_array();
					$listMenu = "";
					$menu = str_replace(']','',str_replace('[', '', $data['privilage']));
					
					$listMenu = $this->db->query("select a.id, a.parent_id, a.nama_menu ,
						CASE WHEN b.id IS NULL THEN a.nama_menu ELSE b.nama_menu END AS sort1,
						CASE WHEN b.id IS NULL THEN NULL ELSE a.nama_menu END AS sort2
						from smart_menu a
						left join smart_menu b on a.parent_id = b.id
						where a.id in ($menu) 
						ORDER BY sort1, sort2;")->result_array();
						

					$this->smarty->assign('listMenu',$listMenu);
				}
				$level = $this->db->get_where('tbl_usrm_level', array('id !='=>'99', 'status'=>1))->result_array();

				$this->db->order_by('noOrder', 'asc');
				$menu0 = $this->db->get_where('smart_menu', array('parent_id'=>0))->result_array();

				$this->smarty->assign('level',$level);
				$this->smarty->assign('menu0',$menu0);
				$this->smarty->assign('data',$data);
				$this->smarty->assign('editstatus',$method);
				$content = "master/form_modul_privilege.html";
			break;
			case 'form_manage_prodi':
				$method = $this->input->post('editstatus');
				$data = "";
				if($method == 'edit'){
					$id = $this->input->post('id');
					$data = $this->db->get_where('adis_prodi', array('kode'=>$id))->row_array();
				}

				$this->smarty->assign('data',$data);
				$this->smarty->assign('editstatus',$method);
				$content = "master/form_prodi.html";
				break;
			case 'form_manage_agama':
				$method = $this->input->post('editstatus');
				$data = "";
				if($method == 'edit'){
					$id = $this->input->post('id');
					$data = $this->db->get_where('adis_type', array('kode'=>$id))->row_array();
				}

				$this->smarty->assign('data',$data);
				$this->smarty->assign('editstatus',$method);
				$content = "master/form_agama.html";
				break;
			case 'form_manage_edu':
				$method = $this->input->post('editstatus');
				$data = "";
				if($method == 'edit'){
					$this->load->library('encrypt');
					$id = $this->input->post('id');
					$data = $this->db->get_where('adis_sys_usr', array('kode'=>$id))->row_array();
					$data['password'] = $this->encrypt->decode($data['password']);
				}

				$this->smarty->assign('data',$data);
				$this->smarty->assign('editstatus',$method);
				$content = "magister/form_manage_edu.html";
				break;
			case 'detil_tele':
				$id = $this->input->post('id');
				$followup = $this->db2->query("SELECT * FROM idx_followup ORDER BY id ASC")->result_array();
				$edu = $this->db2->query("SELECT kode, username FROM adis_sys_usr WHERE id_level = 44 AND status = 0 AND erased = 0 ORDER BY kode ASC")->result_array();

				$sql = "Select * FROM smart_telemarketing A WHERE A.kode = '$id' ;";
				$query = $this->db2->query($sql);

				$data = "";
				if($query->num_rows() > 0){
					$data = $query->row_array();
				}
				$this->smarty->assign('data',$data);
				$this->smarty->assign('edu',$edu);
				$this->smarty->assign('followup',$followup);

				$content = "magister/formTele.html";
				break;
			case 'tolak_forlap_kk':
				$id = $this->input->post('id');

				$sql = "select a.nomor, a.kode, d.singkatan, f.nama, g.tahunLulus, g.jurusan, g.lulusAsal, g.verifikasi_forlap,
					f.rumahCell, g.tolak_forlap_msg, g.lulusankk
					from adis_smb_form a
					INNER JOIN adis_periode_master c ON LEFT(a.bukaSmb,8) = c.kode
					left JOIN adis_prodi d ON RIGHT(a.bukaSmb, 4) = d.kode 
					left JOIN adis_jalur_smb e ON SUBSTR(a.bukaSmb, 12, 2) = e.kode
					left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
					left JOIN adis_smb_usr_edu g ON a.kode = g.smbUsr 
					where a.kode = '$id' ";
				$data = $this->db->query($sql)->row_array();

				$this->smarty->assign('data', $data);
				$content = "kk/form_tolak_ijazah.html";
				break;
			case 'tolak_forlap':
				$id = $this->input->post('id');

				$sql = "select a.nomor, a.kode, d.singkatan, f.nama, g.tahunLulus, g.jurusan, g.lulusAsal, g.verifikasi_forlap,
					f.rumahCell, g.tolak_forlap_msg
					from adis_smb_form a
					INNER JOIN adis_periode_master c ON LEFT(a.bukaSmb,8) = c.kode
					left JOIN adis_prodi d ON RIGHT(a.bukaSmb, 4) = d.kode 
					left JOIN adis_jalur_smb e ON SUBSTR(a.bukaSmb, 12, 2) = e.kode
					left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
					left JOIN adis_smb_usr_edu g ON a.kode = g.smbUsr 
					where a.kode = '$id' ";
				$data = $this->db->query($sql)->row_array();

				$this->smarty->assign('data', $data);
				$content = "magister/form_tolak_ijazah.html";
				break;
			case 'payment_du_kk':
				$id = $this->input->post('id');
				$data = $this->db->query("Select a.kode, a.nomor, a.stsReapplyPaid, a.stsReapplyPaidConfirm, a.reapplyBankTransferAmount, 
					a.reapplyBankTransferTime,
					b.buktiBayarDaftarUlang, a.reapplyMaxDate, c.username as nama, b.namaRekPengirimDaftarUlang, b.pesanTolakDU, b.tolakDU,
					e.nama_rek
					from adis_smb_form a 
					left join adis_smb_usr_keu b on a.kode = b.smbUsr
					left join adis_smb_usr c on a.kode = c.kode
					left join smart_ref_rekening e on a.reapplyBankAccountType = e.nomor_rek
					where a.kode = '$id' ")->row_array();
				if($data){
					$this->smarty->assign('data', $data);
					if($p1 == 'detil'){
						
						$invoice = "select b.*, c.potongan4x as potongan_gelombang
								from adis_smb_form a 
								inner join adis_pembayaran_kk b on left(a.bukaSmb,8) = b.periode_kode and right(a.bukaSmb,4) = b.prodi
								inner join adis_periode c on left(a.bukaSmb,10) = c.kode  
								where a.kode = '$id'
							";
						$invoice = $this->db->query($invoice)->row_array();

						$this->smarty->assign('invoice', $invoice);

						$content = "kk/detil_daftarulang.html";
					}
					if($p1 == 'setbiaya'){

					}
				}
			break;
			case 'payment_du':
				$id = $this->input->post('id');
				$data = $this->db->query("Select a.kode, a.nomor, a.stsReapplyPaid, a.stsReapplyPaidConfirm, a.reapplyBankTransferAmount, 
					a.reapplyBankTransferTime,
					b.buktiBayarDaftarUlang, a.reapplyMaxDate, c.username as nama, b.namaRekPengirimDaftarUlang, b.pesanTolakDU, b.tolakDU,
					b.pilihan_angsuran_km, b.skema_id, b.kode_potongan, d.deskripsi as nama_bea, d.potongan_percent,
					a.reapplyBankAccountType, e.nama_rek, f.nama_skema
					from adis_smb_form a 
					left join adis_smb_usr_keu b on a.kode = b.smbUsr
					left join adis_smb_usr c on a.kode = c.kode
					left join smart_ref_beasiswa d on b.kode_potongan = d.kode
					left join smart_ref_rekening e on a.reapplyBankAccountType = e.nomor_rek
					left join smart_ref_skema_biaya f on b.skema_id = f.id
					where a.kode = '$id' ")->row_array();
				if($data){
					$this->smarty->assign('data', $data);
					if($p1 == 'detil'){
						
						$ref_skema = $this->db->get('smart_ref_skema_biaya')->result_array();
						$this->smarty->assign('ref_skema', $ref_skema);

						$content = "magister/detil_daftarulang.html";
					}
					if($p1 == 'setbiaya'){

					}
				}
			break;
			case 'formbeasiswaMag':
				$editstatus = $this->input->post('editstatus');
				$data = "";
				if($editstatus == 'add'){

				}elseif($editstatus == 'edit'){
					$id = $this->input->post('id');
					$data = $this->db->get_where('smart_ref_beasiswa', array('id'=>$id))->row_array();
				}

				$this->smarty->assign('editstatus', $editstatus);
				$this->smarty->assign('data', $data);
				$content = "magister/form_beasiswa_biaya.html";
			break;
			case 'setPotonganMag':
				$editstatus = $this->input->post('editstatus');
				$skema = $this->db->query('select a.* from smart_skema_biaya_normal a 
					inner join adis_pembayaran_s2 c on c.kode = a.pembayaran_kode
					inner join adis_periode_master d on c.kodePeriode = d.kode and d.status = 1')->result_array();
				$this->smarty->assign('skema', $skema);
				
				$data = "";
				if($editstatus == 'add'){

				}elseif($editstatus == 'edit'){
					$id = $this->input->post('id');
					$data = $this->db->get_where('smart_skema_biaya_beasiswa', array('id'=>$id))->row_array();
				}

				$this->smarty->assign('editstatus', $editstatus);
				$this->smarty->assign('data', $data);
				$content = "magister/form_skema_biaya_bea.html";
			break;
			case 'skemaBiayaForm':
				$editstatus = $this->input->post('editstatus');
				$tarif = $this->db->query('select a.* from adis_pembayaran_s2 a 
					inner join adis_periode_master b on a.kodePeriode = b.kode and b.status = 1')->result_array();
				$this->smarty->assign('tarif', $tarif);

				$ref_skema = $this->db->get('smart_ref_skema_biaya')->result_array();
				$this->smarty->assign('ref_skema', $ref_skema);
				
				$data = "";
				if($editstatus == 'add'){

				}elseif($editstatus == 'edit'){
					$id = $this->input->post('id');
					$data = $this->db->get_where('smart_skema_biaya_normal', array('id'=>$id))->row_array();
				}

				$this->smarty->assign('editstatus', $editstatus);
				$this->smarty->assign('data', $data);
				$content = "magister/form_skema_biaya.html";
			break;
			case 'formSettingTarifkk':
				$editstatus = $this->input->post('editstatus');
				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE jenjangType='".$this->auth['jenjang']."' and erased = 0  order by status desc")->result_array();
				$this->smarty->assign('periode', $periode_aktif);

				$prodi = $this->db->get_where('adis_prodi', array('jenjang'=>'1'))->result_array();
				$this->smarty->assign('prodi', $prodi);

				$data = "";
				if($editstatus == 'add'){

				}elseif($editstatus == 'edit'){
					$id = $this->input->post('id');
					$data = $this->db->get_where('adis_pembayaran_kk', array('kode'=>$id))->row_array();
				}

				$this->smarty->assign('editstatus', $editstatus);
				$this->smarty->assign('data', $data);
				$content = "kk/formSettingTarif.html";
			break;
			case 'formSettingTarif':
				$editstatus = $this->input->post('editstatus');
				$periode_aktif = $this->db2->query("SELECT * FROM adis_periode_master WHERE jenjangType='".$this->auth['jenjang']."' and erased = 0  order by status desc")->result_array();
				$this->smarty->assign('periode', $periode_aktif);

				$prodi = $this->db->get_where('adis_prodi', array('jenjang'=>'2'))->result_array();
				$this->smarty->assign('prodi', $prodi);

				$data = "";
				if($editstatus == 'add'){

				}elseif($editstatus == 'edit'){
					$id = $this->input->post('id');
					$data = $this->db->get_where('adis_pembayaran_s2', array('kode'=>$id))->row_array();
				}

				$this->smarty->assign('editstatus', $editstatus);
				$this->smarty->assign('data', $data);
				$content = "magister/formSettingTarif.html";
			break;
			case 'formHasilSeleksi':

				$beasiswa = $this->db->get('smart_ref_beasiswa')->result_array();
				$this->smarty->assign('beasiswa', $beasiswa);
				
				$id = $this->input->post('id');
				$sql = "select a.*, b.kode_potongan
					from adis_smb_form a
					left JOIN adis_smb_usr_keu b ON a.kode = b.smbUsr
					where a.kode ='$id'";
				$data = $this->db->query($sql)->row_array();
				$this->smarty->assign('data', $data);
				$content = "magister/formPenilaian.html";
			break;
			case 'formTesTPA':
				$id = $this->input->post('id');
				$sql = "select a.nomor, a.kode, f.nama, 
					f.rumahCell, a.stsApplyPaid, a.stsApplyPaidConfirm, a.applyBankTransferAmount, 
					g.metode_purpose, g.tanggal_purpose,g.jam_purpose, g.jam_end_purpose, g.metode_acc, g.tanggal_acc, g.jam_acc, g.is_acc,
					g.jam_end_acc
					from adis_smb_form a
					left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
					left JOIN smart_jadwal_tpa g ON a.kode = g.kode where a.kode ='$id'";
				$data = $this->db->query($sql)->row_array();
				$this->smarty->assign('data', $data);
				$content = "kk/detil_jadwal.html";
			break;
			case 'formTesWawancara':
				$id = $this->input->post('id');
				$sql = "select a.nomor, a.kode, f.nama, 
					f.rumahCell, a.stsApplyPaid, a.stsApplyPaidConfirm, a.applyBankTransferAmount, 
					g.metode_purpose, g.tanggal_purpose,g.jam_purpose, g.metode_acc, g.tanggal_acc, g.jam_acc, g.is_acc, 
					g.fc_ijazah, g.fc_transkrip
					from adis_smb_form a
					left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
					left JOIN smart_jadwal_test g ON a.kode = g.kode where a.kode ='$id'";
				$data = $this->db->query($sql)->row_array();
				$this->smarty->assign('data', $data);
				$content = "magister/detil_jadwal.html";
			break;
			case 'payment_reg':
				$id = $this->input->post('id');
				$data = $this->db->query("Select a.kode, a.applyBankTransferAmount, a.applyBankAccountType, a.applyBankTransferTime,
					b.buktiBayarPendaftaran, c.username as nama, b.namaRekPengirimPendaftaran, b.pesanTolakP, b.tolakPendaftaran
					from adis_smb_form a 
					left join adis_smb_usr_keu b on a.kode = b.smbUsr
					left join adis_smb_usr c on a.kode = c.kode
					where a.kode = '$id' ")->row_array();
				if($data){
					$this->smarty->assign('data', $data);
					if($p1 == 'detil'){
						$content = "magister/detil_payment.html";
					}
					if($p1 == 'setbiaya'){
						$content = "magister/form_biaya.html";
					}
				}
			break;
			case 'payment_reg_kk':
				$id = $this->input->post('id');
				$data = $this->db->query("Select a.kode, a.applyBankTransferAmount, a.applyBankAccountType, a.applyBankTransferTime,
					b.buktiBayarPendaftaran, c.username as nama, b.namaRekPengirimPendaftaran, b.pesanTolakP, b.tolakPendaftaran,
					a.stsApplyPaidConfirm
					from adis_smb_form a 
					left join adis_smb_usr_keu b on a.kode = b.smbUsr
					left join adis_smb_usr c on a.kode = c.kode
					where a.kode = '$id' ")->row_array();
				if($data){
					$this->smarty->assign('data', $data);
					if($p1 == 'detil'){
						$content = "kk/detil_payment.html";
					}
					if($p1 == 'setbiaya'){
						$content = "kk/form_biaya.html";
					}
				}
			break;
		}

		$this->smarty->display($content);	

	}
	
	function getdatagrid($type, $p1 = '', $p2 = ''){
		echo $this->msmb->get_data_grid($type, $p1, $p2);
	}



	function simpandata($p1="",$p2=""){
		if($this->input->post('mod'))$p1=$this->input->post('mod');
		$post = array();
        foreach($_POST as $k=>$v){
			if($this->input->post($k)!=""){
				$post[$k] = $this->input->post($k);
			}else{
				$post[$k] = null;
			}
			
		}
		if(isset($post['editstatus'])){$editstatus = $post['editstatus'];unset($post['editstatus']);}
		else $editstatus = $p2;
		
		echo $this->msmb->simpandata($p1, $post, $editstatus);
	}

	function testmasukoi(){
		$kode = 'testing@lom.com';
		$this->testMasukAccount($kode);
	}

	function testMasukAccount($kode = ''){
		// $token = '0d5bd0a6a31d14f384ed4c5f40383791';
		$token = '03a135507c9d2a19f44f4593c545c2c4';
		$domainName = 'https://usm.bakrie.ac.id';
		$serverUrl;
		$error;

		$serverUrl = $domainName . '/webservice/rest/server.php' . '?wstoken=' . $token;

		$qryMhs = "SELECT A.kode, A.nomor, B.nama, C.password, D.nama as prodi FROM adis_smb_form A
			LEFT JOIN adis_smb_usr_pribadi B ON B.kode = A.kode
			LEFT JOIN adis_smb_usr C ON C.kode = A.kode
			LEFT JOIN adis_prodi D ON D.kode = RIGHT(A.bukaSmb, 4)
			WHERE A.kode = '$kode';";
		$mhs = $this->db2->query($qryMhs)->row_array();
		$this->load->library('encrypt');
		$password = $this->encrypt->decode($mhs['password']);
		if(strlen($password)<6){
			$password = '1234567';
		}

		$fullName = $this->split_name($mhs['nama']);
		if($fullName[1] == ''){
			$fullName[1] = $fullName[0]; 
		}


		$functionName = 'core_user_create_users';

		$user1 = new stdClass();
        $user1->username = strtolower($kode);
        $user1->password = $password ;
        $user1->firstname = $fullName[0];
        $user1->lastname = $fullName[1];
        $user1->email = $kode;
        $user1->auth = 'manual';
        $user1->idnumber = '';
        $user1->lang = 'en';
        $user1->timezone = 'Asia/Jakarta';
        $user1->mailformat = 0;
        $user1->description = '';
        $user1->city = 'Jakarta';
        $user1->country = 'ID';     //list of abrevations is in yourmoodle/lang/en/countries
        // $preferencename1 = 'auth_forcepasswordchange';
        // $user1->preferences = array(
            // array('type' => $preferencename1, 'value' => 'false')
            // );


		$users = array((array)$user1);
		$params = array('users' => (array)$users);
		/// REST CALL
		$restformat = "json";
		$serverurl = $serverUrl . '&wsfunction=' . $functionName. '&moodlewsrestformat=' . $restformat;
		require_once (APPPATH."libraries/curl.php");
		$curl = new curl();
		
		echo "<pre>";
		print_r($params);
		// exit;

		$resp = $curl->post($serverurl, $params);

		print_r($resp);
		
	}

	function getSkemaBiaya(){
		$post = $this->input->post();
		$kode = $post['kode'];
		$skemaId = $post['skema_id'];
		$keu = $this->db->query("select kode_potongan from adis_smb_usr_keu where smbUsr ='$kode' " )->row_array();
		if(!empty($keu['kode_potongan'])){
			$qry = " select b.pembayaran_kode, a.id, a.total, a.tagihan_1, a.potongan, a.tagihan_angsuran,
				b.tagihan_pertama, b.jumlah_angsuran
				from smart_skema_biaya_beasiswa a
				inner join `smart_skema_biaya_normal` b on a.skema_normal_id = b.id
				inner join smart_ref_skema_biaya c on b.`ref_skema_id` = c.id 
				inner join smart_ref_beasiswa d on a.nilai_potongan_percent = d.`potongan_percent` and d.kode = '".$keu['kode_potongan']."'
				where c.id = '$skemaId' ;
			";
		}else{
			$qry = "select a.*, a.tagihan_pertama as tagihan_1 
			from smart_skema_biaya_normal a
			inner join smart_ref_skema_biaya b on a.ref_skema_id = b.id 
			where b.id = '$skemaId';";
		}

		$tagihan = $this->db->query($qry)->row_array();

		$gelombang = $this->db->query('select a.potongan1x, a.potongan4x, a.potongan24x 
			from adis_periode a
			LEFT JOIN adis_periode_master b ON b.kode = a.idPeriodeMaster
			where a.status=1 and b.jenjangType = 2 ')->row_array();

		switch($skemaId){
			case 1:
				$tagihan['potongan'] = (5/100) * $tagihan['tagihan_1'];
				$tagihan['tagihan_invoice'] = $tagihan['tagihan_1']-$tagihan['potongan'];
			break;
			case 2:
				$tagihan['potongan'] = $gelombang['potongan4x'];
				$tagihan['tagihan_invoice'] = $tagihan['tagihan_1']-$tagihan['potongan'];
			break;
			case 3:
				$tagihan['potongan'] = $gelombang['potongan24x'];
				$tagihan['tagihan_invoice'] = $tagihan['tagihan_1']-$tagihan['potongan'];
			break;
		}

		echo json_encode($tagihan);
	}
	
	function viewTable(){	
		$this->smarty->assign('time', time());
		$site = "calonmahasiswa";			
		$this->smarty->assign('site',$site);
		$this->smarty->assign('lokasi',"Daftar Peserta Ujian");
		$this->smarty->display('index.html');
	}
	
	function filterData($periode = '', $jalur = '', $prodi = ''){	
		$this->load->library('Datatable');
        $jsonArray = $this -> datatable -> datatableJson(array('nomor' => 'text'));
		
		$this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
        $this -> output -> set_content_type('application/json') -> set_output(json_encode($jsonArray));	
		$this->smarty->display('smb/DataTable/calonmahasiswa.html');	
	}
		
	function genFormulirPDF(){
		$this->load->library('mlpdf');
		$this->load->model('mportal');
		$pdf = $this->mlpdf->load();
		
		$data = "SELECT A.nomor,A.kode, A.nim AS nim, B.nama AS Nama, D.nama as 'Program Studi', B.ibuNama AS 'Nama Ibu', C.nama AS 'Asal SMA',
			w.nama as 'Propinsi', w2.nama as 'Kab/Kota', w3.kodepos,
			C.nisn_nim AS NISN
			FROM adis_smb_form A 
			LEFT JOIN adis_smb_usr_pribadi B ON B.kode = A.kode
			LEFT JOIN adis_smb_usr_edu C ON C.smbUsr = A.kode
			LEFT JOIN adis_prodi D ON D.kode = RIGHT(A.bukaSmb,4)
						LEFT JOIN adis_wil w ON C.prop = w.kode
						LEFT JOIN adis_wil w2 ON C.kabKota = w2.kode
						LEFT JOIN adis_kodepos w3 ON C.kodePos = w3.kode
			WHERE nim LIKE '1171060%' 
			ORDER BY nim LIMIT 30";
		$data = $this->db2->query($data)->result_array();
		
		$prodi = $this->db2->query("SELECT * FROM adis_prodi WHERE erased = 0")->result_array();
		$this->smarty->assign("prodi", $prodi);
		
		
		foreach($data as $i => $data){
			
			$spdf = new mPDF('', 'A4', 0, '', 12.7, 12.7, 40, 20, 10, 2, 'P');
			$spdf->ignore_invalid_utf8 = true;
			$spdf->allow_charset_conversion = true;     // which is already true by default
			$spdf->charset_in = 'iso-8859-2';  // set content encoding to iso
			$spdf->SetDisplayMode('fullpage');
			
			$profil = $this->msmb->mProfil($data['kode']);
			$this->mportal->mSelectPendidikan($data['kode']);	
			$this->mportal->mOrganisasi($data['kode']);
			$this->mportal->mPrestasi($data['kode']);
			$this->mportal->mSelectOrtu($data['kode']);
			$this->mportal->mSelectSaudara($data['kode']);
			
			$htmlheader = $this->smarty->fetch('smb/formulir/dokumen_header.html');
			$htmlcontent = $this->smarty->fetch('smb/formulir/dokumen_formulir_pdf.html');
			
			$spdf->SetHTMLHeader($htmlheader);
			$spdf->SetHTMLFooter('
				<div style="font-family:arial; font-size:10px; text-align:right; font-weight:bold;">
					Halaman {PAGENO} dari {nbpg}
				</div>
			');		
			
			// $spdf->SetProtection(array());				
			$spdf->WriteHTML($htmlcontent); // write the HTML into the PDF
			//$spdf->Output('repositories/Dokumen_LS/LS_PDF/'.$filename.'.pdf', 'F'); // save to file because we can
			$spdf->Output('formulir/'.$data['nim'].'.pdf', 'F'); // view file
			echo $i.".".$data['nim']. "<br>";
		}
		
		
	}
		
	function smbCalon(){
		$uri = $this->uri->segment(3);
		$jalur = $this->input->post('jalur');
		$periode = $this->input->post('periode');
		$prodi = $this->input->post('prodi');
		
		$andQuery = "";
		
		if ($prodi != 'all'){
			$andQuery = "AND substr(f.bukaSmb, -4) = '$prodi'";
		}
		
		$this->madmisi->mSelectPeriode();
		
		if ($jalur != '' && $uri == "table"){
			
			$periode = implode(".",array($periode,$jalur));
			$this->db2->query("SET @num:=0;");
			$sql = "SELECT @num:=@num+1 AS 'No', f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, u.username as nama_cm , p.nama as progdi, 
			DAYNAME(u.createTime) as hari, DATE(u.createTime) as tanggal, p.singkatan as progdi_inisial,
			f.stsApplyPaid, f.stsApplyPaidConfirm, up.stsPribadiConfirm, f.stsEventConfirm, f.stsEventUsmPresent, up.foto,
			f.stsResultConfirm, f.stsMundurBeforeReapply,
			f.stsReapplyPaid, stsReapplyPaidConfirm, f.stsMundurAfterReapply, j.nama as n_jalur, u.jalur_penerimaan
			FROM adis_smb_form f 
			INNER JOIN adis_smb_usr u ON u.kode = f.kode
			INNER JOIN adis_buka_smb b ON b.kode = f.bukaSmb
			INNER JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
			INNER JOIN adis_prodi p ON p.kode = b.prodi
			INNER JOIN adis_jalur_smb j ON j.kode = b.jalur
			WHERE SUBSTR(f.bukaSmb, 1, 13) = '$periode' $andQuery
			ORDER BY DATE(u.createTime) DESC";
			
			$sql = $this->db2->query($sql)->result();	
			$this->smarty->assign('sql',$sql);
			
			$this->smarty->assign('periode',$periode);
			$this->smarty->display('smb/tblCalonPeserta.html');	
			
		}else{
			$site = "Peserta";			
			$this->smarty->assign('site',$site);
			$this->smarty->assign('lokasi',"Daftar Peserta Ujian");
			$this->smarty->display('index.html');
		}
		
	}
	
	function smbJalur(){		
		$periode = $this->input->post('periode');
		$function = $this->input->post('fungsi');
		
		$jenjang = substr($periode, -3, 1);;
		
		$sql ="SELECT * FROM adis_jalur_smb WHERE erased = 0";
		$sql = $this->db2->query($sql)->result();
		
		$jalur = array(
			(object) array ( 'kode'=>'RAPOR', 'nama'=>'Jalur Rapor'), 
			(object) array  ('kode'=>'TULIS', 'nama'=>'Jalur Tes Tulis'),
			(object) array  ('kode'=>'ODS', 'nama'=>'ODS Online')
			);
			
		$gel = $this->db2->query("SELECT * FROM adis_periode where idPeriodeMaster = '$periode' ")->result();
		
		$sqlProdi = "SELECT * FROM adis_prodi WHERE erased = 0 AND jenjang = $jenjang";
		$sqlProdi = $this->db2->query($sqlProdi)->result();
		
		$this->smarty->assign('fungsi',$function);
		$this->smarty->assign('periode',$periode);	
		$this->smarty->assign('gel',$gel);	
		$this->smarty->assign('jalur',$jalur);		
		$this->smarty->assign('prodi',$sqlProdi);	
		$this->smarty->display('smb/selectJalur.html');	
	}
	
	function smbPay(){
		$this->load->model('mintegrasi');
		$this->load->model('mportal');
		
		$uri = $this->uri->segment(3);
		$opt = $this->input->post("opt");
		$kode = $this->input->post("value");
		$jalur = $this->input->post("jalur");
		$periode = $this->input->post("periode");
		$gelombang = $this->input->post("gelombang");
		$prodi = $this->input->post('prodi');
                
		$stsMahasiswa = $this->db->query("SELECT A.kode,A.nomor, A.stsApplyPaidConfirm, A.bukaSmb, B.jalur_penerimaan
				FROM adis_smb_form A 
				LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
				WHERE A.kode = '$kode'")->row_array();
		
		$andQuery = "";
		
		if ($prodi != 'all'){
			$andQuery = "AND substr(f.bukaSmb, -4) = '$prodi'";
		}
		
		// if ($this->level == '77'){
		// 	$periodes ="SELECT * FROM adis_periode WHERE erased = 0 ORDER by kode DESC";
		// 	$periodes = $this->db2->query($periodes)->result();			
				
		// 	$this->smarty->assign('periode',$periodes);	
		// }else{
		// 	$this->madmisi->mSelectPeriode();
		// }

		$this->madmisi->mSelectPeriode();
		
		if ($uri == "confirm" && $opt == "confirm"){
                    
			if ($stsMahasiswa['stsApplyPaidConfirm'] == '1'){
				echo 0;
			}else{
			
				if(preg_match('#[a-z]#',$stsMahasiswa['nomor'])){
					$qr= "	set @_bukasmb:=(select bukaSmb from adis_smb_form where kode='$kode');"; 
					$qr2= "	set @_tahun:=left(@_bukasmb,4); ";
					$qr3= "	set @_tahun_yy:=right(@_tahun,2); ";
					$qr4= "	set @_jenjang:=right(left(@_bukasmb,6),1); ";
					$qr5= "	set @_sesi:=right(left(@_bukasmb,8),1); ";
					// $qr6= "	set @_kode:=concat(@_tahun_yy,@_jenjang,@_sesi); "; ketika menggunakan format sesi
					$qr6= "	set @_kode:=concat(@_tahun_yy,@_jenjang); "; 
					$qr7= "	set @_concat := concat(@_kode,'%'); ";
					$qr8= "	set @_regOrder := (select max(nomor)+1 from adis_smb_form where nomor LIKE @_concat); ";
					
					$this->db2->query($qr);
					$this->db2->query($qr2);
					$this->db2->query($qr3);
					$this->db2->query($qr4);
					$this->db2->query($qr5);
					$this->db2->query($qr6);
					$this->db2->query($qr7);
					$this->db2->query($qr8);
					
					$echo = $this->db2->query("SELECT @_regOrder as data")->row();
					
					if (!$echo->data){
						$qr0= "	set @_regOrder := concat(@_kode,'10001')";
						$this->db2->query($qr0);
					
					}
							
					$noRegDone = $this->db2->query("SELECT @_regOrder as noreg")->row_array();
					// print_r($echo);exit;
					
					$this->db2->where('kode', $kode);
					$this->db2->update('adis_smb_form', array('nomor'=>$noRegDone['noreg']));
				}

				$jenjangPilihan = substr($stsMahasiswa['bukaSmb'], 5, 1);			
				
				$jadwalCreate  = 0;
				if($stsMahasiswa['jalur_penerimaan'] == 'TULIS'){
					$jadwalCreate = $this->mportal->mRuangSmb($kode);
				}else{
					$jadwalCreate  = 1;
					$this->db2->where("kode", $kode);
					$this->db2->update("adis_smb_form", array(
											'updateUser'=>$kode,
											'stsEventConfirm'=>1
										));
				}
				
				if ($jadwalCreate == 1){
							
					// $noRegDone = $this->db2->query("SELECT @_regOrder as noreg")->row_array();
					
					$qry_keu = $this->db2->query("SELECT nomor FROM adis_smb_form WHERE kode = '$kode';")->row_array();
					$in_nomor = $this->db2->query("UPDATE adis_smb_usr_keu SET no_tagihan_daftar = '".$qry_keu['nomor']."' WHERE smbUsr = '$kode';");
					
					$nomor_tagihan = $stsMahasiswa['nomor'];
					$this->db2->where('kode', $kode);
					$this->db2->update('adis_smb_form', array('stsApplyPaidConfirm'=>1));
					
					$this->db2->where(array('kode_mhs'=>$kode, 'nomor_tagihan'=>$nomor_tagihan));
					$this->db2->update('tbl_tagihan_cmb', array('status_bayar'=>1));
					
					$cmb = $this->db2->query("SELECT * FROM adis_smb_usr_pribadi WHERE kode= '$kode'")->row_array();
					
					$konten = array( 'konten' =>"
						Kepada ".$cmb['nama']."
						<br>
						<p>Bukti bayar Pendaftaran anda telah diterima. Silahkan melanjutkan proses selanjutnya pada aplikasi SMART.
						<br>
						<br>
						<br>
						<br>
						<br>Mohon untuk tidak membalas email ini. Jika ada sanggahan/pertanyaan, silahkan hubungi bagian Admisi Universitas Bakrie
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>Universitas Bakrie, Kawasan Rasuna Epicentrum
						<br>Jl.H.R Rasuna Said Kav C-22, Kuningan Jakarta 
						<br>Jakarta 12920 Indonesia
						<br>Office Ph : (021) 526 1448 
						<br>E-mail : usm@bakrie.ac.id
					  ");
					  
					$subject = "Konfirmasi Bukti Bayar Pendaftaran Diterima - ".$cmb['nama']."";
					$this->mregistrasi->emailNotifikasi($kode, $subject, $konten);
					// $this->msmb->emailNotifikasi($kode, $subject, $konten);	
					
					$jalurPilihan = substr($stsMahasiswa['bukaSmb'], 11, 2);
					if ($jalurPilihan == '01' || $jalurPilihan == '02'){
						// $this->testMasukAccount($kode);
					}
					
					/*** Integrasi data ke ASIK */
					// $this->mintegrasi->validasi_pemb_admisi($kode);
					// $this->mintegrasi->update_noreg($kode);
					
					echo 1;
				}else{
					echo 2;
				}
			}
			/*** end */
			
			//redirect ('/smb/smbPay', 'refresh');
			
		}else if($uri == "table" && $jalur != ""){
			
			if ($jalur == 'all'){
				$wrJalur = "AND SUBSTR(f.bukaSmb, 1, 8) = '$periode'";
			}else{
				// $periode = implode(".",array($periode,$jalur));
				$wrJalur = "AND SUBSTR(f.bukaSmb, 1, 8) = '$periode'";	
				$wrJalur .= " AND u.jalur_penerimaan = '$jalur' ";			
			}
			
			if($gelombang != 'all'){
				$wrJalur .=  " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
			}
			
			
			$sql ="SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, up.nama as nama_cm , p.nama as progdi, j.nama as n_jalur, t.nama as metode, f.stsApplyPaid as statusBayar,
			t2.nama as rekening, DAYNAME(f.applyBankTransferTime) as hari, f.applyBankTransferTime as tglPembayaran, 
			FORMAT(f.applyBankTransferAmount, 2) as jumlah, f.stsApplyPaidConfirm as confirm, f.applyBankAccountType, f.applyBankTransferType,
			f.applyBankTransferValidCode as kodeValidasi, p.singkatan as progdi_inisial, up.rumahCell,
			k.buktiBayarPendaftaran, k.noRekPengirimPendaftaran, k.namaRekPengirimPendaftaran, k.noAtmCardPendaftaran, u.jalur_penerimaan
				FROM adis_smb_form f 
				LEFT JOIN adis_smb_usr u ON u.kode = f.kode
				INNER JOIN adis_periode_master c ON LEFT(f.bukaSmb,8) = c.kode
				INNER JOIN adis_prodi p ON RIGHT(f.bukaSmb, 4) = p.kode 
				INNER JOIN adis_jalur_smb j ON SUBSTR(f.bukaSmb, 12, 2) = j.kode
				LEFT JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
				INNER JOIN adis_smb_usr_keu k ON k.smbUsr = f.kode
				LEFT JOIN adis_type t ON t.kode = f.applyBankTransferType
				LEFT JOIN adis_type t2 ON t2.kode = f.applyBankAccountType 
				WHERE 1=1 
				$wrJalur $andQuery
			ORDER BY f.updateTime, f.stsApplyPaidConfirm ASC";
			$sql = $this->db2->query($sql)->result();	
			$this->smarty->assign('sql',$sql);	
			
			// $sql1 = $this->db2->query("SELECT COUNT(*) as total FROM adis_smb_form f WHERE 1=1
				// $wrJalur AND stsApplyPaidConfirm = 1  $andQuery")->row();
			$sql1 = $this->db2->query("SELECT COUNT(*) as total 
				FROM adis_smb_form f 				
				LEFT JOIN adis_smb_usr u ON u.kode = f.kode
				WHERE 1=1
				$wrJalur AND stsApplyPaid = 1  $andQuery")->row();
			$this->smarty->assign('total',$sql1);
			
			$this->smarty->display('smb/tblPay.html');
		}else{	
			$site = "Pembayaran";			
			$this->smarty->assign('site',$site);
			$this->smarty->assign('lokasi',"Daftar Pembayaran Calon Peserta");
			$this->smarty->display('index.html');
		
		}
	}
		
	function split_name($name) {
		$name = trim($name);
		$last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
		$first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
		return array($first_name, $last_name);
	}
	
	function smbEvent(){
		
		if ($this->auth['level'] != "99" && $this->auth['level'] != "1"){ redirect('/home','refresh');}
		
		$uri = $this->uri->segment(3);
		$jalur = $this->input->post('jalur');
		$periode = $this->input->post('periode');
		$gelombang = $this->input->post('gelombang');
		$prodi = $this->input->post('prodi');
		
		$andQuery = "";
		
		if ($prodi != 'all'){
			$andQuery = "AND substr(f.bukaSmb, -4) = '$prodi'";
		}
		
		$this->madmisi->mSelectPeriode();
		
		if ($jalur != '' && $uri == "table"){
			
			if ($jalur == 'all'){
				$wrJalur = "AND SUBSTR(f.bukaSmb, 1, 8) = '$periode'";
			}else{
				// $periode = implode(".",array($periode,$jalur));
				$wrJalur = " AND SUBSTR(f.bukaSmb, 1, 8) = '$periode'";	
				$wrJalur .= " AND u.jalur_penerimaan = '$jalur' ";			
			}
			
			if($gelombang != 'all'){
				$wrJalur .=  " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
			}
		
			$sql ="SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, up.nama as nama_cm , p.nama as progdi, j.nama as n_jalur, f.`event` as event,
			DAYNAME(e.tanggal) as hari, DAY(e.tanggal) as tanggal, MONTHNAME(e.tanggal) as bulan, YEAR(e.tanggal) as tahun,
			e.tanggal as tglEvent, r.nama as ruang, g.nama as namaGedung, g.alamat, g.kabKota, g.prop, g.kodePos,
			g.tel, g.fax, e.nama as n_periode, f.interviewPhone, j.jenjang,
			DAY(v.tanggal) as tanggalPhone, DAYNAME(v.tanggal) as hariPhone, MONTHNAME(v.tanggal) as bulanPhone, YEAR(v.tanggal) as tahunPhone,
			e.nama as eventName, e.jamMasuk, e.jamKeluar, e.petugas, e.pewawancara, p.singkatan as inisial_prodi, u.jalur_penerimaan
			FROM adis_smb_form f 
				INNER JOIN adis_smb_usr u ON u.kode = f.kode
				INNER JOIN adis_periode_master c ON LEFT(f.bukaSmb,8) = c.kode
				INNER JOIN adis_prodi p ON RIGHT(f.bukaSmb, 4) = p.kode 
				INNER JOIN adis_jalur_smb j ON SUBSTR(f.bukaSmb, 12, 2) = j.kode
				INNER JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
				LEFT JOIN adis_event_smb e ON e.kode = f.`event`
				LEFT JOIN adis_personal o ON o.kode = e.petugas
				LEFT JOIN adis_personal w ON w.kode = e.pewawancara					
				LEFT JOIN adis_ruang r ON r.kode = e.ruang
				LEFT JOIN adis_gedung g ON g.kode = r.gedung
				LEFT JOIN adis_via_phone v ON v.kodeMahasiswa = f.kode
			WHERE f.stsApplyPaidConfirm = 1 $wrJalur $andQuery
			ORDER BY u.createTime DESC";
			$sql = $this->db2->query($sql)->result();
			
			$andsQuery = '';
			if ($prodi != 'all'){
				$andsQuery = "AND substr(bukaSmb, -4) = '$prodi'";
			}
			$sql1 = $this->db2->query("SELECT COUNT(*) as total 
				FROM adis_smb_form f 
				INNER JOIN adis_smb_usr u ON u.kode = f.kode 
				WHERE stsApplyPaidConfirm = 1 
			$wrJalur  $andQuery")->row();
			
			$this->smarty->assign('sql',$sql);	
			$this->smarty->assign('total',$sql1);	
			$this->smarty->display('smb/tblEvent.html');
		}else{
			$site = "Event";			
			$this->smarty->assign('site',$site);
			$this->smarty->assign('lokasi',"Daftar Jadwal Seleksi Peserta");	
			$this->smarty->display('index.html');
		}
	}
	
	function jadwal(){		
		$this->load->model('mportal');
		
		$opt = $this->uri->segment(3);
		$jenjang = $this->uri->segment(4);
		$nomor = $this->uri->segment(5);
		$db = $this->db2->query("SELECT kode FROM adis_smb_form WHERE nomor = '$nomor'")->row();
		$kode = $db->kode;
		$this->mportal->mCmb($kode);
		$this->mportal->mSelectJadwal($kode);
		
		$site = ($jenjang == 1)? "jadwal" : "jadwalPasca";
			
		$this->smarty->assign("kode", $kode);
		$this->smarty->assign("site", $site);
		$this->smarty->assign('lokasi',"Hasil Seleksi Peserta Ujian");		
		$this->smarty->display("index.html");
	}
	
	function generateJadwalPdf($kode = ''){
		$this->load->library('mlpdf');
		$pdf = $this->mlpdf->load();

		$sql = "SELECT u.nama as namaCmb, f.kode, f.nomor, f.bukaSmb, f.`event`, s.nama as progdi, j.nama as jalurCmb,
				e.kode, e.nama as namaEvent, e.periode, DAY(e.tanggal) as days, u.foto,
				e.tanggal, DAYNAME(e.tanggal) as hari, MONTHNAME(e.tanggal) as bulan, YEAR(e.tanggal) as tahun,  
				e.jalur,e.jamMasuk, e.ruang, 
				e.petugas, e.pewawancara, e.jamKeluar, e.statusJadwal, usr.jalur_penerimaan,
				e.totalPeserta, e.pewawancara2, e.prioritas, r.kursiTes, r.nama as namaRuang, 
				g.nama as namaGedung, g.alamat, g.fax, w.nama as kabKota, g.kodePos, wa.nama as prop, g.tel
				FROM adis_smb_form f
				LEFT JOIN adis_smb_usr usr ON f.kode = usr.kode 
				LEFT JOIN adis_event_smb e ON f.`event`=e.kode 
				LEFT JOIN adis_smb_usr_pribadi u ON f.kode=u.kode
				LEFT JOIN adis_jalur_smb j ON j.kode = e.jalur
				LEFT JOIN adis_prodi s ON RIGHT(f.bukaSmb, 4) = s.kode
				LEFT JOIN adis_ruang r ON r.kode = e.ruang
				LEFT JOIN adis_gedung g ON g.kode = r.gedung
				LEFT JOIN adis_wil w ON w.kode = g.kabKota
				LEFT JOIN adis_wil wa ON wa.kode = g.prop
				WHERE f.kode = '$kode'";
		$sql = $this->db2->query($sql)->row();
		$this->smarty->assign('jadwal', $sql);
			
		$spdf = new mPDF('', 'A4', 0, '', 12.7, 12.7, 40, 20, 10, 2, 'P');
		$spdf->ignore_invalid_utf8 = true;
		$spdf->allow_charset_conversion = true;     // which is already true by default
		$spdf->charset_in = 'iso-8859-2';  // set content encoding to iso
		$spdf->SetDisplayMode('fullpage');
			
		$htmlheader = $this->smarty->fetch('portal/jadwal/dokumen_header.html');
		$htmlcontent = $this->smarty->fetch('portal/jadwal/dokumen_konten_pdf.html');
		
		$spdf->SetHTMLHeader($htmlheader);
		$spdf->SetHTMLFooter('
			<div style="font-family:arial; font-size:10px; text-align:right; font-weight:bold;">
				Halaman {PAGENO} dari {nbpg}
			</div>
		');		
		
		// $spdf->SetProtection(array());				
		$spdf->WriteHTML($htmlcontent); // write the HTML into the PDF
		//$spdf->Output('repositories/Dokumen_LS/LS_PDF/'.$filename.'.pdf', 'F'); // save to file because we can
		$spdf->Output('assets/jadwal/'.$sql->nomor.'.pdf', 'D'); // view file
		
	}
	
	function smbSeleksi(){
		$uri = $this->uri->segment(3);
		$jalur = $this->input->post('jalur');
		$periode = $this->input->post('periode');
		$gelombang = $this->input->post('gelombang');
		$prodi = $this->input->post('prodi');
		
		$andQuery = "";
		
		if ($prodi != 'all'){
			$andQuery = "AND substr(f.bukaSmb, -4) = '$prodi'";
		}
		
		$this->madmisi->mSelectPeriode();
		
		if ($jalur != '' && $uri == "table"){
			
			if ($jalur == 'all'){
				$wrJalur = "AND SUBSTR(f.bukaSmb, 1, 8) = '$periode'";
			}else{
				// $periode = implode(".",array($periode,$jalur));
				$wrJalur = "AND SUBSTR(f.bukaSmb, 1, 8) = '$periode'";				
				$wrJalur .= "AND u.jalur_penerimaan = '$jalur'";				
			}
			
			if($gelombang != 'all'){
				$wrJalur .=  " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
			}
			
			$sql ="SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, up.nama as nama_cm, up.statusSaudara, p.nama as progdi, j.nama as n_jalur, f.`event` as event, up.adaSaudara, up.confirmSaudara,
			f.stsEventInterviewPresent as hadirWwc, f.stsEventUsmPresent hadirUsm, 
			f.resultUsm as hasilUsm, f.resultInterview as hasilWwc, f.resultPept, f.stsResultGrade as hasilAkhir,
			f.stsResultPass as lulusTidak, f.stsResultRecommended as recomended, f.stsResultKet as ket, f.stsResultConfirm as konfirm, 
			e.nama as eventName, e.jamMasuk, e.jamKeluar, e.petugas, e.pewawancara,
			DAYNAME(e.tanggal) as hari, e.tanggal as tglEvent, r.nama as ruang, g.nama as namaGedung, g.alamat, g.kabKota, g.prop, g.kodePos,
			g.tel, g.fax, e.nama as n_periode, j.jenjang, u.jalur_penerimaan
			FROM adis_smb_form f 
				INNER JOIN adis_smb_usr u ON u.kode = f.kode
				INNER JOIN adis_periode_master c ON LEFT(f.bukaSmb,8) = c.kode
				INNER JOIN adis_prodi p ON RIGHT(f.bukaSmb, 4) = p.kode 
				INNER JOIN adis_jalur_smb j ON SUBSTR(f.bukaSmb, 12, 2) = j.kode
				INNER JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
				LEFT JOIN adis_event_smb e ON e.kode = f.`event`
				LEFT JOIN adis_personal o ON o.kode = e.petugas
				LEFT JOIN adis_personal w ON w.kode = e.pewawancara					
				LEFT JOIN adis_ruang r ON r.kode = e.ruang
				LEFT JOIN adis_gedung g ON g.kode = r.gedung
			WHERE f.stsApplyPaidConfirm = 1 $wrJalur $andQuery
			ORDER BY u.createTime DESC";			
			$sql = $this->db2->query($sql)->result();	
			$this->smarty->assign('sql',$sql);
			
			$sql1 = $this->db2->query("SELECT COUNT(*) as total 
				FROM adis_smb_form f 
				INNER JOIN adis_smb_usr u ON u.kode = f.kode
				WHERE stsApplyPaidConfirm = 1 
				$wrJalur $andQuery")->row();
			$this->smarty->assign('total',$sql1);
			
			$this->smarty->display('smb/tblSeleksi.html');
			
		}else{
			$site = "Seleksi";			
			$this->smarty->assign('site',$site);
			$this->smarty->assign('lokasi',"Hasil Seleksi Ujian");
			$this->smarty->display('index.html');
		}
	}
	
	function smbSeleksiForm(){
		$opt = $this->input->post("opt");
		$val = $this->input->post("val");
		$uri = $this->uri->segment(3);
		
		$sql = "SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, 
                        f.stsEventInterviewPresent as hadirWwc, f.stsEventUsmPresent hadirUsm, 
                        f.resultUsm as hasilUsm, f.resultInterview as hasilWwc, f.resultPept, f.stsResultGrade as hasilAkhir,
                        f.stsResultPass as lulusTidak, f.stsResultRecommended as recomended, f.stsResultKet as ket, f.stsResultConfirm as konfirm,
                        j.kode as kode_jalur, sks_acc, e.lulus_acc, f.earlyBird, k.alumni_s1, k.keluargacivitas, 
                        k.keluargadlb, k.kode_potongan
                        FROM adis_smb_form f 
                        INNER JOIN adis_smb_usr u ON u.kode = f.kode                             
                        LEFT JOIN adis_jalur_smb j ON SUBSTR(f.bukaSmb, 12, 2) = j.kode
                        LEFT JOIN adis_smb_usr_keu k ON k.smbUsr = f.kode
                        LEFT JOIN adis_smb_usr_edu e ON e.smbUsr = f.kode 
                        WHERE f.nomor = ".$val."
                        ORDER BY u.createTime DESC";
		$sql = $this->db2->query($sql)->row();
		
		$this->load->model('mmaster');
		$mPotongan = $this->mmaster->mPotongan('select', '', 'result');
		$this->smarty->assign('mPotongan', $mPotongan);
		
		
		$this->smarty->assign('hadirUjian', $sql->hadirUsm);
		$this->smarty->assign('hadirWwc', $sql->hadirWwc);
		$this->smarty->assign('hasilAkhir', $sql->hasilAkhir);
		$this->smarty->assign('recomended', $sql->recomended);	
		$this->smarty->assign('lulusTidak', $sql->lulusTidak);	
		$this->smarty->assign('lulusAcc', $sql->lulus_acc);	
		$this->smarty->assign('earlyBird', $sql->earlyBird);
		$this->smarty->assign('alumni', $sql->alumni_s1);
		$this->smarty->assign('kelcivitas', $sql->keluargacivitas);
		$this->smarty->assign('keldlb', $sql->keluargadlb);
		$this->smarty->assign('potongan', $sql->kode_potongan);
		
		$this->smarty->assign('opt', $opt);
		$this->smarty->assign('hasil', $sql);
		$this->smarty->assign('val', $val);
		$this->smarty->display("smb/formPenilaian.html");
	}
	
	function konfirmHasilSeleksi(){
		$kode = $this->input->post("kode");
		$uri = $this->uri->segment(3);
                
		$jalur = $this->input->post('kode_jalur');
		
		$isLulus = $this->input->post('luYa');
		
		if ($kode != "" && $uri == "save"){
			$this->db2->where('kode', $kode);
			$this->db2->update('adis_smb_form', 
				array('stsEventUsmPresent'=>$this->input->post('tpaAda'),
						'stsEventInterviewPresent'=>$this->input->post('wwAda'),
						'stsResultConfirm'=>1,
						//'resultPept'=>$this->input->post('pept'),
						'resultUsm'=>$this->input->post('tpa'),
						'resultInterview'=>$this->input->post('wawancara'),
						'stsResultRecommended'=>$this->input->post('recoYa'),
						'stsResultPass'=>$this->input->post('luYa'),
						'stsResultGrade'=>$this->input->post('grade'),
						'stsResultKet'=>$this->input->post('ket'),
						'earlyBird'=>$this->input->post('earlybird')
					));
			
			if($jalur == '01'){
				
				$this->db2->where('smbUsr', $kode);
				$this->db2->update('adis_smb_usr_keu', array(
						'kode_potongan'=>$this->input->post('potongan')
					));
				
			}
                        
			if($jalur == '02'){
				$this->db2->where('smbUsr', $kode);
				$this->db2->update('adis_smb_usr_keu', array('sks_acc'=>$this->input->post('sks')));
				
				$this->db2->where('smbUsr', $kode);
				$this->db2->update('adis_smb_usr_keu', array(
						'kode_potongan'=>$this->input->post('potongan')
					));
			}
			
			if($jalur == '10'){
				$this->db2->where('smbUsr', $kode);
				$this->db2->update('adis_smb_usr_keu', array(
						'kode_potongan'=>$this->input->post('potongan')
					));
			}					
			
			if($jalur == 'KP'){
				$data_edu = $this->db2->query("SELECT smbUsr FROM adis_smb_usr_edu WHERE smbUsr = '$kode'")->num_rows();
				if($data_edu > 0){
					$this->db2->where('smbUsr', $kode);
					$this->db2->update('adis_smb_usr_edu', 
							array(
								'lulus_acc'=>$this->input->post('jenjang')
							));
				}else{
					$this->db2->insert('adis_smb_usr_edu', array(						
							"kode"=> uniqid(),
							"smbUsr"=> $kode,
							'lulus_acc'=>$this->input->post('jenjang')
					));
				}
				
				$this->db2->where('smbUsr', $kode);
				$this->db2->update('adis_smb_usr_keu', array(
						'kode_potongan'=>$this->input->post('potongan')
					));
			}
			
			$idActivity = "13.5";
			$this->activity($kode, $idActivity, $kode);
					
			$cmb = $this->db2->query("SELECT * FROM adis_smb_usr_pribadi WHERE kode= '$kode'")->row_array();
			
			// $katalulus = ($isLulus == 1 ) ? "Diterima : Silahkan melalukan daftar ulang" : "Tidak Di terima : semoga sukses";
			$katalulus = "";
			if($isLulus == 1){
				$katalulus = "
					Selamat anda telah lulus 
					<br>Seleksi Ujian Saringan Masuk Universitas Bakrie
					<br>
					<br>Silahkan login kembali pada smart.bakrie.ac.id dan lakukan Pendaftaran Ulang untuk masuk tahap berikutnya";
			}else{
				$katalulus = "Mohon maaf, Anda tidak lulus Seleksi Ujian Saringan Masuk Universitas Bakrie.";
			}
			
			$konten = array( "konten" =>"
					Kepada ".$cmb['nama']."
					<br>
					<br>$katalulus 
					<br><br><br>
					<br>Mohon untuk tidak membalas email ini. Jika ada sanggahan/pertanyaan, silahkan hubungi bagian Admisi Universitas Bakrie. 
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>Universitas Bakrie, Kawasan Rasuna Epicentrum
					<br>Jl.H.R Rasuna Said Kav C-22, Kuningan Jakarta 
					<br>Jakarta 12920 Indonesia
					<br>Office Ph : (021) 526 1448 
					<br>E-mail : usm@bakrie.ac.id");
			  
			$subject = "Hasil Ujian Seleksi Admisi Universitas Bakrie - ".$cmb['nama']." ";
			$this->mregistrasi->emailNotifikasi($kode, $subject, $konten);
		}
	}
	
	function activity($email, $idActivity, $user){
		$kode = uniqid();
		$date = date("Y-m-d H:i:s");
		$activity = $this->db2->query("SELECT nama, kode FROM adis_type WHERE kode = '$idActivity'")->row();
		$this->db2->insert('adis_activity', array('kode'=>$kode,
								'id_activity'=>$activity->kode,
								'created_date'=>$date,
								'created_user'=>$user,
								'nama_activity'=>$activity->nama,
								'status_activity'=>0,
								'id_cmb'=>$email
								));
	}
	
	function smbDaftarUlang(){
	
		$this->load->model('mintegrasi');
		
		$uri = $this->uri->segment(3);
		$jalur = $this->input->post('jalur');
		$periode = $this->input->post('periode');
		$gelombang = $this->input->post('gelombang');
		$opt = $this->input->post('opt');
		$val = $this->input->post('val');
		$prodi = $this->input->post('prodi');		
		$date = date("Y-m-d H:i:s");
		
		$andQuery = "";
		$andsQuery = "";
		
		if ($prodi != 'all'){
			$andQuery = "AND substr(f.bukaSmb, -4) = '$prodi'";
		}
		
		$kodeQuery = "";
		if (is_numeric($val)){
			$kodeQuery = "nomor = '$val'";
		}else{
			$kodeQuery = "kode = '$val'";
		}
                
		$stsMahasiswa = $this->db->query("SELECT kode, stsReapplyPaidConfirm
			FROM adis_smb_form WHERE ".$kodeQuery)->row_array();

		
		// if ($this->level == '77'){
		// 	$periodes ="SELECT * FROM adis_periode WHERE erased = 0 ORDER by kode DESC";
		// 	$periodes = $this->db2->query($periodes)->result();			
				
		// 	$this->smarty->assign('periode',$periodes);	
		// }else{
		// 	$this->madmisi->mSelectPeriode();
		// }

		$this->madmisi->mSelectPeriode();

		if($uri == 'mundur' && $opt == 'mundur'){
			$ismundur = $this->input->post('ismundur');
			$this->db2->where('nomor',$val);
			$this->db2->update('adis_smb_form', array(
						'updateUser'=>$this->auth['name'],
						'updateTime'=>$date,
						'stsMundurBeforeReapply'=>$ismundur));

			$html = "";
			if($ismundur == '1'){
				$html ='<span class="btn btn-sm btn-warning">MUNDUR</span>
					<a class="btn tolak btn-sm btn-success" href="javascript:;"  value="'.$val.'" id="tolak" name="tolak" title="Batal Mundur" onclick="mundurAfterPay(\''.$val.'\', \'0\')"><i class="icon-share-alt"></i></a>';
			}elseif($ismundur == '0'){
				$html = '<a class="tolak btn-sm btn red" href="#editing" data-toggle="modal" value="'.$val.'" id="tolak" name="tolak" title="Tolak Pembyaran"><i class="icon-remove"></i></a>
				<a class="tolak btn-sm btn yellow" href="javascript:;" value="'.$val.'" id="mundur" name="mundur" title="Mundur" onclick="mundurAfterPay(\''.$val.'\', \'1\')"><i class="icon-ban-circle"></i></a>
				<a class="confirm btn-sm  btn dark" value="'.$val.'" href="#"  id="confirm" ><i class="icon-play" title="Setujui"></i></a>';
			}
			
			$this->smarty->display('string:'.$html);
			exit;


		}else if ($uri == "confirm" && $opt == "confirm"){                    
                    
			if ($stsMahasiswa['stsReapplyPaidConfirm'] == '1'){
				echo "<script>alert('Data Mahasiswa sudah dikonfirmasi');</script>";
				redirect('cmb','refresh');
			}else{
			
				$this->db2->where('nomor',$val);
				$this->db2->update('adis_smb_form', array(
										'updateUser'=>$this->auth['name'],
										'updateTime'=>$date,
										'stsReapplyPaidConfirm'=>1));

				$kodeSQL = "SELECT kode, SUBSTR(bukaSmb,12, 2) as jalur, nomor FROM adis_smb_form WHERE nomor = $val";
				$kodeSQL = $this->db->query($kodeSQL)->row();	
				$kode = $kodeSQL->kode;
				$jalur = $kodeSQL->jalur;
							
				/**** Integrasi Generate Tagihan pembayaran daftar ulang ke ASIK ****/
				if ($jalur == 'KP'){
					// $stsTagihan = $this->mintegrasi->generateTagParalel($kode);
				}else{
					// $stsTagihan = $this->mintegrasi->generate_tagihan($kode, $jalur);   
					$stsTagihan = '201';                         
				}
				
				if ($stsTagihan != '419'){
							
					if ($jalur == 'KP'){					
					/**** Integrasi validasi pembayaran daftar ulang ke ASIK ****/
						// $stsVal = $this->mintegrasi->validasi_pembayaran($kode, $jalur);
					}else{
						$stsVal = '201';						
					}
					/** end */
					
					if ($stsVal != '419'){
						$this->generateNIM($kode);
						// $this->zimbraAccount('ca', $kode);

						if ($jalur == 'KP'){	
						/**** Integrasi data pembayaran daftar ulang ke ASIK ****/
							// $this->mintegrasi->update_nim($kode);
						/** end */
						}else{
							$stsVal = '201';						
						}
						
						/**** Integrasi data Mahasiswa Lulus ke ASIK ****/
						// $this->mintegrasi->upload_mahasiswa($kode);
						/** end */
						
						$html ='<div id="accordion1" class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
											<h4 class="panel-title">CONFIRMED</h4>
											</div>
										</div>
									</div>';
						
						$this->smarty->display('string:'.$html);
						
						$konten = array( 'konten' =>"
							<br>
							<br>
							<p>Bukti bayar Daftar Ulang anda telah diterima. Silahkan melanjutkan proses selanjutnya pada aplikasi SMART.
							<br>
							<br>
							<br>
							<br>
							<br>Mohon untuk tidak membalas email ini. Jika ada sanggahan/pertanyaan, silahkan hubungi bagian Admisi Universitas Bakrie
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>Universitas Bakrie, Kawasan Rasuna Epicentrum
							<br>Jl.H.R Rasuna Said Kav C-22, Kuningan Jakarta 
							<br>Jakarta 12920 Indonesia
							<br>Office Ph : (021) 526 1448 
							<br>E-mail : usm@bakrie.ac.id
						  ");
						  
						$subject = "Konfirmasi Bukti Bayar Daftar Ulang Diterima ";
						$this->mregistrasi->emailNotifikasi($kode, $subject, $konten);
						
						
						echo "<script>alert('Data berhasil disimpan!');</script>";
					}else{
						$this->db2->where('nomor',$val);
						$this->db2->update('adis_smb_form', array(
									'updateUser'=>$this->auth['name'],
									'updateTime'=>$date,
									'stsReapplyPaidConfirm'=>0));
						echo "<script>alert('Integrasi Ke ASIK Error, Hubungi Admin!');</script>";
					}
				}else{
					$this->db2->where('nomor',$val);
					$this->db2->update('adis_smb_form', array(
								'updateUser'=>$this->auth['name'],
								'updateTime'=>$date,
								'stsReapplyPaidConfirm'=>0));
					echo "<script>alert('Integrasi Ke ASIK Error, Hubungi Admin!');</script>";
				}
            }
                        
//                        echo "<javascript>alert('Data Pembayaran sudah di konfirmasi.');history.go(-1);</script>";
			
		}else if ($jalur != '' && $uri == "table"){
			
			if ($jalur == 'all'){
				$wrJalur = "AND SUBSTR(f.bukaSmb, 1, 8) = '$periode'";
			}else{
				// $periode = implode(".",array($periode,$jalur));
				$wrJalur = "AND SUBSTR(f.bukaSmb, 1, 8) = '$periode'";				
				$wrJalur .= "AND u.jalur_penerimaan = '$jalur'";					
			}
			
			if($gelombang != 'all'){
				$wrJalur .=  " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
			}
			
			$sql ="SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, up.nama as nama_cm , p.nama as progdi, j.nama as n_jalur,
			f.stsReapplyPaid, t.nama as akunBank, f.reapplyBankTransferAmount, 
			DAYNAME(f.reapplyBankTransferTime) as day,f.reapplyBankTransferTime, 
			DAY(f.reapplyBankTransferTime) as tgl,MONTHNAME(f.reapplyBankTransferTime) as bln,YEAR(f.reapplyBankTransferTime) as thn,
			t2.nama as transferTipe, f.reapplyBankTransferValidCode,
			f.reapplySenderBankAccountName, f.reapplySenderBankAccountNumber, f.stsReapplyPaidConfirm, f.stsMundurBeforeReapply,		
			k.buktiBayarDaftarUlang, k.noRekPengirimDaftarUlang, k.namaRekPengirimDaftarUlang, k.totalBiayaDaftarUlang,
			k.noAtmCardPendaftaran, k.noAtmCardDaftarulang, f.earlyBird, k.data_angsuran, u.jalur_penerimaan
				FROM adis_smb_form f 
				INNER JOIN adis_smb_usr u ON u.kode = f.kode
				INNER JOIN adis_periode_master c ON LEFT(f.bukaSmb,8) = c.kode
				INNER JOIN adis_prodi p ON RIGHT(f.bukaSmb, 4) = p.kode 
				INNER JOIN adis_jalur_smb j ON SUBSTR(f.bukaSmb, 12, 2) = j.kode
				INNER JOIN adis_smb_usr_keu k ON k.smbUsr = f.kode
				LEFT JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
				LEFT JOIN adis_type t ON t.kode = f.reapplyBankAccountType 
				LEFT JOIN adis_type t2 ON t2.kode = f.reapplyBankTransferType 
				WHERE f.stsResultConfirm = 1 $wrJalur $andQuery
				ORDER BY up.nama ASC";
			$sql = $this->db2->query($sql)->result();
			$this->smarty->assign('sql',$sql);	
			
			$andsQuery = '';
			if ($prodi != 'all'){
				$andsQuery = "AND substr(bukaSmb, -4) = '$prodi'";
			}
			$sql1 = $this->db2->query("SELECT COUNT(*) as total FROM adis_smb_form f 
				INNER JOIN adis_smb_usr u ON u.kode = f.kode
				WHERE stsResultConfirm = 1 $wrJalur $andsQuery ")->row();
			$this->smarty->assign('total',$sql1);
			
			$sql2 = $this->db2->query("SELECT COUNT(*) as total FROM adis_smb_form f 
				INNER JOIN adis_smb_usr u ON u.kode = f.kode
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1) $wrJalur $andsQuery ")->row();
			$this->smarty->assign('totalDone',$sql2);
			
			$sql3 = $this->db2->query("SELECT COUNT(*) as total FROM adis_smb_form f 
				INNER JOIN adis_smb_usr u ON u.kode = f.kode
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 0) $wrJalur $andsQuery ")->row();
			$this->smarty->assign('totalNone',$sql3);
			
			$this->smarty->display('smb/tblDaftarUlang.html');
		}else{
			$site = "DaftarUlang";			
			$this->smarty->assign('site',$site);
			$this->smarty->assign('lokasi',"Daftar Peserta Daftar Ulang");	
			$this->smarty->display('index.html');
		}
	}
	
	function buktiPembayaran(){
		setlocale (LC_TIME, 'id_ID');
		$this->load->model('mportal');		
		$nomor = $this->uri->segment(3);
		
		$mhs = $this->db->query("SELECT LEFT(bukaSmb,8) as periode, kode, stsReapplyPaid, stsReapplyPaidConfirm, 
				SUBSTR(bukaSmb,12, 2) as jalur, SUBSTR(bukaSmb,6,1 ) as jenjang
				FROM adis_smb_form WHERE nomor = '$nomor'")->row_array();
		$this->msmb->mProfil($mhs['kode'], "");		
		
		$this->mportal->selectType();
		$this->smarty->assign('jalur', $mhs['jalur']);
		$this->smarty->assign('jenjang', $mhs['jenjang']);
		$this->smarty->assign('periode', $mhs['periode']);
		$this->mportal->mPayment($mhs['kode'], $mhs['jenjang'],  $mhs['periode'] , $promo = null, true);
                		
		$this->smarty->assign('site','DaftarUlangDetail');
		$this->smarty->assign('lokasi',"Detail Transaksi Daftar Ulang");	
		$this->smarty->display('index.html');		
	}
	
	function detilTagihan($paid = false, $promobyAdmin = 0, $prodi = ''){
		$this->load->model('mportal');		
		$post = $this->input->post();		
		$periode = $post['periode'];
		$jenjang = $post['jenjang'];
		$kode_promo = $post['kode_promo'];
		$jalur = (isset($post['jalur']) ? $post['jalur'] : null);
		
		
		$periodeQry = "SELECT tanggalMulai, tanggalSelesai FROM adis_periode_master A WHERE kode = '$periode';";
		$periodeQry = $this->db2->query($periodeQry)->row_array();
		
		if($post['option'] == 'metodBayar'){	
		
			$biaya = $this->mportal->biayaUtama($periode, $post['kodePaidType'], $jenjang, $jalur);
			$promo = null;		
					
			if(!in_array($prodi, $this->prodiTidakPromo)){
				$promo = $this->mportal->mDetectPromoNow($periode, $post['jalur'], $kode_promo, $paid, $promobyAdmin);
			}	
			// print_r($promo);
			
			$biayaSaled = $this->mportal->biayaDiskon($biaya, $promo);
			$listBiaya = array();
			
			$first_due_date = $promo['tanggal_akhir'];
			
			$timeAngsur = date("Y-m-d",strtotime($periodeQry['tanggalMulai']." +9 days"));
			
			$semester = ($jenjang == 2 ? $biaya['jumlahSemester'] : 1);
			$x = -1;
			for($i = 0; $i < $post['metode']; $i++){
				if($i == 0){
					$listBiaya[$i]['tagihan'] = (($biayaSaled['biayaSemester1']-$biayaSaled['BPP'])/$post['metode'])+$biayaSaled['BPP'];
				}else{
					$listBiaya[$i]['tagihan'] = ($biayaSaled['biayaSemester1']-$biayaSaled['BPP'])/$post['metode'];
				}
				## tanggal metode lama, sedang tidak digunakan
				/*
				if ($post['metode'] == 2 && $i >0){
					$listBiaya[$i]['tglTempo'] = date("Y-m-d", strtotime($timeAngsur." 3 month"));
				}else{
					$listBiaya[$i]['tglTempo'] = date("Y-m-d", strtotime($timeAngsur." +$i month"));
				}
				*/			
				
				if ($post['metode'] == 2 && $i >0){
					$listBiaya[$i]['tglTempo'] = date("Y-m-d", strtotime($timeAngsur));
				}else{
					$listBiaya[$i]['tglTempo'] = date("Y-m-d", strtotime($timeAngsur." +$x month"));
				}
				$x++;
			}
			
			
			$biaya_du_kp = 0;
			if($jalur == 'KP'){
				
				$pembeda = $this->db->query("SELECT * FROM tbl_pilihan_potongan_biaya 
					WHERE kode_pembayaran = '".$biaya['kode']."' 
						AND kode_potongan = '".$promo['kode_potongan']."' 
						AND kode_periode = '".$promo['kode_periode']."' ")->row_array();		
						
				if($pembeda['biayaAngsuran'] != ''){
					$perbulanfee = $pembeda['biayaAngsuran'];
				}else{
					$biayaPembeda = json_decode($pembeda['biayaPembedaBeasiswa']);
					
					$biaya_du_kp = json_decode($biaya['pilihanBiayaUM']);
					
					$totalkp = $biayaSaled['total1'] - $biayaPembeda[0];
					$perbulanfee = $totalkp / ($biaya['tempoAngsuran']-1);
				
				}
				
				for($i=0;$i <= 5; $i++){
					$listBiaya[$i]['tagihan'] = $perbulanfee;
					$listBiaya[$i]['tglTempo'] = date("Y-m-d", strtotime($timeAngsur." +$i month"));
				}
			}
			
			if($jalur != 'KP'){
				if($promo != null){
					if($promo['tanggal_akhir'] != null){
						$listBiaya[0]['tglTempo'] = $first_due_date; 
					}
				}
			}
			
			$paramBiaya = array();
			$paramBiaya['metode'] = $post['metode'];
			$paramBiaya['kodePaidType'] = $post['kodePaidType'];
			$paramBiaya['promo_id'] = $promo['id'];
			$paramBiaya['promoByAdmin'] = $promo['promoByAdmin'];
			
			$tagihanpertama = ($jalur == 'KP' ? $biaya_du_kp[0] : $listBiaya[0]['tagihan']);			
			$this->smarty->assign('tagihanPertama', $tagihanpertama);
			$this->smarty->assign('listBiaya', $listBiaya);
			$this->smarty->assign('jenjang', $jenjang);
			$this->smarty->assign('dataAngsuran', base64_encode(json_encode($listBiaya)));
			$this->smarty->assign('paramBiaya', base64_encode(json_encode($paramBiaya)));
			$this->smarty->display('portal/tabelNilaiTagihan.html');
		}
		
	}
	
	function detailDaftarUlang(){
		$this->load->model('mportal');		
		$nomor = $this->uri->segment(3);
		
		$kodew = $this->db->query("SELECT LEFT(bukaSmb,8) as periode, kode, stsReapplyPaid, stsReapplyPaidConfirm, 
                                SUBSTR(bukaSmb,12, 2) as jalur, SUBSTR(bukaSmb,6,1 ) as jenjang
				FROM adis_smb_form WHERE nomor = '$nomor'")->row();
		$kode = $kodew->kode;
		
		$this->msmb->mProfil($kode, "");
		$this->mportal->selectType();
		
		$qr = $this->db2->query("SELECT nama FROM adis_smb_usr_kel WHERE smbUsr = '$kode' AND erased = 0");
		$saudara = $qr->num_rows();                
		$jenjang = $kodew->jenjang;
                
		$this->smarty->assign('saudara',$saudara);
		$this->smarty->assign('jalur',$kodew->jalur);
		
		$this->mportal->mPaidDaftarUlang($kode,$saudara, $jenjang, $kodew->periode);
		
		switch($jenjang){
			case '1':
				$site = "DaftarUlangConfirmed";	
				break;
			case '2':
				$site = "DaftarUlangConfirmedPasca";
				break;			
		}
                		
		$this->smarty->assign('site',$site);
		$this->smarty->assign('lokasi',"Detail Transaksi Daftar Ulang");	
		$this->smarty->display('index.html');
	}
	
	function insertEmailMan2018(){
		$this->load->model('mintegrasi');
		
	}
	
	function serviceGTMan2018(){
		$this->load->model('mintegrasi');
		
		$where = " AND nim IS NULL ";
		
		
		$query = $this->db->query("SELECT * FROM tbl_nim_2018 WHERE 1=1 $where LIMIT 0,30")->result_array();
		
		echo "<pre>";
		print_r($query);exit; 
		
		foreach($query as $val){
			$this->mintegrasi->upload_mahasiswa($val['kode']);
			
			// $data  = $this->db2->get_where('adis_smb_form', array('kode'=>$val['kode']))->row_array();
			// echo "<pre>";
			// print_r($data);
			// exit;
			// $this->zimbraAccount('ca', $val['kode']);
			echo $val['kode']."<br> ";
		}
		
	}
	
	function manualGenNim2018(){
		
		$where = " AND nim IS NULL ";
		$query = $this->db->query("SELECT * FROM tbl_nim_2018 WHERE 1=1 $where ")->result_array();
		
		echo "<pre>";
		print_r($query);
		exit; 
				
		foreach($query as $val){
			$this->generateNIM($val['kode']);
			echo $val['kode']."<br> ";
		}
	}

	function generateNIM($kode){
		$datas = $this->db->query("SELECT right(bukaSmb,4) as kode_prodi, nomor FROM adis_smb_form WHERE kode = '$kode';")->row();
		$val = $datas->nomor;
		
		$where_prodi = " AND RIGHT(bukaSmb,4)=@_prodi ";

		if($datas->kode_prodi == '1004'){
			$where_prodi = " AND ( RIGHT(bukaSmb,4)=@_prodi or RIGHT(bukaSmb,4)='1008') ";
		}

		$sql_p= "SET @_periode:=(SELECT LEFT(bukaSmb,8) FROM adis_smb_form WHERE nomor = '$val');";
		$sql  = "set @_bukaSmb:=(SELECT bukaSmb FROM adis_smb_form WHERE nomor = '$val');";
		$sql2 = "set @_tahun:=left(@_bukasmb,4);";
		$sql3 = "set @_tahun_yy:=right(@_tahun,2);";
		$sql4 = "set @_jenjang:=right(left(@_bukasmb,6),1);";
		$sql5 = "set @_periode:=left(@_bukasmb,8);";
		$sql6 = "set @_sesi:=right(left(@_bukasmb,8),1);";
		// $sql7 = "set @_prodi:=right(@_bukasmb,4); ";			
		$sql7 = "set @_prodi:=if(right(@_bukasmb,4)='1008','1004', right(@_bukasmb,4));";			
		$sql8 = "set @_nimOrder:=(select max(nimOrder) from adis_smb_form 
						where LEFT(bukaSmb,8)=@_periode $where_prodi );";
		// $sql9 = "set @_nim:=concat(@_jenjang,@_tahun_yy,@_sesi,right(@_prodi,2),LPAD((@_nimOrder+1),3,0));";
		$sql9 = "set @_nim:=concat(@_jenjang,@_tahun_yy,@_prodi,LPAD((@_nimOrder+1),3,0));";
		$sql10= "update adis_smb_form set nimOrder = (@_nimOrder+1) where nomor='$val';";

		$this->db2->query($sql_p);
		$this->db2->query($sql);
		$this->db2->query($sql2);
		$this->db2->query($sql3);
		$this->db2->query($sql4);
		$this->db2->query($sql5);
		$this->db2->query($sql6);
		$this->db2->query($sql7);
		$this->db2->query($sql8);
		$this->db2->query($sql9);
		$this->db2->query($sql10);

		$qry = "UPDATE adis_smb_form SET nim  = @_nim WHERE nomor='$val'";
		$this->db2->query($qry);
	}
	
	function generateNIMTest($kode){
                $val = $this->db->query("SELECT nomor FROM adis_smb_form WHERE kode = '$kode';")->row();
                $val = $val->nomor;
				// echo $val;exit;
                $sql_p= "SET @_periode:=(SELECT LEFT(bukaSmb,8) FROM adis_smb_form WHERE nomor = '$val');";
                $sql  = "set @_bukaSmb:=(SELECT bukaSmb FROM adis_smb_form WHERE nomor = '$val');";
                $sql2 = "set @_tahun:=left(@_bukasmb,4);";
                $sql3 = "set @_tahun_yy:=right(@_tahun,2);";
                $sql4 = "set @_jenjang:=right(left(@_bukasmb,6),1);";
                $sql5 = "set @_periode:=left(@_bukasmb,8);";
                $sql6 = "set @_sesi:=right(left(@_bukasmb,8),1);";
                $sql7 = "set @_prodi:=right(@_bukasmb,4); ";
                // $sql8 = "set @_nimOrder:=(select max(nimOrder) from adis_smb_form where bukaSmb=@_bukasmb);";			
                $sql8 = "set @_nimOrder:=(select max(nimOrder) from adis_smb_form 
                                where LEFT(bukaSmb,8)=@_periode AND RIGHT(bukaSmb,4)=@_prodi);";
                $sql9 = "set @_nim:=concat(@_jenjang,@_tahun_yy,@_sesi,right(@_prodi,2),LPAD((@_nimOrder+1),3,0));";
                $sql10= "update adis_smb_form set nimOrder = (@_nimOrder+1) where nomor='$val';";

                $this->db2->query($sql_p);
                $this->db2->query($sql);
                $this->db2->query($sql2);
                $this->db2->query($sql3);
                $this->db2->query($sql4);
                $this->db2->query($sql5);
                $this->db2->query($sql6);
                $this->db2->query($sql7);
                $this->db2->query($sql8);
                $this->db2->query($sql9);
                $this->db2->query($sql10);
		
                $qry = "UPDATE adis_smb_form SET nim  = @_nim WHERE nomor='$val'";
                $this->db2->query($qry);
	}
	
	function smbNim($val = "", $opt = "", $uri = ""){
	if ($this->auth['level'] != "99" && $this->auth['level'] != "1"){ redirect('/home','refresh');}
		
		if ($val == "" || $opt == "" || $uri == ""){
			$uri = $this->uri->segment(3);
			$opt = $this->input->post('opt');
			$val = $this->input->post('val');
		}
		
		$jalur = $this->input->post('jalur');
		$periode = $this->input->post('periode');
		$prodi = $this->input->post('prodi');
		
		$andQuery = "";
		
		if ($prodi != 'all'){
			$andQuery = "AND substr(f.bukaSmb, -4) = '$prodi'";
		}
		
		$this->madmisi->mSelectPeriode();
		
		if (($opt == "generate" && $uri == "confirm") || ($opt == "confirm" && $uri == "confirm")){
			
			$sql  = "set @_bukaSmb:=(SELECT bukaSmb FROM adis_smb_form WHERE nomor = '$val');";
			$sql2 = "set @_tahun:=left(@_bukasmb,4);";
			$sql3 = "set @_tahun_yy:=right(@_tahun,2);";
			$sql4 = "set @_jenjang:=right(left(@_bukasmb,6),1);";
			$sql5 = "set @_periode:=left(@_bukasmb,8);";
			$sql6 = "set @_sesi:=right(left(@_bukasmb,8),1);";
			$sql7 = "set @_prodi:=right(@_bukasmb,4); ";
			$sql8 = "set @_nimOrder:=(select max(nimOrder) from adis_smb_form where bukaSmb=@_bukasmb);";
			$sql9 = "set @_nim:=concat(@_jenjang,@_tahun_yy,@_sesi,right(@_prodi,2),LPAD((@_nimOrder+1),3,0));";
			$sql10 = "update adis_smb_form set nimOrder = (@_nimOrder+1) where nomor='$val';";
			
			$this->db2->query($sql);
			$this->db2->query($sql2);
			$this->db2->query($sql3);
			$this->db2->query($sql4);
			$this->db2->query($sql5);
			$this->db2->query($sql6);
			$this->db2->query($sql7);
			$this->db2->query($sql8);
			$this->db2->query($sql9);
			$this->db2->query($sql10);
		
			$qry = "UPDATE adis_smb_form SET nim  = @_nim WHERE nomor='$val'";
			$this->db2->query($qry);
			
			if ($opt == "generate"){
			
				$row = $this->db2->query("SELECT nim FROM adis_smb_form where nomor = '$val';")->row();
				$this->smarty->assign('row',$row);
				
				$html ='<div id="accordion1" class="panel-group">
						  <div class="panel panel-default">
							<div class="panel-heading">
							  <h4 class="panel-title">{$row->nim}</h4>
							</div>
						  </div>
						</div>';
				
				$this->smarty->display('string:'.$html);	
			}
			
		}else if ($jalur != '' && $uri == "table"){
			
			if ($jalur == 'all'){
				$wrJalur = "AND SUBSTR(f.bukaSmb, 1, 8) = '$periode'";
			}else{
				// $periode = implode(".",array($periode,$jalur));
				$wrJalur = "AND SUBSTR(f.bukaSmb, 1, 8) = '$periode'";				
				$wrJalur .= "AND SUBSTR(f.bukaSmb, 12, 2) = '$jalur'";				
			}
			
			$sql ="SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, up.nama as nama_cm , p.nama as progdi, j.nama as n_jalur,
			f.stsResultPass, f.stsReapplyPaidConfirm, f.nimOrder, f.nim, f.stsMundurAfterReapply
			FROM adis_smb_form f 
			INNER JOIN adis_smb_usr u ON u.kode = f.kode
			INNER JOIN adis_periode_master c ON LEFT(f.bukaSmb,8) = c.kode
			INNER JOIN adis_prodi p ON RIGHT(f.bukaSmb, 4) = p.kode 
			INNER JOIN adis_jalur_smb j ON SUBSTR(f.bukaSmb, 12, 2) = j.kode
			LEFT JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
			WHERE f.stsReapplyPaidConfirm = 1 AND f.stsResultPass = 1 $wrJalur $andQuery
			ORDER BY u.createTime DESC";
			$sql = $this->db2->query($sql)->result();
			$this->smarty->assign('sql',$sql);	
			
			$sql1 = $this->db2->query("SELECT COUNT(*) as total FROM adis_smb_form f WHERE stsReapplyPaidConfirm = 1 AND stsResultPass = 1 AND stsMundurAfterReapply = 0 $wrJalur $andQuery")->row();
			$this->smarty->assign('total',$sql1);	

			$sqlundur = $this->db2->query("SELECT COUNT(*) as total FROM adis_smb_form f WHERE stsReapplyPaidConfirm = 1 AND stsResultPass = 1 AND stsMundurAfterReapply = 1 $wrJalur $andQuery")->row();
			$this->smarty->assign('total_undur',$sqlundur);	
			
			$this->smarty->display('smb/tblNim.html');
		}else{
			$site = "Nim";			
			$this->smarty->assign('site',$site);
			$this->smarty->assign('lokasi',"Generate NIM Mahasiswa");	
			$this->smarty->display('index.html');
		}
	}
		
	function admisiBukaSeleksi(){
		$this->db2->query("SET lc_time_names = 'id_ID'");
		$sql = "SELECT b.erased as status, b.kode as id_buka, b.nama as nama_buka, b.periode as periode_buka, 
		DAYNAME(b.tanggalBuka) as hariBuka, DAYNAME(b.tanggalTutup) as hariTutup, b.tanggalBuka, b.tanggalTutup,
		p.nama as nama_periode, j.nama as nama_jalur, pr.nama as nama_prodi,
		FROM adis_buka_smb b
		INNER JOIN adis_periode p ON b.periode = p.kode
		INNER JOIN adis_jalur_smb j ON b.jalur = j.kode
		INNER JOIN adis_prodi pr ON b.prodi = pr.kode;";
		$sql = $this->db2->query($sql)->result();
		$site = "Jadwal";			
		$this->smarty->assign('site',$site);	
		$this->smarty->assign('sql',$sql);	
		$this->smarty->display('index.html');	
	}
	
	function admisiPeriode(){
		$sql ="SELECT * FROM adis_periode";
		$sql = $this->db2->query($sql)->result();		
		$site = "Event";			
		$this->smarty->assign('site',$site);	
		$this->smarty->assign('sql',$sql);	
		$this->smarty->display('index.html');	
	}
	
	function admisiJadwalSeleksi(){
		$uri = $this->uri->segment(3);
		$periode = $this->input->post('periode');
		
		if ($uri == "table" && $periode != ""){
			$this->db2->query("SET lc_time_names = 'id_ID'");
			$sql = "SELECT b.erased as status, b.kode as id_event, b.nama as nama_event, 
			DAYNAME(b.tanggal) as hariEvent, b.tanggal, b.jamMasuk, b.jamKeluar, p.jenjangType as jenjang, r.gedung as gedung_event, r.kode as kode_kelas,
			p.nama as nama_periode, p.tahun as tahun, r.nama as nama_ruang, j.nama as nama_jalur, o.nama as nama_petugas, c.nama as nama_pww
			FROM adis_event_smb b
			INNER JOIN adis_periode p ON b.periode = p.kode
			INNER JOIN adis_ruang r ON b.ruang = r.kode
			INNER JOIN adis_jalur_smb j ON b.jalur = j.kode
			INNER JOIN adis_personal o ON b.petugas =o.kode
			INNER JOIN adis_personal c ON b.pewawancara =c.kode
			WHERE b.periode = '$periode' ORDER BY id_event";
			$sql = $this->db2->query($sql)->result();
			
			$this->smarty->assign('sql',$sql);	
			$this->smarty->display('admisi/tblJadwal.html');	
		}else{
			$sql ="SELECT * FROM adis_periode";
			$sql = $this->db2->query($sql)->result();		
			$site = "Event";			
			$this->smarty->assign('site',$site);	
			$this->smarty->assign('sql',$sql);	
			$this->smarty->display('index.html');
		}
	}
	
	function mundurDiri(){
		$kode = $this->uri->segment(3);
		$opt = $this->uri->segment(4);
		$uri = $this->uri->segment(5);
		//$oper = $this->uri->segment(6);
		
		$opt2 = $this->input->post("opt");
		
				
		if ($kode != "" && $opt == "mundur"){	
			
			
			
		
			if ($uri != 'save'){
			
				if ($uri){
					$this->load->model('msmb');
					$this->msmb->mProfil($kode,'asu');
				}
				
				if (!$uri){
					$this->load->model('mportal');
					$this->mportal->mCmb($kode);				
				}
				
				$site = "Mundur";			
				$this->smarty->assign('lokasi',"Form Pengunduran Diri");	
				$this->smarty->assign('site',$site);	
				$this->smarty->display('index.html');
				
			}else{
				if ($opt2 == "save"){
					$this->msmb->mMundur($kode);
				}
				$this->smbNim();
			}
		}
		
	}
        
        function ubahJadwal(){
		$kode = $this->input->post('val');
		$opt = $this->input->post('opt');
		$uri = $this->uri->segment(3);
                
                if ($opt == 'ubah' && $kode){
                    $cmb = $this->db2->query("SELECT * FROM adis_smb_form WHERE kode = '$kode';")->row_array();
                    $periode = substr($cmb['bukaSmb'], 0, -10);
					// echo $periode;exit;
                    $jadwal = $this->db2->query("SELECT * FROM adis_event_smb WHERE periode LIKE '$periode%' ")->result();
                    $this->smarty->assign('cmb', $cmb);
                    $this->smarty->assign('jadwal', $jadwal);
                    $this->smarty->display('smb/formUbahJadwal.html');
                }else if ($opt == 'simpanJdl' && $uri == 'simpan' && $kode){  
                    $event = $this->input->post('jadwal');    
                    $eventOld = $this->input->post('tgl');  
                    
                    $jdwl = $this->db2->query("SELECT * FROM adis_event_smb WHERE kode = '$event'")->row_array();
                    $jdwlOld = $this->db2->query("SELECT * FROM adis_event_smb WHERE kode = '$eventOld'")->row_array();
                    
                    $this->db2->where('kode',$kode);
                    $this->db2->update("adis_smb_form", array('event'=>$event, 'ruangEvent'=>$jdwl['ruang']));
                    
                    $this->db2->where('kode',$event);
                    $this->db2->update("adis_event_smb", array('totalPeserta'=>$jdwl['totalPeserta']+1));
                    
                    $this->db2->where('kode',$eventOld);
                    $this->db2->update("adis_event_smb", array('totalPeserta'=>$jdwlOld['totalPeserta']-1));
                    
                    redirect('/smb/smbEvent','refresh');
                }
            
        }
	
	function viaPhone(){
		$kode = $this->input->post('val');
		$opt = $this->input->post('opt');
		$uri = $this->uri->segment(3);
		$uri2 = $this->uri->segment(4);
		
		if ($uri == "add" && $opt == "phone"){
			$sql = "SELECT kode, nama FROM adis_smb_usr_pribadi WHERE kode = '$kode'";
			$sql = $this->db2->query($sql)->row();
			
			$this->madmisi->mSelectPetugas();
			
			$this->smarty->assign('calon',$sql);
			$this->smarty->display('smb/formViaPhone.html');
		}else if ($uri == "detail" && $opt == "phoned"){
                    
			$sql = "SELECT  kode, nama FROM adis_smb_usr_pribadi WHERE kode = '$kode'";
			$sql = $this->db2->query($sql)->row();
			$this->madmisi->mSelectPetugas();
			
			$sql2 = "SELECT  v.id, v.tanggal, v.jamMulai, v.jamSelesai, v.no_hp1, v.no_hp2, v.`status`, p.nama as pww1, p2.nama as pww2,
                                        v.idPewawancara, idPewawancara2
					FROM adis_via_phone v
					LEFT JOIN adis_personal p ON p.kode = v.idPewawancara
					LEFT JOIN adis_personal p2 ON p2.kode = v.idPewawancara2
					WHERE kodeMahasiswa = '$kode';";
			$sql2 = $this->db2->query($sql2)->row();
						
			$this->smarty->assign('calon',$sql);
			$this->smarty->assign('phone',$sql2);
			$this->smarty->display('smb/formViaPhoned.html');
			
		}else{
                                               
			if($uri2 == "do"){
				$date = date("Y-m-d H:i:s");
                                $kode = $this->input->post("kode");
                                $unik = uniqid();
                                
                                $formCmb = $this->db2->query("SELECT * FROM adis_smb_form WHERE kode = '$kode'")->row_array();
                                $eventDay = $this->db2->query("SELECT * FROM adis_event_smb WHERE kode = '".$formCmb['event']."'")->row_array();
                                
				$this->db2->insert("adis_via_phone", array(
					"id"=>$unik,
					"createTime"=>$date,
					"createdUser"=>$this->auth['name'],
					"kodeMahasiswa"=>$kode,
					"tanggal"=>$this->input->post('tanggal'),
					"jamMulai"=>$this->input->post('jamMulai'),
					"jamSelesai"=>$this->input->post('jamSelesai'),
					"no_hp1"=>$this->input->post('noTelp'),
					"no_hp2"=>$this->input->post("noHp"),
					"idPewawancara"=>$this->input->post("pww1"),
					"idPewawancara2"=>$this->input->post("pww2"),
					"status"=>$this->input->post("status")
                                        ));
				
				$this->db2->where('kode',$kode);
				$this->db2->update("adis_smb_form", array('interviewPhone'=>1));                                
                                
                                $this->db2->where('kode',$formCmb['event']);
                                $this->db2->update("adis_event_smb", array('totalPeserta'=>$eventDay['totalPeserta']-1));
			}
                        if ($uri2 == "update"){
                            $kodePhone = $this->input->post("kodePhone");
                            
                            $this->db2->where("id", $kodePhone);
                            $this->db2->update("adis_via_phone", array(
					"createdUser"=>$this->auth['name'],
					"tanggal"=>$this->input->post('tanggal'),
					"jamMulai"=>$this->input->post('jamMulai'),
					"jamSelesai"=>$this->input->post('jamSelesai'),
					"no_hp1"=>$this->input->post('noTelp'),
					"no_hp2"=>$this->input->post("noHp"),
					"idPewawancara"=>$this->input->post("pww1"),
					"idPewawancara2"=>$this->input->post("pww2"),
					"status"=>$this->input->post("status")
                                        ));
                            
                        }
			redirect('/smb/smbEvent','refresh');
		}
	}
	
	function exportToExcel(){
		$periode = $this->uri->segment(4);
		$prodi = $this->uri->segment(5);
		$jalur = $this->uri->segment(6);
		$modul = $this->uri->segment(3);
		$gelombang = $this->uri->segment(7);
		$this->load->dbutil();
		$this->load->helper('file');
		$this->load->helper('download');
		$delimiter = ",";
		$newline = "\r\n";
		$filename = "data_peserta_smart.txt";
		
		$sql = $this->msmb->exportCalon($periode, $prodi, $jalur, $gelombang);
		
		$this->export->to_excel($sql, 'Calon_Mahasiswa_Baru_Periode_'. $periode); 	
	}

	function exportInvent(){
		$this->load->helper('file');
		$this->load->helper('download');
		$uri = $this->uri->segment(3);

		$query = "";
		if($uri == 'mm'){
			$query = $this->db->get('tbl_invent_mm');
		}else{
			$query = $this->db->get('tbl_invent');
		}
		
		$this->export->to_excel($query, 'Data Invert '.date('YmdHis')); 	
	}
	
	function exportToCsv(){
		$periode = $this->uri->segment(4);
		$prodi = $this->uri->segment(5);
		$jalur = $this->uri->segment(6);
		$modul = $this->uri->segment(3);
		$gelombang = $this->uri->segment(7);
		$this->load->dbutil();
		$this->load->helper('file');
		$this->load->helper('download');
		$delimiter = ",";
		$newline = "\r\n";
		$filename = "data_peserta_smart.txt";
		
		$sql = $this->msmb->exportCsv($periode, $prodi, $jalur, $gelombang);
		$data = $this->dbutil->csv_from_result($sql);
		force_download($filename, $data);
		
	}

	
	
	function data_pembayaranDaftar($periode = ''){
		$sql = "SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, up.nama as nama_cm , p.nama as progdi, j.nama as n_jalur, t.nama as metode, 
		if(f.stsApplyPaid = 1, 'Terbayar', 'Belum Bayar') as statusBayar,
		t2.nama as rekening, DAYNAME(f.applyBankTransferTime) as hari, f.applyBankTransferTime as tglPembayaran, 
		FORMAT(f.applyBankTransferAmount, 2) as jumlah, if(f.stsApplyPaidConfirm = 1, 'Terkoinfirm', 'Belum Dikonfirm')  as confirm, 
		f.applyBankAccountType, if (f.applyBankTransferType = '04.5', 'Voucher', 'Transfer') as STATUS, p.singkatan as progdi_inisial, up.rumahCell, up.ayahCell as 'Hp Ortu',
		k.namaRekPengirimPendaftaran, u.jalur_penerimaan
			FROM adis_smb_form f 
			LEFT JOIN adis_smb_usr u ON u.kode = f.kode
			INNER JOIN adis_periode_master c ON LEFT(f.bukaSmb,8) = c.kode
			INNER JOIN adis_prodi p ON RIGHT(f.bukaSmb, 4) = p.kode 
			INNER JOIN adis_jalur_smb j ON SUBSTR(f.bukaSmb, 12, 2) = j.kode
			LEFT JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
			INNER JOIN adis_smb_usr_keu k ON k.smbUsr = f.kode
			LEFT JOIN adis_type t ON t.kode = f.applyBankTransferType
			LEFT JOIN adis_type t2 ON t2.kode = f.applyBankAccountType 
		WHERE f.stsApplyPaid = 1  AND f.bukaSmb LIKE('$periode%')
		ORDER BY f.createTime DESC;";
		$data = $this->db2->query($sql);
		$this->export->to_excel($data, 'Pembayaran Periode '. $periode); 
		
	}
	
	function data_pembayaran_du($periode = ''){
		$sql = "SELECT B.nama as 'Nama', A.nomor as 'Nomor',
		A.kode as 'Email', B.rumahCell as 'Kontak', D.nama as 'Prodi', A.reapplyBankTransferAmount as 'Tipe Pembayaran', A.reapplyBankTransferAmount as 'Jumlah', 
		A.reapplyBankTransferTime as 'Tanggal Transfer', A.stsReapplyPaidConfirm as 'Status Approval', E.namaRekPengirimDaftarUlang as 'Nama & Nomor Rek',
		E.kode_potongan, f.nama_potongan, applyBankAccountType 
		FROM `adis_smb_form` A
		LEFT JOIN adis_smb_usr_pribadi B ON A.kode = B.kode
		LEFT JOIN adis_type C ON C.kode = A.reapplyBankTransferType
		LEFT JOIN adis_prodi D ON D.kode = RIGHT(A.bukaSmb, 4)
		LEFT JOIN adis_smb_usr_keu E ON A.kode = E.smbUsr
		left join tbl_master_potongan f on E.kode_potongan = f.kode_potongan 
		WHERE A.stsReapplyPaid = 1  AND A.bukaSmb LIKE('$periode%')
		ORDER BY A.reapplyBankTransferTime ASC;";
		$data = $this->db2->query($sql);
		$this->export->to_excel($data, 'Pemabayaran Periode '. $periode); 
		
	}
        
	function editProfil(){
		$this->load->library('encrypt');
		$uri = $this->uri->segment(4);
		$opt = $this->input->post('opt');
		$kode = $this->input->post('kode');
		$noreg = $this->input->post('noreg');
		
		if($uri == 'save' && $kode != '' && $opt == 'passW'){
			
			$newPass = $this->input->post('newPass');                
			$newPass = $this->encrypt->encode($newPass);
			$this->db->where('kode',$kode);
			$this->db->update('adis_smb_usr', array('password'=>$newPass));                

			redirect ('/smb/profil/'.$noreg, 'refresh');
		}
	}
	
	function approve_rapor(){
		$data_assign = $this->input->post('data');
		$noreg = $this->input->post('noreg');
		if(isset($data_assign)){
			if($data_assign == 7){
				$this->db2->where('smbUsr', $this->input->post('kode'));
				$this->db2->update('adis_smb_usr_edu', array(
					'edit_ke'=> NULL,
					'file_rapor_10_1'=>NULL,
					'file_rapor_10_2'=>NULL,
					'file_rapor_11_1'=>NULL,
					'file_rapor_11_2'=>NULL
					));
				
			}else{
				$this->db2->where('smbUsr', $this->input->post('kode'));
				$this->db2->update('adis_smb_usr_edu', array(
					'approval_rapor'=> $this->input->post('data')));
				
			}
		
			echo 1;
		}else{
			echo 0;
		}
		
		redirect ('/smb/profil/'.$noreg, 'refresh');
	}
	
	function change_birth_date(){
		
		$post = $this->input->post();
		if(isset($post['kode_cmb']) && isset($post['tanggal_lahir'])){
			$cekexist = $this->db->query("SELECT * FROM adis_smb_usr_pribadi WHERE kode = '".$post['kode_cmb']."' ");
			if($cekexist->num_rows > 0){
				$data = $cekexist->row_array();
				
				$new_date = date('Y-m-d', strtotime($post['tanggal_lahir']));
				
				$this->db->where('kode', $post['kode_cmb']);
				$this->db->update('adis_smb_usr_pribadi', array('tanggalLahir'=>$new_date));
			}
		}
		
		redirect ('/smb/profil/'.$post['noreg_cmb'], 'refresh');
	}
	
	function change_prodi(){
		
		$post = $this->input->post();
		if(isset($post['kode_cmb']) && isset($post['prodi'])){
			$cekexist = $this->db->query("SELECT * FROM adis_smb_form WHERE kode = '".$post['kode_cmb']."' ");
			if($cekexist->num_rows > 0){
				$data = $cekexist->row_array();
				$bukaSmb = $data['bukaSmb'];
				$oprodi = substr($bukaSmb, -4);
				$nprodi = $post['prodi'];
				$bukaSmb = substr_replace($bukaSmb, $nprodi, -4);
				
				$this->db->where('kode', $post['kode_cmb']);
				$this->db->update('adis_smb_form', array('bukaSmb'=>$bukaSmb));
			}
		}
		
		redirect ('/smb/profil/'.$post['noreg_cmb'], 'refresh');
	}
	
	function profil(){
		$this->load->model('mportal');
		
		$kode = $this->uri->segment(3);
		$val = $this->input->post('val');
		$id = $this->input->post('id');
		$opt = $this->input->post('opt');

		if ($kode == "confirm" && $val != ''){
			if ($opt == 'confirm'){$statusSau = 7;}else if ($opt == 'tolak'){$statusSau = 9;}
				$this->db2->where('kode',$val);
				$this->db2->update('adis_smb_usr_pribadi', array('confirmSaudara' => $statusSau));

				$this->mportal->mSelectSaudara($val);			
				$this->mportal->mCmb($val);
				$this->smarty->display('smb/tblKeluarga.html');
		}else {		
			
			$nilaiLast = array();
			$nilai = $this->db2->query("SELECT * FROM tbl_nilai_rapor WHERE nomor = '$kode'")->result_array();
			foreach($nilai as $val){
				$nilaiLast[$val['kode']] = $val;
			}

			
			// echo "<pre>";
			// print_r($nilaiLast);exit; 
		
			$this->smarty->assign('nilai', $nilaiLast);

			$kode = $this->db2->query("SELECT kode, stsEventConfirm FROM adis_smb_form WHERE nomor = '$kode'")->row();
			$kode = $kode->kode;
			$maba = $this->mportal->mCmb($kode);
			$jenjangMaba = $maba->jenjangType;
			
			$berkas = $this->db2->query("SELECT * FROM `tbl_master_berkas` A 
					left JOIN tbl_kelengkapan_berkas B ON A.id_berkas = B.id_berkas AND B.kode_cmb = '$kode'
					WHERE  A.jenjang in (".$jenjangMaba.",0);");
					
			$stsBerkas = $this->db2->query("select * from tbl_kelengkapan_berkas WHERE kode_cmb = '$kode' ");
			
			
			if ($stsBerkas->num_rows == 0 ){
				$berkas = $this->db2->query("SELECT A.*, '' as status FROM `tbl_master_berkas` A  where jenjang in ('".$jenjangMaba."',0) ")->result_array(); 
			}else{
				$berkas = $berkas->result_array();
			}
			
			$satusberkas = "";
			$dokumen = "";
			if($jenjangMaba ==  1){
				$satusberkas = $this->db2->query("SELECT * FROM `tbl_smb_berkas` WHERE kode = '$kode';");
				$dokumen = $this->db2->query("SELECT * FROM tbl_smb_berkas WHERE kode = '$kode' ")->row_array();
			}else if($jenjangMaba == 2){
				$satusberkas = $this->db2->query("SELECT * FROM `tbl_smb_berkas_mm` WHERE kode = '$kode';");
				$dokumen = $this->db2->query("SELECT * FROM tbl_smb_berkas_mm WHERE kode = '$kode' ")->row_array();

				$berkas_test = $this->db->query("Select fc_ijazah, fc_transkrip from smart_jadwal_test where kode='$kode'")->row_array();
				$this->smarty->assign('berkas_test', $berkas_test);

			}else if($jenjangMaba == 3){
				$satusberkas = $this->db2->query("SELECT * FROM `tbl_smb_berkas_kk` WHERE kode = '$kode';");
				$dokumen = $this->db2->query("SELECT * FROM tbl_smb_berkas_kk WHERE kode = '$kode' ")->row_array();
			}
			$this->smarty->assign('stsberkas', $satusberkas->num_rows);
			$this->smarty->assign('berkas', $berkas);
			$this->smarty->assign('dokumen', $dokumen);
			
			$jenjang_prodi = $jenjangMaba == 3 ? 1 : $jenjangMaba;
			$prodi = $this->db->query("SELECT * FROM adis_prodi where jenjang = '$jenjang_prodi' ")->result_array();
			$this->smarty->assign('prodi', $prodi);
			
			// $this->mportal->mCmb($kode);
			$this->mportal->mProfil($kode);
			$this->mportal->mSelectOrtu($kode);
			$this->mportal->mSelectPendidikan($kode);
			$this->mportal->mSelectSaudara($kode);
			$this->mportal->mPrestasi($kode);
			$this->mportal->mOrganisasi($kode);
			$site = "Profil";
			$this->smarty->assign('lokasi',"Profil Mahasiswa Baru");	
			$this->smarty->assign("site", $site);		
			$this->smarty->display("index.html");
		}
	}
	
	function berkas(){
		$kode = $this->input->post('kode');
		$nomor = $this->input->post('nomor');
		$jenjang = $this->input->post('jenjang');
		
		$tbl_smb_berkas = $jenjang == 1 ? 'tbl_smb_berkas' : ($jenjang == 3 ? 'tbl_smb_berkas_kk' : 'tbl_smb_berkas_mm');
		$berkas = $this->db2->query("SELECT * FROM tbl_master_berkas where jenjang in ('$jenjang', 0); ")->result_array();
		$this->smarty->assign('berkas', $berkas);
		
		$status_temp = array();

		// echo "<pre>";
		// print_r($berkas);exit;
		
		foreach($berkas as $k => $v){
			$dataBerkas = $this->db2->query("
					SELECT * FROM tbl_kelengkapan_berkas 
					WHERE kode_cmb = '$kode' 
					AND id_berkas = '".$v['id_berkas']."' ;")->num_rows();
				
			$stsBerkasApp = $this->input->post($v['id_berkas']);
			if ($dataBerkas > 0){
				$this->db2->where(array('kode_cmb'=>$kode, 'id_berkas'=>$v['id_berkas']));
				$this->db2->update('tbl_kelengkapan_berkas', array("status"=>$stsBerkasApp));
				
				if($stsBerkasApp != 1){
					$this->db2->where(array('kode'=>$kode));
					$this->db2->update($tbl_smb_berkas, array($v['field']=>NULL));
					$status_temp[] = 0; 
				}else{
					
					$status_temp[] = 1;
				}
			}else{
				$this->db->insert('tbl_kelengkapan_berkas', 
					array(	"kode_cmb"=>$kode, 
							"id_berkas"=>$v['id_berkas'],
							"status"=>$this->input->post($v['id_berkas'])
						)
				);
			}
		}
		
		// print_r($status_temp);exit;
		if(in_array('0', $status_temp)){
			$this->db2->where(array('kode'=>$kode));
			$this->db2->update($tbl_smb_berkas, array('is_done'=>NULL, 'is_upload'=>NULL));
		}else{
			$this->db2->where(array('kode'=>$kode));
			$this->db2->update($tbl_smb_berkas, array('is_done'=>1));
		}
		
        redirect ('/smb/profil/'.$nomor, 'refresh');
		
	}

	function formulir(){
		$this->load->model('mportal');
		
		$kode = $this->uri->segment(3);
		
		$kode = $this->db2->query("SELECT kode, stsEventConfirm FROM adis_smb_form WHERE nomor = '$kode'")->row();
		
		if ($kode->stsEventConfirm == '0') {
			// What happens when the CAPTCHA was entered incorrectly
			echo '<script>alert("Calon Mahasiswa belum melengkapi Data Diri, Silahkan Kembali!");history.go(-1);</script>';
			// die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
				 // "(reCAPTCHA said: " . $resp->error . ")");
		}else{
		
			
			$kode = $kode->kode;
			
			$this->mportal->selectType();
			$this->mportal->mProfil($kode);
			$this->mportal->mSelectPendidikan($kode);		
			$this->mportal->mOrganisasi($kode);
			$this->mportal->mPrestasi($kode);
			$this->mportal->mSelectOrtu($kode);
			$this->mportal->mSelectSaudara($kode);
			
			$site = "Formulir";			
			$this->smarty->assign('lokasi',"Form Pendaftaran Mahasiswa Baru");	
			$this->smarty->assign('site',$site);	
			$this->smarty->display('index.html');	
		
		}
	}

	function tolakBayar(){
		$uri = $this->uri->segment(3);
		$val = $this->input->post('val');
		$opt = $this->input->post('opt');
		$message = $this->input->post('pesan');

		if ($uri == 'save' && $opt == 'save' ){
			if ($val != ''){
				$this->db->where('smbUsr', $val);
				$this->db->update('adis_smb_usr_keu', array(
								'tolakDU'=>1,
								'pesanTolakDU'=>$message,
								'parameter_biaya'=>'',
								'data_angsuran'=>'',
								'metodBayarDaftarUlang'=>''
								));

				$this->db->where('kode', $val);
				$this->db->update('adis_smb_form', array('stsReapplyPaid'=>0));
				
				$this->db2->where('kode_mhs', $val);
				$this->db2->delete('tbl_tagihan_cmb');
			}
			$this->load->model('mportal');
			$cmb = $this->mportal->mCmb($val);	
			
			$konten = array( 'konten' =>"
				Kepada ".$cmb->nama_cm."
				<br>
				<p>Mohon Maaf, Bukti Bayar Daftar Ulang Belum Dapat di Verifikasi,
				<br>Pesan Keuangan : $message
				<br>
				<br>Silahkan untuk dapat mengirimkan kembali bukti bayar anda.
				<br>
				<br>
				<br>Mohon untuk tidak membalas email ini. Jika ada sanggahan/pertanyaan, silahkan hubungi bagian Admisi Universitas Bakrie
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>Universitas Bakrie, Kawasan Rasuna Epicentrum
				<br>Jl.H.R Rasuna Said Kav C-22, Kuningan Jakarta 
				<br>Jakarta 12920 Indonesia
				<br>Office Ph : (021) 526 1448 
				<br>E-mail : usm@bakrie.ac.id
			  ");
			  
			$subject = "Konfirmasi Bukti Bayar Dafar Ulang Ditolak - ".$cmb->nama_cm."";
			$this->mregistrasi->emailNotifikasi($val, $subject, $konten);

			redirect('/smb/smbDaftarUlang','refresh');

		}else if ($uri == 'tolak'){

			$qry = "SELECT f.kode, f.nomor, p.nama FROM adis_smb_form f 
					INNER JOIN adis_smb_usr_pribadi p ON f.kode = p.kode 
					WHERE nomor = '$val'";
			$qry = $this->db->query($qry)->row();

			$this->smarty->assign('mah', $qry);
			$this->smarty->display('smb/formTolakBayar.html');
		}
	}
        
        function earlyBird(){
		$uri = $this->uri->segment(3);
		$val = $this->input->post('val');
		$opt = $this->input->post('opt');
                
                if ($uri == 'save' && $opt == 'confirm' ){
                    if ($val != ''){
                        $this->db->where('kode', $val);
                        $this->db->update('adis_smb_form', array('earlyBird'=>$this->input->post('earlybird')));
                    }
                    

                    redirect('/smb/smbDaftarUlang','refresh');
                }else{
                    $qry = "SELECT f.kode, f.nomor, p.nama FROM adis_smb_form f 
                            INNER JOIN adis_smb_usr_pribadi p ON f.kode = p.kode 
                            WHERE nomor = '$val'";
                    $qry = $this->db->query($qry)->row();

                    $this->smarty->assign('mah', $qry);
                    $this->smarty->display('smb/formEarlyBird.html');                    
                }
                
            
        }
	

	function tolakBayarDaftar(){
		$uri = $this->uri->segment(3);
		$val = $this->input->post('val');
		$opt = $this->input->post('opt');

		if ($uri == 'save' && $opt == 'save' ){
			if ($val != ''){
				$this->db->where('smbUsr', $val);
				$this->db->update('adis_smb_usr_keu', array(
                                        'tolakPendaftaran'=>1,
										'metodBayarDaftarUlang'=>'',
                                        'pesanTolakP'=>$this->input->post('pesan')
                                    ));

				$this->db->where('kode', $val);
				$this->db->update('adis_smb_form', array('stsApplyPaid'=>0));
			}
			$cmb = $this->mportal->mCmb($val);
			$status = 'daftar';
			$message = $this->input->post('pesan');
					
			$konten = array( 'konten' =>"
				Kepada ".$cmb->nama_cm."
				<br>
				<p>Mohon Maaf, Bukti Bayar Pendaftaran Belum Dapat di Verifikasi,
				<br>Pesan Keuangan : $message
				<br>
				<br>Silahkan untuk dapat mengirimkan kembali bukti bayar anda.
				<br>
				<br>
				<br>Mohon untuk tidak membalas email ini. Jika ada sanggahan/pertanyaan, silahkan hubungi bagian Admisi Universitas Bakrie
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>Universitas Bakrie, Kawasan Rasuna Epicentrum
				<br>Jl.H.R Rasuna Said Kav C-22, Kuningan Jakarta 
				<br>Jakarta 12920 Indonesia
				<br>Office Ph : (021) 526 1448 
				<br>E-mail : usm@bakrie.ac.id
			  ");
			  
			$subject = "Konfirmasi Bukti Bayar Pendaftaran Ditolak - ".$cmb->nama_cm."";
			$this->mregistrasi->emailNotifikasi($val, $subject, $konten);

			redirect('/smb/smbPay','refresh');

		}else if ($uri == 'tolak'){

			$qry = "SELECT f.kode, f.nomor, p.nama FROM adis_smb_form f 
					INNER JOIN adis_smb_usr_pribadi p ON f.kode = p.kode 
					WHERE nomor = '$val'";
			$qry = $this->db->query($qry)->row();

			$this->smarty->assign('mah', $qry);
			$this->smarty->display('smb/formTolakBayarDaftar.html');
		}
	}

	function mail($email, $status, $pesan){
		
	}
        
        function userAdmisi($tahun = ''){
            $sql = "SELECT * FROM adis_smb_usr A "
                    . "LEFT JOIN adis_smb_form B LEFT A.kode = B.kode "
                    . "WHERE LEFT(B.bukaSmb, 4) = '$tahun'";
            $this->db2->query($sql)->result_array();
        }
        
        function deleteMhs(){
            $noReg = $this->input->post('id');
            $method = $this->input->post('method');
            $nim = "";
            
            $this->db->trans_begin(); 
            
            if ($noReg && $method){
                $qry = "SELECT kode,nim FROM adis_smb_form WHERE nomor = '$noReg'";
                $qry = $this->db2->query($qry)->row_array();
                $kode = $qry['kode'];
                                
                $delete = $this->db2->query("SET @kode := '$kode';");
                $delete = $this->db2->query("DELETE FROM adis_smb_form WHERE kode = @kode;");
                $delete = $this->db2->query("DELETE FROM adis_smb_usr_edu WHERE smbUsr = @kode;");
                $delete = $this->db2->query("DELETE FROM adis_smb_usr_kel WHERE smbUsr = @kode;");
                $delete = $this->db2->query("DELETE FROM adis_smb_usr_keu WHERE smbUsr = @kode;");
                $delete = $this->db2->query("DELETE FROM adis_smb_usr_kursus WHERE smbUsr = @kode;");
                $delete = $this->db2->query("DELETE FROM adis_smb_usr_org WHERE smbUsr = @kode;");
                $delete = $this->db2->query("DELETE FROM adis_smb_usr_pribadi WHERE kode = @kode;");
                $delete = $this->db2->query("DELETE FROM adis_smb_usr WHERE kode = @kode;");
            }
            
            if($this->db->trans_status() == false){
                    $this->db->trans_rollback();
                    echo "Data not saved";
            } else{
                    echo $this->db->trans_commit();
            }
        }
		
	function zimbraAccount($action, $kode = ''){	
		
	}
}
