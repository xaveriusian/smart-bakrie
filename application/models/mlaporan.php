<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

class Mlaporan extends CUTI_Model{
	
	function __construct(){
		parent::__construct();
		$this->log = $this->config->item('log_path');
	}
	
	function Mlaporan(){
		parent::__construct();
		
		$this->db2->query("SET lc_time_names = 'id_ID'");
		
	}
	
	
	function qriData($periode = "", $prodi  = "", $jalur = "", $potongan = "", $laporan = ""){
		$where = "";
		$select = "";
		$join = "";
		
		if($periode != "" && $periode != 'all'){
			$where .= " AND SUBSTR(A.bukaSmb, 1, 8) = '$periode'";
		}
		if($prodi != "" && $prodi != 'all'){
			$where .= " AND SUBSTR(A.bukaSmb,-4) = '$prodi' ";
		}		
		if($jalur != "" && $jalur != 'all'){
			$where .= " AND SUBSTR(A.bukaSmb,12,2) = '$jalur' ";
		}
		if($potongan != "" && $potongan != '0' ){
			$select .= " , E.nama_potongan as 'Gelombang' ";
			$join .= " LEFT JOIN tbl_potongan_periode D ON D.id = '$potongan' ";
			$join .= " LEFT JOIN tbl_master_potongan E ON E.kode_potongan = D.kode_potongan ";
			$where .= " AND ( (A.createTime >= tanggal_mulai AND A.createTime <= tanggal_akhir) OR (A.reapplyBankTransferTime >= tanggal_mulai AND A.reapplyBankTransferTime <= tanggal_akhir))";
		}
		
		switch($laporan){
			case 'daftar': 
				$where .= ' AND A.stsApplyPaid = 1 ';
			break;
			case 'belum_bu':
				$where .= " AND A.stsApplyPaid = 1 AND stsResultConfirm = 0";
			break;
			case 'belum_du':
				$where .= " AND ( A.stsResultConfirm = 1 AND A.stsReapplyPaid = 0) ";
			break;
			case 'sudah_du':
				$where .= " AND A.stsReapplyPaid = 1 ";
			break;
		}
		
		$query = "SELECT B.nama as 'Nama Lengkap', A.kode as 'Email', B.rumahCell as 'Kontak', AB.nama as Prodi $select
			FROM adis_smb_form A 
			LEFT JOIN adis_smb_usr_pribadi B ON B.kode = A.kode 
			LEFT JOIN adis_smb_usr_keu C ON C.smbUsr = A.kode 
			INNER JOIN adis_buka_smb AA ON AA.kode = A.bukaSmb
			INNER JOIN adis_prodi AB ON AB.kode = AA.prodi
			$join
			WHERE 1=1 $where ORDER BY B.nama ASC";
		// echo $query;exit;
		$data = $this->db2->query($query);
		
		return $data;
		
	}
	
}

?>