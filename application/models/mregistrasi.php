<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}


class Mregistrasi extends CUTI_Model{

	function __construct(){
		parent::__construct();
	}

	function Mregistrasi(){
		parent::__construct();
	}

	function mGenNoPendafataran($kode = ""){
		$noRegDone = 0;

		$qr= "	set @_bukasmb:=(select bukaSmb from adis_smb_form where kode='$kode');";
		$qr2= "	set @_tahun:=left(@_bukasmb,4); ";
		$qr3= "	set @_tahun_yy:=right(@_tahun,2); ";
		$qr4= "	set @_jenjang:=right(left(@_bukasmb,6),1); ";
		$qr5= "	set @_sesi:=right(left(@_bukasmb,8),1); ";
		// $qr6= "	set @_kode:=concat(@_tahun_yy,@_jenjang,@_sesi); ";
		$qr6= "	set @_kode:=concat(@_tahun_yy,@_jenjang); ";
		$qr7= "	set @_concat := concat(@_kode,'%'); ";
		$qr8= "	set @_regOrder := (select max(nomor)+1 from adis_smb_form where nomor LIKE @_concat); ";

		$this->db2->query($qr);
		$this->db2->query($qr2);
		$this->db2->query($qr3);
		$this->db2->query($qr4);
		$this->db2->query($qr5);
		$this->db2->query($qr6);
		$this->db2->query($qr7);
		$this->db2->query($qr8);

		$echo = $this->db2->query("SELECT @_regOrder as data")->row();

		if (!$echo->data){
			$qr0= "	set @_regOrder := concat(@_kode,'10001')";
			$this->db2->query($qr0);
		}

		$noRegDone = $this->db2->query("SELECT @_regOrder as noreg")->row_array();
		$this->db2->query("UPDATE adis_smb_form SET nomor=@_regOrder WHERE kode='$kode';");

		return $noRegDone;


	}

	function mGenerateTagihan($kode = "", $jenis_tagihan = "", $nomor_tagihan = ''){
		$this->load->model('mportal');
		$mhs = $this->mportal->mCmb($kode);
		$jenjang = substr($mhs->bukaSmb, 5, 1);
		$biayaTagihan = 0;

		if ($jenis_tagihan == 1){
			$biayaTagihan = ($jenjang == 1) ? 300000 : 500000;
		}

		$qryTagihan = "SELECT * FROM tbl_tagihan_cmb WHERE jenis_tagihan = 1 AND kode_mhs = '$kode'";
		$qryTagihan = $this->db2->query($qryTagihan)->num_rows();

		if($qryTagihan <= 0){
			$this->db2->trans_start();
			$this->db2->insert('tbl_tagihan_cmb', array(
				'kode_mhs'=>$kode,
				'kode_pembayaran'=>$nomor_tagihan,
				'nomor_tagihan'=>$nomor_tagihan,
				'jumlah_tagihan'=>$biayaTagihan,
				'jenis_tagihan'=>$jenis_tagihan,
				'status_bayar'=>0
			));

			if ($this->db2->trans_status() === FALSE){
				$this->db2->trans_rollback();
			}else{
				$this->db2->trans_commit();
			}

			$moneyFormat =  number_format( $biayaTagihan, 0 , '' , '.' );
			$konten = array( "konten" =>
					"Kepada Saudara ".$mhs->nama_cm.""
				  . "<br>"
				  . "<br>Terima kasih, akun Anda pada Portal Admisi Universitas Bakrie telah aktif."
				  . "<br>Silahkan melakukan pembayaran biaya pendaftaran sesuai dengan nilai tagihan yang telah ditentukan :  "
				  . "<br>"
				  . "<br>"
				  . "<br> <b>Nomor Tagihan : $nomor_tagihan</b>"
				  . "<br> <b>Biaya Pendaftaran : Rp $moneyFormat</b>"
				  . "<br>"
				  . "<br>"
				  . "<br>"
				  . "<br>Terima Kasih"
				  . "<br>Best Regards"
				  . "<br>"
				  . "<br>"
				  . "<br>Panitia SMB Universitas Bakrie"
				  . "<br>"
				  . "<br>"
				  . "<br>"
			  );
			$subject = "Tagihan Biaya Pendaftaran Admisi Universitas Bakrie ";
			$mailRespon = $this->emailNotifikasi($kode, $subject, $konten);
		}
	}

	function mSelectPeriode(){
		$periode ="SELECT * FROM adis_periode WHERE status = 1 AND erased = 0";
		$periode = $this->db2->query($periode)->result();

		$this->smarty->assign('periode',$periode);

	}

	function mSelectGender(){
		$gender = $this->db2->query("SELECT * FROM adis_type WHERE kode LIKE '03.%' AND erased = 0")->result();
		$this->smarty->assign("gender", $gender);
	}

	function mSelectProdi(){
		$prodi ="SELECT * FROM adis_prodi WHERE erased = 0";
		$prodi = $this->db2->query($prodi)->result();

		$this->smarty->assign('prodi',$prodi);

	}

	function mSelectJalur($prodi){
		$jalur ="SELECT j.nama as namajalur, p.kode , b.jalur, j.kode as kodejalur FROM adis_buka_smb b
				INNER JOIN adis_prodi p ON b.prodi = p.kode
				INNER JOIN adis_jalur_smb j ON b.jalur = j.kode
				WHERE b.prodi = '$prodi' AND stsBuka = 1 AND b.erased = 0";
		$jalur = $this->db2->query($jalur)->result();

		$this->smarty->assign('jalur',$jalur);

	}

	function mSelectRuang(){
		$ruang ="SELECT * FROM adis_ruang WHERE erased = 0";
		$ruang = $this->db2->query($ruang)->result();

		$this->smarty->assign('ruang',$ruang);

	}

	function mSelectPetugas(){
		$person ="SELECT * FROM adis_personal WHERE erased = 0";
		$person = $this->db2->query($person)->result();

		$this->smarty->assign('petugas',$person);

	}

	function mAddBuka(){
		$periode = $this->input->post("periode");
		$jalur = $this->input->post("jalur");
		$prodi = $this->input->post("prodi");

		$nama = array($periode,$jalur,$prodi);
		$nama = implode (" - ",$nama);

		$this->db2->insert("adis_buka_smb", array(
				"kode"=>implode (".",array($periode,$jalur,$prodi)),
				"nama"=>$nama,
				"periode"=>$periode,
				"jalur"=>$jalur,
				"prodi"=>$prodi,
				"stsBuka"=>$this->input->post("status"),
				"tanggalBuka"=>$this->input->post("from"),
				"tanggalTutup"=>$this->input->post("to")
			));
	}

	function mEditBuka($kode){
		$periode = $this->input->post("periode");
		$jalur = $this->input->post("jalur");
		$prodi = $this->input->post("prodi");

		$nama = array($periode,$jalur,$prodi);
		$nama = implode (" - ",$nama);
		$this->db2->where("kode", $kode);
		$this->db2->update("adis_buka_smb", array(
				"nama"=>$nama,
				"periode"=>$periode,
				"jalur"=>$jalur,
				"prodi"=>$prodi,
				"stsBuka"=>$this->input->post("status"),
				"tanggalBuka"=>$this->input->post("from"),
				"tanggalTutup"=>$this->input->post("to")
			));
	}

	function mRegKelasKaryawan($validation_key){
		$this->load->library("encrypt");

		$prodi = $this->db2->escape_str($this->input->post("prodi"));
		$email = $this->db2->escape_str($this->input->post("email"));
		$jurusan = $this->db2->escape_str($this->input->post("jurusan"));
		$ptnAsal = $this->db2->escape_str($this->input->post("ptn_asal"));
		$prodiAsal = $this->db2->escape_str($this->input->post("prodi_asal"));
		$lulusan = $this->db2->escape_str($this->input->post("lulusan"));
		$nimasal = $this->db2->escape_str($this->input->post("nim_asal"));

		$pass = $this->encrypt->encode($this->input->post("password"));
		if($prodi){
			$qryBS = "SELECT A.kode as kode_sesi, B.kode as kode_periode
				FROM adis_periode A
				LEFT JOIN adis_periode_master B ON B.kode = A.idPeriodeMaster
				WHERE B.jenjangType = '3' AND A.status = '1';";
			$qryBS = $this->db2->query($qryBS)->row_array();
			$bukaSmb = $qryBS['kode_sesi'].".01.".$prodi;

			$nomor = uniqid();
			$datetime = date("Y-m-d H:i:s");
			$createUser = "console";

			$nama_cmb = $this->db2->escape_str($this->input->post("name"));
			$nama_cmb = ucwords(strtolower($nama_cmb));

			$this->db->trans_start();
			$this->db2->insert("adis_smb_usr", array(
					"kode"=>$email,
					"username"=>$nama_cmb,
					"password"=>$pass,
					"email"=>$email,
					"validation_key"=>$validation_key,
					"validation_status"=>0,
					"createUser"=>$createUser,
					"createTime"=>$datetime
				));

			$this->db2->insert("adis_smb_form", array(
					"kode"=>$email,
					"bukaSmb"=>$bukaSmb,
					"nomor"=>$nomor,
					"applyBankTransferAmount"=>($lulusan == 'SMA' ? '150000':'500000'),
					"createTime"=>$datetime,
					"createUser"=>$createUser
				));

			$this->db2->insert("adis_smb_usr_edu", array(
					"kode"=>base64_encode($email),
					"smbUsr"=>$email,
					"nama"=>$this->db2->escape_str($this->input->post('asal_sma')),
					"lulusankk"=>$this->db2->escape_str($this->input->post('lulusan')),
					"jurusan"=>$this->db2->escape_str($this->input->post('prodi_asal')),
					"lulusAsal"=>$this->db2->escape_str($this->input->post('ptn_asal')),
					"verifikasi_forlap"=>$lulusan == 'SMA' ? '1' : NULL,
					"nisn_nim"=> !empty($nimasal) ? $nimasal : NULL,
					"createTime"=>$datetime,
					"createUser"=>$createUser
				));

			$this->db2->insert("adis_smb_usr_pribadi", array(
					"kode"=>$email,
					"nama"=>$nama_cmb,
					"rumahCell"=>$this->db2->escape_str($this->input->post("no_hp")),
					"stsPribadi"=>0,
					"stsPribadiConfirm"=>0,
					"createUser"=>$createUser,
					"createTime"=>$datetime
				));

			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return 0;
			}else{
				$this->db->trans_commit();
				return 1;
			}
		}

		return 0;

	}

	function mRegMegister($validation_key){
		$this->load->library("encrypt");

		// $post = $this->input->post();

		$prodi = $this->db2->escape_str($this->input->post("prodi"));
		$email = $this->db2->escape_str($this->input->post("email"));
		$jurusan = $this->db2->escape_str($this->input->post("jurusan"));
		$ptnAsal = $this->db2->escape_str($this->input->post("ptn_asal"));
		$prodiAsal = $this->db2->escape_str($this->input->post("prodi_asal"));

		$pass = $this->encrypt->encode($this->input->post("password"));

		if($prodi){
			$qryBS = "SELECT A.kode as kode_sesi, B.kode as kode_periode
				FROM adis_periode A
				LEFT JOIN adis_periode_master B ON B.kode = A.idPeriodeMaster
				WHERE B.jenjangType = '2' AND A.status = '1';";
			$qryBS = $this->db2->query($qryBS)->row_array();
			$bukaSmb = $qryBS['kode_sesi'].".01.".$prodi;

			$nomor = uniqid();
			$datetime = date("Y-m-d H:i:s");
			$createUser = "console";

			$nama_cmb = $this->db2->escape_str($this->input->post("name"));
			$nama_cmb = ucwords(strtolower($nama_cmb));

			$this->db->trans_start();
			$this->db2->insert("adis_smb_usr", array(
					"kode"=>$email,
					"username"=>$nama_cmb,
					"password"=>$pass,
					"email"=>$email,
					"validation_key"=>$validation_key,
					"validation_status"=>0,
					"createUser"=>$createUser,
					"createTime"=>$datetime
				));

			$this->db2->insert("adis_smb_form", array(
					"kode"=>$email,
					"bukaSmb"=>$bukaSmb,
					"nomor"=>$nomor,
					"applyBankTransferAmount"=>'500000',
					"createTime"=>$datetime,
					"createUser"=>$createUser
				));

			$this->db2->insert("adis_smb_usr_edu", array(
					"kode"=>base64_encode($email),
					"smbUsr"=>$email,
					"nama"=>$this->db2->escape_str($this->input->post('asal_sma')),
					"tahunLulus"=>$this->db2->escape_str($this->input->post('tahun_lulus')),
					"jurusan"=>$this->db2->escape_str($this->input->post('prodi_asal')),
					"lulusAsal"=>$this->db2->escape_str($this->input->post('ptn_asal')),
					"createTime"=>$datetime,
					"createUser"=>$createUser
				));

			$this->db2->insert("adis_smb_usr_pribadi", array(
					"kode"=>$email,
					"nama"=>$nama_cmb,
					"rumahCell"=>$this->db2->escape_str($this->input->post("no_hp")),
					"stsPribadi"=>0,
					"stsPribadiConfirm"=>0,
					"createUser"=>$createUser,
					"createTime"=>$datetime
				));

			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return 0;
			}else{
				$this->db->trans_commit();
				return 1;
			}
		}

		return 0;

	}

	function mAddCalonMahasiswa($validation_key, $kampus){

		$this->load->library("encrypt");

		// $periode = $this->session->userdata('periode');
		$prodi = $this->db2->escape_str($this->input->post("prodi"));
		$email = $this->db2->escape_str($this->input->post("email"));
		$jurusan = $this->db2->escape_str($this->input->post("jurusan"));
		$rapor = $this->db2->escape_str($this->input->post("rapor"));
		$pilihan_kelas = $this->db2->escape_str($this->input->post("class_choice"));
		$pass = $this->encrypt->encode($this->input->post("password"));

		$bukaSmb = "";
		if($kampus == 2){
			$qryBS = "SELECT A.kode as kode_sesi, B.kode as kode_periode
				FROM adis_periode A
				LEFT JOIN adis_periode_master B ON B.kode = A.idPeriodeMaster
				WHERE B.jenjangType = '2' AND B.status = '1';";
			$qryBS = $this->db2->query($qryBS)->row_array();
			$bukaSmb = $qryBS['kode_sesi']."."."10".".".$prodi;
		}else{
			$jenjang = $this->input->post("jenjang");
			// $jalur = $this->input->post("jalur");
			// $bukaSmb = $jalur;

			$kode_periode = $this->input->post('kode_periode');
			if(strlen($kode_periode) < 18) {
				$kode_periode = $kode_periode.'1004';
			}
		}

		$nomor = uniqid();
		$datetime = date("Y-m-d H:i:s");
		$createUser = "console";

		$nama_cmb = $this->db2->escape_str($this->input->post("name"));
		$nama_cmb = ucwords(strtolower($nama_cmb));

		$this->db->trans_start();
		$this->db2->insert("adis_smb_usr", array(
				"kode"=>$email,
				"username"=>$nama_cmb,
				"password"=>$pass,
				"email"=>$email,
				"validation_key"=>$validation_key,
				"validation_status"=>0,
				"createUser"=>$createUser,
				"createTime"=>$datetime,
				"jurusan"=>$jurusan,
				"nilaiRapor"=>$rapor,
				"jalur_penerimaan"=>$this->db2->escape_str($this->input->post('jalur_penerimaan')),
				"pilihan_prodi_2"=>$this->db2->escape_str($this->input->post('prodi2'))
			));

		$this->db2->insert("adis_smb_form", array(
				"kode"=>$email,
				"bukaSmb"=>$kode_periode,
				"nomor"=>$nomor,
				"pilihan_kelas"=>$pilihan_kelas,
				"createTime"=>$datetime,
				"createUser"=>$createUser
			));

		$this->db2->insert("adis_smb_usr_edu", array(
				"kode"=>base64_encode($email),
				"smbUsr"=>$email,
				"nilai"=>$rapor,
				"nama"=>$this->db2->escape_str($this->input->post('asal_sma')),
				"tahunLulus"=>$this->db2->escape_str($this->input->post('tahun_lulus')),
				"createTime"=>$datetime,
				"createUser"=>$createUser
			));

		$this->db2->insert("adis_smb_usr_pribadi", array(
				"kode"=>$email,
				"nama"=>$nama_cmb,
				"rumahAlamat"=>$this->db2->escape_str($this->input->post("occupation")),
				"suratAlamat"=>$this->db2->escape_str($this->input->post("occupation")),
				"rumahCell"=>$this->db2->escape_str($this->input->post("no_hp")),
				"stsPribadi"=>0,
				"stsPribadiConfirm"=>0,
				"createUser"=>$createUser,
				"createTime"=>$datetime
			));

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return 0;
		}
		else
		{
			$this->db->trans_commit();
			return 1;
		}

		/*$this->db2->insert("adis_smb_usr_edu", array(
				"kode"=>$nomor,
				"smbUsr"=>$email,
				"nama"=>$this->input->post("sekolah"),
				"jurusan"=>$this->input->post("jurusan"),
				"tahunLulus"=>$this->input->post("tahunLulus"),
				"createUser"=>$createUser,
				"createTime"=>$datetime
			));*/
	}

	function emailNotifikasi_($address, $subject, $konten){

		// Email configuration


	}

	function emailNotifikasi($address, $subject, $konten){

		// PHPMailer object
		$response = false;
		$this->load->library("phpmailer_library");
        $mail = $this->phpmailer_library->load();

		$this->load->library('parser');

		// $mail->SMTPDebug = 2;

	   // SMTP configuration
	   $mail->isSMTP();
	   $mail->Host     = 'smtp.gmail.com'; //sesuaikan sesuai nama domain hosting/server yang digunakan
	   $mail->SMTPAuth = true;
	   $mail->Username = 'noreply-admisi@bakrie.ac.id'; // user email
	   $mail->Password = 'hgzkjxiecodlvyfs'; // password email
	   $mail->SMTPSecure = 'tls';
	   $mail->Port     = 587;

	   $mail->setFrom('noreply-admisi@bakrie.ac.id', 'Admisi Universitas Bakrie'); // user email
	   $mail->addReplyTo('noreply-admisi@bakrie.ac.id', ''); //user email

	   // Add a recipient
	   $mail->addAddress($address); //email tujuan pengiriman email

	   if (strpos($subject, 'Nilai Rapor') !== false) {
			$recipientArr = array('smart.bakrie@gmail.com', 'bakrie.promosi@gmail.com', 'fadjar.handoyo@bakrie.ac.id', 'linggar.putra@bakrie.ac.id');
			foreach($recipientArr as $val){
				$mail->AddCC($val);
			}
		}else if (strpos($subject, 'Bayar') !== false) {
			$mail->AddCC("smart.bakrie@gmail.com");
		}else{
			$mail->AddCC("smart.bakrie@gmail.com");
		}

	   // Email subject
	   $mail->Subject = $subject; //subject email

	   // Set email format to HTML
	   $mail->isHTML(true);
	   $htmlMessage = $this->parser->parse('email_template.html', $konten, true);
	   $mail->msgHTML($htmlMessage);

	   // Send email
	   if(!$mail->send()){
		   return 0;
	   }else{
		   return 1;
	   }
	}



}
