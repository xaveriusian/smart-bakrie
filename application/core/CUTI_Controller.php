<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CUTI_Controller extends CI_Controller{
	
	function CUTI_Controller () {
		parent::__construct();
		
		date_default_timezone_set("Asia/Jakarta");
		
		 if($this->config->item('maintenance_mode') == TRUE) {
			// $this->load->view('maintenance_view.html');
			$this->smarty->display('maintenance_view.html');
			die();
		}
		
		$this->load->database();
		
		$this->cuti = $this->config->item('base_url');		
		$this->host	= $this->config->item('base_url');
		$this->USER = unserialize(base64_decode($this->session->userdata('cuti_parmad')));	
		$this->cmb = unserialize(base64_decode($this->session->userdata('calon_mah')));	
		
		$uri = $this->uri->segment(1);
		$this->smarty->assign('pages',"");

		// print_r($this->USER);exit;
		$menu_side = array();
		$jenjang = $this->USER['jenjang'];
		if($this->USER['level'] == '99'){
			$where_array = "";
			if($jenjang == '1' || $jenjang == '2' || $jenjang == '3'){
				if($jenjang == '3'){
					$where_array = " and (isKaryawan='1' or isAll = '1')";
				}else{
					$where_array = $jenjang == 1 ? ' and (isReguler=1 or isAll=1)' : " and (isMagister='1' or isAll = '1')";
				}
			}
			$menu_side = $this->db->query("select * from smart_menu where 1=1 $where_array order by noOrder")->result_array();
		}else {
			$priv = $this->db->query("select * from smart_menu_privilage where level_user = '".$this->USER['level']."' ")->row_array();
			if($priv){
				$list_menu = json_decode($priv['privilage']);
				$ids = join("','",$list_menu);   
				$menu_side = $this->db->query("select * from smart_menu where id in ('$ids') ")->result_array();
			}
		}
		$this->smarty->assign('menu_side',$menu_side);
		
		/*
		if((! $this->USER && $uri == "") || (! $this->USER  && $uri == "home" || $uri == "dashboard")) {
		
			header("Location: " . $this->host."home");
			
		} 
		else if(! $this->cmb && $uri == "site"){
			
			header("Location: " . $this->host."site");
			
		}	

		*/	
		// else if( (! $this->cmb || $this->cmb) && $uri == "registrasi"){
		
			// //header("Location: " . $this->host."registrasi");
			// redirect ('registrasi', 'refresh');
		// }
		
		
		$this->smarty->assign('USER', $this->USER);
		$this->smarty->assign('host', $this->cuti);
		
		
		$level = $this->USER['level'];
		$this->smarty->assign('levelID',$level);
		
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("If-Modified-Since: Mon, 22 Jan 2008 00:00:00 GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Cache-Control: private");
		header("Pragma: no-cache");
	}
}
?>