<?php
// application/models/Sales_model.php

class Sales_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getSalesData() {
        $sql = "SELECT adis_sys_usr.fullname, adis_sys_usr.kode , adis_sys_usr.is_reg, adis_sys_usr.is_mag, adis_sys_usr.is_kk FROM adis_sys_usr WHERE aktif_edu = '1'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    public function getSalesDetails($kode) {
        // Fetch sales details based on $fullname
        $sql = "SELECT * FROM adis_sys_usr WHERE kode = ?";
        $query = $this->db->query($sql, array($kode));
        return $query->row_array();
    }

    public function getStudentsBySales($kode) {
        // Fetch students associated with the sales based on $fullname
        $sql = "SELECT * FROM tbl_invent WHERE edu = ?";
        $query = $this->db->query($sql, array($kode));
        return $query->result_array();
    }
}


?>