<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CUTI_Controller{
	function __construct(){
		parent::__construct();	
		setlocale (LC_TIME, 'INDONESIA');
		$this->auth = unserialize(base64_decode($this->session->userdata('cuti_parmad')));
		$this->host	= $this->config->item('base_url');		
		$this->db2 = $this->load->database('second', TRUE); 
		
		$modul = "laporan/";
		$this->smarty->assign('modul',$modul);
		$this->smarty->assign('host',$this->host);
		$this->load->model('mlaporan');
		$this->load->model('madmisi');
		$this->db2->query("SET lc_time_names = 'id_ID'");
		
		$nama = $this->auth['name'];
		$this->level = $this->auth['level'];
		$this->smarty->assign('nama',$nama);
		$this->smarty->assign('level',$this->level);
		

	}
	
	function index() {		
				
		$periode ="SELECT * FROM adis_periode_master WHERE erased = 0 ORDER by kode DESC";
		$periode = $this->db2->query($periode)->result();
		$this->smarty->assign('periode',$periode);
		
		$gelombang = $this->db2->query("SELECT A.*, B.nama_potongan, C.nama as jalur FROM tbl_potongan_periode A 
				LEFT JOIN tbl_master_potongan B ON B.kode_potongan = A.kode_potongan 
				LEFT JOIN adis_jalur_smb C ON C.kode = A.kode_jalur
				ORDER BY id DESC")->result_array();
		$this->smarty->assign('gelombang', $gelombang);
		
		$this->madmisi->mSelectJalur();
		$this->madmisi->mSelectProdi();
				
		$this->smarty->assign('site',"Laporan");
		$this->smarty->assign('lokasi',"Laporan Data Peserta SMB");
		$this->smarty->assign('pages',"laporan_data");
		$this->smarty->display('index.html');
	}

	function connbig(){
		$this->big = $this->load->database('big', TRUE); 

		// $data = $this->big->query("SELECT *
		// FROM pg_catalog.pg_tables
		// WHERE schemaname != 'pg_catalog' AND 
		// 	schemaname != 'information_schema';")->result_array();

		$data = $this->big->query("Select * from smart.master_kurikulum")->result_array();
		$data2= $this->big->query("Select * from smart.master_kurikulum_unit")->result_array();
		echo "<pre>";
		print_r($data);
		print_r($data2);

	}

	function landingpage(){
		$where_jenjang = $this->auth['jenjang'] == '99' ? '' : " and jenjangType = '".$this->auth['jenjang']."' ";
		$periode ="SELECT * FROM adis_periode_master WHERE erased = 0 $where_jenjang ORDER by status DESC";
		$periode = $this->db2->query($periode)->result_array();
		$this->smarty->assign('periode',$periode);

		$tbl_invent = 'tbl_invent';
		$tbl_invent = $this->auth['jenjang'] == '1' ? $tbl_invent : ($this->auth['jenjang'] == '2' ? 'tbl_invent_mm' : 'tbl_invent_kk');

		$post= $this->input->post();

		$kode_periode = empty($post['periode']) ? $periode[0]['kode'] : $post['periode'];
		$bulan_post = empty($post['bulan']) ? '' : $post['bulan'];

		$where_bulan = "";

		if($bulan_post != '' && $bulan_post != 'all'){
			$where_bulan .= " and date_format(tanggal_daftar, '%c') = '$bulan_post'  ";
		}

		$pie_qry = " select a.id, a.status, count(b.id) as total
			from idx_status_invent a
			left join $tbl_invent b on  a.id = b.status
			left join (select kode, bukaSmb from adis_smb_form) c on b.email = c.kode and left(c.bukaSmb, 8) = '$kode_periode' 
			where 1=1 $where_bulan
			group by a.status
			order by a.id;
		";
		$pie_data = $this->db->query($pie_qry)->result_array();

		$datas_pie[0] = array("Status", "Total");
		$t = 1;
		foreach($pie_data as $val){
			$datas_pie[$t] = array($val['status'], (int)$val['total']);
			$t++;
		}

		$json_datas_pie = json_encode($datas_pie);
		$this->smarty->assign('pie_data_chart', $json_datas_pie);

		$stats_qry = "select count(b.id) as count_today, count(a.id) as count_all, count(c.kode) as register, (count(a.id) - count(c.kode)) as not_register 
		from $tbl_invent a
		left join (select id from $tbl_invent where tanggal_daftar = date_format(now(), '%Y-%m-%d' )) b on a.id = b.id
		left join (select kode,bukaSmb from adis_smb_form ) c on a.email = c.kode and left(c.bukaSmb, 8) = '$kode_periode' 
		where 1=1 $where_bulan ";
		$stats_qry = $this->db->query($stats_qry)->row_array();
		$this->smarty->assign('stats_data', $stats_qry);

		$campaign = "select a.email, a.sumber, b.kode 
		from $tbl_invent a
		left join (select kode, bukaSmb from adis_smb_form) b on a.email = b.kode and left(b.bukaSmb, 8) = '$kode_periode' 
		where 1=1 $where_bulan ;";
		$campaign = $this->db->query($campaign)->result_array();
		$campaign_ar = array();
		$campaign_src = array();
		foreach($campaign as $val){
			$campaign_ar[$val['email']]['register'] = empty($val['kode']) ? 0 : 1; 
			if($val['sumber'] == 'Direct Link'){
				$campaign_ar[$val['email']]['src'] = $val['sumber'];
			}else{
				$sumber = array();
				$sumber = json_decode($val['sumber'], true);
				if(isset($sumber['utm_campaign'])){
					$campaign_ar[$val['email']]['src'] = empty($sumber['utm_campaign']) ? $sumber['utm_source'] : $sumber['utm_campaign'];
				}else{
					$campaign_ar[$val['email']]['src'] = empty($sumber['utm_medium']) ? '' : $sumber['utm_medium'];
				}
			}
			$campaign_src[$campaign_ar[$val['email']]['src']] = array('reg'=>0, 'not'=>0);
		}

		$src_camp = array('SEM', 'GDN', 'FBIG', 'Direct Link');

		$reg = 0; $nonreg = 0;
		$campaign_chart = array();
		$camp_medium = array();
		foreach($src_camp as $v){
			$camp_medium[$v] = array('reg'=>0,'not'=>0);
		}

		foreach($campaign_ar as $val){
			foreach($src_camp as $v){
				if(strpos($val['src'], $v) !== false){
					if($val['register']){
						$camp_medium[$v]['reg'] += 1; 
					}else{
						$camp_medium[$v]['not'] += 1; 
					}
				}
			}

		}

		$datas = array();
		$datas[0] = array("Sumber ", "Register", "Not Register");
		$i = 1;
		foreach($camp_medium as $k=> $val){
			$datas[$i] = array($k, (int)$val['reg'], (int)$val['not']);
			$i++;
		}

		$src_json = json_encode($datas);
		$this->smarty->assign('srcdata', $src_json);


		$wherePeriode =  empty($kode_periode) ? " and status = 1 ":" and kode ='$kode_periode' ";
		$periodeRow = $this->db2->query("SELECT * FROM adis_periode_master WHERE 1=1 $wherePeriode ")->row_array();

		if($bulan_post == '' || $bulan_post == 'all'){
			$periodeRow['tanggalMulai'] = $periodeRow['tanggalMulai'] == '0000-00-00' ? date('Y').'01-01' : $periodeRow['tanggalMulai'];
			$period = new DatePeriod(
				new DateTime($periodeRow['tanggalMulai']),
				// new DateTime(date('Y-m-d',strtotime("-4 Months"))),
				new DateInterval('P1D'),
				new DateTime(date('Y-m-d'))
			);
		}else{
			$dateStart = "01-".$bulan_post."-".date('Y');
			$period = new DatePeriod(
				new DateTime(date('Y-m-d',strtotime($dateStart))),
				new DateInterval('P1D'),
				new DateTime(date('Y-m-t',strtotime($dateStart)))
			);
		}

		$where_bulan2 = "";
		if($bulan_post != '' && $bulan_post != 'all'){
			$where_bulan2 .= " and date_format(A.createTime, '%c') = '$bulan_post'  ";
		}

		$sql_frek = "
		select 'Smart' as status, count(A.kode) total, DATE_FORMAT(A.createTime, '%d/%m/%y') tanggal 
		from adis_smb_form A 
		where LEFT(A.bukaSmb, 8) = '$kode_periode' $where_bulan2
		group by DATE_FORMAT(A.createTime, '%d-%m-%y') 
		union 
		select 'Landing Page' as status, count(A.id) total, DATE_FORMAT(A.tanggal_daftar, '%d/%m/%y') tanggal 
		from $tbl_invent A 
		left join adis_smb_form B on A.email = B.kode and LEFT(B.bukaSmb, 8) = '$kode_periode' 
		where 1=1 $where_bulan
		group by DATE_FORMAT(A.tanggal_daftar, '%d-%m-%y') 
		union 
		select 'Register' as status, count(A.id) total, DATE_FORMAT(A.tanggal_daftar, '%d/%m/%y') tanggal 
		from $tbl_invent A 
		inner join adis_smb_form B on email = B.kode and LEFT(B.bukaSmb, 8) = '$kode_periode' 
		where 1=1 $where_bulan
		group by DATE_FORMAT(A.tanggal_daftar, '%d-%m-%y') 
		union 
		select 'Not Register' as status, count(A.id) total, DATE_FORMAT(A.tanggal_daftar, '%d/%m/%y') tanggal 
		from $tbl_invent A 
		left join adis_smb_form B on email = B.kode and LEFT(B.bukaSmb, 8) = '$kode_periode' 
		where B.kode is null $where_bulan
		group by DATE_FORMAT(A.tanggal_daftar, '%d-%m-%y') 
		";

		$sql = $this->db->query($sql_frek)->result_array();


		// echo "<pre>";
		// print_r($periodeRow);
		// exit;

		$data_frek[0] = array('Tanggal','Smart','Landing Page', 'Register', 'Not Register');
		$i = 1;
		$totSmart = 0;
		$totLP = 0;
		$totReg = 0;
		$totNotReg = 0;
		foreach ($period as $key => $value) {
			$tanggal = $value->format('d/m/y');
			$data_frek[$i][0] = $tanggal;

			foreach($sql as $val ){
				if($tanggal == $val['tanggal']){
					if($val['status'] == 'Smart'){
						$data_frek[$i][1] = (int)$val['total'];
						$totSmart += $val['total'];
					}else if($val['status'] == 'Landing Page'){
						$data_frek[$i][2] = (int)$val['total'];
						$totLP += $val['total'];
					}else if($val['status'] == 'Register'){
						$data_frek[$i][3] = (int)$val['total'];
						$totReg += $val['total'];
					}else if($val['status'] == 'Not Register'){
						$data_frek[$i][4] = (int)$val['total'];
						$totNotReg += $val['total'];
					}
				}
			}
			$i++;
		}

		$data_frek[0] = array('Tanggal', $totSmart.' Smart',$totLP.' Landing Page', $totReg.' Register', $totNotReg.' Not Register');

		

		
		foreach($data_frek as $k => $val){
			if(empty($val[1])){
				$data_frek[$k][1] = 0;
			}
			if(empty($val[2])){
				$data_frek[$k][2] = 0;
			}
			if(empty($val[3])){
				$data_frek[$k][3] = 0;
			}
			if(empty($val[4])){
				$data_frek[$k][4] = 0;
			}

			ksort($data_frek[$k]);
		}

		$totalPerformLP = json_encode($data_frek);
		$this->smarty->assign('totalPerformLP',$totalPerformLP);

		$this->filter_perform($kode_periode, $bulan_post, 'landingpage');


		if($post['periode']){

			$this->smarty->assign('periodeselected', $kode_periode);
			$this->smarty->assign('bulanselected', 'all');
			$this->smarty->display('laporan/chart_lp.html');
			exit;

		}

		$months = array(1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juli', 8 => 'Agustus', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'Desember');

				
		$this->smarty->assign('bulan',$months);

		$this->smarty->assign('periodeselected', $periode[0]['kode']);
		$this->smarty->assign('bulanselected', 'all');

		$this->smarty->assign('site',"LaporanLP");
		$this->smarty->assign('lokasi',"Laporan Landing Page");
		$this->smarty->assign('pages',"laporan_lp");
		$this->smarty->display('index.html');

	}

	function performa_ec(){


		if($this->input->post('bulan') && $this->input->post('periode')){
			$periode = $this->input->post('periode');
			$bulan = $this->input->post('bulan');
			$wherebulan = $bulan == 'all' ? "" : " and date_format(A.date_updated, '%c') = '$bulan'  ";
			$bulan = $bulan == 'all' ? "" : $bulan;
			$this->filter_performa($periode, $wherebulan, $bulan);
			$this->filter_perform($periode, $wherebulan);
			$this->filter_perform_follow($periode, $wherebulan);

			$this->smarty->assign('periodeselected', $periode);
			$this->smarty->assign('bulanselected', $bulan);

			$this->smarty->display('laporan/chart_ec.html');
			exit;
		}


		$where_jenjang = $this->auth['jenjang'] == '99' ? '' : " and jenjangType = '".$this->auth['jenjang']."' ";
		$periode ="SELECT * FROM adis_periode_master WHERE erased = 0 $where_jenjang ORDER by kode DESC";
		$periode = $this->db2->query($periode)->result_array();
		$this->smarty->assign('periode',$periode);
		
		$gelombang = $this->db2->query("SELECT A.*, B.nama_potongan, C.nama as jalur FROM tbl_potongan_periode A 
				LEFT JOIN tbl_master_potongan B ON B.kode_potongan = A.kode_potongan 
				LEFT JOIN adis_jalur_smb C ON C.kode = A.kode_jalur
				ORDER BY id DESC")->result_array();
		$this->smarty->assign('gelombang', $gelombang);

		$months = array(1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juli', 8 => 'Agustus', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'Desember');

				
		$this->smarty->assign('bulan',$months);

		$this->filter_performa();
		$this->filter_perform();
		$this->filter_perform_follow();

		$this->smarty->assign('periodeselected', $periode[0]['kode']);
		$this->smarty->assign('bulanselected', 'all');

		$this->smarty->assign('site',"LaporanEC");
		$this->smarty->assign('lokasi',"Laporan Performa EC");
		$this->smarty->assign('pages',"laporan_ec");
		$this->smarty->display('index.html');

	}

	function filter_perform($periode = "", $where = "", $modul = ""){

		$where_bulan = "";
		$where_bulan2 = "";
		if($modul == 'landingpage'){
			if($where != '' && $where != 'all'){
				$where_bulan .= " and date_format(C.createTime, '%c') = '$where'  ";
				$where_bulan2 .= " and date_format(B.createTime, '%c') = '$where'  ";
			}

			$where = "";
		}

		$wherePeriode =  empty($periode) ? " and status = 1 ":" and kode ='$periode' ";
		$periodeRow = $this->db2->query("SELECT * FROM adis_periode_master WHERE 1=1 $wherePeriode ")->row_array();


		$periode = empty($periode) ? $periodeRow['kode'] : $periode ;

		// echo $periode;exit;

		$totDU = 0;
		$totClose = 0;

		$datas = array();
		$datas[0] = array("Edu Consultant", $totDU." Total DP", $totClose." Total Lunas");

		
		$query2 = "
			select A.username as edu, A.fullname, count(BB.id) total, 'dp' as label
			from adis_sys_usr A
			left join (
				select A.id,A.edu from 
				smart_telemarketing A 
				left join adis_smb_form C on A.kode = C.kode 
				where  C.stsReapplyPaid =1 and C.nim = '' and LEFT(C.bukaSmb, 8) = '$periode' $where_bulan
			) as BB on A.username = BB.edu
			where A.id_level = 44 and A.erased = 0 
			group by A.username;
		";
		$query2 = "
			select A.username as edu, A.fullname, count(BB.id) total, 'dp' as label
			from adis_sys_usr A
			left join (
				select A.id,A.edu from 
				smart_telemarketing A 
				left join adis_smb_form C on A.kode = C.kode 
				where  C.stsReapplyPaid =1 and C.nim = '' and LEFT(C.bukaSmb, 8) = '$periode' $where_bulan
			) as BB on A.username = BB.edu
			where A.id_level = 44 and A.erased = 0 
			group by A.username;
		";
		$query3 = "
			select A.edu, 'closing' as label, count(A.id) total
			from smart_telemarketing A
			left join adis_smb_form B on A.kode = B.kode 
			left join adis_sys_usr C on A.edu = C.kode and C.id_level = 44 and C.erased = 0
			where LEFT(B.bukaSmb, 8) = '$periode' AND B.stsReapplyPaid =1  and B.nim != ''  AND B.stsMundurAfterReapply = 0 $where
			$where_bulan2
			group by A.edu
		";
		$daftardata = $this->db->query($query2)->result_array();
		$nimdata = $this->db->query($query3)->result_array();

		$eduArr = array();
		foreach($daftardata as $k => $val){
			$eduArr[$val['edu']] = $val['edu'];
		}

		$i = 1;
		foreach($eduArr as $v){
			foreach($daftardata as $k => $val){
				if($val['edu'] == $v){
					$datas[$i] = array(explode(' ',$val['fullname'])[0], (int)$val['total'], 0);
					$totDU += $val['total'];
				}
			}
			foreach($nimdata as $k => $val){
				if($val['edu'] == $v){
					$datas[$i][2] = (int)$val['total'];
					$totClose += $val['total'];
				}else{
					// $totClose += $val['total'];
				}
			}
			$i++;
		}
		$datas[0] = array("Edu Consultant", $totDU." Total DP", $totClose." Total Lunas");

		$data_show = array();
		foreach($datas as $k=>$val){
			if ($val[1]  > 0 || $val[2] > 0){
				$data_show[] = $val;
			}
			
		}
		
		// echo "<pre>";print_r($data_show);exit;
		$json_data = json_encode($data_show);
		// echo $json_data;exit;
		$this->smarty->assign('performedu', $json_data);

	}

	function filter_perform_follow($periode = "", $where = ""){

		$wherePeriode =  empty($periode) ? " and status = 1 ":" and kode ='$periode' ";
		$periodeRow = $this->db2->query("SELECT * FROM adis_periode_master WHERE 1=1 $wherePeriode ")->row_array();

		$periode = empty($periode) ? $periodeRow['kode'] : $periode ;

		$totFolloup = 0;
		$totDU = 0;
		$totClose = 0;

		$datas = array();
		$datas[0] = array("Edu Consultant", $totFolloup." Follow Up", $totDU." Total DP & Lunas");

		$query1 = "
			select A.edu, C.fullname, 'followup' as label, count(A.id) total
			from smart_telemarketing A
			left join adis_smb_form B on A.kode = B.kode 
			inner join adis_sys_usr C on A.edu = C.kode and C.id_level = 44 and C.erased = 0
			where LEFT(B.bukaSmb, 8) = '$periode' $where
			group by A.edu";
		$query2 = "
			select A.edu, C.fullname, 'dp' as label, count(A.id) total
			from smart_telemarketing A
			left join adis_smb_form B on A.kode = B.kode 
			inner join adis_sys_usr C on A.edu = C.kode and C.id_level = 44 and C.erased = 0
			where LEFT(B.bukaSmb, 8) = '$periode' and B.stsReapplyPaid =1 -- AND B.stsMundurAfterReapply = 0 
			$where
			group by A.edu";
		$query3 = "
			select A.edu, C.fullname, 'closing' as label, count(A.id) total
			from smart_telemarketing A
			left join adis_smb_form B on A.kode = B.kode 
			left join adis_sys_usr C on A.edu = C.kode and C.id_level = 44 and C.erased = 0
			where LEFT(B.bukaSmb, 8) = '$periode' AND B.stsReapplyPaid =1  and B.nim != '' AND B.stsMundurAfterReapply = 0 $where
			group by A.edu
		";
		$followupdata = $this->db->query($query1)->result_array();
		$daftardata = $this->db->query($query2)->result_array();
		// $nimdata = $this->db->query($query3)->result_array();

		$eduArr = array();
		foreach($followupdata as $k => $val){
			$eduArr[$val['edu']] = $val['edu'];
		}

		$i = 1;
		foreach($eduArr as $v){
			foreach($followupdata as $k => $val){
				if($val['edu'] == $v){
					$datas[$i] = array(explode(' ',$val['fullname'])[0], (int)$val['total'], 0);
					$totFolloup += $val['total'];
				}
			}
			foreach($daftardata as $k => $val){
				if($val['edu'] == $v){
					$datas[$i][2] =(int)$val['total'];
					$totDU += $val['total'];
				}
			}
			// foreach($nimdata as $k => $val){
			// 	if($val['edu'] == $v){
			// 		$datas[$i][3] = (int)$val['total'];
			// 		$totClose += $val['total'];
			// 	}else{
			// 		// $totClose += $val['total'];
			// 	}
			// }
			$i++;
		}
		$datas[0] = array("Edu Consultant", $totFolloup." Follow Up", $totDU." Total DP & Lunas");
		
		// echo "<pre>";
		// print_r($datas);exit;
		$json_data = json_encode($datas);
		// echo $json_data;exit;
		$this->smarty->assign('performeduFollow', $json_data);

	}

	function filter_performa($periode = "", $where = "", $bulan = ""){

		$wherePeriode =  empty($periode) ? " and status = 1 ":" and kode ='$periode' ";
		$periodeRow = $this->db2->query("SELECT * FROM adis_periode_master WHERE 1=1 $wherePeriode ")->row_array();
		
		$periode = empty($periode) ? $periodeRow['kode'] : $periode ;

		$arrJalur = array(
			array('kode'=>'contacted', 'nama'=>'Contacted'),
			array('kode'=>'uncontacted', 'nama'=>'UnContacted')
		);

		if(empty($bulan)){
			$period = new DatePeriod(
				new DateTime($periodeRow['tanggalMulai']),
				// new DateTime(date('Y-m-d',strtotime("-4 Months"))),
				new DateInterval('P1D'),
				new DateTime(date('Y-m-d'))
			);
		}else{
			$dateStart = "01-".$bulan."-".date('Y');
			$period = new DatePeriod(
				new DateTime(date('Y-m-d',strtotime($dateStart))),
				new DateInterval('P1D'),
				new DateTime(date('Y-m-t',strtotime($dateStart)))
			);
		}

		$sql = "
			select 'contacted' as status, count(A.id) total, DATE_FORMAT(A.date_updated, '%d/%m/%y') tanggal 
			from smart_telemarketing A
			left join adis_smb_form B on A.kode = B.kode 
			where ods in (1,2,3,4,5,6,7,14,15) and LEFT(B.bukaSmb, 8) = '$periode' $where 
			group by DATE_FORMAT(date_updated, '%d-%m-%y')
			union
			select 'uncontacted' as status, count(A.id) total, DATE_FORMAT(A.date_updated, '%d/%m/%y') tanggal 
			from smart_telemarketing A
			left join adis_smb_form B on A.kode = B.kode 
			where ods in (8,9,10,11,12,13) and LEFT(B.bukaSmb, 8) = '$periode' $where 
			group by DATE_FORMAT(A.date_updated, '%d-%m-%y')
			union
			select 'nostatus' as status, count(A.id) total, DATE_FORMAT(A.date_updated, '%d/%m/%y') tanggal 
			from smart_telemarketing A
			left join adis_smb_form B on A.kode = B.kode 
			where ods in (0) and LEFT(B.bukaSmb, 8) = '$periode' $where 
			group by DATE_FORMAT(A.date_updated, '%d-%m-%y')
		";
		$sql = $this->db->query($sql)->result_array();

		$data_frek[0] = array('Tanggal','Contacted','UnContacted', 'No Status');
		$i = 1;
		$totContact = 0;
		$totUncontact = 0;
		$totNoStatus = 0;
		foreach ($period as $key => $value) {
			$tanggal = $value->format('d/m/y');
			$data_frek[$i][0] = $tanggal;

			foreach($sql as $val ){
				if($tanggal == $val['tanggal']){
					if($val['status'] == 'contacted'){
						$data_frek[$i][1] = (int)$val['total'];
						$totContact += $val['total'];
					}else if($val['status'] == 'uncontacted'){
						$data_frek[$i][2] = (int)$val['total'];
						$totUncontact += $val['total'];
					}else if($val['status'] == 'nostatus'){
						$data_frek[$i][3] = (int)$val['total'];
						$totNoStatus += $val['total'];
					}
				}
			}
			$i++;
		}

		$data_frek[0] = array('Tanggal', $totContact.' Contacted',$totUncontact.' UnContacted', $totNoStatus.' No Status');


		
		foreach($data_frek as $k => $val){
			if(empty($val[1])){
				$data_frek[$k][1] = 0;
			}
			if(empty($val[2])){
				$data_frek[$k][2] = 0;
			}
			if(empty($val[3])){
				$data_frek[$k][3] = 0;
			}

			ksort($data_frek[$k]);
		}

		$totalPerformaEdu = json_encode($data_frek);
		$this->smarty->assign('totalPerformaEdu',$totalPerformaEdu);
	}
	
	function export(){
		$post = $this->input->post();
		
		$newdata = array(
               'periode'  => $post['periode'],
               'prodi'     => $post['prodi'],
               'jalur' => $post['jalur'],
               'potongan' => $post['potongan'],
               'laporan' =>  $post['laporan']
           );

		$this->session->set_userdata($newdata);
		
		echo 1;
	}

	function invent(){

		$sql = "select A.*, C.nama as prodi, if(B.kode IS NULL, 'Not Register', 'Registered') as sts_daftar, date_format(B.createTime, '%d-%m-%Y') as createTime
			from tbl_invent A
			LEFT JOIN adis_smb_form B ON A.email = B.kode
			LEFT JOIN adis_prodi C ON RIGHT(B.bukaSmb,4) = C.kode
			order by tanggal_daftar DESC";
		$data = $this->db->query($sql)->result_array();

		$this->smarty->assign('sql', $data);

		$site = "Invent";			
		$this->smarty->assign('site',$site);
		$this->smarty->assign('lokasi',"Telemarketing");
		$this->smarty->assign('pages',"Telemarketing");
		$this->smarty->display('index.html');
	}

	function upload_invent(){
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$config['upload_path'] = realpath('assets/upload/invent/');
        $config['allowed_types'] = 'xlsx|xls|csv';
        $config['max_size'] = '10000';
		$config['encrypt_name'] = true;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload()) {
		
			//upload gagal
			echo "<script> alert('PROSES IMPORT GAGAL! ".$this->upload->display_errors()."');</script>";
			//redirect halaman
			redirect('laporan/invent');

		}else{
			$data_upload = $this->upload->data();

            $excelreader     = new PHPExcel_Reader_Excel2007();
            $loadexcel         = $excelreader->load('assets/upload/invent/'.$data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

			$data = array();

			$db_data = $this->db->query("SELECT * FROM tbl_invent")->result_array();
			$email = array();
			foreach($db_data as $val){
				array_push($email, $val['email']);
			}

			// print_r($email);exit;
			
			$numrow = 1;
            foreach($sheet as $row){
				if($numrow > 1){
					if(!in_array($row['C'], $email, TRUE) && isset($row['C'])){
						array_push($data, array(
							'tanggal_daftar' => date('Y-m-d', strtotime($row['A'])),
							'nama_lengkap'   => $row['B'],
							'email'      	=> $row['C'],
							'nama_sekolah'   => $row['D'],
							'hp'      => $row['E'],
							'cgts'      => $row['F'],
							'noted'      => $row['H'],
							'kode_voucher'      => $row['I'],
							'voucher_expired'      => date('Y-m-d', strtotime($row['J'])),
						));
					}else{
						$this->db->where('email', $row['C']);
						$this->db->update('tbl_invent', array(
							'tanggal_daftar' => date('Y-m-d', strtotime($row['A'])),
							'nama_lengkap'   => $row['B'],
							'nama_sekolah'   => $row['D'],
							'hp'      => $row['E'],
							'cgts'      => $row['F'],
							'noted'      => $row['H'],
							'kode_voucher'      => $row['I'],
							'voucher_expired'      => date('Y-m-d', strtotime($row['J'])),
						));
					}
                }
                $numrow++;
			}
			if($data){
				$this->db->insert_batch('tbl_invent', $data);
			}
			//delete file from server
			unlink(realpath('assets/upload/invent/'.$data_upload['file_name']));
			// loadUrl('".$this->host."smb/getDisplay/invent');
			
			 //upload success
			 echo "<script> 
					alert('PROSES IMPORT BERHASIL! Data berhasil di import!');
					window.location.replace('".$this->host."smb/load_page/Invent');
				 </script>";
			 //redirect halaman
			//  redirect('laporan/invent');
		}

	}
	
	function excell(){
		
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		
		$post = $this->session->all_userdata();
		$sql = $this->mlaporan->qriData($post['periode'], $post['prodi'], $post['jalur'], $post['potongan'], $post['laporan']);		
		
		$gelombang = $this->db2->query("SELECT A.*, B.nama_potongan FROM tbl_potongan_periode A 
				LEFT JOIN tbl_master_potongan B ON B.kode_potongan = A.kode_potongan ORDER BY nama_potongan")->result_array();
		$nama_potongan = "";
		// if($post['potongan'] != "" && $post['potongan'] != '0' ){
			// foreach($gelombang as $val){
				// if($val['id'] == $post['potongan']){
					// $nama_potongan = $val['nama_potongan'];
				// }
			// }
		// }
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setTitle("Laporan ".$post['laporan'])
			->setDescription("Laporan Admisi")
			->setCreator("Admission")
			->setLastModifiedBy("Admission")
			->setCategory("Laporan");
 
		$objPHPExcel->setActiveSheetIndex(0);
		// Field names in the first row
		$fields = $sql->list_fields();
		
		$col = 0;
		foreach ($fields as $field)
		{
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
			$col++;
		}
		
		$row = 2;
		foreach($sql->result() as $data){
			$col = 0;
			foreach ($fields as $field)
			{				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
				$col++;
			}
 
			$row++;
			
		}
		/*
		foreach($sql->result() as $data)
		{
			if($post['potongan'] != "" && $post['potongan'] != '0' ){
				$promo = json_decode(base64_decode($data->Gelombang), true);
				if($promo['promo_id'] == $post['potongan']){
					$col = 0;
					foreach ($fields as $field)
					{						
						if($field == 'Gelombang'){
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $nama_potongan);
						}else{
							$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
						}
						$col++;
					}
		 
					$row++;
				}
			}else{
				$col = 0;
				foreach ($fields as $field)
				{
					
					if($field == 'Gelombang'){
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $nama_potongan);
					}else{
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
					}
					$col++;
				}
	 
				$row++;
			
			}
		}
		*/
 
		$objPHPExcel->setActiveSheetIndex(0);
 
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
 
		// Sending headers to force the user to download the file
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Laporan Admisi '.$post['laporan'].'.xls"');
		header('Cache-Control: max-age=0');
 
		$objWriter->save('php://output');
		
	}

	function export_perform($periode, $bulan){
		$this->load->dbutil();
		$this->load->helper('file');
		$this->load->helper('download');

		$where = $bulan == "all" ? "" : " and date_format(A.date_updated, '%c') = '$bulan' ";
		$sql = "
			select a.edu, c.nama, b.nim, d.nama  as gelombang, a.kode, a.ket_pelunasan 
			from smart_telemarketing a 
			inner join adis_smb_form b on a.kode = b.kode 
			left join adis_smb_usr_pribadi c on a.kode = c.kode
			left join adis_periode d on LEFT(b.bukaSmb,10) = d.kode
			where b.nim != '' and b.stsMundurAfterReapply = 0 and LEFT(b.bukaSmb, 8) = '$periode' $where 
			ORDER BY a.edu 
		";
		$sql = $this->db->query($sql);
		$this->export->to_excel($sql, 'Data Performa Edu '); 
	}

	function laporans($p1 = '', $p2= ''){
		
		$this->load->model('madmisi');
		$post = $this->input->post();
				
		switch ($p1){
			case "bulan":
				$kode = $this->input->post('periode');
				$periode ="SELECT * FROM adis_periode_master WHERE kode = '$kode'";
				$periode = $this->db2->query($periode)->row_array();

				$begin = new DateTime( $periode['tanggalMulai'] );
				$end = new DateTime( $periode['tanggalSelesai'] );
				$interval = new DateInterval('P1M');

				$period = new DatePeriod($begin, $interval, $end);

				$listBulan = array();
				$listBulan['all'] = "Semua bulan";
				foreach ($period as $dt) {
					$listBulan[trim($dt->format('m-Y') . PHP_EOL)] = $dt->format('F Y') . PHP_EOL;
				}

				echo json_encode($listBulan);

				exit;
			break;
			case "select":		
				$site = "Select";	
				$lokasi = "Laporan";
				$pages = "seleksi";	
				
				$periode ="SELECT * FROM adis_periode_master WHERE erased = 0 ORDER by kode DESC";
				$periode = $this->db2->query($periode)->result();
				$this->smarty->assign('periode',$periode);
				
		
				$jalur = array(
					(object) array ( 'kode'=>'RAPOR', 'nama'=>'Jalur Rapor'), 
					(object) array  ('kode'=>'TULIS', 'nama'=>'Jalur Tes Tulis')
				);
				$this->smarty->assign('jalur',$jalur);
				
				$this->madmisi->mSelectProdi();

				$begin = new DateTime($periode[0]->tanggalMulai );
				$end = new DateTime( $periode[0]->tanggalSelesai);
				$interval = new DateInterval('P1M');

				$period = new DatePeriod($begin, $interval, $end);

				$listBulan = array();
				foreach ($period as $dt) {
					$listBulan[trim($dt->format('m-Y') . PHP_EOL)] = $dt->format('F Y') . PHP_EOL;
				}

				$this->smarty->assign('bulan',$listBulan);

			break;
			case 'seleksi':
				$periode = $post['periode'];
				$prodi = $post['prodi'];
				$jalur = $post['jalur'];
				$bulan = $post['bulan'];

				$jenjang = substr($periode, 5, 1);
				$this->smarty->assign('jenjang', $jenjang);
				$whereQry = '';
				
				
				$this->grafikCalonMahasiswa($periode);
				$this->potensiChart($periode);
				##filter all query
				if($periode){ $whereQry = " LEFT(A.bukaSmb, 8) = '$periode' "; }
				if ($jalur != 'all'){
					$whereQry = " LEFT(A.bukaSmb,8) = '$periode' AND B.jalur_penerimaan = '$jalur' ";
				}
				if ($prodi != 'all'){
					if ($jalur == 'all'){
						$whereQry = " LEFT(A.bukaSmb,8) = '$periode' AND RIGHT(A.bukaSmb, 4) = '$prodi' ";
					}else{
						$whereQry = " LEFT(A.bukaSmb,8) = '$periode' 
							AND B.jalur_penerimaan = '$jalur' AND RIGHT(A.bukaSmb, 4) = '$prodi' ";
					}
				}
				$whereQry .=  $bulan != 'all' ? " and date_format (A.createTime, '%m-%Y' ) ='".trim($bulan)."' " : "";

				$this->getfrekuenharian($periode, $whereQry);
				// echo $whereQry;exit;
				$arrayStep = array("Daftar Online", "Bayar Formulir", "Mengikuti Seleksi", "Lulus Seleksi", "Daftar Ulang", "Mengundurkan Diri");
				
				$qryDaftar = "SELECT COUNT(A.kode) as mhsDaftar 
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry";

				$qryDaftar = $this->db2->query($qryDaftar)->row_array();
				
				$qryPayForm = "SELECT COUNT(A.kode) as mhsDaftar 
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry AND A.stsApplyPaid = 1";
				$qryPayForm = $this->db2->query($qryPayForm)->row_array();
				
				$qryHadirUjian = "SELECT COUNT(A.kode) as mhsDaftar
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry  AND A.stsEventConfirm = 1";
				$qryHadirUjian = $this->db2->query($qryHadirUjian)->row_array();
				
				$qrylulusUjian = "SELECT COUNT(A.kode) as mhsDaftar
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry AND A.stsResultConfirm = 1";
				$qrylulusUjian = $this->db2->query($qrylulusUjian)->row_array();
				
				$qryDaftarUlang = "SELECT COUNT(A.kode) as mhsDaftar 
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry  AND A.stsReapplyPaid = 1";
				$qryDaftarUlang = $this->db2->query($qryDaftarUlang)->row_array();
				
				$qryUndur = "SELECT COUNT(A.kode) as mhsDaftar
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry AND A.stsMundurAfterReapply = 1";
				$qryUndur = $this->db2->query($qryUndur)->row_array();
					
				$data[0] = array('Tahap Seleksi CMB','Jumlah CMB');	
				$data[1] = array($arrayStep[0],(int)$qryDaftar['mhsDaftar']);
				$data[2] = array($arrayStep[1],(int)$qryPayForm['mhsDaftar']);
				$data[3] = array($arrayStep[2],(int)$qryHadirUjian['mhsDaftar']);
				$data[4] = array($arrayStep[3],(int)$qrylulusUjian['mhsDaftar']);
				$data[5] = array($arrayStep[4],(int)$qryDaftarUlang['mhsDaftar']);
				$data[6] = array($arrayStep[5],(int)$qryUndur['mhsDaftar']);


				// echo "<pre>";
				// print_r($data);exit;
				
				$totalReg = json_encode($data);
				$this->smarty->assign('totalReg',$totalReg);
				
				$arrIntBulan = '';
				$sql = "";
				
				$arrJalur = array(
					array('kode'=>'RAPOR', 'nama'=>'Jalur Rapor'),
					array('kode'=>'TULIS', 'nama'=>'Jalur Tes Tulis')
				);
				
				
				$this->smarty->display('laporan/kontenSeleksi.html');
				
			break;
		}
		if ($p2 == ''){
			
			$this->smarty->assign('modul',"laporan/");
			$this->smarty->assign('lokasi',$lokasi);
			$this->smarty->assign('pages',$pages);
			$this->smarty->assign('site',$site);
			$this->smarty->display('index.html');
		}
	}

	function getfrekuenharian($periode, $where){

		$periodeRow = $this->db2->query("SELECT * FROM adis_periode_master WHERE kode = '$periode'")->row_array();

		$arrJalur = array(
			array('kode'=>'RAPOR', 'nama'=>'Jalur Rapor'),
			array('kode'=>'TULIS', 'nama'=>'Jalur Tes Tulis'),
			array('kode'=>'ODS', 'nama'=>'One Day Service Online')
		);

		$period = new DatePeriod(
			new DateTime($periodeRow['tanggalMulai']),
			// new DateTime(date('Y-m-d',strtotime("-4 Months"))),
			new DateInterval('P1D'),
			new DateTime(date('Y-m-d'))
		);

		
		$data = array();
		foreach($arrJalur as $k => $v){

			$qry = "SELECT IF(COUNT(A.kode) = null, 0, COUNT(A.kode))  as mhsDaftar , DATE_FORMAT(A.createTime, '%d/%m/%y') as date
			FROM adis_smb_form A 
			LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
			WHERE LEFT(A.bukaSmb, 8) = '$periode' AND B.jalur_penerimaan = '".$v['kode']."' AND $where
			GROUP BY DATE_FORMAT(A.createTime, '%Y-%m%-%d')";

			$sql = $this->db2->query($qry)->result_array();
			$data_frek[0] = array('Tanggal','Rapor','Tes Tulis','ODS');
			$i = 1;
			foreach ($period as $key => $value) {
				$tanggal = $value->format('d/m/y');
				$data_frek[$i][0] = $tanggal;
				// $data_frek[$i][1] = 0;
				// $data_frek[$i][2] = 0;

				foreach($sql as $val ){
					if($tanggal == $val['date']){
						if($v['kode'] == 'RAPOR'){
							$data_frek[$i][1] = (int)$val['mhsDaftar'];
						}else if($v['kode'] == 'TULIS'){
							$data_frek[$i][2] = (int)$val['mhsDaftar'];
						}else if($v['kode'] == 'ODS'){
							$data_frek[$i][3] = (int)$val['mhsDaftar'];
						}  
					}
				}
				$i++;
			}

			// array_push($data, $data_frek);

		}

		foreach($data_frek as $k => $val){
			if(empty($val[1])){
				$data_frek[$k][1] = 0;
			}
			if(empty($val[2])){
				$data_frek[$k][2] = 0;
			}
			if(empty($val[3])){
				$data_frek[$k][3] = 0;
			}

			ksort($data_frek[$k]);
		}

		$totalRegFrek = json_encode($data_frek);
		$this->smarty->assign('totalRegFrek',$totalRegFrek);
		// echo "<pre>";
		// print_r($data_frek);exit;

		

		// $data = array();
		// foreach ($period as $key => $value) {
		// 	$data_frek[0] = $value->format('Y-m-d');  
			
		// 	foreach($sql as $k => $v ){
		// 		if($k == 'RAPOR'){
		// 			$data_frek[1] = (int)$v['mhsDaftar'];
		// 		}else{
		// 			$data_frek[2] = (int)$v['mhsDaftar'];
		// 		}  
		// 	}

		// 	array_push($data, $data_frek);
			
			  
		// }

	}

	function potensiChart($periode){
		$periodeRow = $this->db2->query("SELECT * FROM adis_periode_master WHERE kode = '$periode'")->row_array();

		$qry = "SELECT
			A.singkatan as prodi, COUNT(B.kode) as totalMhs
		FROM
			adis_prodi A
			LEFT JOIN adis_smb_form B ON A.kode = SUBSTR( B.bukaSmb,- 4 ) AND SUBSTR( B.bukaSmb, 1, 8 ) = '$periode' AND B.stsReapplyPaid =1 AND B.nim = ''  AND stsMundurAfterReapply = 0 AND stsMundurBeforeReapply = 0
			GROUP BY A.singkatan ";

		$du_only = $this->db->query($qry)->result_array();

		$qryNim = "SELECT
			A.singkatan as prodi, COUNT(B.kode) as totalMhs
		FROM
			adis_prodi A
			LEFT JOIN adis_smb_form B ON A.kode = SUBSTR( B.bukaSmb,- 4 ) AND SUBSTR( B.bukaSmb, 1, 8 ) = '$periode' AND B.stsReapplyPaid =1 
			AND B.nim != '' AND stsMundurAfterReapply = 0
			GROUP BY A.singkatan ";

		$get_nim = $this->db->query($qryNim)->result_array();

		$totalDU = 0;
		$totalNim = 0;
		$datas = array();
		$datas[0] = array("Program Studi", $totalDU." Belum Lunas", $totalNim." Mendapat NIM");
		$i = 1;
		foreach($get_nim as $k => $val){
			if($val['prodi'] == $du_only[$k]['prodi']){
				$totalDU += $du_only[$k]['totalMhs'];
				$totalNim += $val['totalMhs'];
				$datas[$i] = array($val['prodi'], (int)$du_only[$k]['totalMhs'], (int)$val['totalMhs']);
				// array_push($datas, $data);
			}
			$i++;
		}
		$datas[0] = array("Program Studi", $totalDU." Belum Lunas", $totalNim." Mendapat NIM");
		
		$json_data = json_encode($datas);
		// echo $json_data;exit;
		$this->smarty->assign('potensi', $json_data);


	}
	
	function grafikCalonMahasiswa($periode){
		$periodeRow = $this->db2->query("SELECT * FROM adis_periode_master WHERE kode = '$periode'")->row_array();
		$QryProdi = $this->db2->query('SELECT * FROM `adis_prodi` WHERE jenjang = "'.$periodeRow['jenjangType'].'" 
			AND erased = 0;')->result_array();
			
		$qryjalur = $this->db2->query("SELECT * FROM adis_jalur_smb 
			WHERE jenjang = '".$periodeRow['jenjangType']."' AND erased = 0;")->result_array();
		
		$arrJalur = array(
			array('kode'=>'RAPOR', 'nama'=>'Jalur Rapor'),
			array('kode'=>'TULIS', 'nama'=>'Jalur Tes Tulis'),
			array('kode'=>'ODS', 'nama'=>'ODS Online')
		);
		
		$totalreg = array();         
		if ($periode){       
		
			foreach($arrJalur as $i => $v){
				$qry[$i] = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs 
					FROM adis_smb_form A
					LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
					LEFT JOIN adis_smb_usr C ON A.kode = C.kode 
					WHERE (SUBSTR(bukaSmb, 1, 8) = '".$periode."' AND C.jalur_penerimaan = '".$v['kode']."')
					GROUP BY B.nama;";
				$total[$i] = $this->db2->query($qry[$i])->result_array();
			} 
			
			$data[0] = array('PROGRAM STUDI','Mahasiswa');	
			
			for($i = 0; $i < count($QryProdi); $i++){
				$data[$i+1] = array($QryProdi[$i]['singkatan']);
				$data[0] = array('Program Studi');
				
				
				foreach($arrJalur as $k => $v){
					$dot = strlen($v['nama']) > 15 ? "..." : "";
					$data[0][$k+1] = substr($v['nama'],0,15).$dot;
					$totalall[$k] = 0;
					
					
					foreach($total[$k] as $x => $val){
						if ($val['prodi'] == $QryProdi[$i]['singkatan']){
							$totalall[$k] = $val['totalMhs'];
						}
						
					}	
					
					$data[$i+1][$k+1] = (int)$totalall[$k];
				}
				
			}	
			
			$totalReg = json_encode($data);
			// echo $totalReg;
			$this->smarty->assign('totalPeserta',$totalReg);
		}
	}
}
