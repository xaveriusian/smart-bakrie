<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Portal extends CUTI_Controller{
	function __construct(){
		parent::__construct();
		//setlocale (LC_TIME, 'id_ID');
		// setlocale (LC_TIME, 'INDONESIA');
		date_default_timezone_set("Asia/Jakarta");
		$this->auth = unserialize(base64_decode($this->session->userdata('calon_mah')));
		$this->host	= $this->config->item('base_url');

		$this->prodiTidakPromo = array("2141","1204");

		if (!$this->auth){
			header("Location: " . $this->host."site");
		}

		$this->db2 = $this->load->database('second', TRUE);
		//$this->load->library('recaptcha');

		$modul = "portal/";
		$this->smarty->assign('modul',$modul);
		$this->smarty->assign('host',$this->host);
		$this->load->model('mportal');
		$this->load->model('mregistrasi');
		$this->db2->query("SET lc_time_names = 'id_ID'");


		$nama = $this->auth['username'];
		$jenjang = $this->auth['jenjang'];
		
		$arr = explode(' ',trim($nama));
		$firstname = $arr[0];
		
		$name = "";
		
		if(strlen($nama) > 20){
			$name_tmp = explode(' ', $nama); 
			$cnt_name = 0;
			foreach($name_tmp as $k => $v){
				$cnt_name += strlen($v);
				if($cnt_name <20 ){
					$name .= $v." ";
				}else{
					$name .= ucfirst(substr($v, 0, 1)).". ";
				}
			}
		
			$nama = $name;
		}

		$this->smarty->assign("nama", $nama);
		$this->smarty->assign("jenjang", $jenjang);
		$this->smarty->assign("firstname", $firstname);
		$this->smarty->assign("termcond", '' );

		$kode = $this->auth['kode'];
		$this->dataCmb = $this->mportal->mCmb($kode);
		
		
		$periode_user = $this->db2->query("SELECT B.* FROM adis_smb_form A 
			LEFT JOIN adis_periode B ON LEFT(A.bukaSmb, 10) = B.kode 
			WHERE A.kode = '".$this->auth['kode']."'")->row_array();
			
		$this->smarty->assign('periode_user', $periode_user);

	}

	function index() {
		if (!$this->auth){
			header("Location: " . $this->host."site");
		}else{
			$this->home();
		}
	}
	
	function generateJadwalPdf(){
		$this->load->library('mlpdf');
		$pdf = $this->mlpdf->load();
		
		$kode = $this->auth['kode'];

		$sql = "SELECT u.nama as namaCmb, f.kode, f.nomor, f.bukaSmb, f.`event`, s.nama as progdi, j.nama as jalurCmb,
				e.kode, e.nama as namaEvent, e.periode, DAY(e.tanggal) as days, u.foto,
				e.tanggal, DAYNAME(e.tanggal) as hari, MONTHNAME(e.tanggal) as bulan, YEAR(e.tanggal) as tahun,  
				e.jalur,e.jamMasuk, e.ruang, 
				e.petugas, e.pewawancara, e.jamKeluar, e.statusJadwal, usr.jalur_penerimaan,
				e.totalPeserta, e.pewawancara2, e.prioritas, r.kursiTes, r.nama as namaRuang, 
				g.nama as namaGedung, g.alamat, g.fax, w.nama as kabKota, g.kodePos, wa.nama as prop, g.tel
				FROM adis_smb_form f
				LEFT JOIN adis_smb_usr usr ON f.kode = usr.kode 
				LEFT JOIN adis_event_smb e ON f.`event`=e.kode 
				LEFT JOIN adis_smb_usr_pribadi u ON f.kode=u.kode
				LEFT JOIN adis_jalur_smb j ON j.kode = e.jalur
				LEFT JOIN adis_prodi s ON RIGHT(f.bukaSmb, 4) = s.kode
				LEFT JOIN adis_ruang r ON r.kode = e.ruang
				LEFT JOIN adis_gedung g ON g.kode = r.gedung
				LEFT JOIN adis_wil w ON w.kode = g.kabKota
				LEFT JOIN adis_wil wa ON wa.kode = g.prop
				WHERE f.kode = '$kode'";
		$sql = $this->db2->query($sql)->row();
		$this->smarty->assign('jadwal', $sql);
			
		$spdf = new mPDF('', 'A4', 0, '', 12.7, 12.7, 40, 20, 10, 2, 'P');
		$spdf->ignore_invalid_utf8 = true;
		$spdf->allow_charset_conversion = true;     // which is already true by default
		$spdf->charset_in = 'iso-8859-2';  // set content encoding to iso
		$spdf->SetDisplayMode('fullpage');
			
		$htmlheader = $this->smarty->fetch('portal/jadwal/dokumen_header.html');
		$htmlcontent = $this->smarty->fetch('portal/jadwal/dokumen_konten_pdf.html');
		
		$spdf->SetHTMLHeader($htmlheader);
		$spdf->SetHTMLFooter('
			<div style="font-family:arial; font-size:10px; text-align:right; font-weight:bold;">
				Halaman {PAGENO} dari {nbpg}
			</div>
		');		
		
		// $spdf->SetProtection(array());				
		$spdf->WriteHTML($htmlcontent); // write the HTML into the PDF
		//$spdf->Output('repositories/Dokumen_LS/LS_PDF/'.$filename.'.pdf', 'F'); // save to file because we can
		$spdf->Output('assets/jadwal/'.$sql->nomor.'.pdf', 'D'); // view file
		
	}
	
	function pernyataan(){

		$this->load->model('msmb');
		$this->msmb->mProfil($this->auth['kode']);
		$site = "pernyataan";

		$periode = $this->db2->get_where('adis_periode_master', array('status'=>1))->row_array();
		$this->smarty->assign("periode", $periode);

		$this->smarty->assign("site", $site);
		
		$breadcum = array('pages'=>'Surat Pernyataan', 'icon'=>'fa fa-home', 'url'=>'');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->display("portal/index.html");
		
	}
	
	function submit_voucher(){
		$voucher = $this->input->post('kode_voucher');
		$hp_ortu = $this->input->post('hp_ortu');
		$dataVoucher = $this->db2->query("SELECT * FROM tbl_voucher WHERE kode_voucher = '$voucher'");
		if($dataVoucher->num_rows > 0 ){
			$dataVoucher = $dataVoucher->row_array();
			$today = date("Y-m-d");
			
			if($dataVoucher['isUsed'] == 'Y'){
				echo 2;exit;
			}else
			if($dataVoucher['tanggal_expired'] < $today){
				echo 3;exit;
			}else{
				
				$applaidPayment = $this->mportal->mSimpanVoucherBayar($this->auth['kode'], $voucher);
				if($applaidPayment == 1){				
					$this->db2->where('kode_voucher', $voucher);
					$this->db2->update('tbl_voucher', array('isUsed'=>'Y'));

					$this->db2->where('kode', $this->auth['kode']);
					$this->db2->update('adis_smb_usr_pribadi', array('ayahCell'=>$hp_ortu));
					
					echo 1;
				}
			}
		}else{
			echo 4;
		}
	}
	
	function submit_gratis(){
		$voucher = $this->input->post('kode_voucher');
		$kode_gratis = base64_decode($this->input->post('kode_gratis'));
		
		
		$kode = $this->auth['kode'];
		$usr = $this->db2->query("SELECT rata_rata_rapor FROM adis_smb_usr_edu WHERE smbUsr = '$kode' ")->row_array();
		
		// print_r($usr);exit;

		if($usr['rata_rata_rapor'] < 85){
			echo 2;
			exit;
		}
		
		if($kode_gratis == ($kode.$usr['rata_rata_rapor'])){
				
			$applaidPayment = $this->mportal->mSimpanVoucherBayar($this->auth['kode'], $voucher);
			if($applaidPayment == 1){					
				echo 1;
			}
			
		}else{
			echo 2;
		}
	}
	
	function rapor(){

		$cmb = $this->mportal->mCmb($this->auth['kode']);
		$nilaiLast = array();
		$nilai = $this->db2->query("SELECT * FROM tbl_nilai_rapor WHERE nomor = '".$this->auth['nomor']."'")->result_array();
		foreach($nilai as $val){
			$nilaiLast[$val['kode']] = $val;
		}
		
		$this->smarty->assign('nilai', $nilaiLast);
		$site = "nilai_rapor";
		
		$this->smarty->assign("site", $site);
		
		$breadcum = array('pages'=>'Nilai Rapor', 'icon'=>'fa fa-home', 'url'=>'');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->display("portal/index.html");
		
	}
	
	function simpan_rapor(){
	
		
		$nomor = $this->auth['nomor'];
		$post = $this->input->post();
		
		if($post['jurusan'] == 'SMK'){
			
			$this->upload_rapor();
			
		}else{
	
			$editrapor = $this->db2->query("SELECT edit_ke FROM adis_smb_usr_edu WHERE smbUsr = '".$this->auth['kode']."' ")->row_array();
			
			
			// echo $editrapor['edit_ke'];exit;
			
			if($editrapor['edit_ke'] > 0){
				echo 7; exit;
			}
			
			for($i=1; $i<3; $i++){
				for($x=1; $x<=2; $x++){
					$this->mportal->insertRapor($nomor, 'mat', $i, $x, $post['mat_'.$i.'_'.$x]);
					$this->mportal->insertRapor($nomor, 'ing', $i, $x, $post['ing_'.$i.'_'.$x]);
					// $mat[$i][$x] = $post['mat_'.$i.'_'.$x];
					// $ing[$i][$x] = $post['ing_'.$i.'_'.$x];
				}
			}
			
			if($post['jurusan'] == 'IPA'){
			
				for($i=1; $i<3; $i++){
					for($x=1; $x<=2; $x++){
						$this->mportal->insertRapor($nomor, 'fis', $i, $x, $post['fis_'.$i.'_'.$x]);
						$this->mportal->insertRapor($nomor, 'bio', $i, $x, $post['bio_'.$i.'_'.$x]);
						$this->mportal->insertRapor($nomor, 'kim', $i, $x, $post['kim_'.$i.'_'.$x]);
					}
				}
				
			}else{
			
				for($i=1; $i<3; $i++){
					for($x=1; $x<=2; $x++){
						$this->mportal->insertRapor($nomor, 'eko', $i, $x, $post['eko_'.$i.'_'.$x]);
						$this->mportal->insertRapor($nomor, 'sos', $i, $x, $post['sos_'.$i.'_'.$x]);
						$this->mportal->insertRapor($nomor, 'geo', $i, $x, $post['geo_'.$i.'_'.$x]);
						// $eko[$i][$x] = $post['eko_'.$i.'_'.$x];
						// $sos[$i][$x] = $post['sos_'.$i.'_'.$x];
						// $geo[$i][$x] = $post['geo_'.$i.'_'.$x];
					}
				}
				
			}
			
			$respon = array();
			
			$this->db2->trans_begin();
			
			$isAda = $this->db2->query("SELECT * FROM adis_smb_usr_edu WHERe smbUsr = '".$this->auth['kode']."' ")->num_rows();
			if($isAda > 0){
				
				$this->db2->where('smbUsr', $this->auth['kode']);
				$this->db2->update('adis_smb_usr_edu', array(
					'rata_rata_rapor'=>$this->input->post('nilai_rata_rata_akhir'),
					'upload_rapor'=>1
				));
				
			}else{
				$this->db2->insert('adis_smb_usr_edu', array(
					'smbUsr'=>$this->auth['kode'],
					'kode'=>base64_encode($this->auth['kode']),
				'rata_rata_rapor'=>$this->input->post('nilai_rata_rata_akhir'),
					'upload_rapor'=>1
				));
				
			}	
			if ($this->db2->trans_status() === FALSE)
			{
				$this->db2->trans_rollback();
				$respon[] = 0;
			}
			else
			{
				$this->db2->trans_commit();
				$respon[] = 1;
			}
			
			
			/*
					
			$config['upload_path'] = './assets/upload/rapor';
			$config['allowed_types'] = 'gif|jpg|png|pdf|jpeg';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			$data['upload_data'] = '';
			
			$respon = array();
			for($k = 10; $k <= 11; $k++){
				for($s = 1; $s <=2; $s++){
					// echo 'file_rapor_'.$k.'_'.$s;
					$uploadFile = $this->upload->do_upload('file_rapor_'.$k.'_'.$s);
					if (!$uploadFile){
						echo $this->upload->display_errors();
					}else{
						$data = $this->upload->data();
						$file_name = $data['file_name'];
					
						$this->db2->trans_begin();
						$isAda = $this->db2->query("SELECT * FROM adis_smb_usr_edu WHERe smbUsr = '".$this->auth['kode']."' ")->num_rows();
						if($isAda > 0){
							
							$this->db2->where('smbUsr', $this->auth['kode']);
							$this->db2->update('adis_smb_usr_edu', array(
								'file_rapor_'.$k.'_'.$s=>$file_name,
								'rata_rata_rapor'=>$post['nilai_rata_rata_akhir'],
								'upload_rapor'=>1
							));
							
						}else{
							$this->db2->insert('adis_smb_usr_edu', array(
								'smbUsr'=>$this->auth['kode'],
								'kode'=>base64_encode($this->auth['kode']),
								'file_rapor_'.$k.'_'.$s=>$file_name,
								'rata_rata_rapor'=>$post['nilai_rata_rata_akhir'],
								'upload_rapor'=>1
							));
							
						}	
						if ($this->db2->trans_status() === FALSE)
						{
							$this->db2->trans_rollback();
							$respon[] = 0;
						}
						else
						{
							$this->db2->trans_commit();
							$respon[] = 1;
						}
					}
					
				}
			}
			*/
			
			$jalurpenerimaan = $this->db->query("SELECT jalur_penerimaan FROM adis_smb_usr WHERE kode = '".$this->auth['kode']."' ")->row_array();
			
			$konten = array( "konten" =>
					"Kepada ".$this->auth['username'].""
				  . "<br>"
				  . "<br>Terima kasih telah mengirimkan Nilai rapor anda."
				  . "<br>Nilai Anda sedang dalam proses verifikasi, "
				  . "hasil seleksi akan diumumkan pada Aplikasi SMART Universitas Bakrie."
				  . "<br>"
				  . "<br>Mohon untuk tidak membalas email ini. Informasi lebih lanjut, silahkan hubungi bagian Admisi Universitas Bakrie."
				  . "<br>"
				  . "<br>"
				  . "<br>"
				  . "<br>Terima Kasih"
				  . "<br>Best Regards"
				  . "<br>"
				  . "<br>"
				  . "<br>Panitia USM Universitas Bakrie"
				  . "<br>"
				  . "<br>"
				  . "<br>"
				  . "<br>Jl.H.R Rasuna Said Kav C-22, Kuningan Jakarta "
				  . "<br>Jakarta 12920 Indonesia"
				  . "<br>Office Ph : (021) 526 1448 "
				  . "<br>E-mail : usm@bakrie.ac.id"
			  );
			  
			// $labeljalur = ( $jalurpenerimaan['jalur_penerimaan'] == 'RAPOR') ? ' JALUR RAPOR ' : ' JALUR TES TULIS ';
			$labeljalur = ' JALUR '.$jalurpenerimaan['jalur_penerimaan'];
			  
			$subject = "Verifikasi Nilai Rapor - ".$labeljalur." ".$this->auth['username']." ";  
			$mailRespon = $this->mregistrasi->emailNotifikasi($this->auth['kode'], $subject, $konten);
			
			
			
			echo 1;
		
		}
			
		

		
			
		
	}
	
	function upload_rapor(){
		$nomor = $this->auth['nomor'];
		$post = $this->input->post();
		
		$editrapor = $this->db2->query("SELECT edit_ke FROM adis_smb_usr_edu WHERE smbUsr = '".$this->auth['kode']."' ")->row_array();
		
		
				
		$config['upload_path'] = './assets/upload/rapor';
		$config['allowed_types'] = 'gif|jpg|png|pdf|jpeg';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		$data['upload_data'] = '';
		
		$respon = array();
		for($k = 10; $k <= 11; $k++){
			for($s = 1; $s <=2; $s++){
				// echo 'file_rapor_'.$k.'_'.$s;
				$uploadFile = $this->upload->do_upload('file_rapor_'.$k.'_'.$s);
				if (!$uploadFile){
					echo $this->upload->display_errors();
				}else{
					$data = $this->upload->data();
					$file_name = $data['file_name'];
				
					$this->db2->trans_begin();
					$isAda = $this->db2->query("SELECT * FROM adis_smb_usr_edu WHERe smbUsr = '".$this->auth['kode']."' ")->num_rows();
					if($isAda > 0){
						
						$this->db2->where('smbUsr', $this->auth['kode']);
						$this->db2->update('adis_smb_usr_edu', array(
							'file_rapor_'.$k.'_'.$s=>$file_name,
							'upload_rapor'=>1
						));
						
					}else{
						$this->db2->insert('adis_smb_usr_edu', array(
							'smbUsr'=>$this->auth['kode'],
							'kode'=>base64_encode($this->auth['kode']),
							'file_rapor_'.$k.'_'.$s=>$file_name,
							'upload_rapor'=>1
						));
						
					}	
					if ($this->db2->trans_status() === FALSE)
					{
						$this->db2->trans_rollback();
						$respon[] = 0;
					}
					else
					{
						$this->db2->trans_commit();
						$respon[] = 1;
					}
				}
				
			}
		}
		
		$jalurpenerimaan = $this->db->query("SELECT jalur_penerimaan FROM adis_smb_usr WHERE kode = '".$this->auth['kode']."' ")->row_array();
		
		$konten = array( "konten" =>
				"Kepada ".$this->auth['username'].""
			  . "<br>"
			  . "<br>Terima kasih telah mengirimkan Nilai rapor anda."
			  . "<br>Nilai Anda sedang dalam proses verifikasi, "
			  . "hasil seleksi akan diumumkan pada Aplikasi SMART Universitas Bakrie."
			  . "<br>"
			  . "<br>Mohon untuk tidak membalas email ini. Informasi lebih lanjut, silahkan hubungi bagian Admisi Universitas Bakrie."
			  . "<br>"
			  . "<br>"
			  . "<br>"
			  . "<br>Terima Kasih"
			  . "<br>Best Regards"
			  . "<br>"
			  . "<br>"
			  . "<br>Panitia USM Universitas Bakrie"
			  . "<br>"
			  . "<br>"
			  . "<br>"
			  . "<br>Jl.H.R Rasuna Said Kav C-22, Kuningan Jakarta "
			  . "<br>Jakarta 12920 Indonesia"
			  . "<br>Office Ph : (021) 526 1448 "
			  . "<br>E-mail : usm@bakrie.ac.id"
		  );
		  
		$labeljalur = ( $jalurpenerimaan['jalur_penerimaan'] == 'RAPOR') ? ' JALUR RAPOR ' : ' JALUR TES TULIS ';
		  
		$subject = "Verifikasi Nilai Rapor - ".$labeljalur." ".$this->auth['username']." ";  
		$mailRespon = $this->mregistrasi->emailNotifikasi($this->auth['kode'], $subject, $konten);
		
		
		
		echo 1;
		
		
	}

	function editDataMahasiswa(){

		$kode = $this->auth['kode'];
		$uri = $this->uri->segment(3);

		if ($this->input->post("submit") == "save" && $uri == "update"){
			$this->mportal->updatePerosnalData($kode);
			redirect ('/portal', 'refresh');
		}else{

			$this->mportal->mSelectOrtu($kode);
			$this->mportal->mSelectSaudara($kode);
			$this->mportal->mSelectPendidikan($kode);

			$this->mportal->selectAgama();
			$this->mportal->selectType();
			$this->mportal->selectPropinsi();
			$this->mportal->selectKabKota();
			$this->mportal->selectKodePos();

			$site = "formEditMahasiswa";
			$this->smarty->assign("site", $site);
			$breadcum = array('pages'=>'Edit Data Pribadi', 'icon'=>'fa fa-user', 'url'=>'editDataMahasiswa');
			$this->smarty->assign("breadcum", $breadcum);
			$this->smarty->display("portal/index.html");

		}
	}

	function home(){
		$kode = $this->auth['kode'];
		// $site = 'tiles';
		$site = 'beranda';

		$cmb = $this->mportal->mCmb($kode);

		$nomorReg = $this->db2->query("SELECT nomor FROM adis_smb_form WHERE kode = '$kode'")->row();

		$this->smarty->assign("kode", $kode);
		$this->smarty->assign("nomorReg", $nomorReg->nomor);
		
		if($this->auth['jenjang'] == 1){
		
			if($cmb->jurusan != 'SMK'){
		
				if($cmb->jalur_penerimaan == 'TULIS' || $cmb->jalur_penerimaan == 'ODS'){
					if(empty($cmb->rata_rata_rapor)){
						$nilaiLast = array();
						$nilai = $this->db2->query("SELECT * FROM tbl_nilai_rapor WHERE nomor = '".$this->auth['nomor']."'")->result_array();
						foreach($nilai as $val){
							$nilaiLast[$val['kode']] = $val;
						}
						
						$this->smarty->assign('nilai', $nilaiLast);
						$site = "nilai_rapor";
					}
				}else{
					if($cmb->stsApplyPaidConfirm == 1 && empty($cmb->rata_rata_rapor)){
						$nilaiLast = array();
						$nilai = $this->db2->query("SELECT * FROM tbl_nilai_rapor WHERE nomor = '".$this->auth['nomor']."'")->result_array();
						foreach($nilai as $val){
							$nilaiLast[$val['kode']] = $val;
						}
						
						$this->smarty->assign('nilai', $nilaiLast);
						$site = "nilai_rapor";
					}
				}
			
			}
		}elseif($this->auth['jenjang'] == 2){
			$jadwalTes = $this->db->get_where('smart_jadwal_test', array('kode'=>$this->auth['kode']))->row_array();
			if($jadwalTes){
				if($cmb->stsResultConfirm == 1 && $cmb->stsResultPass == 1){
					if($cmb->stsReapplyPaid == 1){

					}else{
						if ($cmb->tolakDU != 1){
							$site = "magister/undanganPengumuman";
						}
					}
				}else{
					if($jadwalTes['is_acc'] == 1){
						$this->smarty->assign('wwn', $jadwalTes);
						$site = "magister/undanganWawancara";
					}
				}
			}

		}elseif($this->auth['jenjang'] == 3){
			$jadwalTes = $this->db->get_where('smart_jadwal_tpa', array('kode'=>$this->auth['kode']))->row_array();
			if($jadwalTes){
				if($cmb->stsResultConfirm == 1 && $cmb->stsResultPass == 1){
					if($cmb->stsReapplyPaid == 1){

					}else{
						if ($cmb->tolakDU != 1){
							$site = "kk/undanganHasil";
						}
					}
				}else{
					if($jadwalTes['is_acc'] == 1){
						$this->smarty->assign('tpa', $jadwalTes);
						$site = "kk/jadwal";
					}else{
						redirect('portal/jadwalkk');

					}
				}
			}

		}
		
		$this->smarty->assign("site", $site);
		
		$tanggalDB = $this->db->query("
			SELECT A.* FROM `adis_periode` A
			LEFT JOIN adis_periode_master B ON A.idPeriodeMaster = B.kode
			WHERE B.`status` = 1 and jenjangType='".$this->auth['jenjang']."' ;
		")->result_array();
		
		$tgl_all = array();
		$tgl_arr = array();
		
		foreach($tanggalDB as $k => $v){
			
			if(!empty($v['tanggal_pendaftaran'])){
				$tgl_arr['title'] = 'Pendaftaran Gel '.$v['sesi']; 
				$tgl_arr['start'] = $v['tanggal_pendaftaran']; 
				$tgl_arr['className'] = 'bg-info'; 
			}
			
			if(!empty($v['tanggal_tes'])){
				$tgl_arr2['title'] = 'Tes Gel '.$v['sesi']; 
				$tgl_arr2['start'] = $v['tanggal_tes']; 
				$tgl_arr2['className'] = 'bg-danger'; 
			}
			
			if(!empty($v['tanggal_pengumuman'])){
				$tgl_arr3['title'] = 'Pengumuman Gel '.$v['sesi']; 
				$tgl_arr3['start'] = $v['tanggal_pengumuman']; 
				$tgl_arr3['className'] = 'bg-success'; 
			}
			
			if(!empty($v['tanggal_daftar_ulang'])){
				$tgl_arr4['title'] = 'Terakhir Bayar Gel '.$v['sesi']; 
				$tgl_arr4['start'] = $v['tanggal_daftar_ulang']; 
				$tgl_arr4['className'] = 'bg-warning'; 
			}
			
			// $tgl_arr[]['tanggal_daftar_ulang'.$k] = $v['tanggal_daftar_ulang']; 
			
			$tgl_all[] = $tgl_arr;
			$tgl_all[] = $tgl_arr2;
			$tgl_all[] = $tgl_arr3;
			$tgl_all[] = $tgl_arr4;
		}
			
			$tgl_arr5['title'] = 'Pengisian krs'; 
			$tgl_arr5['start'] = '2019-09-09'; 
			$tgl_arr5['className'] = 'bg-info'; 
			
			$tgl_arr6['title'] = 'Pengisian krs'; 
			$tgl_arr6['start'] = '2019-09-10'; 
			$tgl_arr6['className'] = 'bg-info'; 
			
			$tgl_arr7['title'] = "UB's Week"; 
			$tgl_arr7['start'] = '2019-09-11'; 
			$tgl_arr7['className'] = 'bg-danger'; 
			
			$tgl_arr8['title'] = "UB's Week"; 
			$tgl_arr8['start'] = '2019-09-12'; 
			$tgl_arr8['className'] = 'bg-danger'; 
			
			$tgl_all[] = $tgl_arr5;
			$tgl_all[] = $tgl_arr6;
			$tgl_all[] = $tgl_arr7;
			$tgl_all[] = $tgl_arr8;
		
		// echo "<pre>";
		// print_r($tgl_all);exit;
		
		$this->smarty->assign("tgl_cal", json_encode($tgl_all));
		
		$breadcum = array('pages'=>'Dashboard', 'icon'=>'fa fa-home', 'url'=>'');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->display("portal/index.html");
	}

	function pendaftaran(){
		$kode = $this->auth['kode'];

		$cmb = $this->mportal->mCmb($kode);
		
		$year = date('Y');
		
		$minYearLulus = $year - 4;
		$harga = 0;
		
		
		if($cmb->jalur_penerimaan == 'TULIS'){
			##testulis
			if($cmb->approval_rapor == 1 && $cmb->rata_rata_rapor >= 80){
				$harga = 0;
			}else{
				$harga = 200000;
			}
		}else{
			
			$harga = 200000;
			
		}
		
		$this->smarty->assign('biaya', $harga);

		$site = "pendaftaran";
		$breadcum = array('pages'=>'Pendaftaran', 'icon'=>'fa fa-file-text-o', 'url'=>'pendaftaran');
		$this->smarty->assign("site", $site);
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->display("portal/index.html");
	}

	function payment(){
		$kode = $this->auth['kode'];

		$stsConfirm = $this->db2->query("SELECT applyBankTransferAmount, stsApplyPaid, stsApplyPaidConfirm FROM adis_smb_form WHERE kode = '$kode'")->row();
		$cmb = $this->dataCmb;
		
		$harga = $stsConfirm->applyBankTransferAmount;
		
		$this->smarty->assign('biaya', $harga);

		$this->mportal->selectType();

		if ($stsConfirm->stsApplyPaidConfirm == 0  && $stsConfirm->stsApplyPaid == 0){
			// if($harga == 0){
			// 	$kode_gratis = $kode.$cmb->rata_rata_rapor;
			// 	$this->smarty->assign('kode_gratis', base64_encode($kode_gratis));
			// 	$site = "gratis";
			// }else{
			// 	$site = "pembayaranMagister";
			// }

			$site = "pembayaranMagister";

		}else if ($stsConfirm->stsApplyPaidConfirm == 0  && $stsConfirm->stsApplyPaid == 1){

			$this->mportal->mPaidPendaftaran($kode);
			$site = "pembayaranPaid";

		}else if ($stsConfirm->stsApplyPaidConfirm == 1){

			$this->mportal->mPaidPendaftaran($kode);
			$site = "pembayaranPaid";
		}

		$this->smarty->assign("site", $site);
		
		$breadcum = array('pages'=>'Pembayaran', 'icon'=>'fa fa-tags', 'url'=>'pembayaran');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->display("portal/index.html");
	}

	function pembayaran(){
		$kode = $this->auth['kode'];

		$stsConfirm = $this->db2->query("SELECT stsApplyPaid, stsApplyPaidConfirm FROM adis_smb_form WHERE kode = '$kode'")->row();

		$cmb = $this->mportal->mCmb($kode);
		
		$year = date('Y');
		
		$minYearLulus = $year - 4;
		$harga = 0;
		
		
		if($cmb->jalur_penerimaan == 'TULIS'){
			##testulis
			if($cmb->approval_rapor == 1 && $cmb->rata_rata_rapor >= 85){
				$harga = 0;
			}else{
				$harga = 200000;
			}
		}else if($cmb->jalur_penerimaan == 'ODS'){
			##testulis
			if($cmb->approval_rapor == 1 && $cmb->rata_rata_rapor >= 90){
				$harga = 0;
			}else{
				$harga = 200000;
			}
		}else{
			
			$harga = 200000;
			
		}
		
		$this->smarty->assign('biaya', $harga);

		$this->mportal->selectType();
		
		// print_r($stsConfirm);exit;

		if ($stsConfirm->stsApplyPaidConfirm == 0  && $stsConfirm->stsApplyPaid == 0){
			if($harga == 0){
				$kode_gratis = $kode.$cmb->rata_rata_rapor;
				$this->smarty->assign('kode_gratis', base64_encode($kode_gratis));
				$site = "gratis";
			}else{
				$site = "pembayaran";
			}

		}else if ($stsConfirm->stsApplyPaidConfirm == 0  && $stsConfirm->stsApplyPaid == 1){

			$this->mportal->mPaidPendaftaran($kode);
			$site = "pembayaranPaid";

		}else if ($stsConfirm->stsApplyPaidConfirm == 1){

			$this->mportal->mPaidPendaftaran($kode);
			$site = "pembayaranPaid";
		}

		$this->smarty->assign("site", $site);
		
		$breadcum = array('pages'=>'Pembayaran', 'icon'=>'fa fa-tags', 'url'=>'pembayaran');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->display("portal/index.html");
	}

	function confirmPembayaran(){

		$kode = $this->input->post('kode');
		$setuju = $this->input->post('okekah');
		$uri = $this->uri->segment(3);
		$opt = $this->input->post('opt');

		if ($uri == "add" && $opt == "save"){
			// UPLOAD File menggunakan library upload CI
			$config['upload_path'] = './assets/upload/bukti_bayar';
			$config['allowed_types'] = 'gif|jpg|png|pdf|jpeg';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			$data['upload_data'] = '';

			$uploadFile = $this->upload->do_upload('bukti');

			if (!$uploadFile){

				echo "<script>alert('Upload GAGAL! Please :".$this->upload->display_errors()."');history.go(-1);</script>";

			}elseif (!$setuju){

				echo "<script>alert('Apakah Anda masih belum yakin dengan Data Anda? Silahkan centang jika data anda sudah benar.');history.go(-1);</script>";

			}else{

				$data = $this->upload->data();
				$file_name = $data['file_name'];
				$this->mportal->mSimpanBayar($file_name, $kode);
				$bayar = "D";

				$cmb = $this->db->query("SELECT nama FROM adis_smb_usr_pribadi WHERE kode = '$kode';")->row();
				/*
				$konten = array( "konten" =>
								"<p>Dear Bagian Keuangan,</p>"
							  . "<br>"
							  . "<p>Calon mahasiswa baru '$cmb->nama' sudah melakukan pembayaran "
							  . "biaya pendaftaran Admisi Universitas bakrie. Silahkan login ke akun Anda untuk melakukan konfirmasi :</p>"
							  . "<br>"
							  . "<br><a href='".$this->host."smb/smbPay/' class='btn' "
							  . "style='border-radius: 6px;font-family: Arial;color: #ffffff;font-size: 13px;padding: 10px 20px 10px 20px;
									text-decoration: none;background: #3498db;'>Konfirmasi Pembayaran</a></br>"
							  . "<br>"
							  . "<br>"
							  . "<br>"
							  . "<br>Terima Kasih"
							  . "<br>Best Regards"
							  . "<br>"
							  . "<br>"
							  . "<br>Admisi Universitas bakrie"
							  . "<br>"
							  . "<br>"
							  . "<br>"
							  . "<br>"
							  . "<br>"
							  . "<br>Jl. Gatot Subroto Kav. 97 Mampang"
							  . "<br>Jakarta 12790 Indonesia"
							  . "<br>Office Ph : +62 21 7918 1188 Ext 213"
							  . "<br>Office Fax : +62 21 799 3375"
							  . "<br>E-mail : smb@bakrie.ac.id"
						  );
						  */
						  
				$kontenCmb = array('konten'=>"
					Kepada ".$cmb->nama."
					<br>
					<br>Terima kasih telah mengirimkan bukti bayar pendaftaran atau memasukkan kode voucher. 
					<br>
					<br>Bukti bayar anda sedang di verifikasi, tunggu notifikasi pemberitahuan selanjutnya. 
					<br>Bila anda tidak mendapatkan notifikasi dalam 2x24 jam, silahkan menghubungi Admisi Universitas Bakrie. 
					<br>
					<br>Mohon untuk tidak membalas email ini. Jika ada sanggahan/pertanyaan, silahkan hubungi bagian Admisi Universitas Bakrie. 
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>Universitas Bakrie, Kawasan Rasuna Epicentrum
					<br>Jl.H.R Rasuna Said Kav C-22, Kuningan Jakarta 
					<br>Jakarta 12920 Indonesia
					<br>Office Ph : (021) 526 1448 
					<br>E-mail : usm@bakrie.ac.id

				");

				$this->mregistrasi->emailNotifikasi($kode, "Konfirmasi Bukti Bayar Pendaftaran - ".$cmb->nama." ", $kontenCmb);
				
				// $subject = "Pembayaran Biaya Pendaftaran Admisi - ".$cmb->nama."";  
				// $this->mregistrasi->emailNotifikasi("div.keu@bakrie.ac.id", $subject, $konten);

				$idActivity = "13.3";
				$this->activity($kode, $idActivity, $kode);
				
				if ($this->dataCmb->jenjangType == '2'){
					echo '<script>alert("Thank you for register in admission.bakrie.ac.id,\\nfor next step please 
					wait for the confirmation from the admin.\\nIf you need further information please contact our Public Relation\\nwith Lina Anggraeni 0817-6551840");</script>';
				}else{
					echo '<script>alert("Terimakasih anda telah mendaftar sebagai calon mahasiswa baru di Universitas Bakrie,\\nuntuk proses selanjutnya mohon menunggu konfrimasi selanjutnya dari admin\\n.");</script>';
				}

				redirect('/portal/pembayaran','refresh');
			}


		}
	}

	function welcome_text(){
		$site = "welcome_text";
		$data = $this->db->get('adis_perguruan')->row_array();
		$this->smarty->assign("data", $data);
		
		$breadcum = array('pages'=>'Welcome Text', 'icon'=>'fa fa-calendar', 'url'=>'welcome_text');
		$this->smarty->assign("breadcum", $breadcum);

		$this->smarty->assign("site", $site);
		$this->smarty->display("portal/index.html");

	}
	
	function jadwal_calendar(){
		
		$tanggalDB = $this->db->query("
			SELECT A.* FROM `adis_periode` A
			LEFT JOIN adis_periode_master B ON A.idPeriodeMaster = B.kode
			WHERE B.`status` = 1;
		")->result_array();
		
		$tgl_all = array();
		$tgl_arr = array();
		
		foreach($tanggalDB as $k => $v){
			
			$tgl_arr['title'] = 'Pendaftaran Gel '.$v['sesi']; 
			$tgl_arr['start'] = $v['tanggal_pendaftaran']; 
			$tgl_arr['className'] = 'bg-info'; 
			
			$tgl_arr2['title'] = 'Tes Gel '.$v['sesi']; 
			$tgl_arr2['start'] = $v['tanggal_tes']; 
			$tgl_arr2['className'] = 'bg-danger'; 
			
			$tgl_arr3['title'] = 'Pengumuman Gel '.$v['sesi']; 
			$tgl_arr3['start'] = $v['tanggal_pengumuman']; 
			$tgl_arr3['className'] = 'bg-success'; 
			
			$tgl_arr4['title'] = 'Terakhir Bayar Gel '.$v['sesi']; 
			$tgl_arr4['start'] = $v['tanggal_daftar_ulang']; 
			$tgl_arr4['className'] = 'bg-warning'; 
			
			// $tgl_arr[]['tanggal_daftar_ulang'.$k] = $v['tanggal_daftar_ulang']; 
			
			$tgl_all[] = $tgl_arr;
			$tgl_all[] = $tgl_arr2;
			$tgl_all[] = $tgl_arr3;
			$tgl_all[] = $tgl_arr4;
		}
			
			$tgl_arr5['title'] = 'Pengisian krs'; 
			$tgl_arr5['start'] = '2019-09-09'; 
			$tgl_arr5['className'] = 'bg-info'; 
			
			$tgl_arr6['title'] = 'Pengisian krs'; 
			$tgl_arr6['start'] = '2019-09-10'; 
			$tgl_arr6['className'] = 'bg-info'; 
			
			$tgl_arr7['title'] = "UB's Week"; 
			$tgl_arr7['start'] = '2019-09-11'; 
			$tgl_arr7['className'] = 'bg-danger'; 
			
			$tgl_arr8['title'] = "UB's Week"; 
			$tgl_arr8['start'] = '2019-09-12'; 
			$tgl_arr8['className'] = 'bg-danger'; 
			
			$tgl_all[] = $tgl_arr5;
			$tgl_all[] = $tgl_arr6;
			$tgl_all[] = $tgl_arr7;
			$tgl_all[] = $tgl_arr8;
		
		// echo "<pre>";
		// print_r($tgl_all);exit;
		
		$this->smarty->assign("tgl_cal", json_encode($tgl_all));
	
		$site = "calendar";
		
		$breadcum = array('pages'=>'Jadwal Perkuliahan', 'icon'=>'fa fa-file-text-o', 'url'=>'dokumen');
		$this->smarty->assign("breadcum", $breadcum);
		
		$this->smarty->assign("site", $site);
		$this->smarty->display("portal/index.html");
		
	}

	function berkaskk(){
		
		$kode = $this->auth['kode'];
		$uri = $this->uri->segment(3);
		$opt = $this->input->post("opt");

		if($opt == 'save' && $uri == 'add'){
			
			$nomor_registrasi = $this->auth['nomor'];
			
			if (!file_exists('./assets/upload/kk/'.$nomor_registrasi)) {
				mkdir('./assets/upload/kk/'.$nomor_registrasi, 0777, true);
			}

			$config['upload_path'] = './assets/upload/kk/'.$nomor_registrasi;
			$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
			
			$this->load->library('upload', $config);
			$this->upload->overwrite = true;
			$this->upload->initialize($config);

			$data['upload_data'] = '';
			
			// echo "<pre>";
			// print_r($_FILES);exit;
			$upload_status = array();
			
			foreach($_FILES as $key => $val){
				
				$uploadFile = $this->upload->do_upload($key);	
				if ($uploadFile){
					$data = $this->upload->data();
					$upload_status[$key] = 1;
					$file_name[$key] = $data['file_name'];
					
				}else{
					$upload_status = 0;
				}			
			}

			$num = $this->db2->query("SELECT * FROM tbl_smb_berkas_kk WHERE kode = '$kode'");

			if($num->num_rows() > 0){
				$num_data = $num->row_array();
				
				foreach($_FILES as $key => $val){				
					$this->db2->where('id', $num_data['id']);
					$this->db2->update('tbl_smb_berkas_kk', array(
						$key=>empty($file_name[$key]) ? null : $file_name[$key],
						'is_upload'=>1)
					);
				
				}
				
			}else{		
				$array_field =  array(
						'kode'=>$kode,
						'id_card'=>$file_name['id_card'],
						'skl'=>$file_name['skl'],
						'akta'=>$file_name['akta'],
						'kk'=>$file_name['kk'],
						'ska'=>$file_name['ska'],
						'is_upload'=>1
					);	
				$this->db2->insert('tbl_smb_berkas_kk',$array_field);
			}
			
			redirect ('/portal/berkaskk', 'refresh');

		}else{
		
			$berkas_data = $this->db2->query("SELECT * FROM tbl_smb_berkas_kk WHERE kode = '$kode'")->row_array();
			$this->smarty->assign('berkas',$berkas_data);
			
			$this->smarty->assign('nomor',$this->auth['nomor']);

			$site = "kk/dokumen";
			
			$breadcum = array('pages'=>'Kelengkapan Dokumen', 'icon'=>'fa fa-file-text-o', 'url'=>'dokumen');
			$this->smarty->assign("breadcum", $breadcum);
	
			$this->mportal->selectPropinsi();
			
			$this->smarty->assign("site", $site);
			$this->smarty->display("portal/index.html");

		}

	}

	function berkas_mm(){
		
		$kode = $this->auth['kode'];
		$uri = $this->uri->segment(3);
		$opt = $this->input->post("opt");

		if($opt == 'save' && $uri == 'add'){
			
			$nomor_registrasi = $this->auth['nomor'];
			
			if (!file_exists('./assets/upload/magister/'.$nomor_registrasi)) {
				mkdir('./assets/upload/magister/'.$nomor_registrasi, 0777, true);
			}

			$config['upload_path'] = './assets/upload/magister/'.$nomor_registrasi;
			$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
			
			$this->load->library('upload', $config);
			$this->upload->overwrite = true;
			$this->upload->initialize($config);

			$data['upload_data'] = '';
			
			// echo "<pre>";
			// print_r($_FILES);exit;
			$upload_status = array();
			
			foreach($_FILES as $key => $val){
				
				$uploadFile = $this->upload->do_upload($key);	
				if ($uploadFile){
					$data = $this->upload->data();
					$upload_status[$key] = 1;
					$file_name[$key] = $data['file_name'];
					
				}else{
					$upload_status = 0;
				}			
			}

			$num = $this->db2->query("SELECT * FROM tbl_smb_berkas_mm WHERE kode = '$kode'");

			if($num->num_rows() > 0){
				$num_data = $num->row_array();
				
				foreach($_FILES as $key => $val){				
					$this->db2->where('id', $num_data['id']);
					$this->db2->update('tbl_smb_berkas_mm', array(
						$key=>empty($file_name[$key]) ? null : $file_name[$key],
						'is_upload'=>1)
					);
				
				}
				
			}else{		
				$array_field =  array(
						'kode'=>$kode,
						'id_card'=>$file_name['id_card'],
						'cv'=>$file_name['cv'],
						'rekomendasi'=>$file_name['rekomendasi'],
						'is_upload'=>1
					);	
				$this->db2->insert('tbl_smb_berkas_mm',$array_field);
			}
			
			redirect ('/portal/berkas_mm', 'refresh');

		}else{
		
			$berkas_data = $this->db2->query("SELECT * FROM tbl_smb_berkas_mm WHERE kode = '$kode'")->row_array();
			$this->smarty->assign('berkas',$berkas_data);
			
			$this->smarty->assign('nomor',$this->auth['nomor']);

			$site = "magister/dokumen";
			
			$breadcum = array('pages'=>'Kelengkapan Dokumen', 'icon'=>'fa fa-file-text-o', 'url'=>'dokumen');
			$this->smarty->assign("breadcum", $breadcum);
	
			$this->mportal->selectPropinsi();
			
			$this->smarty->assign("site", $site);
			$this->smarty->display("portal/index.html");

		}

	}
	
	function dokumen(){
		$kode = $this->auth['kode'];
		$uri = $this->uri->segment(3);
		$opt = $this->input->post("opt");
		
		if($opt == 'save' && $uri == 'add'){
			
			$nomor_registrasi = $this->auth['nomor'];
			
			if (!file_exists('./assets/upload/pemberkasan/'.$nomor_registrasi)) {
				mkdir('./assets/upload/pemberkasan/'.$nomor_registrasi, 0777, true);
			}
			
			$config['upload_path'] = './assets/upload/pemberkasan/'.$nomor_registrasi;
			$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
			
			$this->load->library('upload', $config);
			$this->upload->overwrite = true;
			$this->upload->initialize($config);

			$data['upload_data'] = '';
			
			// echo "<pre>";
			// print_r($_FILES);exit;
			$upload_status = array();
			
			foreach($_FILES as $key => $val){
				
				$uploadFile = $this->upload->do_upload($key);	
				if ($uploadFile){
					// echo "<script>alert('Upload GAGAL!! Error :".$this->upload->display_errors()."');history.go(-1);</script>";
					$data = $this->upload->data();
					$upload_status[$key] = 1;
					// $file_name[$key] = $val['name'];
					$file_name[$key] = $data['file_name'];
					
				}else{
					$upload_status = 0;
				}			
			}
			
			$num = $this->db2->query("SELECT * FROM tbl_smb_berkas WHERE kode = '$kode'");
			
			
			
			if($num->num_rows() > 0){
				$num_data = $num->row_array();
				
				foreach($_FILES as $key => $val){				
					$this->db2->where('id', $num_data['id']);
					$this->db2->update('tbl_smb_berkas', array(
						$key=>empty($file_name[$key]) ? null : $file_name[$key],
						'is_upload'=>1)
					);
				
				}
				
			}else{		
				$array_field =  array(
						'kode'=>$kode,
						'akta'=>$file_name['akta'],
						'kk'=>$file_name['kk'],
						'id_card'=>$file_name['id_card'],
						'ktp_ortu'=>$file_name['ktp_ortu'],
						'skck'=>$file_name['skck'],
						'skbn'=>$file_name['skbn'],
						'sks'=>$file_name['sks'],
						'sktbw'=>$file_name['sktbw'],
						'rapor101'=>$file_name['rapor101'],
						'rapor102'=>$file_name['rapor102'],
						'rapor111'=>$file_name['rapor111'],
						'rapor112'=>$file_name['rapor112'],
						'is_upload'=>1
					);	
				$this->db2->insert('tbl_smb_berkas',$array_field);
			}
			
			redirect ('/portal/dokumen', 'refresh');

			

			
		}else{
			$berkas_data = $this->db2->query("SELECT * FROM tbl_smb_berkas WHERE kode = '$kode'")->row_array();
			$this->smarty->assign('berkas',$berkas_data);
			
			$this->smarty->assign('nomor',$this->auth['nomor']);
		
			if($this->auth['jenjang'] == 1 || $this->auth['jenjang'] == 3){
				$site = "dokumen";
			}else{
				$site = "magister/dokumen";
			}
			
			$breadcum = array('pages'=>'Kelengkapan Dokumen', 'icon'=>'fa fa-file-text-o', 'url'=>'dokumen');
			$this->smarty->assign("breadcum", $breadcum);

			$this->mportal->selectPropinsi();
			
			$this->smarty->assign("site", $site);
			$this->smarty->display("portal/index.html");
			
		}
		
	}
	
	function asuransi(){
		$kode = $this->auth['kode'];
		$post = $this->input->post();
		$uri = $this->uri->segment(3);
		
		if(isset($post) &&  $uri == 'add'){
			$this->mportal->mAddOrtu($this->auth['kode']);
			redirect ('/portal/asuransi', 'refresh');
		}else{
			$this->mportal->mSelectOrtu($this->auth['kode']);
		
			$site = "formAsuransi";
			
			$breadcum = array('pages'=>'Data Asuransi', 'icon'=>'fa fa-file-text-o', 'url'=>'asuransi');
			$this->smarty->assign("breadcum", $breadcum);

			$this->mportal->selectPropinsi();
			
			$this->smarty->assign("site", $site);
			$this->smarty->display("portal/index.html");
		
		}
		
	}

	function formulir(){
		$config['upload_path'] = './assets/upload/berkas';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		$kode = $this->auth['kode'];
		$this->smarty->assign("kode", $kode);

		$uri = $this->uri->segment(3);
		$id = $this->input->post('id');

		$stsConfirm = $this->db2->query("SELECT stsApplyPaid, stsApplyPaidConfirm FROM adis_smb_form WHERE kode = '$kode'")->row();

		$dataCmb = $this->mportal->mCmb($kode);
		$dataCmbArray = json_decode(json_encode($dataCmb), True);
		
		// echo "<pre>";
		// print_r($dataCmbArray);exit;		
		// $dataCmbArray = array_map("html_entity_decode", $dataCmbArray);
		// $dataCmb = html_entity_decode($dataCmb);
		// echo json_encode($dataCmbArray);exit;
		
		$this->smarty->assign('json_cmb', json_encode($dataCmbArray));

		$this->mportal->selectAgama();
		$this->mportal->selectPropinsi();
		$this->mportal->selectKabKota();
		$this->mportal->selectKodePos();
		$this->mportal->selectType();

		$this->mportal->mPaidPendaftaran($kode);

		$this->mportal->mSelectSaudara($kode);
		$this->smarty->assign('idTab', $uri);

		if ($uri == "alamat"){
			if ($id == "formAlamat"){
				$this->smarty->display("portal/formAlamat.html");
			}else{
				$this->mportal->mDetailAlamat($kode);
				$this->smarty->display("portal/detailAlamat.html");
			}
		}else if ($uri == "keluarga"){
			$this->mportal->mSelectSaudara($kode);

			if ($id == "formKeluarga"){
				ini_set('display_errors', 1);
				ini_set('display_startup_errors', 1);
				error_reporting(E_ALL);
				$this->smarty->assign('sama', 0);
				$this->smarty->display("portal/formKeluarga.html");
			}else if ($id == "tambah"){
				$kode = $this->auth['kode'];
				$uri2 = $this->uri->segment(4);
				$opt = $this->input->post("opt");
				if ($uri2 == "add"){
					$this->mportal->mAddSaudara($kode);
				}else if($uri2 == "delete" && $opt == "delete"){
					$val = $this->input->post("val");
					$this->db2->where('kode',$val);
					$this->db2->update('adis_smb_usr_kel', array('erased' => 1));
				}
				$this->mportal->mSelectSaudara($kode);
				$this->smarty->display("portal/tblKeluarga.html");
			}else if ($id == "Ortu"){
				$this->mportal->mAddOrtu($kode);

			}else if ($id == "saudara"){
				$kode = $this->auth['kode'];
				$uri2 = $this->uri->segment(4);
				if ($uri2 == "saudaraConf"){
					$this->db2->where('kode',$kode);
					$this->db2->update('adis_smb_usr_pribadi', array('statusSaudara' => 1));
				}
			}else if($id == "sama"){
				$idSts = $this->input->post('stsSama');
				$this->smarty->assign('sama', $idSts);
				$this->mportal->mDetailAlamat($kode);
				$this->smarty->display("portal/form_keluarga_new.html");
				
			}else{
				$this->mportal->mSelectOrtu($kode);
				$this->smarty->display("portal/detailKeluarga.html");
			}
		}else if ($uri == "pendidikan"){

			$this->mportal->mSelectPendidikan($kode);

			if ($id == "formPendidikan"){
				$this->smarty->display("portal/formEdu.html");
			}else if ($id == "tambah"){
				$kode = $this->auth['kode'];
				$uri2 = $this->uri->segment(4);
				$opt = $this->input->post("opt");
				$ijazah = '';
				$rapor = '';
				if ($uri2 == "add"){
					if ($dataCmb->jalur == 'JP'){
						if($_FILES['ijazah']['name']!=''){
							if (!$this->upload->do_upload('ijazah')){
								echo "<script>alert('Ijzah Gagal diuplod! Error :".$this->upload->display_errors()."');history.go(-1);</script>";
							}else{
								$data = $this->upload->data();
								$ijazah =   $data['file_name'];
							}
						}
						for($i =1;$i <= 4;$i++){
							if($_FILES['rapor'.$i]['name']!=''){
								if (!$this->upload->do_upload('rapor'.$i)){
									echo "<script>alert('Rapor ".$i." gagal di upload! Error :".$this->upload->display_errors()."');history.go(-1);</script>";
								}else{
									$data = $this->upload->data();
									$rapor[$i]=   $data['file_name'];
								}
							}
						}
					}

					$this->mportal->mAddPendidikan($kode, $ijazah, $rapor);
					$this->mportal->mSelectPendidikan($kode);
					// $this->smarty->display("portal/tblPendidikan.html");
					redirect ('/portal/formulir', 'refresh');
				}
			}else{
				$this->mportal->mDetailAlamat($kode);
				$this->smarty->display("portal/formEdu.html");
			}
		}else if ($uri == "prestasi"){
			$kode = $this->auth['kode'];
			$uri2 = $this->uri->segment(4);

			$this->mportal->mPrestasi($kode);
			$this->mportal->mOrganisasi($kode);

			if (stripos($id, "formPrestasi") !== false ){
				$this->smarty->display("portal/formPrestasi.html");
			}else if ($id == "tambahPres"){
				$file_prestasi = '';
				if ($uri2 == "add"){
					if ($dataCmb->jalur == 'JP'){
						if($_FILES['file_prestasi']['name']!=''){
							if (!$this->upload->do_upload('file_prestasi')){
								echo "<script>alert('File Gagal diuplod! Error :".$this->upload->display_errors()."');history.go(-1);</script>";
							}else{
								$data = $this->upload->data();
								$file_prestasi =   $data['file_name'];
							}
						}
					}

					$this->mportal->mAddPrestasi($kode, $file_prestasi);
					$this->mportal->mPrestasi($kode);
					$this->smarty->display("portal/tblPrestasi.html");
				}else if ($uri2 == "delete"){
					$val = $this->input->post('val');
					$this->db2->where('kode', $val);
					$this->db2->update('adis_smb_usr_prestasi',array('erased'=>1));
				}else{
					$this->mportal->mPrestasi($kode);
					$this->smarty->display("portal/tblPrestasi.html");
				}
			}else if ($id == "tambahOrg"){
				if ($uri2 == "add"){
				$this->mportal->mAddOrganisasi($kode);
				$this->mportal->mOrganisasi($kode);
				$this->smarty->display("portal/tblOrganisasi.html");
				}else if ($uri2 == "delete"){
					$val = $this->input->post('val');
					$this->db2->where('kode', $val);
					$this->db2->update('adis_smb_usr_org',array('erased'=>1));
				}else{
					$this->mportal->mOrganisasi($kode);
					$this->smarty->display("portal/tblOrganisasi.html");
				}
			}
		}else if ($uri == "konfirm" && $id = "akhir"){
			$kode = $this->input->post('kode');
			$konfirm = $this->input->post('konfirm');
			if ($konfirm == "setuju"){
				$sqlStatus = "SELECT p.statusAlamat, p.statusKeluarga, e.status
							 FROM adis_smb_usr_pribadi p
							 INNER JOIN adis_smb_usr_edu e ON p.kode = e.smbUsr
							 WHERE p.kode = '$kode'";
				$status = $this->db2->query($sqlStatus)->row();
				 if ($status->statusAlamat == 0){
					 echo "<script>alert('Form Alamat Wajib Diisi!');history.go(-1);</script>";
				 }else if($status->status == 0){
					 echo "<script>alert('Lengkapi Form Pendidikan!');history.go(-1);</script>";
				 }else{

				if ($this->dataCmb->jenjangType == 1){
					// $this->mportal->mRuangSmb($kode);
					$this->db2->where("kode", $kode);
					$this->db2->update("adis_smb_usr_pribadi", array('stsPribadiConfirm'=>1));
					
				}else{
					$this->db2->where("kode", $kode);
					$this->db2->update("adis_smb_usr_pribadi", array('stsPribadiConfirm'=>1));

					$this->db2->where("kode", $kode);
					$this->db2->update("adis_smb_form", array(
										'updateUser'=>$kode,
										'stsEventConfirm'=>1
										));
				}

					$idActivity = "13.4";
					$this->activity($kode, $idActivity, $kode);

				}
			}
			redirect ('/portal/asuransi', 'refresh');
		}else{
			
			$jenjang = $this->auth['jenjang'];
			if($jenjang == 2){
				$dbS2 = $this->db->query("SELECT A.*, B.* FROM adis_smb_usr_pribadi A 
					LEFT JOIN adis_smb_usr_edu B ON B.smbusr = A.kode 
					WHERE A.kode ='$kode' ")->row_array();
				$this->smarty->assign('status', $dbS2);
			}

			$site = ($jenjang == 1) ? "formulir" : "formulir";
			
			$breadcum = array('pages'=>'Formulir data diri', 'icon'=>'fa fa-file-text-o', 'url'=>'formulir');
			$this->smarty->assign("breadcum", $breadcum);

			$this->smarty->assign("site", $site);
			$this->smarty->display("portal/index.html");

		}
	}

	function uploadFile(){
		$config['upload_path'] = './assets/upload/berkas';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ($this->upload->do_upload('poto')){
			// echo "<script>alert('Upload GAGAL!! Error :".$this->upload->display_errors()."');history.go(-1);</script>";
			return null;
		}else{
			$data = $this->upload->data();
			$file_name =   $upload_data['file_name'];

			return $file_name;
		}

	}

	function saveAlamat(){
		$uri = $this->uri->segment(3);
		$opt = $this->input->post("opt");
		$kode = $this->input->post("kode");
		$date = date("Y-m-d H:i:s");

		if ($uri == "save" && $opt == "save"){
			$this->mportal->mSaveAlamat("", $kode, $date);
			redirect ('/portal/formulir', 'refresh');
		}
	}
	
	function saveformulirS2(){
		$uri = $this->uri->segment(3);
		$post = $this->input->post();
		if ($post['konfirm'] == "setuju"){
			if ($uri == 'save'){
				if (isset($post['kode'])){
					if($this->mportal->saveFormulirS2($post)){
						echo 1;
					}else{
						echo 0;
					}
				}
			}
		}else{
			echo 2;
		}
	}
	
	function form_guide(){		
		
		$breadcum = array('pages'=>'REGISTRATION FORM GUIDE', 'icon'=>'fa fa-info', 'url'=>'form_guide');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->assign("site", "form_guide");
		$this->smarty->display("portal/index.html");
	}
	
	function proses_seleksi(){
		
		$site = 'proses_seleksi';
		$breadcum = array('pages'=>'Proses Seleksi', 'icon'=>'fa fa-calendar', 'url'=>'jadwal');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->assign("site", $site);
		$this->smarty->display("portal/index.html");
		
	}

	function settingJadwalkk(){
		$post = $this->input->post();
		if(isset($post['kode'])){
			$nomor_registrasi = $this->auth['nomor'];

			$db_jadwal = $this->db->get_where('smart_jadwal_tpa', array('kode'=>$post['kode']));
			$row_arr = array(
				'metode_purpose'=>$post['metode_purpose'],
				'tanggal_purpose'=>date('Y-m-d', strtotime($post['tanggal_purpose'])),
				'jam_purpose'=>$post['jam_start'],
				'jam_end_purpose'=>$post['jam_end'],
				'date_created'=>date('Y-m-d H:i:s'),
				'created_by'=>$this->auth['username']
			);
			if($db_jadwal->num_rows() > 0){
				$this->db->where('kode', $post['kode']);
				$this->db->update('smart_jadwal_tpa', $row_arr);
			}else{
				$row_arr['kode'] = $post['kode'];
				$this->db->insert('smart_jadwal_tpa', $row_arr);
			}

			
			redirect('/portal/jadwalkk','refresh');
		}

	}

	function settingJadwal(){
		$post = $this->input->post();
		if(isset($post['kode'])){
			$nomor_registrasi = $this->auth['nomor'];
			
			if (!file_exists('./assets/upload/magister/'.$nomor_registrasi)) {
				mkdir('./assets/upload/magister/'.$nomor_registrasi, 0777, true);
			}
			
			$config['upload_path'] = './assets/upload/magister/'.$nomor_registrasi;
			$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
			
			$this->load->library('upload', $config);
			$this->upload->overwrite = true;
			$this->upload->initialize($config);

			$data['upload_data'] = '';
			$upload_status = array();
			
			foreach($_FILES as $key => $val){
				
				$uploadFile = $this->upload->do_upload($key);	
				if ($uploadFile){
					$data = $this->upload->data();
					$upload_status[$key] = 1;
					$file_name[$key] = $data['file_name'];
					
				}else{
					$upload_status = 0;
				}			
			}

			$db_jadwal = $this->db->get_where('smart_jadwal_test', array('kode'=>$post['kode']));
			$row_arr = array(
				'metode_purpose'=>$post['metode_purpose'],
				'tanggal_purpose'=>date('Y-m-d', strtotime($post['tanggal_purpose'])),
				'jam_purpose'=>$post['jam_purpose'],
				'fc_ijazah'=>$file_name['fc_ijazah'],
				'fc_transkrip'=>$file_name['fc_transkrip'],
				'date_created'=>date('Y-m-d H:i:s'),
				'created_by'=>$this->auth['username']
			);
			if($db_jadwal->num_rows() > 0){
				$this->db->where('kode', $post['kode']);
				$this->db->update('smart_jadwal_test', $row_arr);
			}else{
				$row_arr['kode'] = $post['kode'];
				$this->db->insert('smart_jadwal_test', $row_arr);
			}

			
			redirect('/portal/jadwalMagister','refresh');
		}

	}

	function jadwalkk(){
		$data = $this->db->get_where('smart_jadwal_tpa', array('kode'=>$this->auth['kode']))->row_array();
		$this->smarty->assign('data', $data);
		if($data){
			if($data['is_acc'] == 1){
				
				$this->smarty->assign('wwn', $data);
				$site = "kk/undanganWawancara";
			}else{
				$site = "kk/form_jadwal";
			}
		}else{
			$site = "kk/form_jadwal";
		}
		
		$breadcum = array('pages'=>'Jadwal Tes Potensi Akademik', 'icon'=>'fa fa-calendar', 'url'=>'jadwal');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->assign("site", $site);
		$this->smarty->display("portal/index.html");
	}

	function jadwalMagister(){
		$data = $this->db->get_where('smart_jadwal_test', array('kode'=>$this->auth['kode']))->row_array();
		$this->smarty->assign('data', $data);
		if($data){
			if($data['is_acc'] == 1){
				
				$this->smarty->assign('wwn', $data);
				$site = "magister/undanganWawancara";
			}else{
				$site = "magister/form_jadwal";
			}
		}else{
			$site = "magister/form_jadwal";
		}
		
		$breadcum = array('pages'=>'Jadwal Tes & Wawancara', 'icon'=>'fa fa-calendar', 'url'=>'jadwal');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->assign("site", $site);
		$this->smarty->display("portal/index.html");
	}

	function jadwal(){

		$kode = $this->auth['kode'];
		$jenjang = $this->dataCmb->jenjangType;
		
		
		$page = $this->uri->segment(3);
		
		
		// if($jenjang == 2 && $page != 'detil'){			
		// 	redirect($this->host.'portal/form_guide','refresh');
		// }

		$interview = $this->db2->query("SELECT interviewPhone FROM adis_smb_form WHERE kode = '$kode'")->row();
		$interview = $interview->interviewPhone;
		if($interview == '1'){

			$this->mportal->mJadwalInterview($kode);

			$qry = "SELECT id, tanggal, DAY(tanggal) as tanggalPhone,
					DAYNAME(tanggal) as hariPhone, MONTHNAME(tanggal) as bulanPhone, YEAR(tanggal) as tahunPhone,
					no_hp1, no_hp2, jamMulai, jamSelesai
					FROM adis_via_phone
					WHERE kodeMahasiswa = '$kode'";
			$query = $this->db2->query($qry)->row();

			$this->smarty->assign('phone',$query);
			$site = "jadwalInterview";

		}else{

			$this->mportal->mSelectJadwal($kode);
			// $site = ($jenjang == 1 ) ? "jadwal" : "jadwalPascaNew";
			$site = "jadwal";

		}
		
		$breadcum = array('pages'=>'Jadwal Seleksi', 'icon'=>'fa fa-calendar', 'url'=>'jadwal');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->assign("site", $site);
		$this->smarty->display("portal/index.html");
	}

	function seleksi(){
		$kode = $this->auth['kode'];
		if($this->auth['jenjang'] == '1'){

			$termcond = $this->db2->query("SELECT termncondition FROM adis_smb_usr WHERE kode = '$kode'")->row();
			$this->smarty->assign("termcond", $termcond->termncondition);

			$this->mportal->mHasilSeleksi($kode);

			$site = "hasilSeleksi";
		}else if($this->auth['jenjang'] == '2'){
			$site = 'magister/undanganPengumuman';
		}else if($this->auth['jenjang'] == '3'){
			$site = 'kk/undanganHasil';
		}
			
		$breadcum = array('pages'=>'Hasil Seleksi', 'icon'=>'fa fa-calendar', 'url'=>'jadwal');
		$this->smarty->assign("breadcum", $breadcum);

		$this->smarty->assign("site", $site);
		$this->smarty->display("portal/index.html");
	}

	function daftarUlang(){
		$this->load->model('msmb');
		$kode = $this->auth['kode'];
		$periode = $this->auth['periode'];

		$kodew = $this->db->query("SELECT RIGHT(bukaSmb,4) as prodi,kode, stsReapplyPaid, SUBSTR(bukaSmb,12, 2) as jalur, 
				SUBSTR(bukaSmb,6,1 ) as jenjang
				FROM adis_smb_form A 
				WHERE kode = '$kode'")->row();

		$stsConfirm = $this->db2->query("SELECT A.stsReapplyPaid, A.stsReapplyPaidConfirm , SUBSTR(A.bukaSmb,12, 2) as jalur, 
			B.kode_potongan, C.tanggalSelesai, C.tanggal_daftar_ulang, D.nama_potongan
				FROM adis_smb_form A
				LEFT JOIN adis_smb_usr_keu B ON A.kode = B.smbUsr
				LEFT JOIN adis_periode C ON LEFT(A.bukaSmb, 10) = C.kode 
				LEFT JOIN tbl_master_potongan D ON B.kode_potongan = D.kode_potongan
				WHERE A.kode = '$kode'")->row();
			
		$this->smarty->assign('keu_user', $stsConfirm);

		//$this->mportal->mCmb($kode);
		$this->msmb->mProfil($kode);

		$this->mportal->selectType();

		$qr = $this->db2->query("SELECT nama FROM adis_smb_usr_kel WHERE smbUsr = '$kode' AND erased = 0");
		$saudara = $qr->num_rows();

		$statusSaudara = $this->db->query("SELECT confirmSaudara FROM adis_smb_usr_pribadi WHERE kode = '$kode'")->row();

		$this->smarty->assign('statusSaudara', $statusSaudara->confirmSaudara);
		$this->smarty->assign('saudara',$saudara);
		$jenjang = $kodew->jenjang;
		$prodi = $kodew->prodi;

		$this->smarty->assign('statusBayar', $stsConfirm->stsReapplyPaidConfirm);
		$this->smarty->assign('periode',$periode);

		if ($stsConfirm->stsReapplyPaidConfirm == 0  && $stsConfirm->stsReapplyPaid == 0){
			$this->load->model('mmaster');
			$kode_potongan = "";
			$promo = null;		
			
			$promo = $this->mportal->mPromoByAdmin($kode);	
			if ($promo == null){				
				if(!in_array($prodi, $this->prodiTidakPromo)){
					$promo = $this->mportal->mDetectPromoNow($periode, $stsConfirm->jalur);
				}				
			}
			if ($promo){ $kode_potongan = $promo['kode_potongan'];}
			
			$detailPotongan = $this->mmaster->mSelectParameterBiaya($kode_potongan);
			
			$this->smarty->assign('promo', $promo);
			$this->smarty->assign('detil_potongan', $detailPotongan);

			$biaya = $this->db2->query("SELECT kode, nama FROM adis_pembayaran WHERE status = 1 AND erased = 0")->result();
			$this->smarty->assign('biaya',$biaya);
			$this->smarty->assign('jalur',$stsConfirm->jalur);
			// $site = "daftarUlang";
		   // echo '<script>alert("'.$stsConfirm->jalur.'");</script>';

			// $this->mportal->mPaidDaftarUlang($kode, $saudara,$jenjang,  $periode);
			// if ($stsConfirm->jalur != 'KP'){
				$this->mportal->mPayment($kode, $jenjang,  $periode , $promo);
			// }
			
			if ($stsConfirm->jalur == 'KP'){
					// $this->mportal->mPaidDaftarUlang($kode, $saudara,$jenjang,  $periode);
					$this->msmb->mProfil($kode);

					$site = "kontenDaftarUlangKM";
					$site = "daftarUlang";
			}else
			if ($stsConfirm->jalur == '10' || $stsConfirm->jalur == 'F1' || $stsConfirm->jalur == 'F2' || $stsConfirm->jalur == 'F5'
				|| $stsConfirm->jalur == 'F7' || $stsConfirm->jalur == 'I5'
				){
					$this->msmb->mProfil($kode);
					$site = "daftarUlang";
					// $site = "kontenDaftarUlangPasca";
			}else{
					$this->msmb->mProfil($kode);
					$site = "daftarUlang";
			}
			
			
			$paramBiaya = array();
			$paramBiaya['metode'] = 1;
			$paramBiaya['kodePaidType'] = $kode_potongan;
			$paramBiaya['promo_id'] = $promo['id'];
			$paramBiaya['promoByAdmin'] = $promo['promoByAdmin'];
			$this->smarty->assign('paramBiaya', base64_encode(json_encode($paramBiaya)));

		}else if ($stsConfirm->stsReapplyPaidConfirm == 0  && $stsConfirm->stsReapplyPaid == 1){

			// $this->mportal->mPaidDaftarUlang($kode, $saudara, $jenjang, $periode);
			$this->smarty->assign('jalur',$stsConfirm->jalur);
			$this->smarty->assign('periode',$periode);
//			$site = "daftarUlangPaid";
			// $site = "daftarUlangConfirmed";
			//$site = "kontenDaftarUlangConfirmed";
			      
		
			switch($jenjang){
				case '1':
					$site = "daftarUlang";	
					break;
				case '2':
					$site = "daftarUlang";
					break;			
			}
			
			$this->mportal->mPayment($kode, $jenjang,  $periode , $promo = null, true);
			$site = "daftarUlang";	
			

		}else if ($stsConfirm->stsReapplyPaidConfirm == 1){

			$this->mportal->mPaidDaftarUlang($kode, $saudara, $jenjang,  $periode);
			$this->smarty->assign('jalur',$stsConfirm->jalur);
			// $site = "daftarUlangConfirmed";
			
			switch($jenjang){
				case '1':
					$site = "daftarUlang";	
					break;
				case '2':
					$site = "daftarUlang";
					break;			
			}
			
			$this->mportal->mPayment($kode, $jenjang,  $periode , $promo = null, true);
			$site = "daftarUlang";	
		}

			
		$breadcum = array('pages'=>'Invoice', 'icon'=>'fa fa-tags', 'url'=>'daftarUlang');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->assign("site", $site);
		$this->smarty->display("portal/index.html");
	}

	
	function formDaftarUlang(){
		
		
		$this->load->model('msmb');
		$kode = $this->auth['kode'];
		$periode = $this->auth['periode'];

		$kodew = $this->db->query("SELECT RIGHT(bukaSmb,4) as prodi,kode, stsReapplyPaid, SUBSTR(bukaSmb,12, 2) as jalur, SUBSTR(bukaSmb,6,1 ) as jenjang
				FROM adis_smb_form WHERE kode = '$kode'")->row();

		$stsConfirm = $this->db2->query("SELECT A.stsReapplyPaid, A.stsReapplyPaidConfirm , SUBSTR(A.bukaSmb,12, 2) as jalur, B.kode_potongan, C.nama_potongan 
			FROM adis_smb_form A
			LEFT JOIN adis_smb_usr_keu B ON A.kode = B.smbUsr
			LEFT JOIN tbl_master_potongan C ON B.kode_potongan = C.kode_potongan
			WHERE A.kode = '$kode'")->row();
			
		$this->smarty->assign('keu_user', $stsConfirm);

		//$this->mportal->mCmb($kode);
		$this->msmb->mProfil($kode);

		$this->mportal->selectType();

		$qr = $this->db2->query("SELECT nama FROM adis_smb_usr_kel WHERE smbUsr = '$kode' AND erased = 0");
		$saudara = $qr->num_rows();

		$statusSaudara = $this->db->query("SELECT confirmSaudara FROM adis_smb_usr_pribadi WHERE kode = '$kode'")->row();

		$this->smarty->assign('statusSaudara', $statusSaudara->confirmSaudara);
		$this->smarty->assign('saudara',$saudara);
		$jenjang = $kodew->jenjang;
		$prodi = $kodew->prodi;

		$this->smarty->assign('statusBayar', $stsConfirm->stsReapplyPaidConfirm);
		$this->smarty->assign('periode',$periode);
		
		
		$this->load->model('mmaster');
		$kode_potongan = "";
		$promo = null;		
		
		$promo = $this->mportal->mPromoByAdmin($kode);	
		if ($promo == null){				
			if(!in_array($prodi, $this->prodiTidakPromo)){
				$promo = $this->mportal->mDetectPromoNow($periode, $stsConfirm->jalur);
			}				
		}
		if ($promo){ $kode_potongan = $promo['kode_potongan'];}
		
		$detailPotongan = $this->mmaster->mSelectParameterBiaya($kode_potongan);
		
		$this->smarty->assign('promo', $promo);
		$this->smarty->assign('detil_potongan', $detailPotongan);

		$biaya = $this->db2->query("SELECT kode, nama FROM adis_pembayaran WHERE status = 1 AND erased = 0")->result();
		$this->smarty->assign('biaya',$biaya);
		$this->smarty->assign('jalur',$stsConfirm->jalur);
		// $site = "daftarUlang";
	   // echo '<script>alert("'.$stsConfirm->jalur.'");</script>';

		// $this->mportal->mPaidDaftarUlang($kode, $saudara,$jenjang,  $periode);
		// if ($stsConfirm->jalur != 'KP'){
		$this->mportal->mPayment($kode, $jenjang,  $periode , $promo);
		
			
		$payment_detail = "SELECT A.reapplyBankAccountType, A.reapplyBankTransferAmount, A.reapplyBankTransferTime, B.buktiBayarDaftarUlang, 
			B.namaRekPengirimDaftarUlang, C.ibuNama, C.nomorKtp
			FROM adis_smb_form A 
			LEFT JOIN adis_smb_usr_keu B ON A.kode = B.smbUsr 
			LEFT JOIN adis_smb_usr_pribadi C ON A.kode = C.kode
			WHERE A.kode = '$kode' ";
		$payment_detail = $this->db2->query($payment_detail)->row_array();
		$this->smarty->assign('detailpay', $payment_detail);

		if ($stsConfirm->stsReapplyPaidConfirm == 0  && $stsConfirm->stsReapplyPaid == 0){
		
			$site = "formDaftarUlang";
				
		}else if ($stsConfirm->stsReapplyPaidConfirm == 0  && $stsConfirm->stsReapplyPaid == 1){
		
			$site = "formDaftarUlangPaid";
			
		}else if ($stsConfirm->stsReapplyPaidConfirm == 1){
			
			$payment_detail = "SELECT A.reapplyBankAccountType, A.reapplyBankTransferAmount, A.reapplyBankTransferTime, B.buktiBayarDaftarUlang, 
				B.namaRekPengirimDaftarUlang , C.ibuNama, C.nomorKtp
				FROM adis_smb_form A 
				LEFT JOIN adis_smb_usr_keu B ON A.kode = B.smbUsr 
				LEFT JOIN adis_smb_usr_pribadi C ON A.kode = C.kode
				WHERE A.kode = '$kode' ";
			$payment_detail = $this->db2->query($payment_detail)->row_array();
			$this->smarty->assign('detailpay', $payment_detail);
			
			$site = "formDaftarUlangPaid";
			
		}
			
		$paramBiaya = array();
		$paramBiaya['metode'] = 1;
		$paramBiaya['kodePaidType'] = $kode_potongan;
		$paramBiaya['promo_id'] = $promo['id'];
		$paramBiaya['promoByAdmin'] = $promo['promoByAdmin'];
		$this->smarty->assign('paramBiaya', base64_encode(json_encode($paramBiaya)));
		
		$breadcum = array('pages'=>'Daftar Ulang', 'icon'=>'fa fa-tags', 'url'=>'daftarUlang');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->assign("site", $site);
		$this->smarty->display("portal/index.html");
		
	}

	function formDaftarUlangKK(){
		$uri = $this->uri->segment(3);
		$post = $this->input->post();

		if($uri == 'add' && isset($post['kode'])){

			$nomor_registrasi = $this->auth['nomor'];
			
			if (!file_exists('./assets/upload/kk/'.$nomor_registrasi)) {
				mkdir('./assets/upload/kk/'.$nomor_registrasi, 0777, true);
			}
			
			$config['upload_path'] = './assets/upload/kk/'.$nomor_registrasi;
			$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			$data['upload_data'] = '';

			$uploadFile = $this->upload->do_upload('bukti');
			if (!$uploadFile){
				echo "<script>alert('Penyimpanan Gagal!! :".$this->upload->display_errors()."');history.go(-1);</script>";
			}else{
				$data = $this->upload->data();
				$post['file_name'] = $data['file_name'];
				$this->mportal->submit_kk_du($post);
			}

			header("Location: " .  $this->host."portal/formulir");
			exit;

		}

		$data = array();
		if($this->dataCmb->stsReapplyPaid == 1){
			$data_qry = "select a.*, b.kode_potongan, b.buktiBayarDaftarUlang, b.noRekPengirimDaftarUlang,  b.metodBayarDaftarUlang, 
				b.pilihan_angsuran_km, b.skema_id
				from adis_smb_form a 
				inner join adis_smb_usr_keu b on a.kode = b.smbUsr 
				where a.kode = '".$this->auth['kode']."'
			";
			$data = $this->db->query($data_qry)->row_array();
		}
		$this->smarty->assign('data', $data);

		$invoices = "select b.totalBiaya, c.potongan4x as potongan_gelombang
				from adis_smb_form a 
				inner join adis_pembayaran_kk b on left(a.bukaSmb,8) = b.periode_kode and right(a.bukaSmb,4) = b.prodi
				inner join adis_periode c on left(a.bukaSmb,10) = c.kode  
				where a.kode = '".$this->auth['kode']."'
			";
		$invoices = $this->db->query($invoices)->row_array();

		$this->smarty->assign('invoice', $invoices);

		$ref_rek = $this->db->get('smart_ref_rekening')->result_array();
		$this->smarty->assign('ref_rek', $ref_rek);

		$site = 'kk/form_du_kk';
		
		$breadcum = array('pages'=>'Daftar Ulang', 'icon'=>'fa fa-tags', 'url'=>'daftarUlang');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->assign("site", $site);
		$this->smarty->display("portal/index.html");

	}
	
	function formMagisterDu(){
		$uri = $this->uri->segment(3);
		$post = $this->input->post();

		if($uri == 'add' && isset($post['kode'])){

			$nomor_registrasi = $this->auth['nomor'];
			
			if (!file_exists('./assets/upload/magister/'.$nomor_registrasi)) {
				mkdir('./assets/upload/magister/'.$nomor_registrasi, 0777, true);
			}
			
			$config['upload_path'] = './assets/upload/magister/'.$nomor_registrasi;
			$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			$data['upload_data'] = '';

			$uploadFile = $this->upload->do_upload('bukti');
			if (!$uploadFile){
				echo "<script>alert('Penyimpanan Gagal!! :".$this->upload->display_errors()."');history.go(-1);</script>";
			}else{
				$data = $this->upload->data();
				$post['file_name'] = $data['file_name'];
				$this->mportal->submit_magister_du($post);
			}

			header("Location: " .  $this->host."portal/invoices");
			exit;

		}

		$data = array();
		if($this->dataCmb->stsReapplyPaid == 1){
			$data_qry = "select a.*, b.kode_potongan, b.buktiBayarDaftarUlang, b.noRekPengirimDaftarUlang,  b.metodBayarDaftarUlang, 
				b.pilihan_angsuran_km, b.skema_id
				from adis_smb_form a 
				inner join adis_smb_usr_keu b on a.kode = b.smbUsr 
				where a.kode = '".$this->auth['kode']."'
			";
			$data = $this->db->query($data_qry)->row_array();
		}
		$this->smarty->assign('data', $data);


		$ref_skema = $this->db->get('smart_ref_skema_biaya')->result_array();
		$this->smarty->assign('ref_skema', $ref_skema);

		$ref_rek = $this->db->get('smart_ref_rekening')->result_array();
		$this->smarty->assign('ref_rek', $ref_rek);

		$site = 'magister/form_du_magister';
		
		$breadcum = array('pages'=>'Daftar Ulang', 'icon'=>'fa fa-tags', 'url'=>'daftarUlang');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->assign("site", $site);
		$this->smarty->display("portal/index.html");

	}

	function tagihan(){
		$kode = $this->auth['kode'];
		$site = 'kk/invoice';

		$data = array();
		$data_qry = "select b.*, c.potongan4x as potongan_gelombang
				from adis_smb_form a 
				inner join adis_pembayaran_kk b on left(a.bukaSmb,8) = b.periode_kode and right(a.bukaSmb,4) = b.prodi
				inner join adis_periode c on left(a.bukaSmb,10) = c.kode  
				where a.kode = '".$this->auth['kode']."'
			";
		$data = $this->db->query($data_qry)->row_array();

		$this->smarty->assign('data', $data);
			
		$breadcum = array('pages'=>'Invoice', 'icon'=>'fa fa-tags', 'url'=>'daftarUlang');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->assign("site", $site);
		$this->smarty->display("portal/index.html");
	}

	function invoices(){
		$kode = $this->auth['kode'];
		$site = 'magister/invoice';

		$data = array();
		$data_qry = "select a.*, b.kode_potongan, b.buktiBayarDaftarUlang, b.noRekPengirimDaftarUlang,  b.metodBayarDaftarUlang, 
				b.pilihan_angsuran_km, b.skema_id, a.stsReapplyPaid
				from adis_smb_form a 
				inner join adis_smb_usr_keu b on a.kode = b.smbUsr 
				where a.kode = '".$this->auth['kode']."'
			";
		$data = $this->db->query($data_qry)->row_array();
		$this->smarty->assign('data', $data);

		$this->db->order_by('id', 'desc');
		$ref_skema = $this->db->get('smart_ref_skema_biaya')->result_array();
		$this->smarty->assign('ref_skema', $ref_skema);
			
		$breadcum = array('pages'=>'Invoice', 'icon'=>'fa fa-tags', 'url'=>'daftarUlang');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->assign("site", $site);
		$this->smarty->display("portal/index.html");
	}

	function getSkemaBiaya(){
		$post = $this->input->post();
		$kode = $post['kode'];
		$skemaId = $post['skema_id'];
		$keu = $this->db->query("select kode_potongan from adis_smb_usr_keu where smbUsr ='$kode' " )->row_array();
		if(!empty($keu['kode_potongan'])){
			$qry = " select b.pembayaran_kode, a.id, a.total, a.tagihan_1, a.potongan, a.tagihan_angsuran,
				b.tagihan_pertama, b.jumlah_angsuran
				from smart_skema_biaya_beasiswa a
				inner join `smart_skema_biaya_normal` b on a.skema_normal_id = b.id
				inner join smart_ref_skema_biaya c on b.`ref_skema_id` = c.id 
				inner join smart_ref_beasiswa d on a.nilai_potongan_percent = d.`potongan_percent` and d.kode = '".$keu['kode_potongan']."'
				where c.id = '$skemaId' ;
			";
		}else{
			$qry = "select a.*, a.tagihan_pertama as tagihan_1 
			from smart_skema_biaya_normal a
			inner join smart_ref_skema_biaya b on a.ref_skema_id = b.id 
			where b.id = '$skemaId';";
		}

		$tagihan = $this->db->query($qry)->row_array();

		$gelombang = $this->db->query('select a.potongan1x, a.potongan4x, a.potongan24x 
			from adis_periode a
			LEFT JOIN adis_periode_master b ON b.kode = a.idPeriodeMaster
			where a.status=1 and b.jenjangType = 2 ')->row_array();

		switch($skemaId){
			case 1:
				$tagihan['potongan'] = (5/100) * $tagihan['tagihan_1'];
				$tagihan['tagihan_invoice'] = $tagihan['tagihan_1']-$tagihan['potongan'];
			break;
			case 2:
				$tagihan['potongan'] = $gelombang['potongan4x'];
				$tagihan['tagihan_invoice'] = $tagihan['tagihan_1']-$tagihan['potongan'];
			break;
			case 3:
				$tagihan['potongan'] = $gelombang['potongan24x'];
				$tagihan['tagihan_invoice'] = $tagihan['tagihan_1']-$tagihan['potongan'];
			break;
		}

		echo json_encode($tagihan);
	}
	
	function pilihanPembayaran(){
		$post = $this->input->post();
		$kode = $this->auth['kode'];
		$periode = $this->auth['periode'];
		
		$opt = $post['opt'];
		$pembValue = $post['val'];
		
	}



	function detailPayment(){
		$kode = $this->auth['kode'];
		$periode = $this->auth['periode'];
		$opt = $this->input->post("opt");
		$saudara = 0;

		$pembayaran = $this->input->post("val");
		$saudara = $this->input->post("sau");

		$kodew = $this->db->query("SELECT kode, stsReapplyPaid, SUBSTR(bukaSmb,12, 2) as jalur, SUBSTR(bukaSmb,6,1 ) as jenjang
				FROM adis_smb_form WHERE kode = '$kode'")->row();
                $jenjang = $kodew->jenjang;
                $jalur = $kodew->jalur;

                $this->smarty->assign('saudara',$saudara);
                $this->smarty->assign('jalur',$jalur);

		$quer = "SELECT f.bukaSmb,p.kode, SUBSTR(bukaSmb,12, 2) as jalur,
                            CASE p.kode WHEN 1204 THEN 3 WHEN 1203 THEN 2 WHEN 1202 THEN 2 ELSE 1 END AS tipeProdi,
                            f.stsResultGrade
                            from adis_smb_form f
                            INNER JOIN adis_buka_smb b ON f.bukaSmb = b.kode
                            INNER JOIN adis_prodi p ON b.prodi = p.kode
                            WHERE f.kode ='$kode';";
		$quer = $this->db2->query($quer)->row();


		if ($jalur == 'KP'){
			$prodiType = "";
//			if ($quer->kode == '1206'){
//				$prodiType = 'IK';
//			}else if ($quer->kode == '1208'){
//				$prodiType = 'MNJ';
//			}
		}else{
			$prodiType = $quer->kode;
		}

		$this->mportal->mDaftarUlangReg($kode, $saudara, $jenjang, $pembayaran, $prodiType, $periode);
		if ($jalur == 'KP'){
			$this->smarty->display('portal/detailOngkosKM.html');
		}else{
		   $this->smarty->display('portal/detailOngkos.html');
		}

	}

	function mDetailBayar(){
		$user = $this->auth['kode'];
		$opt = $this->input->post("opt");
		$saudara = 0;

		$pembayaran = $this->input->post("val");
		$saudara = $this->input->post("sau");

		$quer = "SELECT f.bukaSmb,p.kode, SUBSTR(bukaSmb,12, 2) as jalur,
				CASE p.kode WHEN 1204 THEN 3 WHEN 1203 THEN 2 WHEN 1202 THEN 2 ELSE 1 END AS tipeProdi,
				f.stsResultGrade
				from adis_smb_form f
				INNER JOIN adis_buka_smb b ON f.bukaSmb = b.kode
				INNER JOIN adis_prodi p ON b.prodi = p.kode
				WHERE f.kode ='$user';";
		$quer = $this->db2->query($quer)->row();

		$jalur = $quer->jalur;
		if ($jalur == 'KP'){
			if ($quer->kode == '1206'){
				$prodiType = 'IK';
			}else if ($quer->kode == '1208'){
				$prodiType = 'MNJ';
			}
		}else{
			$prodiType = $quer->kode;
		}

		$rank = $quer->stsResultGrade;

		$tipeBayar = implode(".",array($prodiType,$pembayaran,$jalur));

		$qryHsl = "SELECT p.*, c.*, c.kode as code, p.kode as kode_pembayaran FROM adis_pembayaran p
			LEFT JOIN adis_cicilan c ON c.id_pembayaran = p.kode
			WHERE p.kode='$tipeBayar'";
		$biaya = $this->db2->query($qryHsl)->row();

		// echo "to :".$tipeBayar;
		//echo "Do :".$biaya->jumlahSks;
		// echo "Rank :".$rank;

		if ($rank == '1'){$total = $biaya->total1;}
		if ($rank == '2'){$total = $biaya->total2;}
		if ($rank == '3'){$total = $biaya->total3;}

		if ($rank == '1'){$uangMasuk = $biaya->uangMasuk1;}
		if ($rank == '2'){$uangMasuk = $biaya->uangMasuk2;}
		if ($rank == '3'){$uangMasuk = $biaya->uangMasuk3;}


		$sisaAng = 0;
		$totalAll = 0;
		$diskonSau = "";

		//Potongan memiliki saudara
		if ($quer->kode != '1204'){
			if ($saudara != 0){
				if ($pembayaran == 1){
					if ($rank == '1'){$diskonSau = 20/100*$biaya->uangMasuk1;}
					if ($rank == '2'){$diskonSau = 20/100*$biaya->uangMasuk2;}
					if ($rank == '3'){$diskonSau = 20/100*$biaya->uangMasuk3;}

					$total = $total - $diskonSau;
					$totalAll = $total;
				}else{
					if ($rank == '1'){$diskonSau = 20/100*$biaya->uangMasuk1;}
					if ($rank == '2'){$diskonSau = 20/100*$biaya->uangMasuk2;}
					if ($rank == '3'){$diskonSau = 20/100*$biaya->uangMasuk3;}
				}
			}
		}
		//Potongan pembayaran lunas
		$angsuran = '';
		if ($pembayaran == 1){
			if ($quer->kode != '1204'){
				if($quer->jalur != 'KP'){
					$total = $total - 1000000;
				}
			}
		}else{
			if ($quer->jalur == 'KP'){
				if ($quer->kode == '1206'){
					$totalAll = $total + $biaya->angsuran2 + $biaya->angsuran3 + $biaya->angsuran4;
				}else if($quer->kode == '1208'){
					$totalAll = $total + $biaya->angsuran2 + $biaya->angsuran3 + $biaya->angsuran4 + $biaya->angsuran6 + $biaya->angsuran6 + $biaya->angsuran7 + $biaya->angsuran8;
				}
			}else{
				if ($rank == '1'){$totalAll = ($total - $uangMasuk);}// + $biaya->angsuran1_1;}
				if ($rank == '2'){$totalAll = ($total - $uangMasuk);}// + $biaya->angsuran1_2;}
				if ($rank == '3'){$totalAll = ($total - $uangMasuk);}// + $biaya->angsuran1_3;}

				if ($rank == '1'){
					$angsuran = array(
									'angsuran1'=>$biaya->angsuran1_1,
									'angsuran2'=>$biaya->angsuran2_1,
									'angsuran3'=>$biaya->angsuran3_1 - $diskonSau,
								);
				}
				if ($rank == '2'){
					$angsuran = array(
									'angsuran1'=>$biaya->angsuran1_2,
									'angsuran2'=>$biaya->angsuran2_2,
									'angsuran3'=>$biaya->angsuran3_3 - $diskonSau,
								);
				}
				if ($rank == '3'){
					$angsuran = array(
									'angsuran1'=>$biaya->angsuran1_3,
									'angsuran2'=>$biaya->angsuran2_3,
									'angsuran3'=>$biaya->angsuran3_3 - $diskonSau,
								);
				}

				$sisaAng = $angsuran['angsuran1']+$angsuran['angsuran2']+($angsuran['angsuran3']- $diskonSau);
			}
		}
		$this->smarty->assign('sau', $saudara);
		$this->smarty->assign('saudara', $saudara);
		$this->smarty->assign('uangMasuk', $uangMasuk);
		$this->smarty->assign('diskonSau', $diskonSau);
		$this->smarty->assign('total', $total);
		$this->smarty->assign('totalbayar', $totalAll);
		$this->smarty->assign('biaya', $biaya);
		$this->smarty->assign('angsuran',$angsuran);
		$this->smarty->assign('method',$pembayaran);
		$this->smarty->assign('sisaAng',$sisaAng);
		$this->smarty->assign('prodi',$quer->kode);
		$this->smarty->assign('jalur',$quer->jalur);
		$this->smarty->assign('prodiType',$tipeBayar);
		$this->smarty->display('portal/detailOngkos.html');
	}
	
	function mGenTagihan($kode = '', $count = 5, $nomor_tagihan = '11801004'){
		
		$numCurrent = 1;
		
		if($count > 0){
			for($i = 0; $i < $count; $i++){				
				echo $nomor_tagihan_count = $nomor_tagihan . str_pad($numCurrent, 2, 0, STR_PAD_LEFT); echo '<br>';
				$numCurrent++;
			}
			
		}
		
	}

	function confirmDaftarUlang(){
		$this->load->model('mintegrasi');

		$kode = $this->input->post('kode');
		$uri = $this->uri->segment(3);
		$opt = $this->input->post('opt');
		$kode_jalur = $this->input->post('kode_jalur');
		$tipePembayaran = $this->input->post("typeTrans");
		$file_name = '';

		// $namaIbu = $this->input->post('nama_ibu');$noKtp = $this->input->post('no_ktp');
		// if(empty($namaIbu) || empty($noKtp)){
		// 	echo "<script>alert('Nama Ibu dan No KTP tidak boleh kosong!');history.go(-1);</script>";
		// 	exit;
		// }
		
		$kontenCmb = array('konten'=>"
			Kepada ".$this->auth['username']."
			<br>
			<br>Terima kasih telah mengirimkan bukti bayar daftar ulang. 
			<br>
			<br>Bukti bayar anda sedang di verifikasi, tunggu notifikasi pemberitahuan selanjutnya. 
			<br>Bila anda tidak mendapatkan notifikasi dalam 2x24 jam, silahkan menghubungi Admisi Universitas Bakrie. 
			<br>
			<br>Mohon untuk tidak membalas email ini. Jika ada sanggahan/pertanyaan, silahkan hubungi bagian Admisi Universitas Bakrie. 
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>Universitas Bakrie, Kawasan Rasuna Epicentrum
			<br>Jl.H.R Rasuna Said Kav C-22, Kuningan Jakarta 
			<br>Jakarta 12920 Indonesia
			<br>Office Ph : (021) 526 1448 
			<br>E-mail : usm@bakrie.ac.id

		");

		$konten = array( "konten" =>
				"Dear Bagian Keuangan,"
			  . "<br>"
			  . "<br>Peserta Seleksi Mahasiswa Baru '".$this->auth['username']."' Sudah melakukan pembayaran biaya "
			  . "Daftar Ulang Admisi Universitas bakrie. Silahkan login ke akun Anda untuk melakukan konfirmasi pembayaran."
			  . "<br>"
			  . "<br><a href='".$this->host."smb/smbPay/' class='btn' "
			  . "style='border-radius: 6px;font-family: Arial;color: #ffffff;font-size: 13px;padding: 10px 20px 10px 20px;
					text-decoration: none;background: #3498db;'>Konfirmasi Daftar Ulang</a></br>"
			  . "<br>"
			  . "<br>"
			  . "<br>"
			  . "<br>Terima Kasih"
			  . "<br>Best Regards"
			  . "<br>"
			  . "<br>"
			  . "<br>Panitia SMB Universitas bakrie"
			  . "<br>"
			  . "<br>"
			  . "<br>"
			  . "<br>"
			  . "<br>"
			  . "<br>Jl. Gatot Subroto Kav. 97 Mampang"
			  . "<br>Jakarta 12790 Indonesia"
			  . "<br>Office Ph : +62 21 7918 1188 Ext 213"
			  . "<br>Office Fax : +62 21 799 3375"
			  . "<br>E-mail : smb@bakrie.ac.id"
				  );

		if ($uri == "add" && $opt == "save"){
			// UPLOAD File menggunakan library upload CI
			$config['upload_path'] = './assets/upload/bukti_bayar';
			$config['allowed_types'] = 'gif|jpg|png|pdf|jpeg';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			$data['upload_data'] = '';

			$uploadFile = $this->upload->do_upload('bukti');
			if (!$uploadFile){
				echo "<script>alert('Penyimpanan Gagal!! :".$this->upload->display_errors()."');history.go(-1);</script>";
			}else{
				$data = $this->upload->data();
				$file_name = $data['file_name'];
				$this->mportal->mSimpanDaftarUlang($file_name, $kode, $this->auth['jenjang'], $kode_jalur);
				$bayar = "DU";				
				
				// $subject = "Pembayaran Biaya Daftar Ulang Admisi - ".$this->auth['username']."";  
				// $this->mregistrasi->emailNotifikasi("div.keu@bakrie.ac.id", $subject, $konten);

				$this->mregistrasi->emailNotifikasi($kode, "Konfirmasi Bukti Bayar Daftar Ulang - ".$this->auth['username']." ", $kontenCmb);
				echo "<script>alert('Pendaftaran Ulang Berhasil Disimpan!');</script>";
			}

			header("Location: " .  $this->host."portal/formulir");
			// redirect('/portal/daftarUlang','refresh');

		}
	}

	// function generateNIM($kode){
			// $val = $this->db->query("SELECT nomor FROM adis_smb_form WHERE kode = '$kode';")->row();
			// $val = $val->nomor;

			// $sql  = "set @_bukaSmb:=(SELECT bukaSmb FROM adis_smb_form WHERE nomor = '$val');";
			// $sql2 = "set @_tahun:=left(@_bukasmb,4);";
			// $sql3 = "set @_tahun_yy:=right(@_tahun,2);";
			// $sql4 = "set @_jenjang:=right(left(@_bukasmb,6),1);";
			// $sql5 = "set @_periode:=left(@_bukasmb,8);";
			// $sql6 = "set @_sesi:=right(left(@_bukasmb,8),1);";
			// $sql7 = "set @_prodi:=right(@_bukasmb,4); ";
			// $sql8 = "set @_nimOrder:=(select max(nimOrder) from adis_smb_form where bukaSmb=@_bukasmb);";
			// $sql9 = "set @_nim:=concat(@_jenjang,@_tahun_yy,@_sesi,right(@_prodi,2),LPAD((@_nimOrder+1),3,0));";
			// $sql10 = "update adis_smb_form set nimOrder = (@_nimOrder+1) where nomor='$val';";

			// $this->db2->query($sql);
			// $this->db2->query($sql2);
			// $this->db2->query($sql3);
			// $this->db2->query($sql4);
			// $this->db2->query($sql5);
			// $this->db2->query($sql6);
			// $this->db2->query($sql7);
			// $this->db2->query($sql8);
			// $this->db2->query($sql9);
			// $this->db2->query($sql10);

			// $qry = "UPDATE adis_smb_form SET nim  = @_nim WHERE nomor='$val'";
			// $this->db2->query($qry);
	// }

	function profil(){
		$kode = $this->auth['kode'];
		$this->mportal->mProfil($kode);
		$this->mportal->mSelectOrtu($kode);
		$this->mportal->mSelectPendidikan($kode);
		$this->mportal->mSelectSaudara($kode);
		$this->mportal->mPrestasi($kode);
		$this->mportal->mOrganisasi($kode);
		$site = "profil";
		$this->smarty->assign("site", $site);

		$breadcum = array('pages'=>'Profil mahasiswa', 'icon'=>'fa fa-user', 'url'=>'profil');
		$this->smarty->assign("breadcum", $breadcum);
		$this->smarty->display("portal/index.html");
	}

	function editProfil(){
		$this->load->library('encrypt');
		$uri = $this->uri->segment(4);
		$val = $this->input->post('val');
		$opt = $this->input->post('opt');
		$kode = $this->input->post('kode');

		if (!$uri && $opt == 'edit'){
			$this->smarty->assign('val',$val);
			$this->smarty->display('portal/formFoto.html');
		}else if($uri == 'save' && $kode != '' && $opt == 'passW'){

			$user = $this->db->query("SELECT password FROM adis_smb_usr WHERE kode = '$kode'")->row();
			$pass = $this->encrypt->decode($user->password);
			$oldPass = $this->input->post('oldPass');
			$newPass = $this->input->post('newPass');
			if($oldPass != $pass){
				echo "<script>alert('Password Yang Anda Masukkan Salah!');history.go(-1);</script>";
			}else {
				$newPass = $this->encrypt->encode($newPass);
				$this->db->where('kode',$kode);
				$this->db->update('adis_smb_usr', array('password'=>$newPass));
			}

			redirect ('/portal/profil', 'refresh');

		}else if ($uri == 'do'&& $opt == 'foto'){
			$kode = $this->auth['kode'];

			$config['upload_path'] = './assets/upload/foto';
			$config['allowed_types'] = 'gif|jpg|png';
			// $config['max_size']    = '10000';
			// $config['max_width']  = '1000';
			// $config['max_height']  = '1200';
			$config['min_height']  = '400';
			$config['min_width']  = '200';

			$config['file_name'] = $kode.'.jpg';

			$this->load->library('upload', $config);

			$this->upload->initialize($config);

			// $this->upload->set_allowed_types('*');

			$data['upload_data'] = '';

			$uploadFile = $this->upload->do_upload('poto');

			if (!$uploadFile){

				echo "<script>alert('Upload GAGAL!! Error :".$this->upload->display_errors()."');history.go(-1);</script>";

			}else{

			$data = $this->upload->data();
			$file_name = $data['file_name'];
			$this->db->where('kode', $kode);
			$this->db->update('adis_smb_usr_pribadi', array('foto'=>$file_name));

			}

			if(empty($this->input->post('page'))){
				redirect ('/portal', 'refresh');
			}else{
				if($this->auth['jenjang'] == 1){
					redirect ('/portal/dokumen', 'refresh');
				}else if($this->auth['jenjang'] == 2){
					redirect ('/portal/berkas_mm', 'refresh');
				}else if($this->auth['jenjang'] == 3){
					redirect ('/portal/berkaskk', 'refresh');
				}
			}
		}
	}

	function selectKabKota(){
		$propinsi = $this->input->post("propinsi");

		$prop = $this->db2->get_where('adis_wil', array('parent'=>$propinsi))->result();
		$view = '	{foreach name=lope from=$prop item=row}
						<option value="{$row->kode}">{$row->nama}</option>
					{/foreach}';

		$this->smarty->assign('prop',$prop);
		$this->smarty->display('string:'.$view);
	}

	function selectKodePos(){
		$kab = $this->input->post("kabkota");

		$kab = $this->db2->query("SELECT * FROM adis_kodepos WHERE kabupatenkota = '".$kab."' ORDER BY kodepos ASC;")->result();
		$view = '	{foreach name=lope from=$kab item=row}
						<option value="{$row->kode}">{$row->kodepos}</option>
					{/foreach}';

		$this->smarty->assign('kab',$kab);
		$this->smarty->display('string:'.$view);
	}

	function activity($email, $idActivity, $user){
		$kode = uniqid();
		$date = date("Y-m-d H:i:s");
		$activity = $this->db2->query("SELECT nama, kode FROM adis_type WHERE kode = '$idActivity'")->row();
		$this->db2->insert('adis_activity', array('kode'=>$kode,
								'id_activity'=>$activity->kode,
								'created_date'=>$date,
								'created_user'=>$user,
								'nama_activity'=>$activity->nama,
								'status_activity'=>0,
								'id_cmb'=>$email
								));
	}

	function formulirReport(){

		// $kode = $this->uri->segment(3);
            $kode = $this->auth['kode'];

		$kode = $this->db2->query("SELECT kode, stsEventConfirm FROM adis_smb_form WHERE nomor = '$kode'")->row();

		if ($kode->stsEventConfirm == '0') {
			// What happens when the CAPTCHA was entered incorrectly
			echo '<script>alert("Calon Mahasiswa belum melengkapi Data Diri, Silahkan Kembali!");history.go(-1);</script>';
			// die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
				 // "(reCAPTCHA said: " . $resp->error . ")");
		}else{


			$kode = $kode->kode;
			$this->mportal->selectType();
			$this->mportal->mProfil($kode);
			$this->mportal->mSelectPendidikan($kode);
			$this->mportal->mOrganisasi($kode);
			$this->mportal->mPrestasi($kode);
			$this->mportal->mSelectOrtu($kode);
			$this->mportal->mSelectSaudara($kode);

			$site = "kontenFormulir";
			$this->smarty->assign("site", $site);
			$breadcum = array('pages'=>'Formulir mahasiswa', 'icon'=>'fa fa-user', 'url'=>'formulirReport');
			$this->smarty->assign("breadcum", $breadcum);
			$this->smarty->display("portal/index.html");

		}
	}

        function lampiran($p=""){
            $kode = $this->auth['kode'];
            $this->smarty->assign('host',$this->host);

            if ($p == 1){
                $title = "Lampiran 1";
				$page = "lampiran";
                $lamp = "";
                $header = '';
                $this->smarty->assign('header',$header);
                $this->smarty->assign('lamp',$lamp);
            }else if ($p == 2){
                $title = "Lampiran 2";
				$page = "lampiran2";
                $lamp =   "";
                $this->smarty->assign('lamp',$lamp);

                $header = '';
                $this->smarty->assign('header',$header);
            }else if ($p == 3){
                $title = "Lampiran 3";
				$page = "lampiran3";
                $lamp = "";
                $this->smarty->assign('lamp',$lamp);

                $header = '';

                $this->smarty->assign('header',$header);
            }else if ($p == 4){
                $title = "";
				$page = "lampiran4";
                $lamp = "";
                $this->smarty->assign('lamp',$lamp);

                $header = '';

                $this->smarty->assign('header',$header);
            }else if ($p == 5){
				$this->mportal->mProfil($kode);

				$dataBerkas = $this->db2->query("SELECT * FROM tbl_kelengkapan_berkas WHERE kode_cmb = '$kode' ")->num_rows();
				$masterBerkas = $this->db2->query("SELECT * FROM tbl_master_berkas")->result_array();
				if($dataBerkas > 0 ){
					$infoBerkas = $this->db2->query("SELECT * FROM `tbl_master_berkas` A
						INNER JOIN tbl_kelengkapan_berkas B ON A.id_berkas = B.id_berkas
						WHERE kode_cmb = '$kode';")->result_array();
					$this->smarty->assign('infoBerkas',$infoBerkas);
				}
                $title = "";
				$page = "lampiran5";
                $lamp = "";
                $this->smarty->assign('lamp',$lamp);
                $this->smarty->assign('dataBerkas',$dataBerkas);
                $this->smarty->assign('masterBerkas',$masterBerkas);

                $header = '';

                $this->smarty->assign('header',$header);
            }else if ($p == 'agrreed'){
                $val = $this->uri->segment(4);
                if ($val == 1){
                    $this->db2->where('kode', $kode);
                    $this->db2->update('adis_smb_usr', array('termncondition'=>1));
//                    redirect ('/portal/daftarUlang', 'refresh');
                    return 1;
                }
            }
		$this->smarty->assign('title', $title);
		$this->smarty->assign('page', $page);
		$this->smarty->display("pendaftaran/index.html");
        }

		function listAngsuran(){

            $post = $this->input->post();
            $opt = $post['opt'];
            $pilihan = $post['val'];
            $kodebayar = $post['kodebayar'];
			$kode_potongan = $post['kode_potongan'];
            $periode = $this->auth['periode'];

            $qry = "SELECT p.*, pr.singkatan as alias_prodi, pr.nama as prodi, p.kode as code
                        FROM adis_pembayaran_kelasmalam p
                        LEFT JOIN adis_prodi pr ON p.prodiTipe = pr.kode
                        WHERE p.kode = '$kodebayar' AND p.periode_kode = '$periode'";
            $sql = $this->db2->query($qry)->result_array();
			
			if($kode_potongan != ""){
				$listPotongan = $this->mportal->selectPotonganMaster($kodebayar, $kode_potongan, $periode);
			}

            $arry = array();
            $response = array();
            $arryPil = array();

            foreach($sql as $val){
                $arry['uangMasuk'] = $val['uangMasuk'];
                $arry['spp'] = $val['spp'];
                $arry['jumlahSks'] = $val['jumlahSks'];
                $arry['tempoAngsuran'] = $val['tempoAngsuran'];
                $arry['biayaPerSks'] = $val['biayaPerSks'];
                $arry['pilihanBiayaUM'] = json_decode($val['pilihanBiayaUM']);
				if(!$kode_potongan){
					$arry['biayaPembeda'] = json_decode($val['biayaPembeda']);
					$arry['totalBiaya'] = ($val['uangMasuk']+$val['spp'])+($val['jumlahSks']*$val['biayaPerSks']);
				}else{
					$arry['biayaPembeda'] = json_decode($listPotongan[0]['biayaPembedaBeasiswa']);
					$uangMasuk = $val['uangMasuk'];
					$spp = $val['spp'];
					$sks = $val['jumlahSks']*$val['biayaPerSks'];
					$bpp = 0;
					
					foreach($listPotongan as $nilai){
						switch ($nilai['kode_biaya']){
							case 'UM':
								if($nilai['lambang'] == '%'){
									$uangMasuk = $val['uangMasuk'] - ($val['uangMasuk']*($nilai['nilai']/100));	
								}else{
									$uangMasuk = $val['uangMasuk']-$nilai['nilai'];	
								}
							break;
							case 'SPP':
							break;
							case 'SKS':
							break;
							case 'BPP':
							break;
						} 
					}
					
					$arry['totalBiaya'] = $uangMasuk + $spp + $sks + $bpp;
				}

                foreach(json_decode($val['pilihanBiayaUM']) as $k => $v){
                    $arry2['biayaAngsur'.$k] = $arry['totalBiaya'] - $v - $arry['biayaPembeda'][$k];
                    for($i = 0; $i < $val['tempoAngsuran']-1; $i++){
                        $arryAngsur[$i] = $arry2['biayaAngsur'.$k]/($val['tempoAngsuran']-1);
                    }
                    $arryPil[$k] = $arryAngsur;
                }
            }

            $arrTglTempo = array();
            $sqlTglTempo = $this->db2->query("SELECT * FROM tbl_tgl_tempo_kelasmalam "
                    . " WHERE kode_pembayaran = '$kodebayar' AND kode_periode = '$periode';")->row_array();
            $arrTglTempo = json_decode($sqlTglTempo['tgl_jatuh_tempo']);

            $keyPil = array_search($pilihan, $arry['pilihanBiayaUM']);
//            print_r($keyPil);exit;

            $this->smarty->assign('resTglTempo', $arrTglTempo);
            $response[] = $arry;
            $this->smarty->assign('sql', $response);
            $this->smarty->assign('pilih', $arryPil);
            $this->smarty->assign('keypilih', $keyPil);
            $this->smarty->assign('choose', $pilihan);
            $this->smarty->display('portal/listAngsuran.html');
        }

		function survey(){
			$qryQuest = $this->db2->query("SELECT * FROM tbl_pertanyaan_survei")->result_array();
			$this->smarty->assign('quest', $qryQuest);

			$qryAns = $this->db2->query("SELECT A.*, B.kriteria_jawaban as pilihan  FROM
					tbl_jawaban_survei A
					LEFT JOIN tbl_kriteria_survey B ON A.jawaban = B.id ")->result_array();
			$this->smarty->assign('answer', $qryAns);

			$qryKriteria = $this->db2->query("SELECT * FROM tbl_kriteria_survey")->result_array();
			$this->smarty->assign('kriteria', $qryKriteria);

			$this->smarty->assign('title', "FORM SURVEY ADMISSION");
			$this->smarty->assign('page', 'survey/form_survey');
			$this->smarty->display("pendaftaran/index.html");
		}
		
	function detilTagihan($paid = false, $promobyAdmin = 0){
		setlocale (LC_TIME, 'id_ID');
		$post = $this->input->post();		
		$periode = $post['periode'];
		$jenjang = $post['jenjang'];
		$kode_promo = $post['kode_promo'];
		$jalur = (isset($post['jalur']) ? $post['jalur'] : null);
		
		
		$periodeQry = "SELECT tanggalMulai, tanggalSelesai FROM adis_periode_master A WHERE kode = '$periode';";
		$periodeQry = $this->db2->query($periodeQry)->row_array();
		
		if($post['option'] == 'metodBayar'){	
		
			$biaya = $this->mportal->biayaUtama($periode, $post['kodePaidType'], $jenjang, $jalur);
			$promo = null;		
					
			if(!in_array($this->auth['prodi'], $this->prodiTidakPromo)){
				$promo = $this->mportal->mDetectPromoNow($periode, $post['jalur'], $kode_promo, $paid, $promobyAdmin);
			}	
			
			$biayaSaled = $this->mportal->biayaDiskon($biaya, $promo);
			$listBiaya = array();
			
			$first_due_date = $promo['tanggal_akhir'];
			
			$timeAngsur = date("Y-m-d",strtotime($periodeQry['tanggalMulai']." +9 days"));
			
			$semester = ($jenjang == 2 ? $biaya['jumlahSemester'] : 1);
			$x = -1;
			for($i = 0; $i < $post['metode']; $i++){
				if($i == 0){
					$listBiaya[$i]['tagihan'] = (($biayaSaled['biayaSemester1']-$biayaSaled['BPP'])/$post['metode'])+$biayaSaled['BPP'];
				}else{
					$listBiaya[$i]['tagihan'] = ($biayaSaled['biayaSemester1']-$biayaSaled['BPP'])/$post['metode'];
				}
				## tanggal metode lama, sedang tidak digunakan
				/*
				if ($post['metode'] == 2 && $i >0){
					$listBiaya[$i]['tglTempo'] = date("Y-m-d", strtotime($timeAngsur." 3 month"));
				}else{
					$listBiaya[$i]['tglTempo'] = date("Y-m-d", strtotime($timeAngsur." +$i month"));
				}
				*/			
				
				if ($post['metode'] == 2 && $i >0){
					$listBiaya[$i]['tglTempo'] = date("Y-m-d", strtotime($timeAngsur));
				}else{
					$listBiaya[$i]['tglTempo'] = date("Y-m-d", strtotime($timeAngsur." +$x month"));
				}
				
				$x++;
			}
				
			
			$biaya_du_kp = 0;
			if($jalur == 'KP'){
				
				$pembeda = $this->db->query("SELECT * FROM tbl_pilihan_potongan_biaya 
					WHERE kode_pembayaran = '".$biaya['kode']."' 
						AND kode_potongan = '".$promo['kode_potongan']."' 
						AND kode_periode = '".$promo['kode_periode']."' ")->row_array();				
				$biayaPembeda = json_decode($pembeda['biayaPembedaBeasiswa']);
				
				
				if($pembeda['biayaAngsuran'] != ''){
					$perbulanfee = $pembeda['biayaAngsuran'];
				}else{
					$biaya_du_kp = json_decode($biaya['pilihanBiayaUM']);
					
					$totalkp = $biayaSaled['total1'] - $biayaPembeda[0];
					$perbulanfee = $totalkp / ($biaya['tempoAngsuran']-1);
				
				}
				
				for($i=0;$i <= 5; $i++){
					$listBiaya[$i]['tagihan'] = $perbulanfee;
					$listBiaya[$i]['tglTempo'] = date("Y-m-d", strtotime($timeAngsur." +$i month"));
				}
			}
			
			// echo '<pre>';
			// echo $biaya['tempo'];
			// print_r($biaya);
			// print_r($promo);
			// print_r($biayaSaled);
			// print_r($listBiaya);
			// exit;
						
			if($jalur != 'KP'){
				if($promo != null){
					if($promo['tanggal_akhir'] != null){
						$listBiaya[0]['tglTempo'] = $first_due_date; 
					}
				}
			}
			
			$paramBiaya = array();
			$paramBiaya['metode'] = $post['metode'];
			$paramBiaya['kodePaidType'] = $post['kodePaidType'];
			$paramBiaya['promo_id'] = $promo['id'];
			$paramBiaya['promoByAdmin'] = $promo['promoByAdmin'];
			$this->smarty->assign('paramBiaya', base64_encode(json_encode($paramBiaya)));
			
			$tagihanpertama = ($jalur == 'KP' ? $biaya_du_kp[0] : $listBiaya[0]['tagihan']);			
			$this->smarty->assign('tagihanPertama', $tagihanpertama);
			$this->smarty->assign('listBiaya', $listBiaya);
			$this->smarty->assign('dataAngsuran', base64_encode(json_encode($listBiaya)));
			$this->smarty->display('portal/tabelNilaiTagihan.html');
		}
		
	}
}
