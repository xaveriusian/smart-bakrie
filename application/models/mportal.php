<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

class Mportal extends CUTI_Model{

	function __construct(){
		parent::__construct();
	}

	function Mportal(){
		parent::__construct();

		$this->db2->query("SET lc_time_names = 'id_ID'");
	}

	#untuk melihat kondisi ketika promo ditentukan berdasrkan periode gelombang potongan
	function mDetectPromoNow($periode = "", $jalur = "", $kode = "", $done = false, $promoByAdmin = 0){

		if($kode != ""){
			if($done){
				if($promoByAdmin == 0){
					##QUery untuk mengambil data setelah calon mahasiswa memilih
					$sql = "SELECT A.id, A.kode_potongan, A.kode_periode, A.kode_jalur, A.tanggal_akhir, A.tanggal_mulai,
						B.nama_potongan, 0 as 'promoByAdmin'
						FROM tbl_potongan_periode A LEFT JOIN tbl_master_potongan B ON A.kode_potongan = B.kode_potongan
						WHERE A.kode_potongan = '$kode'";
				}else{
					##QUery untuk mengambil data beasiswa dengan ketentuan admin dan sudah membayar
					$sql = "SELECT A.id, A.kode_potongan, B.kode_periode, B.kode_jalur,  B.tanggal_akhir, B.tanggal_mulai,
						A.nama_potongan, 1 as 'promoByAdmin'
						FROM tbl_master_potongan A
						LEFT JOIN `tbl_potongan_periode` B ON A.kode_potongan = B.kode_potongan
						LEFT JOIN adis_smb_usr_keu C ON C.kode_potongan = A.kode_potongan
						WHERE A.kode_potongan = '$kode'";

				}
			}else{
				if($promoByAdmin == 0){
					##QUery untuk mengambil data setelah calon mahasiswa memilih
					$sql = "SELECT A.id, A.kode_potongan, A.kode_periode, A.kode_jalur, A.tanggal_akhir, A.tanggal_mulai,
						B.nama_potongan, 0 as 'promoByAdmin'
						FROM tbl_potongan_periode A LEFT JOIN tbl_master_potongan B ON A.kode_potongan = B.kode_potongan
						WHERE A.kode_potongan = '$kode'";

				}else{
					##QUery untuk mengambil data beasiswa dengan ketentuan admin
					$sql = "SELECT A.id, A.kode_potongan, B.kode_periode, B.kode_jalur,  B.tanggal_akhir, B.tanggal_mulai,
						A.nama_potongan, 1 as 'promoByAdmin'
						FROM tbl_master_potongan A
						LEFT JOIN `tbl_potongan_periode` B ON A.kode_potongan = B.kode_potongan
						LEFT JOIN adis_smb_usr_keu C ON C.kode_potongan = A.kode_potongan
						WHERE A.kode_potongan = '$kode'";
				}
			}
		}else{
		##QUery untuk melakukan mengambil data seblum calon mahasiswa memilih dan berdasarkan gelombang
			$sql = "SELECT A.id,  A.kode_potongan, A.kode_periode, A.kode_jalur, A.tanggal_akhir,
				A.tanggal_mulai, B.nama_potongan , 0 as 'promoByAdmin',
				DATE(NOW())
				FROM `tbl_potongan_periode` A
				LEFT JOIN tbl_master_potongan B ON A.kode_potongan = B.kode_potongan
				WHERE A.kode_periode = '$periode' AND A.kode_jalur = '$jalur'
					AND (DATE(NOW()) >= tanggal_mulai AND DATE(NOW()) <= tanggal_akhir) ";
		}
		$sql = $this->db2->query($sql)->row_array();
		if($sql){
			return $sql;
		}else{
			return null;
		}
	}

	#untuk melihat kondisi ketika promo ditentukan oleh admin, contoh : S2 medco
	function mPromoByAdmin($kode_cmb){
		$sql = "SELECT A.id, A.kode_potongan, B.kode_periode, B.kode_jalur,  B.tanggal_akhir, B.tanggal_mulai, A.nama_potongan
			, 1 as 'promoByAdmin'
			FROM tbl_master_potongan A
			LEFT JOIN `tbl_potongan_periode` B ON A.kode_potongan = B.kode_potongan
			LEFT JOIN adis_smb_usr_keu C ON C.kode_potongan = A.kode_potongan
			WHERE C.smbUsr = '$kode_cmb'";
		$sql = $this->db2->query($sql)->row_array();
		if($sql){
			return $sql;
		}else{
			return null;
		}
	}

	function mSelectPromo($id, $promoByAdmin = 0){
		$sql = "SELECT A.* ,B.nama_potongan, $promoByAdmin as 'promoByAdmin'
			FROM `tbl_potongan_periode` A
			LEFT JOIN tbl_master_potongan B ON A.kode_potongan = B.kode_potongan
			WHERE A.id = '$id'";
		$sql = $this->db2->query($sql)->row_array();
		if($sql){
			return $sql;
		}else{
			return null;
		}
	}

	function mPayment($kode = "", $jenjang = "", $periode = "", $promo = "", $paid = false){
		$qry = "SELECT
					f.kode AS kode_smb,
					f.bukaSmb,
					f.nomor AS no_smb,
					u.username AS nama_cm,
					p.nama AS progdi,
					RIGHT (f.bukaSmb, 4) AS kodeProdi,
					SUBSTR(bukaSmb, 12, 2) AS jalur,
					DAYNAME(u.createTime) AS hari,
					DATE(u.createTime) AS tanggal,
					u.validation_status,
					m.jenjangType,
					up.rumahCell,
					f.stsReapplyPaid,
					f.stsReapplyPaidConfirm,
					j.nama AS n_jalur,
					b.jalur AS kode_jalur,
					t.kode AS kode_typeTrans,
					m.tahun,
					m.semester,
					t.nama AS typeTrans,
					t2.nama AS typeAccount,
					DAYNAME(f.reapplyBankTransferTime) AS hari_trans,
					DATE(f.reapplyBankTransferTime) AS tanggal_trans,
					k.buktiBayarDaftarUlang,
					f.reapplyBankAccountType,
					f.reapplyBankTransferAmount,
					k.noRekPengirimDaftarUlang,
					k.namaRekPengirimDaftarUlang,
					f.stsResultGrade,
					k.metodBayarDaftarUlang,
					k.totalBiayaDaftarUlang,
					k.noAtmCardDaftarulang,
					k.sks_acc,
					k.noAtmCardPendaftaran,
					SUBSTR(
						k.metodBayarDaftarUlang,
						12,
						3
					) AS lulusan,
					k.alumni_s1 AS alumni,
					e.lulus_acc,
					f.earlyBird,
					k.earlybird AS earlybirdS2,
					k.keluargacivitas,
					k.keluargadlb,
					k.metodePembayaran,
					k.miaiPindahan,
					MP.nama_potongan,
					k.kode_potongan,
					k.pilihan_angsuran_km,
					parameter_biaya,
					data_angsuran
				FROM
					adis_smb_form f
				LEFT JOIN adis_smb_usr u ON u.kode = f.kode
				LEFT JOIN adis_buka_smb b ON b.kode = f.bukaSmb
				LEFT JOIN adis_periode d ON d.kode = b.periode
				LEFT JOIN adis_periode_master m ON m.kode = d.idPeriodeMaster
				LEFT JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
				LEFT JOIN adis_prodi p ON p.kode = b.prodi
				LEFT JOIN adis_jalur_smb j ON j.kode = SUBSTR(f.bukaSmb, 12, 2)
				LEFT JOIN adis_type t ON t.kode = f.reapplyBankTransferType
				LEFT JOIN adis_type t2 ON t2.kode = f.reapplyBankAccountType
				LEFT JOIN adis_smb_usr_keu k ON k.smbUsr = f.kode
				LEFT JOIN adis_smb_usr_edu e ON e.smbUsr = f.kode
				LEFT JOIN tbl_master_potongan MP ON MP.kode_potongan = k.kode_potongan
				WHERE
					f.kode = '$kode'";
		$sql = $this->db2->query($qry)->row_array();

		if($paid == true){
			$this->load->model('mmaster');
			$promoParam = json_decode(base64_decode($sql['parameter_biaya']));

			if($promoParam->promoByAdmin == 1){
				$promo = $this->mPromoByAdmin($kode);
			}else{
				$promo = $this->mSelectPromo($promoParam->promo_id, 0);

			}

			// print_r($promoParam);
			// print_r($promo);exit;

			$kode_potongan = "";
			if ($promo){ $kode_potongan = $promo['kode_potongan'];}
			$detailPotongan = $this->mmaster->mSelectParameterBiaya($kode_potongan);

			$this->smarty->assign('paramBiaya', $promoParam);
			$this->smarty->assign('promo', $promo);
			$this->smarty->assign('detil_potongan', $detailPotongan);
			$this->smarty->assign('transaksi', $sql);
		}

		$biayaSaled = '';
		$biaya = '';
		$kodePaidType = '';
		if($sql){
			$this->smarty->assign("transPaid", $sql);
			$jalur_code = $sql['jalur'];

			switch($jenjang){
				case '1':
					switch($jalur_code){
						case 'KIP':

						break;
						default:

							$sks_acc = $sql['sks_acc'];
							$rank = $sql['stsResultGrade'];
							$kodePaidType = $sql['metodBayarDaftarUlang'];
							if(!$kodePaidType){
								if ($jalur_code == 'KP'){
									$kodePaidType = $sql['kodeProdi'].'.'.'69'.'.'.'KP'.'.'.$sql['lulus_acc'];
								}else{
									$kodePaidType = $sql['kodeProdi'].'.'.'1'.'.'.'01';
								}
							}
							$biaya = $this->biayaUtama($periode, $kodePaidType, $jenjang, $jalur_code);


							if ($biaya == null){
								echo '<script>alert("Setting pembayaran uang masuk belum dibuat. Hubungi Admin.");</script>';
								redirect ('/portal', 'refresh');
							}
							$biayaSaled = $this->biayaDiskon($biaya, $promo, $sql['kodeProdi']);

							/*

							if($promo['kode_potongan'] == 'GEMILANG'){
								switch($sql['kodeProdi']){
									case '1002':
									case '1004':
									case '1006':
									case '1009':
									case '1010':
									case '1011':
										$biaya['bpp'] = 1500000;
									break;
									case '1003':
									case '1001':
									case '1007':
									case '1008':
										$biaya['bpp'] = 2500000;
									break;
									case '1005':
										$biaya['bpp'] = 5000000;
									break;

								}
							}

							if($promo['kode_potongan'] == 'UNGGULAN'){
								$biaya['uang_masuk'] = 4000000;
							}
							*/

							if ($jalur_code == 'KP'){
								$biaya_daftar_kp = json_decode($biaya['pilihanBiayaUM']);
								$biaya['daftar_ulang_kp'] = $biaya_daftar_kp[0];
								$biayaSaled['totalBiaya'] = $biaya_daftar_kp[0]+$biayaSaled['totalBiaya'];
								$biayaSaled['total1'] = $biaya_daftar_kp[0]+$biayaSaled['total1'];

								if($promo){
									$biayaSaled['total2'] = $biaya_daftar_kp[0]+$biayaSaled['total2'];
								}
							}
							// echo "<pre>";
							// print_r($biaya);exit;

							if(!empty($biaya['jumlahSemester'])){
								$biayaSemester1 = ($biayaSaled['totalBiaya']/$biaya['jumlahSemester'])+$biaya['bpp'];
							}else{
								$biayaSemester1 = ($biayaSaled['totalBiaya'])+$biaya['bpp'];
							}
							$this->smarty->assign('biayaSemester1', $biayaSemester1);

						break;
					}
				break;
				case '2':
					$kodePaidType = $sql['metodBayarDaftarUlang'];
					if(!$kodePaidType){
						$kodePaidType = $periode.'.'.$sql['kodeProdi'].'.'.'1';
					}
					$biaya = $this->biayaUtama($periode, $kodePaidType, $jenjang);

					$biayaSemester1 = 0;
					if ($biaya == null){
						echo '<script>alert("Re-registration fee not yet set. Contact Admin!.");</script>';
						redirect ('/portal', 'refresh');
					}

					$biayaSaled = $this->biayaDiskon($biaya, $promo);

					$biayaSemester1 = $biayaSaled['totalBiaya']/$biaya['jumlahSemester'];
					$this->smarty->assign('biayaSemester1', $biayaSemester1);

				break;
			}

			// echo "<pre>";
			// print_r($biayaSaled);exit;

			$this->smarty->assign('biayaSale', $biayaSaled);
			$this->smarty->assign('biaya', $biaya);
			$this->smarty->assign('kode_pilihan_biaya', $kodePaidType);
		}
	}

	function biayaUtama($periode = "", $kodePaidType = "", $jenjang = "", $jalur = ""){
		if ($jenjang == 1){
			if ($jalur == 'KP'){
				$bayarSql = "SELECT A.*, A.uangMasuk as uang_masuk, A.sks as sks,
					A.spp as spp,
					((A.sksSemester1*A.biayaPerSks)+A.uangMasuk+(A.spp/A.jumlahSemester)+A.bpp) as biaya_total,
					C.tempo, C.tgl_jatuh_tempo, A.jumlahSemester
					FROM `adis_pembayaran_kelasmalam` A
					LEFT JOIN tbl_tgl_tempo_kelasmalam C ON C.kode_periode = A.periode_kode
					WHERE A.kode = '$kodePaidType' AND A.periode_kode = '$periode' ;";
			}else{
				$bayarSql = "SELECT p.*, c.*, c.kode as code, p.kode as kode_pembayaran,
						p.sks, p.spp,
						p.uangMasuk1 as uang_masuk, p.total1 as biaya_total, p.jumlah_semester as jumlahSemester
						FROM adis_pembayaran p
						LEFT JOIN adis_cicilan c ON c.id_pembayaran = p.kode AND c.periode_kode = '$periode'
						WHERE p.kode='$kodePaidType'  AND p.periode_kode = '$periode'";

			}
		}elseif($jenjang == 2){
			$bayarSql = "SELECT p.*, p.kode as kode_pembayaran, p.uangMasuk as uang_masuk,
				p.biayaSpp as spp, biayaSks as sks, 0 as bpp, p.totalBiaya as biaya_total,
				p.jumlahSemester
				FROM adis_pembayaran_s2 p
				WHERE p.kode='$kodePaidType' AND kodePeriode = '$periode'";
		}

		$biaya = $this->db2->query($bayarSql)->row_array();

		if ($biaya == null){
			return null;
		}else{
			return $biaya;
		}

	}

	function biayaDiskon($biaya = "", $promo = "", $kode_prodi = ""){

		$this->load->model('mmaster');
		$biayaSaled = array();
		$biayaSaled['totalBiaya'] = 0;


		if($promo){

			$detailPotongan = $this->mmaster->mSelectParameterBiaya($promo['kode_potongan']);
			$potongan_uangpangkal_allprodi = $this->db2->query("SELECT * FROM tbl_potongan_up_semua_prodi WHERE tbl_master_potongan_id = '".$promo['id']."' ")->row_array();
			$potongan_bpp_prodi = $this->db2->query("SELECT * FROM tbl_potongan_bpp_prodi WHERE tbl_master_potongan_id = '".$promo['id']."' AND kode_prodi = '$kode_prodi' ")->row_array();


			$paramBiaya = "";
			foreach($detailPotongan as $k => $val){
				switch($val['kode_biaya']){
					case 'UM':
						$paramBiaya = $biaya['uang_masuk'];
					break;
					case 'SPP':
						$paramBiaya = $biaya['spp'];
					break;
					case 'SKS':
						$paramBiaya = $biaya['sks'];
					break;
					case 'BPP':
						$paramBiaya = $biaya['bpp'];
					break;
					case 'JA':
						$paramBiaya = $biaya['almamater'];
					break;
					case 'REG':
						$paramBiaya = $biaya['biaya_registrasi'];
					break;
				}

				if ($val['nilai'] != 0){
					// $biayaSaled[$val['kode_biaya']] = $paramBiaya - ($paramBiaya*$val['nilai']/100);
					if($val['tipe'] == 1){
						$biayaSaled[$val['kode_biaya']] = $paramBiaya - ($paramBiaya*$val['nilai']/100);
					}else{
						$biayaSaled[$val['kode_biaya']] = $paramBiaya - $val['nilai'];
					}
				}else{
					$biayaSaled[$val['kode_biaya']] = $paramBiaya;
				}

			}

			if(!empty($potongan_uangpangkal_allprodi) && $potongan_uangpangkal_allprodi['nilai_potongan'] != 0){
				$biayaSaled['UM'] = $potongan_uangpangkal_allprodi['nilai_potongan'];
			}

			if(!empty($potongan_bpp_prodi)){
				$biayaSaled['BPP'] = $biaya['bpp'] - $potongan_bpp_prodi['nilai_potongan'];
			}

			$biayaSaled['totalBiaya'] += $biaya['uang_masuk'];
			$biayaSaled['totalBiaya'] += $biaya['spp'];
			$biayaSaled['totalBiaya'] += $biaya['sks'];
			$biayaSaled['totalBiaya'] += $biaya['almamater'];
			$biayaSaled['totalBiaya'] += $biaya['biaya_registrasi'];
			// $biayaSaled['totalBiaya'] += $biaya['bpp'];

			$biayaSaled['total1'] = $biayaSaled['UM']+$biayaSaled['SKS'] +$biayaSaled['SPP'];
			$biayaSaled['total2'] = $biayaSaled['UM']+$biayaSaled['SKS'] +$biayaSaled['SPP']+$biayaSaled['BPP']+$biayaSaled['JA']+$biayaSaled['REG'];
			// $biayaSaled['total2'] = $biayaSaled['UM']+$biayaSaled['SKS'] +$biayaSaled['SPP']+$biayaSaled['BPP'];

			if(!empty($biaya['jumlahSemester']) ){
				$biayaSaled['biayaSemester1'] = ($biayaSaled['total1']/$biaya['jumlahSemester'])+$biayaSaled['BPP'];
			}else{
				$biayaSaled['biayaSemester1'] = ($biayaSaled['total1'])+$biayaSaled['BPP'];
			}

		}else{
			$biayaSaled['UM'] = $biaya['uang_masuk'];
			$biayaSaled['SPP'] = $biaya['spp'];
			$biayaSaled['SKS'] = $biaya['sks'];
			$biayaSaled['BPP'] = $biaya['bpp'];


			$biayaSaled['totalBiaya'] += $biaya['uang_masuk'];
			$biayaSaled['totalBiaya'] += $biaya['spp'];
			$biayaSaled['totalBiaya'] += $biaya['sks'];
			$biayaSaled['totalBiaya'] += $biaya['almamater'];
			$biayaSaled['totalBiaya'] += $biaya['biaya_registrasi'];

			$biayaSaled['total1'] = $biayaSaled['UM']+$biayaSaled['SKS'] +$biayaSaled['SPP'];

			// echo "<pre>";
			// print_r($biaya);exit;
			if(!empty($biaya['jumlahSemester'])){
				$biayaSaled['biayaSemester1'] = ($biayaSaled['total1']/$biaya['jumlahSemester'])+$biayaSaled['BPP'];
			}else{
				$biayaSaled['biayaSemester1'] = ($biayaSaled['total1'])+$biayaSaled['BPP'];
			}
		}

		// echo "<pre>";
		// print_r($biayaSaled);exit;


		return $biayaSaled;

	}


	function mCmb($kode){
		if (is_numeric($kode)){
			$wer = "WHERE f.nomor = '$kode'";
		}else{
			$wer ="WHERE f.kode = '$kode'";
		}

		$sql = "SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, up.nama as nama_cm ,
				p.nama as progdi, p.kode as kodeProdi, p.singkatan as prodiSingkatan,
				DAYNAME(u.createTime) as hari, DATE(u.createTime) as tanggal, up.agamaType,
				DAY(u.createTime) as cDay, MONTHNAME(u.createTime) as cMonth, YEAR(u.createTime) as cYear,
				u.validation_status, m.jenjangType, up.rumahCell, up.genderType, up.nomorKtp,
				up.suratKabKota, up.suratKodePos, up.suratProp, up.suratTel, up.rumahFax,
				up.tempatLahir, up.tanggalLahir, DAYNAME(up.tanggalLahir) as hLahir, DATE_FORMAT(up.tanggalLahir,'%d-%m-%Y') as dateBirth,
				DAY(up.tanggalLahir) as tLahir, MONTHNAME(up.tanggalLahir) as bLahir, YEAR(up.tanggalLahir) as yLahir,
				up.rumahAlamat, up.suratAlamat, up.statusAlamat, up.nama as gender, up.statusKeluarga, e.status as statusPend,
				up.statusPrestasi, up.stsPribadiConfirm, up.statusSaudara, up.foto, up.confirmSaudara,
				f.stsApplyPaid, f.stsApplyPaidConfirm, up.stsPribadiConfirm, f.stsEventConfirm, f.stsResultConfirm, f.stsMundurBeforeReapply,
				f.stsReapplyPaid, stsReapplyPaidConfirm, f.stsMundurAfterReapply, j.nama as n_jalur, f.stsResultPass, f.stsReapplyPaidConfirm, f.stsReapplyPaid,
				m.tahun, m.semester, f.nim, m.nama as namaPeriode, j.kode as kode_jalur,
				k.tolakPendaftaran, k.tolakDU, TRIM(TRAILING '\n' FROM k.pesanTolakP) as pesanTolakP, TRIM(TRAILING '\n' FROM k.pesanTolakDU) as  pesanTolakDU, j.nama as jalur, e.nisn_nim, e.rata_rata_rapor, u.jurusan, e.approval_rapor,
				e.tahunLulus, u.jalur_penerimaan, up.nama_panggilan,
				if (m.jenjangType = 1, ber.is_done, ber2.is_done) as sts_berkas, if (m.jenjangType = 1, ber.is_upload, ber2.is_upload) as uploaded_berkas
				, e.file_rapor_10_1, e.file_rapor_10_2, e.file_rapor_11_1, e.file_rapor_11_2, e.upload_rapor, e.edit_ke,
				u.validation_key, e.verifikasi_forlap, e.jurusan as jurusanAsal, e.lulusAsal, e.tolak_forlap_msg,
				d.tanggal_daftar_ulang, m.tanggalSelesai as periodeEndDate, if(m.semester = 1, 'Ganjil', 'Genap') as tipePeriodeDesc,
				p.kaprodi, up.rumahKecamatan, up.rumahKelurahan, up.rumahRT, rumahRW, e.lulusankk, k.kode_potongan, m.kode as kode_periode, d.kode as kode_gelombang
				FROM adis_smb_form f
				LEFT JOIN adis_smb_usr u ON u.kode = f.kode
				LEFT JOIN adis_periode d ON d.kode = LEFT(f.bukaSmb,10)
				LEFT JOIN adis_periode_master m ON m.kode = d.idPeriodeMaster
				LEFT JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
				LEFT JOIN adis_smb_usr_edu e ON e.smbUsr = f.kode
				LEFT JOIN adis_prodi p ON RIGHT(f.bukaSmb,4)= p.kode
				LEFT JOIN adis_jalur_smb j ON SUBSTR(f.bukaSmb, 10, 2) = j.kode
				LEFT JOIN adis_type t ON t.kode = up.genderType
				LEFT JOIN adis_smb_usr_keu k ON k.smbUsr = f.kode
				LEFT JOIN tbl_smb_berkas ber ON f.kode = ber.kode
				LEFT JOIN tbl_smb_berkas_mm ber2 ON f.kode = ber2.kode
				$wer";
		$sql = $this->db2->query($sql)->row();
		$this->smarty->assign("cmb", $sql);

		return $sql;

	}

	function mProfil($kode){
		if (is_numeric($kode)){
			$wer = "WHERE f.nomor = '$kode'";
		}else{
			$wer ="WHERE f.kode = '$kode'";
		}

		$sql = "SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, up.nama as nama_cm , p.nama as progdi, p.kode as proKode,
				m.jenjangType, up.rumahCell, up.genderType, up.tempatLahir, up.tanggalLahir, DAYNAME(up.tanggalLahir) as hLahir,
				DAY(up.tanggalLahir) as tLahir, MONTHNAME(up.tanggalLahir) as bLahir, YEAR(up.tanggalLahir) as yLahir,
				up.nomorKtp, up.rumahEmail, up.suratAlamat, tA.nama as suratProp, tD.kodepos as suratKodPos,
				up.suratTel, up.suratFax,
				up.rumahAlamat, t2.nama as propNama, t3.nama as kabKotaNama, t4.kodepos as kodePos, up.rumahTel, up.rumahFax,
				up.ayahNama, up.ayahAlamat, up.ayahCell, up.ayahEmail,
				up.ibuNama, up.ibuAlamat, up.ibuCell, up.ibuEmail,
				up.waliNama, up.waliAlamat, up.waliCell, up.waliEmail,
				t.nama as gender, t.kode as kodeGender, t1.nama as agamaName,
				up.statusAlamat, up.statusKeluarga, f.nim,
				e.status as statusPend, e.nama as namaEdu, e.jurusan,
				up.statusPrestasi, up.stsPribadiConfirm, up.statusSaudara, up.foto,
				f.stsApplyPaid, f.stsApplyPaidConfirm, up.stsPribadiConfirm, f.stsEventConfirm, f.stsResultConfirm, f.stsMundurBeforeReapply,
				f.stsReapplyPaid, stsReapplyPaidConfirm, f.stsMundurAfterReapply, j.nama as n_jalur, f.stsResultPass, f.stsReapplyPaidConfirm,
				m.tahun, m.semester, e.file_ijazah, m.jenjangType as jenjang, up.statusKeluarga
				, e.file_rapor_10_1, e.file_rapor_10_2, e.file_rapor_11_1, e.file_rapor_11_2, e.upload_rapor, e.approval_rapor,
				m.nama as nama_periode, d.nama as nama_gelombang,f.reapplyMaxDate, e.lulusankk
				FROM adis_smb_form f
				INNER JOIN adis_smb_usr u ON u.kode = f.kode
				LEFT JOIN adis_periode d ON d.kode = LEFT(f.bukaSmb,10)
				LEFT JOIN adis_periode_master m ON m.kode = d.idPeriodeMaster
				INNER JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
				LEFT JOIN adis_smb_usr_edu e ON e.smbUsr = f.kode
				LEFT JOIN adis_prodi p ON RIGHT(f.bukaSmb,4)= p.kode
				LEFT JOIN adis_jalur_smb j ON SUBSTR(f.bukaSmb, 10, 2) = j.kode
				LEFT JOIN adis_type t ON t.kode = up.genderType
				LEFT JOIN adis_type t1 ON t1.kode = up.agamaType
				LEFT JOIN adis_wil t2 ON t2.kode = up.rumahProp
				LEFT JOIN adis_wil t3 ON t3.kode = up.rumahKabKota
				LEFT JOIN adis_kodepos t4 ON t4.kode = up.rumahKodePos
				LEFT JOIN adis_wil tA ON tA.kode = up.suratProp
				LEFT JOIN adis_kodepos tD ON tD.kode = up.suratKodePos
				$wer";
		$sql = $this->db2->query($sql)->row();
		$this->smarty->assign("prof", $sql);

		$rapor = $sql->file_rapor_10_1;
		$rapor = json_decode($rapor);
		$this->smarty->assign('rapor', $rapor);
	}

	function insertRapor($nomor='', $sing='', $kelas='', $sem='', $nilai='' ){
		$kode = $sing.'_'.$kelas.'_'.$sem;
		$num_rows = $this->db2->query("SELECT * FROM tbl_nilai_rapor WHERE kode = '$kode'AND nomor ='$nomor' ;")->num_rows();

		$this->db2->trans_begin();

		if($num_rows> 0){
			$this->db2->where('smbUsr', $this->auth['kode']);
			$this->db2->update('adis_smb_usr_edu', array('edit_ke'=>1));

			$this->db2->where(array('kode'=>$kode, 'nomor'=>$nomor));
			$this->db2->update('tbl_nilai_rapor', array(
				'sing_pelajaran'=>$sing,
				'kelas'=>$kelas,
				'semester'=>$sem,
				'nilai'=>$nilai
			));

		}else{
			$this->db2->insert('tbl_nilai_rapor', array(
				'nomor'=>$nomor,
				'kode'=>$kode,
				'sing_pelajaran'=>$sing,
				'kelas'=>$kelas,
				'semester'=>$sem,
				'nilai'=>$nilai
			));
		}

		$this->db->trans_complete();
	}

	function mStatus($kode){
		$sql = "SELECT f.kode
				FROM adis_smb_form f
				INNER JOIN adis_smb_usr_pribadi u ON u.kode = f.kode
				INNER JOIN adis_smb_usr_edu e ON e.smbUsr = f.kode";
	}

	function mDetailAlamat($kode){
		$sql ="SELECT up.kode, up.nama, up.rumahCell, t.nama as gender, up.tempatLahir, up.tanggalLahir, up.nomorKtp,
		agamaType, t2.nama as agama, up.nama_panggilan,
		rumahProp, w.nama as propRumah, rumahKabKota, w2.nama as kabkotaRumah, rumahKodePos, k.kodepos as kodeposRumah,
		suratProp, ws.nama as propSurat, suratKabKota, ws2.nama as kabkotaSurat, suratKodePos, ks.kodepos as kodeposSurat,
		up.rumahAlamat, up.rumahKabKota, up.rumahProp, up.rumahKodePos, rumahTel, rumahFax,
		up.suratAlamat, up.suratKabKota, up.suratProp, up.suratKodePos, suratTel, suratFax, suratCell,
		up.statusAlamat, up.genderType
		FROM adis_smb_usr_pribadi up
		LEFT JOIN adis_type t ON t.kode = up.genderType
		LEFT JOIN adis_type t2 ON t2.kode = up.agamaType
		LEFT JOIN adis_wil w ON w.kode = up.rumahProp
		LEFT JOIN adis_wil w2 ON w2.kode = up.rumahKabKota
		LEFT JOIN adis_kodepos k ON k.kode = up.rumahKodePos
		LEFT JOIN adis_wil ws ON ws.kode = up.suratProp
		LEFT JOIN adis_wil ws2 ON ws2.kode = up.suratKabKota
		LEFT JOIN adis_kodepos ks ON k.kode = up.suratKodePos
		WHERE up.kode = '$kode'";
		$sql = $this->db2->query($sql)->row();
		$this->smarty->assign("alamat", $sql);
	}

	function mPaidPendaftaran($kode){
		$sql = "SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, u.username as nama_cm , p.nama as progdi,
		DAYNAME(u.createTime) as hari, DATE(u.createTime) as tanggal, u.validation_status, m.jenjangType, up.rumahCell,
		FORMAT(f.applyBankTransferAmount, 2) as biaya, f.stsApplyPaid, f.stsApplyPaidConfirm, j.nama as n_jalur, t.kode as kode_typeTrans,
		m.tahun, m.semester, t.nama as typeTrans, t2.nama as typeAccount, DAYNAME(f.applyBankTransferTime) as hari_trans,
		DATE(f.applyBankTransferTime) as tanggal_trans, k.buktiBayarPendaftaran as buktiBayarPendaftaran,
		k.noRekPengirimPendaftaran, k.namaRekPengirimPendaftaran, k.noAtmCardPendaftaran, f.applyBankAccountType, up.ayahCell
		FROM adis_smb_form f
		LEFT JOIN adis_smb_usr u ON u.kode = f.kode
		LEFT JOIN adis_buka_smb b ON b.kode = f.bukaSmb
		LEFT JOIN adis_periode d ON d.kode = b.periode
		LEFT JOIN adis_periode_master m ON m.kode = d.idPeriodeMaster
		LEFT JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
		LEFT JOIN adis_prodi p ON p.kode = b.prodi
		LEFT JOIN adis_jalur_smb j ON j.kode = b.jalur
		LEFT JOIN adis_type t ON t.kode = f.applyBankTransferType
		LEFT JOIN adis_type t2 ON t2.kode = f.applyBankAccountType
		LEFT JOIN adis_smb_usr_keu k ON k.smbUsr = f.kode
		WHERE f.kode = '$kode'";
		$sql = $this->db2->query($sql)->row();
		$this->smarty->assign("transPaid", $sql);
	}

	function mSimpanVoucherBayar($kode = "", $voucher = ""){
		$unik = uniqid();
		$date = date("Y-m-d H:i:s");

		$tipe_kode_voucher = ($voucher == "GRATIS") ? '04.6' : '04.5';
		$tipe_voucher = ($voucher == "GRATIS") ? "GRATIS" : "VOUCHER";

		$this->db2->trans_begin();

		$this->db2->where("kode", $kode);
		$this->db2->update("adis_smb_form", array(
				"applyBankTransferType"=>$tipe_kode_voucher,
				"applyBankAccountType"=>$voucher,
				"applyBankTransferAmount"=>0,
				"applyBankTransferTime"=>$date,
				"stsApplyPaid"=>1
			));

		$dbExist = $this->db->query("SELECT kode, smbUsr FROM adis_smb_usr_keu WHERE smbUsr = '$kode'")->num_rows();
		if ( $dbExist == 0){
			$this->db2->insert("adis_smb_usr_keu", array(
					"kode"=>$unik,
					"smbUsr"=>$kode,
					"createTime"=>$date,
					"createUser"=>$kode,
					"buktiBayarPendaftaran"=>$tipe_voucher,
					"noRekPengirimPendaftaran"=>$tipe_voucher,
					"namaRekPengirimPendaftaran"=>$tipe_voucher,
					"noAtmCardPendaftaran"=>$tipe_voucher
				));
		}else{
			$this->db2->where("smbUsr", $kode);
			$this->db2->update("adis_smb_usr_keu", array(
					"smbUsr"=>$kode,
					"createTime"=>$date,
					"createUser"=>$kode,
					"buktiBayarPendaftaran"=>$tipe_voucher,
					"noRekPengirimPendaftaran"=>$tipe_voucher,
					"namaRekPengirimPendaftaran"=>$tipe_voucher,
					"noAtmCardPendaftaran"=>$tipe_voucher,
					"tolakPendaftaran"=>0
				));
		}

		if ($this->db2->trans_status() === FALSE){
			$this->db2->trans_rollback();
			return 0;
		}else{
			$this->db2->trans_commit();
			return 1;
		}

	}

	function mSimpanBayar($file_name, $kode){
		$unik = uniqid();
		$date = date("Y-m-d H:i:s");

		$this->db2->trans_begin();
		$dateBayar = date("Y-m-d", strtotime($this->input->post("tanggalBayar")));

		$this->db2->where("kode", $kode);
		$this->db2->update("adis_smb_form", array(
				"applyBankTransferType"=>$this->input->post("typeTrans"),
				"applyBankAccountType"=>$this->input->post("bankAkun"),
				"applyBankTransferAmount"=>str_replace('.', '', $this->input->post("totalBiaya")),
				"applyBankTransferTime"=>$dateBayar,
				"stsApplyPaid"=>1
			));

		$this->db2->where('kode', $kode);
		$this->db2->update('adis_smb_usr_pribadi', array('ayahCell'=>$this->input->post('no_hp')));


		$dbExist = $this->db->query("SELECT kode, smbUsr FROM adis_smb_usr_keu WHERE smbUsr = '$kode'")->num_rows();
		if ( $dbExist == 0){
			$this->db2->insert("adis_smb_usr_keu", array(
					"kode"=>$unik,
					"smbUsr"=>$kode,
					"createTime"=>$date,
					"createUser"=>$kode,
					"buktiBayarPendaftaran"=>$file_name,
					"noRekPengirimPendaftaran"=>$this->input->post("no_rek_cmb"),
					"namaRekPengirimPendaftaran"=>$this->input->post("nama_rek_cmb"),
					"noAtmCardPendaftaran"=>$this->input->post("noAtmCard")
				));
		}else{
			$this->db2->where("smbUsr", $kode);
			$this->db2->update("adis_smb_usr_keu", array(
					"smbUsr"=>$kode,
					"createTime"=>$date,
					"createUser"=>$kode,
					"buktiBayarPendaftaran"=>$file_name,
					"noRekPengirimPendaftaran"=>$this->input->post("no_rek_cmb"),
					"namaRekPengirimPendaftaran"=>$this->input->post("nama_rek_cmb"),
					"noAtmCardPendaftaran"=>$this->input->post("noAtmCard"),
					"tolakPendaftaran"=>0
				));
		}

                if ($this->db2->trans_status() === FALSE){
                    $this->db2->trans_rollback();
                    return 0;
                }else{
                    $this->db2->trans_commit();
                    return 1;
                }
	}

	function mEmailKonfirm($kode, $bayar, $konten = ""){

	}

	function mPaidDaftarUlang($kode, $saudara, $jenjang = '', $periode){
		$angsuran = '';

		$sql = "SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, u.username as nama_cm , p.nama as progdi, RIGHT(f.bukaSmb,4) as kodeProdi,
			SUBSTR(bukaSmb,12, 2) as jalur,
			DAYNAME(u.createTime) as hari, DATE(u.createTime) as tanggal, u.validation_status, m.jenjangType, up.rumahCell,
			f.stsReapplyPaid, f.stsReapplyPaidConfirm, j.nama as n_jalur,b.jalur as kode_jalur,
			t.kode as kode_typeTrans,
			m.tahun, m.semester, t.nama as typeTrans, t2.nama as typeAccount, DAYNAME(f.reapplyBankTransferTime) as hari_trans,
			DATE(f.reapplyBankTransferTime) as tanggal_trans, k.buktiBayarDaftarUlang, f.reapplyBankTransferAmount,
			k.noRekPengirimDaftarUlang, k.namaRekPengirimDaftarUlang, f.stsResultGrade,
			k.metodBayarDaftarUlang, k.totalBiayaDaftarUlang, k.noAtmCardDaftarulang, k.sks_acc,  k.noAtmCardPendaftaran,
			SUBSTR(k.metodBayarDaftarUlang, 12, 3) as lulusan, k.alumni_s1 as alumni, e.lulus_acc, f.earlyBird,
			k.earlybird as earlybirdS2, k.keluargacivitas, k.keluargadlb, k.metodePembayaran, k.miaiPindahan,
			MP.nama_potongan, k.kode_potongan, k.pilihan_angsuran_km
			FROM adis_smb_form f
			LEFT JOIN adis_smb_usr u ON u.kode = f.kode
			LEFT JOIN adis_buka_smb b ON b.kode = f.bukaSmb
			LEFT JOIN adis_periode d ON d.kode = b.periode
			LEFT JOIN adis_periode_master m ON m.kode = d.idPeriodeMaster
			LEFT JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
			LEFT JOIN adis_prodi p ON p.kode = b.prodi
			LEFT JOIN adis_jalur_smb j ON j.kode =  SUBSTR(f.bukaSmb, 12, 2)
			LEFT JOIN adis_type t ON t.kode = f.reapplyBankTransferType
			LEFT JOIN adis_type t2 ON t2.kode = f.reapplyBankAccountType
			LEFT JOIN adis_smb_usr_keu k ON k.smbUsr = f.kode
			LEFT JOIN adis_smb_usr_edu e ON e.smbUsr = f.kode
			LEFT JOIN tbl_master_potongan MP ON MP.kode_potongan = k.kode_potongan
			WHERE f.kode = '$kode'";
		$sql = $this->db2->query($sql)->row();
		$this->smarty->assign("transPaid", $sql);

		$jalur_code = $sql->jalur;
		$earlybird = $sql->earlyBird;

		switch($jenjang){
			case '1':
				IF  ($jalur_code == 'KP'){
//                                    $kodePaidType = $sql-> metodBayarDaftarUlang;
					$kodePaidType = "";
					if(!$kodePaidType){
						$kodePaidType = $sql->kodeProdi.'.'.'69'.'.'.'KP'.'.'.$sql->lulus_acc;
					}
		// echo '<script>alert("'.$kodePaidType.'");</script>';

					$metodBayar = "SELECT A.*,
						C.tempo, C.tgl_jatuh_tempo
						FROM `adis_pembayaran_kelasmalam` A
						LEFT JOIN tbl_tgl_tempo_kelasmalam C ON C.kode_periode = A.periode_kode
						WHERE A.kode = '$kodePaidType' AND A.periode_kode = '$periode' ;";

					$biaya = $this->db2->query($metodBayar)->row();

					$pilBiaya = json_decode($biaya->pilihanBiayaUM);
					$this->smarty->assign('pilihanBiaya',$pilBiaya);

					$biayaSks = $biaya->biayaPerSks * $biaya->jumlahSks;
					$total = $biaya->uangMasuk + $biaya->spp + $biayaSks;
					$angsurPerBulan = $total/$biaya->tempoAngsuran;

					$tglJatuhTempo = $biaya->tgl_jatuh_tempo;
					$tglJatuhTempo = json_decode($tglJatuhTempo);

					$detailAngsur = array();
					$angsuran = array();

					$potonganBiaya = 0;
						if($sql->kode_potongan != ""){
							$listPotongan = $this->mportal->selectPotonganMaster($kodePaidType, $sql->kode_potongan, $periode);
							$potUangMasuk = 0; $potSpp = 0; $potSks = 0; $potBpp = 0;
							foreach($listPotongan as $nilai){
								switch ($nilai['kode_biaya']){
									case 'UM':
										if($nilai['lambang'] == '%'){
											$potUangMasuk = ($biaya->uangMasuk*($nilai['nilai']/100));
										}else{
											$potUangMasuk = $biaya->uangMasuk-$nilai['nilai'];
										}
									break;
									case 'SPP':
									break;
									case 'SKS':
									break;
									case 'BPP':
									break;
								}
							}
							$potonganBiaya = $potUangMasuk + $potSpp + $potSks + $potBpp;
						}
					$this->smarty->assign('potonganBiaya', $potonganBiaya);

					foreach ($tglJatuhTempo as $k => $val){
						$dateTempo = date("Y-m-d", strtotime($val));
						$angsuran[$k] = array('angsuran'=>$angsurPerBulan,'tglTempo'=>$dateTempo);
					}

					$pilihan = isset($sql->pilihan_angsuran_km) ? $sql->pilihan_angsuran_km : $sql->reapplyBankTransferAmount;
					$this->listAngsuran($pilihan , $kodePaidType, $periode, $sql->kode_potongan);

					$this->smarty->assign('uangMasuk', $biaya->uangMasuk);
					$this->smarty->assign('total', $total);
					$this->smarty->assign('method','69');
					$this->smarty->assign('biaya', $biaya);
					$this->smarty->assign('jalur', $sql->jalur);
					$this->smarty->assign('prodi', $sql->kodeProdi);
					$this->smarty->assign('angsuran', $angsuran);
					$this->smarty->assign('kodePayTipe', $kodePaidType);

				}ELSE{

					$sks_acc = $sql->sks_acc;
					$rank = $sql->stsResultGrade;
					$kodePaidType = $sql->metodBayarDaftarUlang;
					if(!$kodePaidType){
						$kodePaidType = $sql->kodeProdi.'.'.'1'.'.'.'01';
					}

				   //echo '<script>alert("Kode Tipe :'.$kodePaidType.'");</script>';
					$metodBayar = "SELECT p.*, c.*, c.kode as code, p.kode as kode_pembayaran
							FROM adis_pembayaran p
							LEFT JOIN adis_cicilan c ON c.id_pembayaran = p.kode AND c.periode_kode = '$periode'
							WHERE p.kode='$kodePaidType'  AND p.periode_kode = '$periode'";
					$biaya = $this->db2->query($metodBayar)->row();
					if ($biaya == null){
						echo '<script>alert("Setting pembayaran uang masuk belum dibuat. Hubungi Admin.");</script>';
						redirect ('/portal', 'refresh');
					}


					$pembayaran = $biaya->pembayaran;

					if ($jalur_code == 'JP'){
						if ($rank == '1'){$uangMasuk = $biaya->uangMasuk1*0.25;}
						if ($rank == '2'){$uangMasuk = $biaya->uangMasuk1*0.5;}
						if ($rank == '3'){$uangMasuk = $biaya->uangMasuk1*0.75;}

						if($rank == 1){	$total = ($biaya->total1-$biaya->uangMasuk1)+$uangMasuk;}
						if($rank == 2){	$total = ($biaya->total1-$biaya->uangMasuk1)+$uangMasuk;}
						if($rank == 3){	$total = ($biaya->total1-$biaya->uangMasuk1)+$uangMasuk;}

					}else{
						$total = $biaya->total1;
						$uangMasuk = $biaya->uangMasuk1;

						if($rank == 1){	$total = $biaya->total1;}
						if($rank == 2){	$total = $biaya->total2;}
						if($rank == 3){	$total = $biaya->total3;}

						if ($rank == '1'){$uangMasuk = $biaya->uangMasuk1;}
						if ($rank == '2'){$uangMasuk = $biaya->uangMasuk2;}
						if ($rank == '3'){$uangMasuk = $biaya->uangMasuk3;}
					}
					// echo '<script>alert("Uang masuk :'.$total.'");</script>';

					if ($rank == 5){ $total = $biaya->total1 - $biaya->uangMasuk1;}
					if ($rank == 5){ $uangMasuk = 0;}

					$sisaAng = 0;
					$angsuran1 = 0;
					$angsuran2 = 0;
					$angsuran3 = 0;
					$totalDiskon = 0;
					$diskonSau = "";
					$diskonSKS = 0;
					$totalAll = $total;
					$totalUangMasuk = $uangMasuk;
					$earlyBirdPot = 0;

					//Potongan memiliki saudara && Early Bird
					if ($sql->kodeProdi != '1204'){

						if ($earlybird == 99){
							$earlyBirdPot = 30/100*$uangMasuk;
						}

						if ($saudara != 0){
							if ($earlybird != 99 || $pembayaran != 1){
								$diskonSau = 20/100*$uangMasuk;
							}else{
								$diskonSau = 20/100*($uangMasuk - $earlyBirdPot);
							}
						}
					}


					if ($sks_acc > 40 && $sks_acc <= 80){
							$diskonSKS = (25/100)*($totalUangMasuk-$diskonSau);
					}else if ($sks_acc > 80){
							$diskonSKS = (50/100)*($totalUangMasuk-$diskonSau);
                                        }

					//JUMLAH DISKON SAUDARA DENGAN SKS
					$totalDiskon = $diskonSau + $diskonSKS ;

					//Potongan pembayaran lunas
					if ($pembayaran == 1){
							if ($sql->kodeProdi != '1204'){
									if($sql->jalur == '01' || $sql->jalur == '02'){
										if ($earlybird == 99){
											$totalDiskon = $diskonSau + $diskonSKS + $earlyBirdPot;
										}else{
											$totalDiskon = $diskonSau + $diskonSKS + 1000000;
										}
									}
							}
					}else{
					//Pemabyaran Angsuran

						if ($rank == '1'){$totalAll = ($total - $uangMasuk);} //+ $biaya->angsuran1_1;}
						if ($rank == '2'){$totalAll = ($total - $uangMasuk);} //+ $biaya->angsuran1_2;}
						if ($rank == '3'){$totalAll = ($total - $uangMasuk);} //+ $biaya->angsuran1_3;}

						if ($rank == '1'){
								$angsuran = array(
										'angsuran1'=>$biaya->angsuran1_1,
										'angsuran2'=>$biaya->angsuran2_1,
										'angsuran3'=>$biaya->angsuran3_1 - $totalDiskon,
								);
						}else
						if ($rank == '2'){
								$angsuran = array(
										'angsuran1'=>$biaya->angsuran1_2,
										'angsuran2'=>$biaya->angsuran2_2,
										'angsuran3'=>$biaya->angsuran3_2 - $totalDiskon,
								);
						}else
						if ($rank == '3'){
								$angsuran = array(
										'angsuran1'=>$biaya->angsuran1_3,
										'angsuran2'=>$biaya->angsuran2_3,
										'angsuran3'=>$biaya->angsuran3_3 - $totalDiskon
								);
						}

						// echo '<script>alert("Uang masuk :'.$angsuran['angsuran1'].'");</script>';

						$sisaAng = $angsuran['angsuran1']+$angsuran['angsuran2']+$angsuran['angsuran3'];

						$angsuran1 = $angsuran['angsuran1'];
						if ($angsuran['angsuran3']<0){
								$angsuran2 = $angsuran['angsuran2'] + $angsuran['angsuran3'];
						}else{
								$angsuran2 = $angsuran['angsuran2'];
						}

						if ($angsuran['angsuran3'] < 0){
						 $angsuran3 = $angsuran['angsuran3'] - $angsuran['angsuran3'];
						}else{
								$angsuran3 = $angsuran['angsuran3'];
						}

						// $totalAll = ($totalAll + $angsuran['angsuran1']) - ($totalDiskon-$diskonSau);
						$totalAll = ($totalAll + $angsuran['angsuran1']); // - ($totalDiskon-$diskonSau);
						// echo '<script>alert("Diskon :'.$totalAll.'");</script>';

					}

					$totalUangMasuk = $uangMasuk - $totalDiskon ;
					$total = $total - $totalDiskon;


					$this->smarty->assign('earlyBird', $earlybird);
					$this->smarty->assign('uangMasuk', $uangMasuk);
					$this->smarty->assign('angsuran',$angsuran);
					$this->smarty->assign('angsuran1',$angsuran1);
					$this->smarty->assign('angsuran2',$angsuran2);
					$this->smarty->assign('angsuran3',$angsuran3);
					$this->smarty->assign('sisaAng',$sisaAng);
					$this->smarty->assign('diskonSau', $diskonSau);
					$this->smarty->assign('totalDiskon', $totalDiskon);
					$this->smarty->assign('totalUangMasuk', $totalUangMasuk);
					$this->smarty->assign('totalAll', $totalAll);
					$this->smarty->assign('total', $total);
					$this->smarty->assign('method',$pembayaran);
					$this->smarty->assign('biaya', $biaya);
					$this->smarty->assign('jalur', $sql->jalur);
					$this->smarty->assign('prodi', $sql->kodeProdi);


				}
			break;
			case '2':
				$kodePaidType = $periode.'.'.$sql->kodeProdi.'.'.'1';
                $potonganBea = 0;

				$metodBayar = "SELECT p.*, p.kode as kode_pembayaran FROM adis_pembayaran_s2 p
					WHERE p.kode='$kodePaidType' AND kodePeriode = '$periode'";
				$biaya = $this->db2->query($metodBayar)->row();
				if ($biaya == null){
					echo '<script>alert("Re-registration fee not yet set. Contact Admin!.");</script>';
					redirect ('/portal', 'refresh');
				}
				$diskonVal = 0;
				$uangMasuk = $biaya->uangMasuk;
				$jumlahSem = $biaya->jumlahSemester;
				$biayaSpp = $biaya->biayaSpp;
				$biayaSks = $biaya->biayaSks;
                // echo '<script>alert("Jalur Tipe : '.$kodePaidType.', Periode : '.$periode.'");</script>';

				if ($jalur_code == 'F2'){
					$potonganBea = 0.25;
					$diskonVal += $potonganBea*$uangMasuk;
					$biayaSks = $biaya->sksSem1F25;
					$uangMasuk = $uangMasuk - ($potonganBea*$uangMasuk);
					$biayaSpp = $biayaSpp - ($potonganBea*$biayaSpp);
				}else if  ($jalur_code == 'F5'){
					$potonganBea = 0.5;
					$diskonVal += $potonganBea*$uangMasuk;
					$biayaSks = $biaya->sksSem1F50;
					$uangMasuk = $uangMasuk - ($potonganBea*$uangMasuk);
					$biayaSpp = $biayaSpp - ($potonganBea*$biayaSpp);
				}else if  ($jalur_code == 'F7'){
					$potonganBea = 0.75;
					$biayaSks = $biaya->sksSem1F75;
					$diskonVal += $potonganBea*$uangMasuk;
					$uangMasuk = $uangMasuk - ($potonganBea*$uangMasuk);
					$biayaSpp = $biayaSpp - ($potonganBea*$biayaSpp);
				}else if  ($jalur_code == 'F1'){
					$potonganBea = 1;
					// $biayaSks = $biaya->sksSem1F75;
					$diskonVal += $potonganBea*$uangMasuk;
					$uangMasuk = $uangMasuk - ($potonganBea*$uangMasuk);
				}

				$prodi = $sql->kodeProdi;
				$alumni = $sql->alumni;
				$earlybirdS2 = $sql->earlybirdS2;
				$keluargacivitas = $sql->keluargacivitas;
				$keluargadlb = $sql->keluargadlb;
				$metPembayaran = $sql->metodePembayaran;
				$isMIAI = $sql->miaiPindahan;

				$sisaAng = 0;
				$diskonAlum = 0;
				$diskonMIAI = 0;
				$diskonEarlyBird = 0;
				$diskonKlgCivitas = 0;
				$diskonKlgDlb = 0;
				$diskonLunas = 0;
				$hasilUM = $uangMasuk;

//                                Potongan Early Bird
				if ($jalur_code == '10'){
//                                Potongan Early Bird
					if ($earlybirdS2 == 1){
						if ($alumni || $keluargacivitas || $keluargadlb){
							$diskonEarlyBird = 0;
						}else{
							$diskonEarlyBird = 2000000;
							$diskonVal += 2000000;
						}
						$hasilUM = $uangMasuk - $diskonEarlyBird;
					}

					//Potongan Alumni
					if ($alumni == 1){
						$diskonAlum = $uangMasuk*1;
						$hasilUM = $uangMasuk - $diskonAlum;
						$diskonVal += $diskonAlum;
					}

					//Potongan Keluarga Inti Civitas Akademika
					if ($keluargacivitas == 1){
						$diskonKlgCivitas = $uangMasuk*1;
						$hasilUM = $uangMasuk - $diskonKlgCivitas;
						$diskonVal += $diskonKlgCivitas;
					}

					//Potongan Keluarga Inti DLB
					if ($keluargadlb == 1){
						$diskonKlgDlb = 0.7*$uangMasuk;
						$hasilUM = $uangMasuk - $diskonKlgDlb;
						if ($metPembayaran != 1){
							$uangMasuk = $hasilUM/3;
						}
						$diskonVal += $diskonKlgDlb;
					}

					//Potongan Keluarga Inti DLB
					if ($isMIAI == 1){
						$diskonMIAI = $uangMasuk;
						$hasilUM = $uangMasuk - $uangMasuk;
						$diskonVal += $diskonMIAI;
					}

					//Potongan pembayaran lunas
					if ($metPembayaran == 1){
						$diskonLunas = "";
						if ($alumni || $keluargacivitas || $keluargadlb){
							$diskonLunas = 0;
						}else{
							if ($isMIAI = 1){
								$diskonLunas = 0;
							}else{
								$diskonLunas = 3000000;
								$diskonVal += $diskonLunas;
							}
						}
						$totalDiskonUM = $diskonEarlyBird + $diskonLunas + $diskonAlum + $diskonKlgCivitas + $diskonKlgDlb + $diskonMIAI;
						$hasilUM = $uangMasuk - $totalDiskonUM;
					}else{
						$totalDiskonUM = $hasilUM;
					}
				}


				// Total Uang Masuk dibagi jumlah semester
				$hasilUM = $hasilUM/$jumlahSem;
				$totalSemester = $hasilUM + $biayaSpp + $biayaSks;

				$this->smarty->assign('diskonVal', $diskonVal);
				$this->smarty->assign('totalAll', $totalSemester);
				$this->smarty->assign('uangMasuk', $hasilUM);
				$this->smarty->assign('method',$metPembayaran);
				$this->smarty->assign('biaya', $biaya);
				$this->smarty->assign('jalur', $sql->jalur);
				$this->smarty->assign('prodi', $prodi);
				$this->smarty->assign('alumni', $alumni);
				$this->smarty->assign('earlybird', $diskonEarlyBird);
				$this->smarty->assign('diskonLunas', $diskonLunas);
				$this->smarty->assign('klgCivitas', $diskonKlgCivitas);
				$this->smarty->assign('klgDlb', $diskonKlgDlb);
				$this->smarty->assign('diskonAlum', $diskonAlum);
				$this->smarty->assign('diskonMIAI', $diskonMIAI);
				$this->smarty->assign('hasilUM', $hasilUM);
				$this->smarty->assign('biayaSpp', $biayaSpp);
				$this->smarty->assign('biayaSks', $biayaSks);

			break;
		}



		return true;
	}

	function mDaftarUlangReg($kode, $saudara, $jenjang = '', $pembayaran, $prodiType, $periode){
		$angsuran = '';

		$sql = "SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, u.username as nama_cm , p.nama as progdi, RIGHT(f.bukaSmb,4) as kodeProdi,
				SUBSTR(bukaSmb,12, 2) as jalur,
				DAYNAME(u.createTime) as hari, DATE(u.createTime) as tanggal, u.validation_status, m.jenjangType, up.rumahCell,
				f.stsReapplyPaid, f.stsReapplyPaidConfirm, j.nama as n_jalur,b.jalur as kode_jalur,
				t.kode as kode_typeTrans,
				m.tahun, m.semester, t.nama as typeTrans, t2.nama as typeAccount, DAYNAME(f.reapplyBankTransferTime) as hari_trans,
				DATE(f.reapplyBankTransferTime) as tanggal_trans, k.buktiBayarDaftarUlang, f.reapplyBankTransferAmount,
				k.noRekPengirimDaftarUlang, k.namaRekPengirimDaftarUlang, f.stsResultGrade,
				k.metodBayarDaftarUlang, k.totalBiayaDaftarUlang, k.noAtmCardDaftarulang, k.sks_acc,  k.noAtmCardPendaftaran,
				SUBSTR(metodBayarDaftarUlang, 12, 3) as lulusan, k.alumni_s1 as alumni, f.earlyBird, e.lulus_acc,
				k.kode_potongan
				FROM adis_smb_form f
				LEFT JOIN adis_smb_usr u ON u.kode = f.kode
				LEFT JOIN adis_buka_smb b ON b.kode = f.bukaSmb
				LEFT JOIN adis_periode d ON d.kode = b.periode
				LEFT JOIN adis_periode_master m ON m.kode = d.idPeriodeMaster
				LEFT JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
				LEFT JOIN adis_prodi p ON p.kode = b.prodi
				LEFT JOIN adis_jalur_smb j ON j.kode =  SUBSTR(f.bukaSmb, 12, 2)
				LEFT JOIN adis_type t ON t.kode = f.reapplyBankTransferType
				LEFT JOIN adis_type t2 ON t2.kode = f.reapplyBankAccountType
				LEFT JOIN adis_smb_usr_keu k ON k.smbUsr = f.kode
                LEFT JOIN adis_smb_usr_edu e ON e.smbUsr = f.kode
				WHERE f.kode = '$kode'";
		$sql = $this->db2->query($sql)->row();
		$this->smarty->assign("transPaid", $sql);

		$jalur_code = $sql->jalur;
		$kodePaidType = "";
		$earlybird = $sql->earlyBird;
		//echo '<script>alert("'.$jalur_code.'");</script>';
//		echo '<script>alert("'.$kodePaidType.'");</script>';

		switch($jenjang){
			case '1':
				IF  ($jalur_code == 'KP'){

						$kodePaidType = $sql-> metodBayarDaftarUlang;
						if (!$kodePaidType){
							$kodePaidType = $sql->kodeProdi.'.'.'69'.'.'.'KP'.'.'.$sql->lulus_acc;
						}

//                            		echo '<script>alert("'.$kodePaidType.'");</script>';
						$metodBayar = "SELECT A.*,
							C.tempo, C.tgl_jatuh_tempo
							FROM `adis_pembayaran_kelasmalam` A
							LEFT JOIN tbl_tgl_tempo_kelasmalam C ON C.kode_periode = A.periode_kode
							WHERE A.kode = '$kodePaidType' AND A.periode_kode = '$periode' ;";

						$biaya = $this->db2->query($metodBayar)->row();

						$potonganBiaya = 0;
						if($sql->kode_potongan != ""){
							$listPotongan = $this->mportal->selectPotonganMaster($kodePaidType, $sql->kode_potongan, $periode);
							$potUangMasuk = 0; $potSpp = 0; $potSks = 0; $potBpp = 0;
							foreach($listPotongan as $nilai){
								switch ($nilai['kode_biaya']){
									case 'UM':
										if($nilai['lambang'] == '%'){
											$potUangMasuk = ($biaya->uangMasuk*($nilai['nilai']/100));
										}else{
											$potUangMasuk = $biaya->uangMasuk-$nilai['nilai'];
										}
									break;
									case 'SPP':
									break;
									case 'SKS':
									break;
									case 'BPP':
									break;
								}
							}
							$potonganBiaya = $potUangMasuk + $potSpp + $potSks + $potBpp;
						}
						$this->smarty->assign('potonganBiaya', $potonganBiaya);

						$pilBiaya = json_decode($biaya->pilihanBiayaUM);
						$this->smarty->assign('pilihanBiaya',$pilBiaya);

						$biayaSks = $biaya->biayaPerSks * $biaya->jumlahSks;
						$total = $biaya->uangMasuk + $biaya->spp + $biayaSks;
						$angsurPerBulan = $total/$biaya->tempoAngsuran;

						$tglJatuhTempo = $biaya->tgl_jatuh_tempo;
						$tglJatuhTempo = json_decode($tglJatuhTempo);

						$detailAngsur = array();
						$angsuran = array();

						foreach ($tglJatuhTempo as $k => $val){
							$dateTempo = date("Y-m-d", strtotime($val));
							$angsuran[$k] = array('angsuran'=>$angsurPerBulan,'tglTempo'=>$dateTempo);
						}
//                                    print_r($angsuran);

						$this->smarty->assign('uangMasuk', $biaya->uangMasuk);
						$this->smarty->assign('total', $total);
						$this->smarty->assign('method','69');
						$this->smarty->assign('biaya', $biaya);
						$this->smarty->assign('jalur', $sql->jalur);
						$this->smarty->assign('prodi', $sql->kodeProdi);
						$this->smarty->assign('angsuran', $angsuran);
						$this->smarty->assign('angsuranPerBulan', $angsurPerBulan);
						$this->smarty->assign('kodePayTipe', $kodePaidType);

				}ELSE{
					$ori_jalur = '';
					if ($jalur_code == '02' || $jalur_code == 'JP'){
						$ori_jalur = $jalur_code;
						$jalur_code = '01';
					}
					$kodePaidType = implode(".",array($prodiType,$pembayaran,$jalur_code));

					$sks_acc = $sql->sks_acc;
					$rank = $sql->stsResultGrade;
//						$kodePaidType = $sql->metodBayarDaftarUlang;
//						echo '<script>alert("Kode Tipe :'.$kodePaidType.'");</script>';
					$metodBayar = "SELECT p.*, c.*, c.kode as code, p.kode as kode_pembayaran
							FROM adis_pembayaran p
							LEFT JOIN adis_cicilan c ON c.id_pembayaran = p.kode AND p.periode_kode = c.periode_kode
							WHERE p.kode='$kodePaidType' AND p.periode_kode = '$periode'";
					$biaya = $this->db2->query($metodBayar)->row();

					if ($ori_jalur == 'JP'){
						if ($rank == '1'){$uangMasuk = $biaya->uangMasuk1*0.25;}
						if ($rank == '2'){$uangMasuk = $biaya->uangMasuk1*0.5;}
						if ($rank == '3'){$uangMasuk = $biaya->uangMasuk1*0.75;}

						if($rank == 1){	$total = ($biaya->total1-$biaya->uangMasuk1)+$uangMasuk;}
						if($rank == 2){	$total = ($biaya->total1-$biaya->uangMasuk1)+$uangMasuk;}
						if($rank == 3){	$total = ($biaya->total1-$biaya->uangMasuk1)+$uangMasuk;}

					}else{
						if($rank == 1){	$total = $biaya->total1;}
						if($rank == 2){	$total = $biaya->total2;}
						if($rank == 3){	$total = $biaya->total3;}

						if ($rank == '1'){$uangMasuk = $biaya->uangMasuk1;}
						if ($rank == '2'){$uangMasuk = $biaya->uangMasuk2;}
						if ($rank == '3'){$uangMasuk = $biaya->uangMasuk3;}
					}
					// echo '<script>alert("Uang masuk :'.$uangMasuk.'");</script>';

					if ($rank == 5){ $total = $biaya->total1 - $biaya->uangMasuk1;}
					if ($rank == 5){ $uangMasuk = 0;}

					$sisaAng = 0;
					$angsuran1 = 0;
					$angsuran2 = 0;
					$angsuran3 = 0;
					$totalDiskon = 0;
					$diskonSau = "";
					$diskonSKS = 0;
					$totalAll = $total;
					$totalUangMasuk = $uangMasuk;
					$earlyBirdPot = 0;

					//Potongan memiliki saudara
					if ($sql->kodeProdi != '1204'){
						if ($earlybird == 99){
							$earlyBirdPot = 30/100*$uangMasuk;
						}

						if ($saudara != 0){
							if ($earlybird != 99 || $pembayaran != 1){
								$diskonSau = 20/100*$uangMasuk;
							}else{
								$diskonSau = 20/100*($uangMasuk - $earlyBirdPot);
							}
						}
					}


					if ($sks_acc > 40 && $sks_acc <= 80){
							$diskonSKS = (25/100)*($totalUangMasuk-$diskonSau);
					}else if ($sks_acc > 80){
							$diskonSKS = (50/100)*($totalUangMasuk-$diskonSau);
					}

					//JUMLAH DISKON SAUDARA DENGAN SKS
					$totalDiskon = $diskonSau + $diskonSKS;

					//Potongan pembayaran lunas
					if ($pembayaran == 1){
							if ($sql->kodeProdi != '1204'){
								if($sql->jalur == '01' || $sql->jalur == '02'){
									if ($earlybird == 99){
										$totalDiskon = $diskonSau + $diskonSKS + $earlyBirdPot;
									}else{
										$totalDiskon = $diskonSau + $diskonSKS + 1000000;
									}
								}
							}
					}else{
						//Pemabyaran Angsuran

					if ($rank == '1'){$totalAll = ($total - $uangMasuk);} //+ $biaya->angsuran1_1;}
					if ($rank == '2'){$totalAll = ($total - $uangMasuk);} //+ $biaya->angsuran1_2;}
					if ($rank == '3'){$totalAll = ($total - $uangMasuk);} //+ $biaya->angsuran1_3;}

					if ($rank == '1'){
							$angsuran = array(
									'angsuran1'=>$biaya->angsuran1_1,
									'angsuran2'=>$biaya->angsuran2_1,
									'angsuran3'=>$biaya->angsuran3_1 - $totalDiskon,
							);
					}
					if ($rank == '2'){
							$angsuran = array(
									'angsuran1'=>$biaya->angsuran1_2,
									'angsuran2'=>$biaya->angsuran2_2,
									'angsuran3'=>$biaya->angsuran3_2 - $totalDiskon,
							);
					}
					if ($rank == '3'){
							$angsuran = array(
									'angsuran1'=>$biaya->angsuran1_3,
									'angsuran2'=>$biaya->angsuran2_3,
									'angsuran3'=>$biaya->angsuran3_3 - $totalDiskon
							);
					}

					$sisaAng = $angsuran['angsuran1']+$angsuran['angsuran2']+$angsuran['angsuran3'];

					$angsuran1 = $angsuran['angsuran1'];
					if ($angsuran['angsuran3']<0){
							$angsuran2 = $angsuran['angsuran2'] + $angsuran['angsuran3'];
					}else{
							$angsuran2 = $angsuran['angsuran2'];
					}

					if ($angsuran['angsuran3'] < 0){
							$angsuran3 = $angsuran['angsuran3'] - $angsuran['angsuran3'];
					}else{
							$angsuran3 = $angsuran['angsuran3'];
					}

					$totalAll = ($totalAll + $angsuran['angsuran1']) - ($totalDiskon-$diskonSau);

				}

				$totalUangMasuk = $uangMasuk - $totalDiskon ;
				$total = $total - $totalDiskon;

				$this->smarty->assign('uangMasuk', $uangMasuk);
				$this->smarty->assign('earlyBird', $earlybird);
				$this->smarty->assign('angsuran',$angsuran);
				$this->smarty->assign('angsuran1',$angsuran1);
				$this->smarty->assign('angsuran2',$angsuran2);
				$this->smarty->assign('angsuran3',$angsuran3);
				$this->smarty->assign('sisaAng',$sisaAng);
				$this->smarty->assign('diskonSau', $diskonSau);
				$this->smarty->assign('totalDiskon', $totalDiskon);
				$this->smarty->assign('totalUangMasuk', $totalUangMasuk);
				$this->smarty->assign('totalAll', $totalAll);
				$this->smarty->assign('total', $total);
				$this->smarty->assign('method',$pembayaran);
				$this->smarty->assign('biaya', $biaya);
				$this->smarty->assign('jalur', $sql->jalur);
				$this->smarty->assign('prodi', $sql->kodeProdi);


			}
			break;
			case '2':

				$totalAll = 0;
				$diskonAlum = 0;
                                $total = 0;
                                $pembayaran = 0;
                                $biaya = 0;
				$prodi = $sql->kodeProdi;
				$alumni = $sql->alumni;

				$sisaAng = 0;

				$this->smarty->assign('totalAll', $totalAll);
				$this->smarty->assign('diskonAlum', $diskonAlum);
				$this->smarty->assign('total', $total);
				$this->smarty->assign('method',$pembayaran);
				$this->smarty->assign('biaya', $biaya);
				$this->smarty->assign('jalur', $sql->jalur);
				$this->smarty->assign('prodi', $prodi);
				$this->smarty->assign('alumni', $alumni);
                        break;
			case '2_later':
				$kodePaidType = $sql->metodBayarDaftarUlang;
				$metodBayar = "SELECT p.*, p.kode as kode_pembayaran FROM adis_pembayaran_pasca p
					WHERE p.kode='$kodePaidType'";
				$biaya = $this->db2->query($metodBayar)->row();
				$pembayaran = $biaya->pembayaran;
				$total = $biaya->totalAll;
				$totalDU = $biaya->totalDaftarUlang;

				$prodi = $sql->kodeProdi;
				$alumni = $sql->alumni;

				$sisaAng = 0;
				$totalAll = 0;
				$diskonAlum = 0;

				//Potongan Alumni
				if ($alumni == 1){
					if ($prodi != '1234'){
						$total = $total - $biaya->totalUangMasuk;
						$diskonAlum = $biaya->totalUangMasuk;
					}else{
						$total = $total - (0.5*$biaya->totalUangMasuk);
						$diskonAlum = 0.5*$biaya->totalUangMasuk;
					}
				}

				//Potongan pembayaran lunas
				if ($pembayaran == 1){
					$totalAll = $total - 5000000;
				}else{
					$totalAll =  $total ;
				}


				$this->smarty->assign('totalAll', $totalAll);
				$this->smarty->assign('diskonAlum', $diskonAlum);
				$this->smarty->assign('total', $total);
				$this->smarty->assign('method',$pembayaran);
				$this->smarty->assign('biaya', $biaya);
				$this->smarty->assign('jalur', $sql->jalur);
				$this->smarty->assign('prodi', $prodi);
				$this->smarty->assign('alumni', $alumni);

			break;
		}



		return true;
	}

	function submit_kk_du($data){
		$data['jumlahBayar'] = str_replace('.', '', $data['jumlahBayar']);

		$this->db2->trans_begin();

		$this->db2->where("kode", $data['kode']);
		$this->db2->update("adis_smb_form", array(
			"reapplyBankTransferType"=>$data["typeTrans"],
			"reapplyBankAccountType"=>$data["bankAkun"],
			"reapplyBankTransferAmount"=>$data['jumlahBayar'],
			"reapplyBankTransferTime"=>date('Y-m-d', strtotime($data['tanggalBayar'])),
			"stsReapplyPaid"=>1
		));

		$this->db2->where("smbUsr", $data['kode']);
		$this->db2->update("adis_smb_usr_keu", array(
			"updateTime"=>date('Y-m-d H:i:s'),
			"updateUser"=>$data['kode'],
			"buktiBayarDaftarUlang"=>$data['file_name'],
			"noRekPengirimDaftarUlang"=>$data["nama_rek_cmb"],
			"namaRekPengirimDaftarUlang"=>$data["nama_rek_cmb"],
			"metodBayarDaftarUlang"=>$data['pembayaran_kode'],
			"totalBiayaDaftarUlang"=>$data['jumlahBayar'],
			"pilihan_angsuran_km"=>(isset($data['pilih_bayar']) ? $data['pilih_bayar'] : NULL),
			"tolakDU"=>0
		));

		if ($this->db2->trans_status() === FALSE){
			$this->db2->trans_rollback();
			return 0;
		}else{
			$this->db2->trans_commit();
			return 1;
		}
	}

	function submit_magister_du($data){
		$data['jumlahBayar'] = str_replace('.', '', $data['jumlahBayar']);

		$this->db2->trans_begin();

		$this->db2->where("kode", $data['kode']);
		$this->db2->update("adis_smb_form", array(
			"reapplyBankTransferType"=>$data["typeTrans"],
			"reapplyBankAccountType"=>$data["bankAkun"],
			"reapplyBankTransferAmount"=>$data['jumlahBayar'],
			"reapplyBankTransferTime"=>date('Y-m-d', strtotime($data['tanggalBayar'])),
			"stsReapplyPaid"=>1
		));

		$this->db2->where("smbUsr", $data['kode']);
		$this->db2->update("adis_smb_usr_keu", array(
			"updateTime"=>date('Y-m-d H:i:s'),
			"updateUser"=>$data['kode'],
			"buktiBayarDaftarUlang"=>$data['file_name'],
			"noRekPengirimDaftarUlang"=>$data["nama_rek_cmb"],
			"namaRekPengirimDaftarUlang"=>$data["nama_rek_cmb"],
			'pilihan_angsuran_km'=>$data['skema_biaya'],
			"metodBayarDaftarUlang"=>$data['pembayaran_kode'],
			"totalBiayaDaftarUlang"=>$data['jumlahBayar'],
			"skema_id"=>$data['id_skema'],
			"tolakDU"=>0
		));

		if ($this->db2->trans_status() === FALSE){
			$this->db2->trans_rollback();
			return 0;
		}else{
			$this->db2->trans_commit();
			return 1;
		}
	}


	function mSimpanDaftarUlang($file_name, $kode, $jenjang, $jalur = ''){
		$date = date("Y-m-d H:i:s");

		$jumlahBayar = str_replace(' ', '', str_replace('.', '', $this->input->post("jumlahBayar")));
		$earlybird = "0";

		$this->db2->trans_begin();

		// $this->db2->where('kode', $kode);
		// $this->db2->update('adis_smb_usr_pribadi', array(
		// 	'ibuNama'=>$this->input->post('nama_ibu'),
		// 	'nomorKtp'=>$this->input->post('no_ktp')
		// ));

		$this->db2->where("kode", $kode);
		$this->db2->update("adis_smb_form", array(
				"reapplyBankTransferType"=>$this->input->post("typeTrans"),
				"reapplyBankAccountType"=>$this->input->post("bankAkun"),
				"reapplyBankTransferAmount"=>$jumlahBayar,
				"reapplyBankTransferTime"=>date('Y-m-d', strtotime($this->input->post("tanggalBayar"))),
				"stsReapplyPaid"=>1,
				'earlyBird'=>$earlybird
			));

		$qryMhs = "SELECT LEFT(A.bukaSmb, 8) as periode, RIGHT(A.bukaSmb, 4) as prodi, B.lulus_acc, nomor
			FROM adis_smb_form A
			LEFT JOIN adis_smb_usr_edu B ON B.smbUsr = A.kode
			WHERE A.kode = '$kode';";
		$qryMhs = $this->db2->query($qryMhs)->row_array();

		$kodeS2 = implode (".", array($qryMhs['periode'], $qryMhs['prodi'],'1'));

		if($jalur == 'KP'){
			$kodeS1 = implode (".", array($qryMhs['prodi'],'69', 'KP', $qryMhs['lulus_acc']));
		}else{
			$kodeS1 = implode (".", array($qryMhs['prodi'],'1', '01'));
		}

		$kodeMetod = '';
		if( $jenjang == '1') {
			$kodeMetod = $kodeS1;
		}else if($jenjang == '2'){
			$kodeMetod = $kodeS2;
		}else{
			echo '<script>alert("Sesi ada telah habis, silahkan ulang transaksi Anda!");</script>';
			redirect ('/portal', 'refresh');
		}
		// $kodeMetod = ($jenjang == '1' ) ? $kodeS1 : $kodeS2;
		$paramBiaya = $this->input->post('paramBiaya');
		$dataAngsuran = $this->input->post('dataAngsuran');

		// $this->mGenTagihan($kode, $qryMhs['nomor'], $dataAngsuran);
		$this->db2->where("smbUsr", $kode);
		$this->db2->update("adis_smb_usr_keu", array(
				"updateTime"=>$date,
				"updateUser"=>$kode,
				"buktiBayarDaftarUlang"=>$file_name,
				"noRekPengirimDaftarUlang"=>$this->input->post("no_rek_cmb"),
				"namaRekPengirimDaftarUlang"=>$this->input->post("nama_rek_cmb"),
				'pilihan_angsuran_km'=>$this->input->post('pilihDU'),
				"metodBayarDaftarUlang"=>$kodeMetod,
				"totalBiayaDaftarUlang"=>$jumlahBayar,
				"noAtmCardDaftarulang"=>$this->input->post("noAtmCard"),
				'cicilan1'=>$this->input->post('angsuran1'),
				'cicilan2'=>$this->input->post('angsuran2'),
				'cicilan3'=>$this->input->post('angsuran3'),
				'parameter_biaya'=>$paramBiaya,
				'data_angsuran'=>$dataAngsuran,
				"tolakDU"=>0
			));

		if ($this->db2->trans_status() === FALSE){
			$this->db2->trans_rollback();
		}else{
			$this->db2->trans_commit();
		}
	}

	function mGenTagihan($kode = '', $nomor = '', $angsuran = ''){

		$angsuran = json_decode(base64_decode($angsuran), true);

		$numCurrent = 1;

		foreach($angsuran as $k => $val){
			$nomor_tagihan = $nomor . str_pad($numCurrent, 2, 0, STR_PAD_LEFT);

			$this->db2->trans_start();
			$this->db2->insert('tbl_tagihan_cmb', array(
				'kode_mhs'=>$kode,
				'kode_pembayaran'=>$nomor_tagihan,
				'nomor_tagihan'=>$nomor_tagihan,
				'jumlah_tagihan'=>$val['tagihan'],
				'tanggal_tempo'=>$val['tglTempo'],
				'jenis_tagihan'=>2,
				'status_bayar'=>0
			));

			if ($this->db2->trans_status() === FALSE){
				$this->db2->trans_rollback();
			}else{
				$this->db2->trans_commit();
			}

			$numCurrent++;
		}

	}

	function mSaveAlamat($file_name, $kode, $date){

		$tgl_lahir = $this->input->post('date_');
		$tgl_lahir .= '-'.$this->input->post('month_');
		$tgl_lahir .= '-'.$this->input->post('year_');

		$tgl_lahir = date("Y-m-d", strtotime($tgl_lahir));

		$this->db2->where('kode', $kode);
		$this->db2->update('adis_smb_usr_pribadi',
				array(  "updateUser"=>$kode,
						"updateTime"=>$date,
						"nama"=> ucwords($this->input->post("nama")),
						"nama_panggilan"=> ucwords($this->input->post("nama_panggilan")),
						"tempatLahir" => ucwords($this->input->post("tempatLahir")),
						'nomorKtp'=>$this->input->post('no_id'),
						// "tanggalLahir" => date("Y-m-d", strtotime($this->input->post("tanggalLahir"))),
						"tanggalLahir" => $tgl_lahir,
						"agamaType" => $this->input->post("agama"),
						"rumahAlamat" => ucwords($this->input->post("occupation")),
						"rumahProp" => $this->input->post("propinsi"),
						"rumahKabKota" => $this->input->post("kabkota"),
						"rumahKodePos" => $this->input->post("kodepos"),
						"rumahCell" => $this->input->post("nomorHP"),
						"rumahTel" => $this->input->post("telRumah"),
						"suratAlamat" => $this->input->post("occupationsur"),
						"suratProp" => $this->input->post("propinsi2"),
						"suratKabKota" => $this->input->post("kabkota2"),
						"suratKodePos" => $this->input->post("kodepos2"),
						"suratTel" => $this->input->post("telRumah2"),
						"suratCell" => $this->input->post("nomorHPOrtu"),
						"genderType" => $this->input->post("gender"),
						"statusAlamat" =>1,
						"rumahKecamatan" => ucwords($this->input->post("kecamatan")),
						"rumahKecamatanKode" => ucwords($this->input->post("rumahKecamatanKode")),
						"rumahKelurahan" => ucwords($this->input->post("kelurahan")),
						"rumahRT" => $this->input->post("rt"),
						"rumahRW" => $this->input->post("rw")
                ));
	}

	function mAddSaudara($kode){
		$unik = uniqid();
		$time = date("Y-m-d H:i:s");
		$this->db2->insert('adis_smb_usr_kel', array(
							"createUser"=>$kode,
							"createTime"=>$time,
							"kode"=>$unik,
							"smbUsr"=>$kode,
							"nomor"=>$this->input->post("nomorKe"),
							"nama"=>$this->input->post("namaSaudara"),
							"genderType"=>$this->input->post("sexSaudara"),
							"prodi"=>$this->input->post("pendSaudara"),
							"lulus"=>$this->input->post("sekSau"),
							"nim"=>$this->input->post("nim"),
							"kerja"=>$this->input->post("perSau"),
							"angkatan"=>$this->input->post("angkatan"),
							"tanggalLahir"=>$this->input->post("tglLahirSau"),
							"status"=>1));

		$this->db2->where('kode',$kode);
		$this->db2->update('adis_smb_usr_pribadi', array('adaSaudara' => +1));
	}

	function mSelectSaudara($kode){
		$query = "	SELECT s.kode as kodeSaudara, s.smbUsr, s.nomor, s.nama as namaSaudara, s.genderType,
					t.nama as gender, p.nama as prodi, s.confirmed,
					p.singkatan as singProdi, s.angkatan, s.lulus, s.nim,
					s.tanggalLahir, s.kerja, s.status
					FROM adis_smb_usr_kel s
					INNER JOIN adis_type t ON t.kode = s.genderType
					LEFT JOIN adis_prodi p ON p.kode = s.prodi
					WHERE s.smbUsr = '$kode' AND s.erased = 0 ORDER BY s.nomor;
				 ";
		$result = $this->db2->query($query)->result();
		$resultSau = $this->db2->query($query)->row();
		$num_rows =  $this->db2->query($query)->num_rows();

		$this->smarty->assign('saudara',$result);
		$this->smarty->assign('sau',$resultSau);
		$this->smarty->assign('saudara_rows',$num_rows);
	}



	function mAddOrtu($kode){
		$time = date("Y-m-d H:i:s");
		$this->db2->where('kode',$kode);
		$this->db2->update('adis_smb_usr_pribadi', array(
							'updateuser'=>$kode,
							'updateTime'=>$time,
							'ibuNama'=>ucwords($this->input->post('namaIbu')),
							'ibuAlamat'=>ucwords($this->input->post('alamatIbu')),
							'ibuKabKota'=>$this->input->post('kabIbu'),
							'ibuProp'=>$this->input->post('propIbu'),
							'ibuKodePos'=>$this->input->post('kposIbu'),
							'ibuTel'=>$this->input->post('hpIbu'),
							'ibuCell'=>$this->input->post('hpIbu'),
							'ayahNama'=>ucwords($this->input->post('namaAyah')),
							'ayahAlamat'=>ucwords($this->input->post('alamatIbu')),
							'ayahKabKota'=>$this->input->post('kabIbu'),
							'ayahProp'=>$this->input->post('propIbu'),
							'ayahKodePos'=>$this->input->post('kposIbu'),
							'ayahTel'=>$this->input->post('hpIbu'),
							'ayahCell'=>$this->input->post('hpIbu'),
							'waliNama'=>$this->input->post('namaAyah'),
							'waliAlamat'=>ucwords($this->input->post('alamatIbu')),
							'waliKabKota'=>$this->input->post('kabIbu'),
							'waliProp'=>$this->input->post('propIbu'),
							'waliKodePos'=>$this->input->post('kposIbu'),
							'waliCell'=>$this->input->post('hpIbu'),
							'pekerjaan'=>ucwords($this->input->post('pekerjaan')),
							'pendidikan'=>$this->input->post('pendidikan'),
							'stsPribadi'=>1,
							'statusKeluarga'=>1,
							'sumber_biaya'=>ucwords($this->input->post('sumber_biaya'))
							));

		// $this->db2->where("kode", $kode);
		// $this->db2->update("adis_smb_form", array(
		// 	"stsReapplyPaid"=>1
		// ));


	}

	function mSelectOrtu($kode){
		$qry =	"SELECT
				p.kode, p.ayahNama, p.ayahAlamat, p.ayahKabKota, p.ayahProp, p.ayahKodePos,	p.ayahCell, p.ayahTel,p.ayahEmail,
				p.ibuNama, p.ibuAlamat,	p.ibuKabKota, p.ibuProp, p.ibuKodePos, p.ibuCell, p.ibuTel,	p.ibuEmail,
				p.waliNama, p.waliAlamat, p.waliKabKota, p.waliProp, p.waliKodePos, p.waliCell,	p.waliTel, p.waliEmail,
				t.nama as ayahPropNama, t2.nama as ayahKabNama, t3.nama as ibuKabNama, t4.nama as ibuPropNama,
				t6.nama as waliPropNama, t7.nama as waliKabNama, sumber_biaya,
				k1.kodepos as ayahKodePosNama, k2.kodepos as ibuKodePosNama, k3.kodepos as waliKodePosNama, p.statusKeluarga, p.pekerjaan, p.pendidikan
				FROM adis_smb_usr_pribadi p
				LEFT JOIN adis_wil t ON p.ayahProp = t.kode
				LEFT JOIN adis_wil t2 ON p.ayahKabKota = t2.kode
				LEFT JOIN adis_wil t3 ON p.ibuKabKota = t3.kode
				LEFT JOIN adis_wil t4 ON p.ibuProp = t4.kode
				LEFT JOIN adis_wil t6 ON p.waliProp = t6.kode
				LEFT JOIN adis_wil t7 ON p.waliKabKota = t7.kode
				LEFT JOIN adis_kodepos k1 ON p.ayahKodePos = k1.kode
				LEFT JOIN adis_kodepos k2 ON p.ibuKodePos = k2.kode
				LEFT JOIN adis_kodepos k3 ON p.waliKodePos = k3.kode
				WHERE p.kode ='$kode';";
		$qry = $this->db2->query($qry)->row();
		$this->smarty->assign('ortu',$qry);
	}

	function mSelectPendidikan($kode){
		$qry = "SELECT e.kode as kodeEdu, e.smbUsr, e.sekolahType, e.sekolahOwnerType,
				e.nama as sekolah, e.prop, e.kabKota, e.kodePos,
				e.jurusan, e.tahunLulus, e.nilai, e.nisn_nim, lulusAsal,
				t.nama as sekolahNama, t2.nama as sekolahTipe, w.nama as propNama, w2.nama as kabNama, k.kodepos
				FROM adis_smb_usr_edu e
				LEFT JOIN adis_type t ON t.kode = e.sekolahType
				LEFT JOIN adis_type t2 ON t2.kode = e.sekolahOwnerType
				LEFT JOIN adis_wil w ON w.kode = e.prop
				LEFT JOIN adis_wil w2 ON w2.kode = e.kabKota
				LEFT JOIN adis_kodepos k ON k.kode = e.kodePos
				WHERE e.smbUsr = '$kode' AND e.erased = 0;
				";
		$qry = $this->db2->query($qry)->result();
		$this->smarty->assign('eduList', $qry);

		$qry1 = "SELECT e.kode as kodeEdu, e.smbUsr, e.sekolahType, e.sekolahOwnerType, e.nama as sekolah, e.prop, e.kabKota, e.kodePos,
				e.jurusan, e.tahunLulus, e.nilai, e.nisn_nim, lulusAsal,
				t.nama as sekolahNama, t2.nama as sekolahTipe, w.nama as propNama, w2.nama as kabNama, k.kodepos as kodeposN
				FROM adis_smb_usr_edu e
				LEFT JOIN adis_type t ON t.kode = e.sekolahType
				LEFT JOIN adis_type t2 ON t2.kode = e.sekolahOwnerType
				LEFT JOIN adis_wil w ON w.kode = e.prop
				LEFT JOIN adis_wil w2 ON w2.kode = e.kabKota
				LEFT JOIN adis_kodepos k ON k.kode = e.kodePos
				WHERE e.smbUsr = '$kode' AND e.erased = 0;
				";
		$qry1 = $this->db2->query($qry1)->row();
		$this->smarty->assign('eduData', $qry1);
	}

	function mAddPendidikan($kode, $ijazah, $rapor){
		$date = date("Y-m-d H:i:s");
		$exist = $this->db2->query("SELECT smbUsr FROM adis_smb_usr_edu WHERE smbUsr = '$kode'")->num_rows();
		if($exist > 0){
			$this->db2->where('smbUsr', $kode);
			$this->db2->update('adis_smb_usr_edu',array(
								"updateUser"=>$kode,
								"updateTime"=>$date,
								'sekolahType'=> $this->input->post("pendType"),
								'sekolahOwnerType'=> $this->input->post("sekType"),
								"nama"=> ucwords($this->input->post("namaSekolah")),
								'prop'=> $this->input->post("prop"),
								'kabKota'=> $this->input->post("kab"),
								'kodePos'=> $this->input->post("kpos"),
								'tahunLulus'=> $this->input->post("tahunLulus"),
								'jurusan'=> ucwords($this->input->post("jurusan")),
								'nilai'=> $this->input->post("nilai"),
								'nisn_nim'=>$this->input->post("nisn"),
								'status'=>1,
								'nama_guru'=>$this->input->post("guru"),
								'jabatan_guru'=>$this->input->post("jabatan"),
								'hp_telp_guru'=>$this->input->post("hp")
								));
		}else{
			$unik = uniqid();
			$this->db2->insert('adis_smb_usr_edu',array(
								"kode"=> $unik,
								"smbUsr"=> $kode,
								"updateUser"=>$kode,
								"updateTime"=>$date,
								'sekolahType'=> $this->input->post("pendType"),
								'sekolahOwnerType'=> $this->input->post("sekType"),
								"nama"=> $this->input->post("namaSekolah"),
								'prop'=> $this->input->post("prop"),
								'kabKota'=> $this->input->post("kab"),
								'kodePos'=> $this->input->post("kpos"),
								'tahunLulus'=> $this->input->post("tahunLulus"),
								'jurusan'=> $this->input->post("jurusan"),
								'nilai'=> $this->input->post("nilai"),
								'nisn_nim'=>$this->input->post("nisn"),
								'status'=>1,
								'nama_guru'=>$this->input->post("guru"),
								'jabatan_guru'=>$this->input->post("jabatan"),
								'hp_telp_guru'=>$this->input->post("hp")
								));
		}
	}

	function mAddPrestasi($kode, $file_prestasi){
		$date = date("Y-m-d H:i:s");
		$unik = uniqid();
		$this->db2->insert('adis_smb_usr_prestasi',array(
							"createUser"=>$kode,
							"createTime"=>$date,
							"kode"=>$unik,
							'smbUsr'=> $kode,
							'namaInstitusi'=> $this->input->post("instPres"),
							'namaPrestasi'=> $this->input->post("namaPres"),
							'tahun'=> $this->input->post("tahun"),
							'file_prestasi'=>$file_prestasi // @tambahBaru
							));
	}

	function mAddOrganisasi($kode){
		$date = date("Y-m-d H:i:s");
		$unik = uniqid();
		$this->db2->insert('adis_smb_usr_org',array(
							"createUser"=>$kode,
							"createTime"=>$date,
							"kode"=>$unik,
							'smbUsr'=> $kode,
							'namaKegiatan'=> $this->input->post("namaOrg"),
							'jabatan'=> $this->input->post("jabatanOrg"),
							'tempat'=> $this->input->post("tempatOrg"),
							'dariTahun'=> $this->input->post("mulaiOrg"),
							'sampaiTahun'=> $this->input->post("selesaiOrg")
							));
	}

	function saveFormulirS2($post){
		$date = date("Y-m-d H:i:s");
		$dateLahir = date("Y-m-d", strtotime($post["birth"]));

		$this->db2->trans_begin();

		$this->db2->where('kode', $post['kode']);
		$this->db2->update('adis_smb_usr_pribadi',
				array(  "updateUser"=>$post['kode'],
						"updateTime"=>$date,
						"nama"=> $post["nama"],
						"tempatLahir" => $post["birth"],
						"tanggalLahir" => $dateLahir,
						"agamaType" => $post["religion"],
						"rumahAlamat" => $post["occupation"],
						"rumahProp" => $post["propinsi"],
						"rumahKodePos" => $post["postal_code"],
						"genderType" => $post["gender"],
						'ibuNama'=> $post['mother_name'],
						'status_nikah'=> $post['marital'],
						'company_name'=> $post['company'],
						'postion_comp'=> $post['position'],
						'phone_comp'=> $post['phone_comp'],
						'city_comp'=> $post['city_comp'],
						"statusAlamat" =>1,
						"stsPribadiConfirm"=>1
                ));

		$unik = uniqid();
		$this->db2->insert('adis_smb_usr_edu',array(
							"kode"=> $unik,
							"smbUsr"=> $post['kode'],
							"updateUser"=>$post['kode'],
							"updateTime"=>$date,
							"nama"=> $post["graduated_univ"],
							'tahunLulus'=> $post["grad_year"],
							'jurusan'=> $post["major"],
							'nilai'=> $post["gpa"],
							'nisn_nim'=>$post["student_id"],
							'status'=>1
							));

		if ($this->db2->trans_status() === FALSE){
			$this->db2->trans_rollback();
			return false;
		}else{
			$this->db2->trans_commit();
			return true;
		}

	}

	function mRuangSmb($kode){
		$date = date("Y-m-d H:i:s");
		// $this->db2->query("SET @_periode = (SELECT substr(bukaSmb, 1, 8) FROM adis_smb_form WHERE kode ='$kode');");
		$this->db2->query("SET @_periode = (
				SELECT a.kode FROM adis_periode a
				inner join adis_periode_master b on a.idPeriodeMaster = b.kode
				WHERE a.status = 1 and `jenjangType` = 1);");
		$this->db2->query("SET @_jalur = (SELECT substr(bukaSmb, 12, 2) FROM adis_smb_form WHERE kode ='$kode');");

		$this->db2->query("SET @_prioritas = (SELECT min(e.prioritas) FROM adis_event_smb e
									INNER JOIN adis_ruang r ON r.kode = e.ruang
									WHERE periode = @_periode AND jalur = @_jalur
									AND e.totalPeserta < r.kursiTes
									AND e.erased = 0 AND statusJadwal = 1 LIMIT 1);");

		$ruang = "SELECT e.kode, e.nama, e.periode, e.tanggal, e.jalur,e.jamMasuk, e.ruang, e.petugas, e.pewawancara, e.jamKeluar, e.statusJadwal,
							e.totalPeserta, e.pewawancara2, e.prioritas, r.kursiTes
							FROM adis_event_smb e
							INNER JOIN adis_ruang r ON r.kode = e.ruang
							WHERE periode = @_periode
							AND jalur = @_jalur
							AND e.erased = 0 AND e.statusJadwal = 1
							AND e.totalPeserta != r.kursiTes
							AND e.prioritas = @_prioritas LIMIT 1;";

		$ruang = $this->db2->query($ruang)->row();


		if (!$ruang){
			return 0;

		}else{
			$kodeEvent = $ruang->kode;
			$totalPeserta = $ruang->totalPeserta;
			$ruangEvent = $ruang->ruang;

			$this->db2->where("kode", $kodeEvent);
			$this->db2->update("adis_event_smb", array('totalPeserta'=>$totalPeserta+1));

			$this->db2->where("kode", $kode);
			$this->db2->update("adis_smb_form", array(
								'updateUser'=>$kode,
								'updateTime'=>$date,
								'event'=>$kodeEvent,
								'ruangEvent'=>$ruangEvent,
								'stsEventConfirm'=>1
								));

			return 1;
		}
	}

	function mSelectJadwal($kode){
		$sql = "SELECT u.nama as namaCmb, f.kode, f.nomor, f.bukaSmb, f.`event`, s.nama as progdi, j.nama as jalurCmb,
				e.kode, e.nama as namaEvent, e.periode, DAY(e.tanggal) as days, u.foto,
				e.tanggal, DAYNAME(e.tanggal) as hari, MONTHNAME(e.tanggal) as bulan, YEAR(e.tanggal) as tahun,
				e.jalur,e.jamMasuk, e.ruang, usr.jurusan,
				e.petugas, e.pewawancara, e.jamKeluar, e.statusJadwal, usr.jalur_penerimaan,
				e.totalPeserta, e.pewawancara2, e.prioritas, r.kursiTes, r.nama as namaRuang,
				g.nama as namaGedung, g.alamat, g.fax, w.nama as kabKota, g.kodePos, wa.nama as prop, g.tel,
				per.pengumuman, per.ipaKey, per.ipsKey
				FROM adis_smb_form f
				LEFT JOIN adis_smb_usr usr ON f.kode = usr.kode
				LEFT JOIN adis_event_smb e ON f.`event`=e.kode
				LEFT JOIN adis_smb_usr_pribadi u ON f.kode=u.kode
				LEFT JOIN adis_jalur_smb j ON j.kode = e.jalur
				LEFT JOIN adis_prodi s ON RIGHT(f.bukaSmb, 4) = s.kode
				LEFT JOIN adis_ruang r ON r.kode = e.ruang
				LEFT JOIN adis_gedung g ON g.kode = r.gedung
				LEFT JOIN adis_wil w ON w.kode = g.kabKota
				LEFT JOIN adis_wil wa ON wa.kode = g.prop
				left join adis_periode per on left(f.bukaSmb, 10) = per.kode
				WHERE f.kode = '$kode'";
		$sql = $this->db2->query($sql)->row();
		$this->smarty->assign('jadwal',$sql);
	}

	function mJadwalInterview($kode){
		$sql = "SELECT u.nama as namaCmb, f.kode, f.nomor, f.bukaSmb, f.`event`, s.nama as progdi, j.nama as jalurCmb,
				e.kode, e.nama as namaEvent, e.periode, DAY(e.tanggal) as days, u.foto,
				e.tanggal, DAYNAME(e.tanggal) as hari, MONTHNAME(e.tanggal) as bulan, YEAR(e.tanggal) as tahun,
				e.jalur,e.jamMasuk, e.ruang,
				e.petugas, e.pewawancara, e.jamKeluar, e.statusJadwal,
				e.totalPeserta, e.pewawancara2, e.prioritas
				FROM adis_smb_form f
				INNER JOIN adis_event_smb e ON e.kode = f.`event`
				INNER JOIN adis_smb_usr_pribadi u ON u.kode = f.kode
				INNER JOIN adis_buka_smb b ON b.kode = f.bukaSmb
				INNER JOIN adis_jalur_smb j ON j.kode = e.jalur
				INNER JOIN adis_prodi s ON s.kode = b.prodi
				WHERE f.kode = '$kode';";
		$sql = $this->db2->query($sql)->row();
		$this->smarty->assign('jadwal',$sql);
	}

	function mHasilSeleksi($kode){
		$sql = "SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb,
				f.stsEventInterviewPresent as hadirWwc, f.stsEventUsmPresent hadirUsm,
				f.resultUsm as hasilUsm, f.resultInterview as hasilWwc, f.resultPept, f.stsResultGrade as hasilAkhir,
				f.stsResultPass as lulusTidak, f.stsResultRecommended as recomended, f.stsResultKet as ket, f.stsResultConfirm as konfirm,
				p.nama as prodi, p.nama as progdi, u.nama as student, k.kode_potongan, pot.nama_potongan
				FROM adis_smb_form f
				INNER JOIN adis_smb_usr_pribadi u ON f.kode = u.kode
				LEFT JOIN adis_smb_usr_keu k ON f.kode = k.smbUsr
				INNER JOIN adis_prodi p ON RIGHT(f.bukaSmb,4) = p.kode
				LEFT JOIN tbl_master_potongan pot ON k.kode_potongan = pot.kode_potongan
				WHERE f.kode = '$kode'";
		$sql = $this->db2->query($sql)->row();

		$this->smarty->assign('hasil', $sql);
	}

	function mPrestasi($kode){
		$prestasi = $this->db2->get_where('adis_smb_usr_prestasi', array('erased'=>0,'smbUsr'=>$kode))->result();
		$this->smarty->assign("prestasi", $prestasi);
	}

	function mOrganisasi($kode){
		$organisasi = $this->db2->get_where('adis_smb_usr_org', array('erased'=>0,'smbUsr'=>$kode))->result();
		$this->smarty->assign("organisasi", $organisasi);
	}

	function selectType(){
		$typeBayar = $this->db2->query("SELECT * FROM adis_type WHERE kode LIKE '04.%'")->result();
		$this->smarty->assign("typeBayar", $typeBayar);

		$bankAkun = $this->db2->query("SELECT * FROM adis_type WHERE kode LIKE '05.1'")->row();
		$this->smarty->assign("bankAkun", $bankAkun);

		$sex = $this->db2->query("SELECT * FROM adis_type WHERE kode LIKE '03.%' AND erased = 0")->result();
		$this->smarty->assign("sex", $sex);

		$edu = $this->db2->query("SELECT * FROM adis_type WHERE kode LIKE '08.%' AND erased = 0")->result();
		$this->smarty->assign("edu", $edu);

		$eduSMA = $this->db2->query("SELECT * FROM adis_type WHERE kode LIKE '08.1%' AND erased = 0")->result();
		$this->smarty->assign("eduSMA", $eduSMA);

		$prodi = $this->db2->query("SELECT * FROM adis_prodi WHERE erased = 0")->result();
		$this->smarty->assign("prodi", $prodi);

	}

	function selectAgama(){
		$agama = $this->db2->query("SELECT * FROM adis_type WHERE kode LIKE '02.%' ORDER BY kode = '02.I' DESC")->result();
		$this->smarty->assign("religion", $agama);
	}

	function selectPropinsi(){
		$this->db2->order_by('nama', 'asc');
		$wil = $this->db2->get_where('adis_wil', array('parent'=>NULL))->result();
		$this->smarty->assign("wil", $wil);
	}

	function selectKabKota(){
		$wil = $this->db2->query("SELECT * FROM adis_wil WHERE parent != '' ORDER BY nama ASC;")->result();
		$this->smarty->assign("kab", $wil);
	}

	function selectKodePos(){
		$wil = $this->db2->query("SELECT * FROM adis_kodepos WHERE erased != '1' ORDER BY kodepos ASC;")->result();
		$this->smarty->assign("kodepos", $wil);
	}

	function updatePerosnalData($kode){

		$date = date("Y-m-d H:i:s");
		$tanggalLahir = date("Y-m-d", strtotime($this->input->post("tanggalLahir")));

		$this->db2->where('kode', $kode);
		$this->db2->update('adis_smb_usr', array(
								"updateUser"=>$kode,
								"updateTime"=>$date,
								"username"=> $this->input->post("nameFull")
								));

		$this->db2->where('kode', $kode);
		$this->db2->update('adis_smb_usr_pribadi',
							array("updateUser"=>$kode,
								"updateTime"=>$date,
								"nama"=> $this->input->post("nameFull"),
								"tempatLahir" => $this->input->post("tempatLahir"),
								"genderType"=>$this->input->post("sex"),
								"rumahCell"=>$this->input->post("no_hp"),
								"tanggalLahir" => $tanggalLahir,
								"nomorKtp" => $this->input->post("no_id"),
								"agamaType" => $this->input->post("agama"),
								"rumahAlamat" => $this->input->post("occupation")
		));

		/*
		$this->db2->where('kode',$kode);
		$this->db2->update('adis_smb_usr_pribadi', array(
							'updateuser'=>$kode,
							'updateTime'=>$date,
							'ayahNama'=>$this->input->post('namaAyah'),
							'ayahAlamat'=>$this->input->post('alamatAyah'),
							'ayahKabKota'=>$this->input->post('kabAyah'),
							'ayahProp'=>$this->input->post('propAyah'),
							'ayahKodePos'=>$this->input->post('kposAyah'),
							'ayahTel'=>$this->input->post('telAyah'),
							'ayahCell'=>$this->input->post('hpAyah'),
							'ayahEmail'=>$this->input->post('emailAyah'),
							'ibuNama'=>$this->input->post('namaIbu'),
							'ibuAlamat'=>$this->input->post('alamatIbu'),
							'ibuKabKota'=>$this->input->post('kabIbu'),
							'ibuProp'=>$this->input->post('propIbu'),
							'ibuKodePos'=>$this->input->post('kposIbu'),
							'ibuTel'=>$this->input->post('telIbu'),
							'ibuCell'=>$this->input->post('hpIbu'),
							'ibuEmail'=>$this->input->post('emailIbu'),
							'waliNama'=>$this->input->post('namaWali'),
							'waliAlamat'=>$this->input->post('alamatWali'),
							'waliKabKota'=>$this->input->post('kabWali'),
							'waliProp'=>$this->input->post('propWali'),
							'waliKodePos'=>$this->input->post('kposWali'),
							'waliTel'=>$this->input->post('telWali'),
							'waliCell'=>$this->input->post('hpWali'),
							'waliEmail'=>$this->input->post('emailWali')
							));

		$this->db2->where('smbUsr', $kode);
		$this->db2->update('adis_smb_usr_edu',array(
							"updateUser"=>$kode,
							"updateTime"=>$date,
							'sekolahType'=> $this->input->post("pendType"),
							'sekolahOwnerType'=> $this->input->post("sekType"),
							"nama"=> $this->input->post("namaSekolah"),
							'prop'=> $this->input->post("propSek"),
							'kabKota'=> $this->input->post("kabSek"),
							'kodePos'=> $this->input->post("kposSek"),
							'tahunLulus'=> $this->input->post("tahunLulus"),
							'jurusan'=> $this->input->post("jurusan"),
							'nilai'=> $this->input->post("nilai")
							));
		*/


	}

	function listAngsuran($pilihan, $kodebayar, $periode, $kode_potongan = ""){

            $qry = "SELECT p.*, pr.singkatan as alias_prodi, pr.nama as prodi, p.kode as code
                        FROM adis_pembayaran_kelasmalam p
                        LEFT JOIN adis_prodi pr ON p.prodiTipe = pr.kode
                        WHERE p.kode = '$kodebayar' AND p.periode_kode = '$periode'";
            $sql = $this->db2->query($qry)->result_array();

            $arry = array();
            $response = array();
            $arryPil = array();

		$listPotongan = array();
		if($kode_potongan != ""){
			$listPotongan = $this->selectPotonganMaster($kodebayar, $kode_potongan, $periode);
		}

        foreach($sql as $val){
                $arry['uangMasuk'] = $val['uangMasuk'];
                $arry['spp'] = $val['spp'];
                $arry['jumlahSks'] = $val['jumlahSks'];
                $arry['tempoAngsuran'] = $val['tempoAngsuran'];
                $arry['biayaPerSks'] = $val['biayaPerSks'];
                $arry['pilihanBiayaUM'] = json_decode($val['pilihanBiayaUM']);

				if($kode_potongan == ''){
					$arry['biayaPembeda'] = json_decode($val['biayaPembeda']);
					$arry['totalBiaya'] = ($val['uangMasuk']+$val['spp'])+($val['jumlahSks']*$val['biayaPerSks']);
				}else{
					$arry['biayaPembeda'] = json_decode($listPotongan[0]['biayaPembedaBeasiswa']);
					$uangMasuk = $val['uangMasuk'];
					$spp = $val['spp'];
					$sks = $val['jumlahSks']*$val['biayaPerSks'];
					$bpp = 0;

					foreach($listPotongan as $nilai){
						switch ($nilai['kode_biaya']){
							case 'UM':
								if($nilai['lambang'] == '%'){
									$uangMasuk = $val['uangMasuk'] - ($val['uangMasuk']*($nilai['nilai']/100));
								}else{
									$uangMasuk = $val['uangMasuk']-$nilai['nilai'];
								}
							break;
							case 'SPP':
							break;
							case 'SKS':
							break;
							case 'BPP':
							break;
						}
					}

					$arry['totalBiaya'] = $uangMasuk + $spp + $sks + $bpp;
				}

                foreach(json_decode($val['pilihanBiayaUM']) as $k => $v){
                    $arry2['biayaAngsur'.$k] = $arry['totalBiaya'] - $v - $arry['biayaPembeda'][$k];
                    for($i = 0; $i < $val['tempoAngsuran']-1; $i++){
                        $arryAngsur[$i] = $arry2['biayaAngsur'.$k]/($val['tempoAngsuran']-1);
                    }
                    $arryPil[$k] = $arryAngsur;
                }
            }

            $arrTglTempo = array();
            $sqlTglTempo = $this->db2->query("SELECT * FROM tbl_tgl_tempo_kelasmalam "
                    . " WHERE kode_pembayaran = '$kodebayar' AND kode_periode = '$periode';")->row_array();
            $arrTglTempo = json_decode($sqlTglTempo['tgl_jatuh_tempo']);

            $keyPil = array_search($pilihan, $arry['pilihanBiayaUM']);
           // print_r($pilihan);exit;

            $this->smarty->assign('resTglTempo', $arrTglTempo);
            $response[] = $arry;
            $this->smarty->assign('sql', $response);
            $this->smarty->assign('pilih', $arryPil);
            $this->smarty->assign('keypilih', $keyPil);
            $this->smarty->assign('choose', $pilihan);
        }


	function selectPotonganMaster($kodeBayar, $kodePotongan, $kodePeriode){
		$wherePotongan = "";
		if($kodePotongan != ''){
			$wherePotongan = " AND A.kode_potongan = '$kodePotongan' ";
		}
		$query = "SELECT A.* , B.kode_biaya, B.nilai, C.nama_potongan as lambang_name, C.lambang
			FROM tbl_pilihan_potongan_biaya A
			LEFT JOIN tbl_detil_potongan B ON B.kode_potongan = A.kode_potongan
			LEFT JOIN adis_metode_potongan C ON C.id = B.tipe
			WHERE A.kode_pembayaran = '$kodeBayar'
				AND A.kode_periode = '$kodePeriode'
				$wherePotongan;";
		$query = $this->db2->query($query)->result_array();
		return $query;
	}


}
