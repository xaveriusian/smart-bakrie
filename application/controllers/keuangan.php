<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Keuangan extends CUTI_Controller {
	function __construct(){
		parent::__construct();	
		//setlocale (LC_TIME, 'id_ID');
		setlocale (LC_TIME, 'INDONESIA');
		$this->auth = unserialize(base64_decode($this->session->userdata('cuti_parmad')));
		$this->host	= $this->config->item('base_url');
		
		if(! $this->auth) {header("Location: " . $this->host);}	
		if($this->auth['level'] != "77") {header("Location: " . $this->host);}	
		
		$this->db2 = $this->load->database('second', TRUE); 
		
		$modul = "keuangan/";
		$this->smarty->assign('modul',$modul);
		$this->smarty->assign('host',$this->host);
		$this->load->model('madmisi');
		$this->db2->query("SET lc_time_names = 'id_ID'");
		
		$nama = $this->auth['name'];
		$this->smarty->assign('nama',$nama);
		
	}
	
	function test(){
		$post = $this->input->post();
		$cntPost = count($post['kode']);
		$kode = json_encode($post['kode']);
		
		$periode ="SELECT * FROM adis_periode_master WHERE erased = 0 AND jenjangType = 1 ORDER by kode DESC";
		$periode = $this->db2->query($periode)->result();
		$this->smarty->assign('periode',$periode);
		$this->smarty->assign('kode', base64_encode($kode));
		$this->smarty->display('keuangan/periodeGenerateBulk.html');
	}
	
	function denda(){
		$post = $this->input->post();
		$cntPost = count($post['kode']);
		$kode = json_encode($post['kode']);
		
		$periode ="SELECT * FROM adis_periode_master WHERE erased = 0 AND jenjangType = 1 ORDER by kode DESC";
		$periode = $this->db2->query($periode)->result();
		$this->smarty->assign('periode',$periode);
		$this->smarty->assign('kode', base64_encode($kode));
		$this->smarty->display('keuangan/periodeGenerateDenda.html');
	}
	
	function tag_paralel($p1 = '', $p2= ''){
		$post = $this->input->post();
		switch ($p1){
			case "select":		
				$site = "Select";	
				$lokasi = "keuangan";
				$pages = "kontenGenTagihan";	
				
				$periode ="SELECT * FROM adis_periode_master WHERE erased = 0  AND jenjangType = 1
					ORDER by kode DESC";
				$periode = $this->db2->query($periode)->result();
				$this->smarty->assign('periode',$periode);
				
				$prodi = $this->db2->query("SELECT * FROM adis_prodi WHERE erased = 0 AND jenjang = 1;")->result();
				$this->smarty->assign('prodi',$prodi);	
				
			break;
			case "table":
				$whereProdi = "";
				if($post['prodi'] != 'all'){
					$whereProdi = " AND RIGHT(A.bukaSmb, 4) = '".$post['prodi']."' ";
				}
				$sqlMhs = "SELECT A.bukaSmb, A.kode, A.nim, F.nama as nama, C.nama as prodi, 
						D.noTagihanParalel, E.lulus_acc
						FROM `adis_smb_form` A
						LEFT JOIN adis_smb_usr B ON A.kode = B.kode
						LEFT JOIN adis_prodi C ON C.kode = RIGHT(A.bukaSmb, 4)
						LEFT JOIN adis_smb_usr_keu D ON D.smbUsr = A.kode
						LEFT JOIN adis_smb_usr_edu E ON E.smbUsr = A.kode 
						LEFT JOIN adis_smb_usr_pribadi F ON F.kode = A.kode
						WHERE LEFT(A.bukaSmb, 8)='".$post['periode']."' $whereProdi
						AND (A.nim != '' AND SUBSTR(A.bukaSmb, 12, 2) = 'KP');";
				$sqlMhs = $this->db->query($sqlMhs)->result_array();
				
				$arrTag = array();
				foreach($sqlMhs as $k => $val){
					$sqlMhs[$k]['countTag'] = count(json_decode($val['noTagihanParalel']));
				}
				$this->smarty->assign('mhs', $sqlMhs);
				
				$this->smarty->display('keuangan/kontenGenTagihan.html');
			break;
			case 'detil':
				$this->load->model('mportal');
				
				$kode = $post['kode'];
				$sqlPar = "SELECT A.bukaSmb, A.kode, A.nim, E.nama as nama, C.nama as prodi, D.noTagihanParalel,
					D.metodBayarDaftarUlang, A.reapplyBankTransferAmount, LEFT(A.bukaSmb, 8) as periode,
					D.kode_potongan, D.pilihan_angsuran_km
					FROM `adis_smb_form` A
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode
					LEFT JOIN adis_prodi C ON C.kode = RIGHT(A.bukaSmb, 4)
					LEFT JOIN adis_smb_usr_keu D ON D.smbUsr = A.kode
					LEFT JOIN adis_smb_usr_pribadi E ON E.kode = A.kode
					WHERE A.kode = '$kode'";
				$sqlPar = $this->db2->query($sqlPar)->row_array();
				
				$jsonTag = json_decode($sqlPar['noTagihanParalel']);
				$this->mportal->listAngsuran($sqlPar['pilihan_angsuran_km'], $sqlPar['metodBayarDaftarUlang'], $sqlPar['periode'], $sqlPar['kode_potongan']);
				
				$this->smarty->assign('tagSls', count($jsonTag)-1);
				$this->smarty->assign('sqlPar', $sqlPar);
				// echo "<pre>";
				// print_r($jsonTag);exit;
				$this->smarty->display('keuangan/listAngsuran.html');
				// foreach($jsonTag as $k=>$v){
					
				// }
			break;
			case "periode":
				$periode ="SELECT * FROM adis_periode_master WHERE erased = 0 AND jenjangType = 1 ORDER by kode DESC";
				$periode = $this->db2->query($periode)->result();
				$this->smarty->assign('periode',$periode);
				$this->smarty->assign('kode',base64_encode(json_encode($post['kode'])));
				$this->smarty->display('keuangan/periodeGenerate.html');
			break;
			case 'angsuran':
				$kode = json_decode(base64_decode($post['kode']));
				$angsuran = $this->generateTagParalel($kode, $post['periode'], 'view');
				
				$periode ="SELECT * FROM adis_periode_master WHERE kode='".$post['periode']."' ";
				$periode = $this->db2->query($periode)->row_array();
				
				$view = '<label>Tanggal Periode : <b>{$periode.tanggalMulai|date_format:"%d-%m-%Y"} s/d {$periode.tanggalSelesai|date_format:"%d-%m-%Y"}</b></label>
						{if isset($biaya)}
						{foreach from=$biaya item=row}
						<table class="table table-bordered  table-advance ">
							<thead>
								<tr>
									<th>No.</th>
									<th>Periode</th>
									<th>Biaya</th>
									<th>Tanggal</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										{foreach from=$row.tagihanPeriode item=v key=k}
											{$k+1}<br>
										{/foreach}
									</td>
									<td>
										{foreach from=$row.tagihanPeriode item=v}
											{$v}<br>
										{/foreach}
									</td>
									<td>
										{foreach from=$row.nominalCicilan item=v}
											{$v|number_format:0:",":"."}<br>
										{/foreach}
									</td>
									<td>
										{foreach from=$row.tglJatuhTempo  item=v}
											{$v|date_format:"%d-%m-%Y"}<br>
										{/foreach}
									</td>

								</tr>
							</tbody>
						</table>
						{/foreach}
						{else}
							<br>
							<h4 class="alert">Setting tagihan belum tersedia</h4>
						{/if}';
				
				$this->smarty->assign('periode', $periode);
				$this->smarty->assign('biaya', $angsuran);
				$this->smarty->display('string:'.$view);
			break;
			case "generate":
			
				$kode = json_decode(base64_decode($post['kode']));
				$denda = 0;
				$denda = (isset($post['denda'])) ? $post['denda'] : 0;
								
				// $denda = $post['denda'];
				
				$angsuran = array();
				$respon = array();
				foreach($kode as $val){
					$angsuran[$val] = $this->generateTagParalel($val, $post['periode'], 'integrasi', $denda, $post);					
				}
				
				$log = array();
				foreach($angsuran as $k => $v ){					
					$qryMhs = $this->db2->query("SELECT nama from adis_smb_usr_pribadi WHERE kode = '$k'")->row_array();
					
					if($v == 201){
						$respon[] = 1;
						$log[$qryMhs['nama']] = " - Generate success";
					}else{
						$respon[] = 0;
						$log[$qryMhs['nama']] = " - Generate error";
					}
				}
				
				if(in_array(0, $respon)){					
					foreach($log as $k => $v){
						echo $k ." ".$v;
						echo "\n";
					}					
					echo "\n";
					echo "Tagihan angsuran paralel gagal di generate, silahkan coba lagi atau hubungi Admin!";
				}else{
					echo 1;	
				}
			break;
		}
		
		if ($p2 == ''){
			
			$this->smarty->assign('modul',"keuangan/");
			$this->smarty->assign('lokasi',$lokasi);
			$this->smarty->assign('pages',$pages);
			$this->smarty->assign('site',$site);
			$this->smarty->display('index.html');
		}
	}
	
	function generateTagParalel($kode = '', $periode = '', $method = '', $denda = 0, $post = null){    
		$this->load->model('mintegrasi');

		$host_asik = "";
		$service_url = $host_asik . 'index.php?mod=service&sub=generateTagihanRegistrasiPar&act=rest&typ=rest';               

		$username = '';
		$password = '';
			
		$sql = "SELECT A.nomor as mahasiswaNoPendaftaran, CONCAT(LEFT(A.bukaSmb,4), D.semester) as mahasiswaPeriodeKode, 
				(LEFT(A.bukaSmb,8)) as periodeAdmisi, B.metodBayarDaftarUlang as kodePembayaran,
				'1' as tagihanIsCicilan, '1' as potonganEarlyBird, '0' as potonganMahasiswaTransfer,
				C.spp, C.uangMasuk,B.totalBiayaDaftarUlang as pilihanUM , C.biayaPerSks,C.jumlahSks,
				(C.biayaPerSks*C.jumlahSks) as biayaSksSemester, F.tgl_jatuh_tempo, C.tempoAngsuran, C.biayaPembeda
				, C.pilihanBiayaUM , B.kode_potongan, A.reapplyBankTransferAmount, B.pilihan_angsuran_km
				FROM adis_smb_form A
				LEFT JOIN adis_smb_usr_keu B ON B.smbUsr = A.kode
				INNER JOIN adis_pembayaran_kelasmalam C ON C.kode = B.metodBayarDaftarUlang AND C.periode_kode = (LEFT(A.bukaSmb,8))
				LEFT JOIN adis_periode_master D ON D.kode = LEFT(A.bukaSmb,8)
				INNER JOIN tbl_tgl_tempo_kelasmalam F ON F.kode_periode = (LEFT(A.bukaSmb,8)) AND F.kode_pembayaran = B.metodBayarDaftarUlang
				WHERE A.stsReapplyPaid = 1 AND A.kode = '$kode'";		
		$sql = $this->db2->query($sql)->row_array();
		$tahun = substr($sql['periodeAdmisi'], 0, 4);
		
		$periodeQry = "SELECT A.*, CONCAT(A.tahun, A.semester)as periode 
			FROM adis_periode_master A 
			WHERE (tahun >= '$tahun' AND jenjangType = 1) AND kode = '$periode';";
		$result_per = $this->db2->query($periodeQry)->result_array();
		
		if( $sql['kode_potongan'] != ""){
		
			$arryAngsur = $this->listAngsuran(
					$sql['pilihan_angsuran_km'], $sql['kodePembayaran'], 
					$sql['periodeAdmisi'], $sql['kode_potongan'], $sql['biayaPembeda'] );
				
		}
				
		
		$arry = array();
		$arry['uangMasukPilihan'] = $sql['pilihanUM'];
		$arry['spp'] = $sql['spp'];
		$arry['jumlahSks'] = $sql['jumlahSks'];
		$arry['tempoAngsuran'] = $sql['tempoAngsuran'];
		$arry['biayaPerSks'] = $sql['biayaPerSks'];
		
		$arry['totalBiaya'] = ($sql['uangMasuk']+$sql['spp'])+($sql['jumlahSks']*$sql['biayaPerSks']); 
		$pilihanUM = json_decode($sql['pilihanBiayaUM']);
		$biayaPembeda = json_decode($sql['biayaPembeda']);
		$idxArrPilihan = "";
		foreach ($pilihanUM as $k => $val){
			if ($val == $sql['pilihanUM']){
				$idxArrPilihan = $k;
			}
		}
		
		$arry['$idxArrPilihan'] = $idxArrPilihan;              
		$arry['biayaPembeda'] = $biayaPembeda[$idxArrPilihan];              

		$arry2['biayaAngsur'] = $arry['totalBiaya'] - $arry['uangMasukPilihan'] - $arry['biayaPembeda'];
		
		if( $sql['kode_potongan'] == ""){
			for($i = 0; $i < $arry['tempoAngsuran']-1; $i++){
				$arryAngsur[$i] = $arry2['biayaAngsur']/($arry['tempoAngsuran']-1);
			}  
		}
		
		$arrTglTempo = array();
		$sqlTglTempo = $this->db2->query("SELECT * FROM tbl_tgl_tempo_kelasmalam "
				. " WHERE kode_pembayaran = '".$sql['kodePembayaran']."' 
				  AND kode_periode = '".$sql['periodeAdmisi']."';")->row_array();
		
		
		$arrTglTempo = json_decode($sqlTglTempo['tgl_jatuh_tempo']);
		
		if(count($arrTglTempo) != count($arryAngsur)){
			$arryAngsur[$arry['tempoAngsuran']-1] =  $arry['biayaPembeda'];
		}
		// echo count($arrTglTempo)."----".count($arryAngsur);exit;
		
		$arryPil = $arryAngsur; 
		foreach ($result_per as $key => $v){
			foreach ($arrTglTempo as $k => $val){
				$tglMulai = $v['tanggalMulai'];
				// $tglMulai = '2017-03-01';
				if (($val > $tglMulai) && ($val < $v['tanggalSelesai']))
				{
				  $arryPeriode[$k] = $v['periode'];
				  $arryTglTagihan[$k] = $val;
				  $arryValTagihan[$k] = $arryPil[$k];
				}
			}
		}  
		
		$data = null;
		if(isset($arryPeriode)){
			$lengthArr = count($arryPeriode); //Disimpan untuk parameter yang akan digunakan pada generate berikutnya
			$arry['periodeTagihan'] = $arryPeriode;
			$arry['tglTagihan'] = array_slice($arrTglTempo, 0, $lengthArr);
			$arry['angsuranTagihan'] = array_slice($arryPil, 0, $lengthArr);
			
			$periodeUM = array($tahun."1");
			$tglTagiUM = array($arry['tglTagihan'][0]);
			$valTagiUM = array($arry['uangMasukPilihan']);
			
			array_splice( $arry['periodeTagihan'] , 0, 0, $periodeUM);
			array_splice( $arry['tglTagihan'], 0, 0, $tglTagiUM );
			array_splice( $arry['angsuranTagihan'], 0, 0, $valTagiUM);
			
			##untuk generate tagihan paralel di periode periode berikutnya
			$data['mahasiswaNoPendaftaran'] = $sql['mahasiswaNoPendaftaran'];
			$data['tagihanIsCicilan'] = $sql['tagihanIsCicilan'];
			
		
//		
//		
			if($denda == 1){
				$k = 0;
				foreach($arryPeriode as $i => $v){
					$data['tagihanPeriode'][$k] = $arryPeriode[$i];
					$data['tglJatuhTempo'][$k] = $arryTglTagihan[$i];
					$data['nominalCicilan'][$k] = $arryValTagihan[$i];
					
					$k ++;
				}
				
				for($i = 1; $i <= 5; $i++){
					if ($post['biaya_'.$i] != '' && $post['tgl_tempo_'.$i] != ''){
						$data['tagihanPeriode'][$k] = substr($periode, 0, 4).substr($periode, 7, 1);
						$data['tglJatuhTempo'][$k] = date('Y-m-d',strtotime($post['tgl_tempo_'.$i]));
						$data['nominalCicilan'][$k] = $post['biaya_'.$i];		
						$k ++;
					}					
				}
			}else{
				$k = 0;
				foreach($arryPeriode as $i => $v){
					$data['tagihanPeriode'][$k] = $arryPeriode[$i];
					$data['tglJatuhTempo'][$k] = $arryTglTagihan[$i];
					$data['nominalCicilan'][$k] = $arryValTagihan[$i];
					
					$k ++;
				}
			}
			
			$data = array($data);
		}
		
		// echo "<pre>";
		// print_r($data);exit;
//		
	   
	   switch ($method){
		   case 'view':
				return $data;
		   break;
		   case 'integrasi':
				
				$curl_post_data = json_encode($data);
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, $service_url);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
				curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,0);
				curl_setopt($curl, CURLOPT_TIMEOUT, 400); //timeout in seconds
				curl_setopt($curl, CURLOPT_VERBOSE, true); #debugging purpose only

				$curl_response = curl_exec($curl);
				curl_close($curl);			

				$response = json_decode($curl_response, true);
				// print_r($curl_response);
				
				$respon = $response['gtfwResult'];
				$status = $respon['status'];
				$notagihantemp = $respon['data'];

				if($status == '201'){	
					$tagMhs = $this->db2->query("SELECT noTagihanParalel FROM adis_smb_usr_keu WHERE smbUsr = '$kode'; ")->row_array();
					$tagParalel = json_decode($tagMhs['noTagihanParalel']);
					$tagNew = array_merge($tagParalel, $notagihantemp);
					
					$jsonTagihan = json_encode($tagNew);
					
					if($denda == 0){
					
						$this->db2->query("UPDATE adis_smb_usr_keu "
							. " SET noTagihanParalel = '$jsonTagihan' "
							. " WHERE smbUsr='$kode';");

					}else{
					
						$this->db2->query("UPDATE adis_smb_usr_keu "
							. " SET noTagihanParalel = '$jsonTagihan' "
							. " WHERE smbUsr='$kode';");
							
						$this->db->query("
							INSERT INTO tbl_tagihan_cmb 
								(kode_mhs, kode_pembayaran, nomor_tagihan, jumlah_tagihan, jenis_tagihan) 
							VALUES 
								(	'$kode', '".json_encode($notagihantemp)."', 
									'".json_encode($data[0]['nominalCicilan'])."', 
									'0','7'
								);
						");
					}					
				}else{
					// print_r($curl_response); 
					// echo "\n\n\n";
				}          

				$step = "gen_tagihan";                
				$this->mintegrasi->status_integrasi($kode, $status, $step);
				$this->mintegrasi->log_asik_resp($kode, "Generate Tagihan : ".$curl_response." Parameter : ".$curl_post_data);

				if ($status == '419'){$this->mintegrasi->sendMail($step, $kode, $curl_response);}

				return $status;
		   break;
	   }
	
		
	}
	
	function listAngsuran($pilihan, $kodebayar, $periode, $kode_potongan = "", $biayaPembeda = ""){  
		$this->load->model('mportal');          
            
		$qry = "SELECT p.*, pr.singkatan as alias_prodi, pr.nama as prodi, p.kode as code
					FROM adis_pembayaran_kelasmalam p
					LEFT JOIN adis_prodi pr ON p.prodiTipe = pr.kode
					WHERE p.kode = '$kodebayar' AND p.periode_kode = '$periode'";
		$sql = $this->db2->query($qry)->result_array();
		
		$arry = array();
		$response = array();
		$arryPil = array();
			
		$listPotongan = array();
		if($kode_potongan != ""){
			$listPotongan = $this->mportal->selectPotonganMaster($kodebayar, $kode_potongan, $periode);
		}

        foreach($sql as $val){
                $arry['uangMasuk'] = $val['uangMasuk'];
                $arry['spp'] = $val['spp'];
                $arry['jumlahSks'] = $val['jumlahSks'];
                $arry['tempoAngsuran'] = $val['tempoAngsuran'];
                $arry['biayaPerSks'] = $val['biayaPerSks'];
                $arry['pilihanBiayaUM'] = json_decode($val['pilihanBiayaUM']);
				
				if($kode_potongan == ''){
					$arry['biayaPembeda'] = json_decode($val['biayaPembeda']);
					$arry['totalBiaya'] = ($val['uangMasuk']+$val['spp'])+($val['jumlahSks']*$val['biayaPerSks']);
				}else{
					$arry['biayaPembeda'] = json_decode($listPotongan[0]['biayaPembedaBeasiswa']);
					// $arry['biayaPembeda'] = json_decode($biayaPembeda);
					$uangMasuk = $val['uangMasuk'];
					$spp = $val['spp'];
					$sks = $val['jumlahSks']*$val['biayaPerSks'];
					$bpp = 0;
					
					foreach($listPotongan as $nilai){
						switch ($nilai['kode_biaya']){
							case 'UM':
								if($nilai['lambang'] == '%'){
									$uangMasuk = $val['uangMasuk'] - ($val['uangMasuk']*($nilai['nilai']/100));	
								}else{
									$uangMasuk = $val['uangMasuk']-$nilai['nilai'];	
								}
							break;
							case 'SPP':
							break;
							case 'SKS':
							break;
							case 'BPP':
							break;
						} 
					}
					
					$arry['totalBiaya'] = $uangMasuk + $spp + $sks + $bpp;
				}
		

                foreach(json_decode($val['pilihanBiayaUM']) as $k => $v){
                    $arry2['biayaAngsur'.$k] = $arry['totalBiaya'] - $v - $arry['biayaPembeda'][$k];
                    for($i = 0; $i < $val['tempoAngsuran']-1; $i++){
                        $arryAngsur[$i] = $arry2['biayaAngsur'.$k]/($val['tempoAngsuran']-1);
                    }
                    $arryPil[$k] = $arryAngsur;
                }
            }

			
            $arrTglTempo = array();
            $sqlTglTempo = $this->db2->query("SELECT * FROM tbl_tgl_tempo_kelasmalam "
                    . " WHERE kode_pembayaran = '$kodebayar' AND kode_periode = '$periode';")->row_array();
            $arrTglTempo = json_decode($sqlTglTempo['tgl_jatuh_tempo']);
            
            $keyPil = array_search($pilihan, $arry['pilihanBiayaUM']); 
			
			
			return $arryPil[$keyPil];

            // $this->smarty->assign('resTglTempo', $arrTglTempo);
            // $response[] = $arry;
            // $this->smarty->assign('sql', $response);
            // $this->smarty->assign('pilih', $arryPil);
            // $this->smarty->assign('keypilih', $keyPil);
            // $this->smarty->assign('choose', $pilihan);
			
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */