<?php
class Phpmailer_library
{
    public function __construct()
    {
        log_message('Debug', 'PHPMailer class is loaded.');
    }

    public function load()
    {
		require APPPATH.'libraries/phpmailer/src/Exception.php';
        require_once(APPPATH.'libraries/phpmailer/src/PHPMailer.php');
        require_once(APPPATH.'libraries/phpmailer/src/SMTP.php');

        $objMail = new PHPMailer\PHPMailer\PHPMailer();
        return $objMail;
    }
}

?>