<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CUTI_Controller{
	function __construct(){
		parent::__construct();		
		$this->auth = unserialize(base64_decode($this->session->userdata('cuti_parmad')));
		$this->host	= $this->config->item('base_url');
		if(! $this->auth) {redirect('/site','refresh');}
		
		$this->db2 = $this->load->database('second', TRUE); 
		
		$site = "";
		$modul = "";
		$this->smarty->assign('modul',$modul);
		$this->smarty->assign('site',$site);
		$this->smarty->assign('lokasi',"Dashboard");
		$this->smarty->assign('host',$this->host);
		
		$nama = $this->auth['name'];
		$level = $this->auth['level'];
		$this->smarty->assign('nama',$nama);
		$this->smarty->assign('level',$level);
		
		// $this->notification();
		// $this->activity();
		$whereJenjang_ = $this->auth['jenjang'] == 99 ? '' : " AND jenjangType = '".$this->auth['jenjang']."' ";
		
		$periodeQry = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 $whereJenjang_ ")->result_array();
		$this->smarty->assign('periode', $periodeQry);
		
		if($periodeQry){
		$this->laporan('seleksi','', $periodeQry[0]['kode']);
		}
		
		$this->smarty->display('index.html');
	}
	
	function index() {
		
	}
		
	function laporan($p1 = '', $p2= '', $periode = ''){
		
		$this->load->model('madmisi');
				
		switch ($p1){
			case "select":		
				$site = "Select";	
				$lokasi = "Laporan";
				$pages = "seleksi";	
				
				$periode ="SELECT * FROM adis_periode_master WHERE erased = 0 and jenjangType = '".$this->auth['jenjang']."' ORDER by kode DESC";
				$periode = $this->db2->query($periode)->result();
				$this->smarty->assign('periode',$periode);
				
		
				$jalur = array(
					(object) array ( 'kode'=>'RAPOR', 'nama'=>'Jalur Rapor'), 
					(object) array  ('kode'=>'TULIS', 'nama'=>'Jalur Tes Tulis')
					);
				$this->smarty->assign('jalur',$jalur);
				
				$this->madmisi->mSelectProdi();
			break;
			case 'seleksi':
				$prodi = 'all';
				$jalur = 'all';
				$jenjang = substr($periode, 5, 1);
				$this->smarty->assign('jenjang', $jenjang);
				$whereQry = '';
				
				
				$this->grafikCalonMahasiswa($periode);
				##filter all query
				if($periode){ $whereQry = " LEFT(A.bukaSmb, 8) = '$periode' "; }
				if ($jalur != 'all'){
					$whereQry = " LEFT(A.bukaSmb,8) = '$periode' AND B.jalur_penerimaan = '$jalur' ";
				}
				if ($prodi != 'all'){
					if ($jalur == 'all'){
						$whereQry = " LEFT(A.bukaSmb,8) = '$periode' AND RIGHT(A.bukaSmb, 4) = '$prodi' ";
					}else{
						$whereQry = " LEFT(A.bukaSmb,8) = '$periode' 
							AND B.jalur_penerimaan = '$jalur' AND RIGHT(A.bukaSmb, 4) = '$prodi' ";
					}
				}
				// echo $whereQry;exit;
				$arrayStep = array("Daftar Online", "Bayar Formulir", "Mengikuti Seleksi", "Lulus Seleksi", "Daftar Ulang", "Mengundurkan Diri");
				
				$qryDaftar = "SELECT COUNT(A.kode) as mhsDaftar 
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry";
				$qryDaftar = $this->db2->query($qryDaftar)->row_array();
				
				$qryPayForm = "SELECT COUNT(A.kode) as mhsDaftar 
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry AND A.stsApplyPaid = 1";
				$qryPayForm = $this->db2->query($qryPayForm)->row_array();
				
				$qryHadirUjian = "SELECT COUNT(A.kode) as mhsDaftar
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry  AND A.stsEventConfirm = 1";
				$qryHadirUjian = $this->db2->query($qryHadirUjian)->row_array();
				
				$qrylulusUjian = "SELECT COUNT(A.kode) as mhsDaftar
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry AND A.stsResultConfirm = 1";
				$qrylulusUjian = $this->db2->query($qrylulusUjian)->row_array();
				
				$qryDaftarUlang = "SELECT COUNT(A.kode) as mhsDaftar 
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry  AND A.stsReapplyPaid = 1";
				$qryDaftarUlang = $this->db2->query($qryDaftarUlang)->row_array();
				
				$qryUndur = "SELECT COUNT(A.kode) as mhsDaftar
					FROM adis_smb_form A 
					LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
					WHERE $whereQry AND A.stsMundurAfterReapply = 1";
				$qryUndur = $this->db2->query($qryUndur)->row_array();
					
				$data[0] = array('Tahap Seleksi CMB','Jumlah CMB');	
				$data[1] = array($arrayStep[0],(int)$qryDaftar['mhsDaftar']);
				$data[2] = array($arrayStep[1],(int)$qryPayForm['mhsDaftar']);
				$data[3] = array($arrayStep[2],(int)$qryHadirUjian['mhsDaftar']);
				$data[4] = array($arrayStep[3],(int)$qrylulusUjian['mhsDaftar']);
				$data[5] = array($arrayStep[4],(int)$qryDaftarUlang['mhsDaftar']);
				$data[6] = array($arrayStep[5],(int)$qryUndur['mhsDaftar']);
				
				$totalReg = json_encode($data);
				$this->smarty->assign('totalReg',$totalReg);
				
				$arrIntBulan = '';
				$sql = "";
				
				$arrJalur = array(
					array('kode'=>'RAPOR', 'nama'=>'Jalur Rapor'),
					array('kode'=>'TULIS', 'nama'=>'Jalur Tes Tulis'),
					array('kode'=>'ODS', 'nama'=>'Jalur ODS'),
				);
				
				$dataFrek[0] = array('Bulan','Pendaftar Jalur Rapor','Pendaftar Jalur Tes Tulis','Pendaftar Jalur ODS');
				
				foreach($arrJalur as $k => $v){
					for($i = 1; $i <= 12; $i++){
						if ($i < 10){
							$t = '0'.$i."-".date("Y");
						}else{
							$t= $i."-".date("Y");
						}
						
						$dataFrek[$i][0] = $t;
						
						$sql[$i] = "SELECT IF(COUNT(A.kode) = null, 0, COUNT(A.kode))  as mhsDaftar 
							FROM adis_smb_form A 
							LEFT JOIN adis_smb_usr B ON A.kode = B.kode 
							WHERE DATE_FORMAT(A.createTime,'%m-%Y') = '$t' AND B.jalur_penerimaan = '".$v['kode']."' AND $whereQry
							GROUP BY LEFT(A.bukaSmb,10);";
						$sql[$i] = $this->db2->query($sql[$i])->row_array();
						if (!isset($sql[$i]['mhsDaftar'])){
							$sql[$i]['mhsDaftar'] = 0;
						}
						
						$arrIntBulan[$t] = $sql[$i]['mhsDaftar'];
						if($v['kode'] == 'RAPOR'){
							$dataFrek[$i][1] = (int)$sql[$i]['mhsDaftar'];
						}else if($v['kode'] == 'TULIS'){
							$dataFrek[$i][2] = (int)$sql[$i]['mhsDaftar'];
						}else if($v['kode'] == 'ODS'){
							$dataFrek[$i][3] = (int)$sql[$i]['mhsDaftar'];
						}
					}	
				}
				
				
				
				
				// $dataFrek[1] = array('Okt',(int)$sql[10]['mhsDaftar']);
				// $dataFrek[2] = array('Nov',(int)$sql[11]['mhsDaftar']);
				// $dataFrek[3] = array('Des',(int)$sql[12]['mhsDaftar']);
				// $dataFrek[4] = array('Jan',(int)$sql[1]['mhsDaftar']);
				// $dataFrek[5] = array('Peb',(int)$sql[2]['mhsDaftar']);
				// $dataFrek[6] = array('Mar',(int)$sql[3]['mhsDaftar']);
				// $dataFrek[7] = array('Apr',(int)$sql[4]['mhsDaftar']);
				// $dataFrek[8] = array('Mei',(int)$sql[5]['mhsDaftar']);
				// $dataFrek[9] = array('Jun',(int)$sql[6]['mhsDaftar']);
				// $dataFrek[10] = array('Jul',(int)$sql[7]['mhsDaftar']);
				// $dataFrek[11] = array('Ags',(int)$sql[8]['mhsDaftar']);
				// $dataFrek[12] = array('Sep',(int)$sql[9]['mhsDaftar']);
				
				// echo "<pre>";
				// print_r($dataFrek);exit;
				
				
				$totalRegFrek = json_encode($dataFrek);
				$this->smarty->assign('totalRegFrek',$totalRegFrek);
				
				// $this->smarty->display('laporan/kontenSeleksi.html');
				
			break;
		}
		// if ($p2 == ''){
			
			// $this->smarty->assign('modul',"laporan/");
			// $this->smarty->assign('lokasi',$lokasi);
			// $this->smarty->assign('pages',$pages);
			// $this->smarty->assign('site',$site);
			// $this->smarty->display('index.html');
		// }
	}
	
	function notification(){
		$notif = $this->db2->query("SELECT t.nama as namaAct, a.kode as kodeActivity, a.id_activity, a.created_date,
				SUBSTRING(MONTHNAME(a.created_date),1,3) as bulan, DAYNAME(a.created_date) as hari, DAY(a.created_date) as tgl,
				HOUR(a.created_date) as jam, MINUTE(a.created_date) as menit
				FROM adis_activity a
				INNER JOIN adis_type t ON t.kode = a.id_activity
				WHERE a.status_activity = '0'
				ORDER BY a.created_date DESC LIMIT 20
				");
		$notifRes = $notif->result();
		$notifNum = $notif->num_rows();
		$this->smarty->assign('notif',$notifRes);
		$this->smarty->assign('notifNum',$notifNum);
                
		
		$periodeQry = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 AND jenjangType = 1");
		$periode = $periodeQry->result_array();
		$periodeRows = $periodeQry->num_rows();
		$wherePeriode = "";
		if($periodeRows > 0){
			foreach ($periode as $k => $val){
				if ($k > 0){
					$wherePeriode .= " OR LEFT(periode,8) = '".$val['kode']."' ";						
				}else{
					$wherePeriode .= " LEFT(periode,8) = '".$val['kode']."' ";
				}
			}
		}
		
		$event = "SELECT CONCAT(SUBSTRING(B.nama,1, 3),'-',A.ruang,'-', TIME_FORMAT(A.jamMasuk, '%H:%m')) as title, 
			A.tanggal as start, A.tanggal as end, A.kode
			FROM adis_event_smb A
			LEFT JOIN adis_jalur_smb B ON B.kode = A.jalur
			WHERE $wherePeriode";
		$event = $this->db->query($event)->result_array();
				
		$child = '';
		foreach ($event as $k => $v){
			$child[$k]["title"] = $v["title"];
			$child[$k]["start"] = $v['start']; 
			$child[$k]["end"] = $v['end']; 
			$child[$k]["url"] = $this->host.'admisi/absen/'.$v['kode'].'/table'; 
		}
//                $json_data =array($child);
		$json = json_encode($child);
		file_put_contents('assets/json/even.json', $json);
                                               
	}
	
	function activity(){
		$QryProdi = $this->db2->query('SELECT * FROM `adis_prodi` WHERE jenjang = 1 AND erased = 0;')->result_array();
		$periodeQry = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 AND jenjangType = 1");
		$periode = $periodeQry->result_array();
		$periodeRows = $periodeQry->num_rows();
		
		$this->smarty->assign('periode',$periode);
		$notif = $this->db2->query("SELECT t.nama as namaAct, a.kode, a.id_activity, a.created_date, id_cmb,
				SUBSTRING(MONTHNAME(a.created_date),1,3) as bulan, DAYNAME(a.created_date) as hari, DAY(a.created_date) as tgl,
				HOUR(a.created_date) as jam, MINUTE(a.created_date) as menit
				FROM adis_activity a
				INNER JOIN adis_type t ON t.kode = a.id_activity
				ORDER BY a.created_date DESC LIMIT 20
				");
		$notifRes = $notif->result();
		$notifNum = $notif->num_rows();
		$this->smarty->assign('aktifiti',$notifRes);
		$this->smarty->assign('aktifitiNum',$notifNum);
		
		$totalreg = array(); 
		$wherePeriode = "";
		if ($periode){  
			if($periodeRows > 0){
				foreach ($periode as $k => $val){
					if ($k > 0){
						$wherePeriode .= " OR LEFT(bukaSmb,8) = '".$val['kode']."' ";						
					}else{
						$wherePeriode .= " LEFT(bukaSmb,8) = '".$val['kode']."' ";
					}
				}
			}
		
			$newReg = "SELECT COUNT(kode) as newReg FROM adis_smb_form WHERE ($wherePeriode) AND stsApplyPaid = 0;";
			$newPay = "SELECT COUNT(kode)  as newPay FROM adis_smb_form WHERE ($wherePeriode) AND (stsApplyPaid = 1 AND stsApplyPaidConfirm = 0); ";
			$newRereg = "SELECT COUNT(kode)  as newRereg FROM adis_smb_form WHERE ($wherePeriode) AND (stsReapplyPaid = 1 AND stsReapplyPaidConfirm = 0);";
			$newStudent = "SELECT COUNT(kode) as newStudent FROM adis_smb_form WHERE ($wherePeriode) AND nim != '' AND A.stsMundurAfterReapply != 1;";
		
			$daftar = $this->db2->query($newReg)->row_array();
			$bayarDaftar = $this->db2->query($newPay)->row_array();
			$daftarUlang = $this->db2->query($newRereg)->row_array();
			$mhsBaru = $this->db2->query($newStudent)->row_array();
			
			$this->smarty->assign('newReg',$daftar);
			$this->smarty->assign('newPay',$bayarDaftar);
			$this->smarty->assign('newRereg',$daftarUlang);
			$this->smarty->assign('newStudent',$mhsBaru);
			// echo $periode['kode'];exit;
			$qryReg = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1)  
				AND (($wherePeriode) AND SUBSTR(A.bukaSmb,12,2) = '01')
				GROUP BY B.nama;";
			$totalReg = $this->db2->query($qryReg)->result_array();
			
			$qryTRF = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1)  
				AND (($wherePeriode) AND SUBSTR(A.bukaSmb,12,2) = '02')
				GROUP BY B.nama;";
			$totalTRF = $this->db2->query($qryTRF)->result_array();
			
			$qryPSR = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1)  
				AND (($wherePeriode) AND SUBSTR(A.bukaSmb,12,2) = 'PS')
				GROUP BY B.nama;";
			$totalPSR = $this->db2->query($qryPSR)->result_array();
			
			$qryBM = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1)  
				AND (($wherePeriode) AND SUBSTR(A.bukaSmb,12,2) = 'BM')
				GROUP BY B.nama;";
			$totalBM = $this->db2->query($qryBM)->result_array();
			
			$qryPF = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1)  
				AND (($wherePeriode) AND SUBSTR(A.bukaSmb,12,2) = '03')
				GROUP BY B.nama;";
			$totalPF = $this->db2->query($qryPF)->result_array();
			
			$qryKPD3 = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				INNER JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				INNER JOIN adis_smb_usr_edu C ON C.smbUsr = A.kode  
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1) 
				AND ($wherePeriode AND (SUBSTR(bukaSmb, 9, 5)) = '.1.KP' )
				AND C.lulus_acc = 'D3'
				GROUP BY B.nama;";
			$totalKPD3 = $this->db2->query($qryKPD3)->result_array();
			
			
			$qryKPSMA = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				LEFT JOIN adis_smb_usr_edu C ON C.smbUsr = A.kode  
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1) 
				AND ($wherePeriode )  AND (SUBSTR(bukaSmb, 9, 5)) = '.1.KP'
				AND C.lulus_acc = 'SMA'
				GROUP BY B.nama;";
			$totalKPSMA = $this->db2->query($qryKPSMA)->result_array();
			
			
			$qryJP = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				LEFT JOIN adis_smb_usr_edu C ON C.smbUsr = A.kode  
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1) 
				AND ($wherePeriode ) AND (SUBSTR(bukaSmb, 9, 5)) = '.1.JP'
				GROUP BY B.nama;";
				// echo $qryJP; exit;
			$totalJP = $this->db2->query($qryJP)->result_array();
		
			$data[0] = array('PROGRAM STUDI','Mahasiswa');	
			// for($i = 0; $i < count($totalReg); $i++){
			for($i = 0; $i < count($QryProdi); $i++){
				$data[0] = array('Program Studi','REGULER', 'PARALEL D3', 'PARALEL SMA', "PSR", "FELLOWSHIP", "BIDIK MISI", "TRANSFER", "PRESTASI");
				$totalRegAll = 0;
				$totalParD3 = 0;
				$totalParSMA = 0;						
				$totalPSRAll = 0;	
				$totalBMAll = 0;
				$totalPFAll = 0;
				$totalTRFAll = 0;
				$totalJPAll = 0;
				
				for($j = 0; $j < count($totalReg); $j++){
					if ($totalReg[$j]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalRegAll = $totalReg[$j]['totalMhs'];
					}
					
				}
				for($j = 0; $j < count($totalJP); $j++){
					if ($totalJP[$j]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalJPAll = $totalJP[$j]['totalMhs'];
					}
					
				}
				
				for($j = 0; $j < count($totalKPD3); $j++){
					if ($totalKPD3[$j]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalParD3 = $totalKPD3[$j]['totalMhs'];
					}
					
				}
				
				for($k = 0; $k < count($totalKPSMA); $k++){
					if ($totalKPSMA[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalParSMA = $totalKPSMA[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalPSR); $k++){
					if ($totalPSR[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalPSRAll = $totalPSR[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalBM); $k++){
					if ($totalBM[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalBMAll = $totalBM[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalPF); $k++){
					if ($totalPF[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalPFAll = $totalPF[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalTRF); $k++){
					if ($totalTRF[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalTRFAll = $totalTRF[$k]['totalMhs'];
					}
				}
				
				$data[$i+1] = array(
					$QryProdi[$i]['singkatan'],
					(int)$totalRegAll, 
					(int)$totalParD3, 
					(int)$totalParSMA,
					(int)$totalPSRAll,
					(int)$totalPFAll,
					(int)$totalBMAll,
					(int)$totalTRFAll,
					(int)$totalJPAll
					);
				
			}	
			$totalReg = json_encode($data);
			// echo $totalReg;exit;
			$this->smarty->assign('totalReg',$totalReg);
		}
	}
	
	function activity__(){
		$QryProdi = $this->db2->query('SELECT * FROM `adis_prodi` WHERE jenjang = 1 AND erased = 0;')->result_array();
		$periode = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 AND jenjangType = 1")->row_array();
		$this->smarty->assign('periode',$periode);
		$notif = $this->db2->query("SELECT t.nama as namaAct, a.kode, a.id_activity, a.created_date, id_cmb,
				SUBSTRING(MONTHNAME(a.created_date),1,3) as bulan, DAYNAME(a.created_date) as hari, DAY(a.created_date) as tgl,
				HOUR(a.created_date) as jam, MINUTE(a.created_date) as menit
				FROM adis_activity a
				INNER JOIN adis_type t ON t.kode = a.id_activity
				ORDER BY a.created_date DESC LIMIT 20
				");
		$notifRes = $notif->result();
		$notifNum = $notif->num_rows();
		$this->smarty->assign('aktifiti',$notifRes);
		$this->smarty->assign('aktifitiNum',$notifNum);
		
		$totalreg = array();         
		if ($periode){       
		
			$newReg = "SELECT COUNT(kode) as newReg FROM adis_smb_form WHERE LEFT(bukaSmb,8) = '".$periode['kode']."' AND stsApplyPaid = 0;";
			$newPay = "SELECT COUNT(kode)  as newPay FROM adis_smb_form WHERE LEFT(bukaSmb,8) = '".$periode['kode']."' AND (stsApplyPaid = 1 AND stsApplyPaidConfirm = 0); ";
			$newRereg = "SELECT COUNT(kode)  as newRereg FROM adis_smb_form WHERE LEFT(bukaSmb,8) = '".$periode['kode']."' AND (stsReapplyPaid = 1 AND stsReapplyPaidConfirm = 0);";
			$newStudent = "SELECT COUNT(kode) as newStudent FROM adis_smb_form WHERE LEFT(bukaSmb,8) = '".$periode['kode']."' AND nim != '';";
		
			$daftar = $this->db2->query($newReg)->row_array();
			$bayarDaftar = $this->db2->query($newPay)->row_array();
			$daftarUlang = $this->db2->query($newRereg)->row_array();
			$mhsBaru = $this->db2->query($newStudent)->row_array();
			
			$this->smarty->assign('newReg',$daftar);
			$this->smarty->assign('newPay',$bayarDaftar);
			$this->smarty->assign('newRereg',$daftarUlang);
			$this->smarty->assign('newStudent',$mhsBaru);
			// echo $periode['kode'];exit;
			$qryReg = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1)  
				AND (SUBSTR(bukaSmb, 1, 8) = '".$periode['kode']."' AND SUBSTR(A.bukaSmb,12,2) = '01')
				GROUP BY B.nama;";
			$totalReg = $this->db2->query($qryReg)->result_array();
			
			$qryTRF = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1)  
				AND (SUBSTR(bukaSmb, 1, 8) = '".$periode['kode']."' AND SUBSTR(A.bukaSmb,12,2) = '02')
				GROUP BY B.nama;";
			$totalTRF = $this->db2->query($qryTRF)->result_array();
			
			$qryPSR = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1)  
				AND (SUBSTR(bukaSmb, 1, 8) = '".$periode['kode']."' AND SUBSTR(A.bukaSmb,12,2) = 'PS')
				GROUP BY B.nama;";
			$totalPSR = $this->db2->query($qryPSR)->result_array();
			
			$qryBM = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1)  
				AND (SUBSTR(bukaSmb, 1, 8) = '".$periode['kode']."' AND SUBSTR(A.bukaSmb,12,2) = 'BM')
				GROUP BY B.nama;";
			$totalBM = $this->db2->query($qryBM)->result_array();
			
			$qryPF = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1)  
				AND (SUBSTR(bukaSmb, 1, 8) = '".$periode['kode']."' AND SUBSTR(A.bukaSmb,12,2) = '03')
				GROUP BY B.nama;";
			$totalPF = $this->db2->query($qryPF)->result_array();
			
			$qryKPD3 = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				INNER JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				INNER JOIN adis_smb_usr_edu C ON C.smbUsr = A.kode  
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1) 
				AND (SUBSTR(bukaSmb, 1, 13) = '".$periode['kode'].'.1.KP'."' AND substr(bukaSmb, -4)) 
				AND C.lulus_acc = 'D3'
				GROUP BY B.nama;";
			$totalKPD3 = $this->db2->query($qryKPD3)->result_array();
			
			
			$qryKPSMA = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				LEFT JOIN adis_smb_usr_edu C ON C.smbUsr = A.kode  
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1) 
				AND (SUBSTR(bukaSmb, 1, 13) = '".$periode['kode'].'.1.KP'."' AND substr(bukaSmb, -4)) 
				AND C.lulus_acc = 'SMA'
				GROUP BY B.nama;";
			$totalKPSMA = $this->db2->query($qryKPSMA)->result_array();
			
			
			$qryJP = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				LEFT JOIN adis_smb_usr_edu C ON C.smbUsr = A.kode  
				WHERE (stsResultConfirm = 1 AND  stsReApplyPaid = 1) 
				AND (SUBSTR(bukaSmb, 1, 13) = '".$periode['kode'].'.1.JP'."' AND substr(bukaSmb, -4))
				GROUP BY B.nama;";
			$totalJP = $this->db2->query($qryJP)->result_array();
		
			$data[0] = array('PROGRAM STUDI','Mahasiswa');	
			// for($i = 0; $i < count($totalReg); $i++){
			for($i = 0; $i < count($QryProdi); $i++){
				$data[0] = array('Program Studi','REGULER', 'PARALEL D3', 'PARALEL SMA', "PSR", "FELLOWSHIP", "BIDIK MISI", "TRANSFER", "PRESTASI");
				$totalRegAll = 0;
				$totalParD3 = 0;
				$totalParSMA = 0;						
				$totalPSRAll = 0;	
				$totalBMAll = 0;
				$totalPFAll = 0;
				$totalTRFAll = 0;
				$totalJPAll = 0;
				
				for($j = 0; $j < count($totalReg); $j++){
					if ($totalReg[$j]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalRegAll = $totalReg[$j]['totalMhs'];
					}
					
				}
				for($j = 0; $j < count($totalJP); $j++){
					if ($totalJP[$j]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalJPAll = $totalJP[$j]['totalMhs'];
					}
					
				}
				
				for($j = 0; $j < count($totalKPD3); $j++){
					if ($totalKPD3[$j]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalParD3 = $totalKPD3[$j]['totalMhs'];
					}
					
				}
				
				for($k = 0; $k < count($totalKPSMA); $k++){
					if ($totalKPSMA[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalParSMA = $totalKPSMA[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalPSR); $k++){
					if ($totalPSR[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalPSRAll = $totalPSR[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalBM); $k++){
					if ($totalBM[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalBMAll = $totalBM[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalPF); $k++){
					if ($totalPF[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalPFAll = $totalPF[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalTRF); $k++){
					if ($totalTRF[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalTRFAll = $totalTRF[$k]['totalMhs'];
					}
				}
				
				$data[$i+1] = array(
					$QryProdi[$i]['singkatan'],
					(int)$totalRegAll, 
					(int)$totalParD3, 
					(int)$totalParSMA,
					(int)$totalPSRAll,
					(int)$totalPFAll,
					(int)$totalBMAll,
					(int)$totalTRFAll,
					(int)$totalJPAll
					);
				
			}	
			$totalReg = json_encode($data);
			// echo $totalReg;exit;
			$this->smarty->assign('totalReg',$totalReg);
		}
	}
	
	function grafikCalonMahasiswa($periode){
		$periodeRow = $this->db2->query("SELECT * FROM adis_periode_master WHERE kode = '$periode'")->row_array();
		$QryProdi = $this->db2->query('SELECT * FROM `adis_prodi` WHERE jenjang = "'.$periodeRow['jenjangType'].'" 
			AND erased = 0;')->result_array();
			
		$qryjalur = $this->db2->query("SELECT * FROM adis_jalur_smb 
			WHERE jenjang = '".$periodeRow['jenjangType']."' AND erased = 0;")->result_array();
		
		$arrJalur = array(
			array('kode'=>'RAPOR', 'nama'=>'Jalur Rapor'),
			array('kode'=>'TULIS', 'nama'=>'Jalur Tes Tulis'),
			array('kode'=>'ODS', 'nama'=>'ODS Online')
		);
		
		$totalreg = array();         
		if ($periode){       
		
			foreach($arrJalur as $i => $v){
				$qry[$i] = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs 
					FROM adis_smb_form A
					LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
					LEFT JOIN adis_smb_usr C ON A.kode = C.kode 
					WHERE (SUBSTR(bukaSmb, 1, 8) = '".$periode."' AND C.jalur_penerimaan = '".$v['kode']."')
					GROUP BY B.nama;";
				$total[$i] = $this->db2->query($qry[$i])->result_array();
			} 
			
			$data[0] = array('PROGRAM STUDI','Mahasiswa');	
			
			for($i = 0; $i < count($QryProdi); $i++){
				$data[$i+1] = array($QryProdi[$i]['singkatan']);
				$data[0] = array('Program Studi');
				
				
				foreach($arrJalur as $k => $v){
					$dot = strlen($v['nama']) > 15 ? "..." : "";
					$data[0][$k+1] = substr($v['nama'],0,15).$dot;
					$totalall[$k] = 0;
					
					
					foreach($total[$k] as $x => $val){
						if ($val['prodi'] == $QryProdi[$i]['singkatan']){
							$totalall[$k] = $val['totalMhs'];
						}
						
					}	
					
					$data[$i+1][$k+1] = (int)$totalall[$k];
				}
				
			}	
			
			$totalReg = json_encode($data);
			// echo $totalReg;exit;
			$this->smarty->assign('totalPeserta',$totalReg);
		}
	}
	
	function grafikCalonMahasiswae(){
		$QryProdi = $this->db2->query('SELECT * FROM `adis_prodi` WHERE jenjang = 1 AND erased = 0;')->result_array();
		$periodeQry = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 AND jenjangType = 1");
		$periode = $periodeQry->result_array();
		$periodeRows = $periodeQry->num_rows();
		
		$totalreg = array();         
		if ($periode){       
		
			$wherePeriode = "";
			if($periodeRows > 0){
				foreach ($periode as $k => $val){
					if ($k > 0){
						$wherePeriode .= " OR (LEFT(bukaSmb,8)) = '".$val['kode']."' ";						
					}else{
						$wherePeriode .= " (LEFT(bukaSmb,8)) = '".$val['kode']."' ";
					}
				}
			}
		
			$qryReg = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (($wherePeriode) AND SUBSTR(A.bukaSmb,12,2) = '01')
				GROUP BY B.nama;";
			$totalReg = $this->db2->query($qryReg)->result_array();
			
			$qryTRF = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (($wherePeriode) AND SUBSTR(A.bukaSmb,12,2) = '02')
				GROUP BY B.nama;";
			$totalTRF = $this->db2->query($qryTRF)->result_array();
			
			$qryPSR = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (($wherePeriode) AND SUBSTR(A.bukaSmb,12,2) = 'PS')
				GROUP BY B.nama;";
			$totalPSR = $this->db2->query($qryPSR)->result_array();
			
			$qryBM = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (($wherePeriode) AND SUBSTR(A.bukaSmb,12,2) = 'BM')
				GROUP BY B.nama;";
			$totalBM = $this->db2->query($qryBM)->result_array();
			
			$qryPF = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (($wherePeriode) AND SUBSTR(A.bukaSmb,12,2) = '03')
				GROUP BY B.nama;";
			$totalPF = $this->db2->query($qryPF)->result_array();
			
			$qryKPD3 = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				INNER JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				INNER JOIN adis_smb_usr_edu C ON C.smbUsr = A.kode  
				WHERE ($wherePeriode) AND (SUBSTR(bukaSmb, 9, 5)) = '.1.KP'
				AND C.lulus_acc = 'D3'
				GROUP BY B.nama;";
			$totalKPD3 = $this->db2->query($qryKPD3)->result_array();
			
			
			$qryKPSMA = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				LEFT JOIN adis_smb_usr_edu C ON C.smbUsr = A.kode  
				WHERE
				($wherePeriode) AND (SUBSTR(bukaSmb, 9, 5)) = '.1.KP' 
				AND C.lulus_acc = 'SMA'
				GROUP BY B.nama;";
			$totalKPSMA = $this->db2->query($qryKPSMA)->result_array();
			// echo $qryKPSMA;
			// print_r($totalKPSMA);exit;
			
			
			$qryJP = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				LEFT JOIN adis_smb_usr_edu C ON C.smbUsr = A.kode  
				WHERE 
				($wherePeriode) AND (SUBSTR(bukaSmb, 9, 5)) = '.1.JP'
				GROUP BY B.nama;";
			$totalJP = $this->db2->query($qryJP)->result_array();
		
			$data[0] = array('PROGRAM STUDI','Mahasiswa');	
			
			// for($i = 0; $i < count($totalReg); $i++){
			for($i = 0; $i < count($QryProdi); $i++){
				$data[0] = array('Program Studi','REGULER', 'PARALEL D3', 'PARALEL SMA', "PSR", "FELLOWSHIP", "BIDIK MISI", "TRANSFER", "PRESTASI");
				$totalParD3 = 0;
				$totalParSMA = 0;						
				$totalPSRAll = 0;	
				$totalBMAll = 0;
				$totalPFAll = 0;
				$totalTRFAll = 0;
				$totalJPAll = 0;
				$totalRegAll = 0;
				
				for($j = 0; $j < count($totalReg); $j++){
					if ($totalReg[$j]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalRegAll = $totalReg[$j]['totalMhs'];
					}
					
				}
				for($j = 0; $j < count($totalJP); $j++){
					if ($totalJP[$j]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalJPAll = $totalJP[$j]['totalMhs'];
					}
					
				}
				
				for($j = 0; $j < count($totalKPD3); $j++){
					if ($totalKPD3[$j]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalParD3 = $totalKPD3[$j]['totalMhs'];
					}
					
				}
				
				for($k = 0; $k < count($totalKPSMA); $k++){
					if ($totalKPSMA[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalParSMA = $totalKPSMA[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalPSR); $k++){
					if ($totalPSR[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalPSRAll = $totalPSR[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalBM); $k++){
					if ($totalBM[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalBMAll = $totalBM[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalPF); $k++){
					if ($totalPF[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalPFAll = $totalPF[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalTRF); $k++){
					if ($totalTRF[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalTRFAll = $totalTRF[$k]['totalMhs'];
					}
				}
				
				$data[$i+1] = array(
					$QryProdi[$i]['singkatan'],
					(int)$totalRegAll, 
					(int)$totalParD3, 
					(int)$totalParSMA,
					(int)$totalPSRAll,
					(int)$totalPFAll,
					(int)$totalBMAll,
					(int)$totalTRFAll,
					(int)$totalJPAll
					);
				
			}	
				
			// print_r($totalRegAll);exit;
			$totalReg = json_encode($data);
			// echo $totalReg;exit;
			$this->smarty->assign('totalPeserta',$totalReg);
		}
	}
	
	function grafikCalonMahasiswa_(){
		$QryProdi = $this->db2->query('SELECT * FROM `adis_prodi` WHERE jenjang = 1 AND erased = 0;')->result_array();
		$periode = $this->db2->query("SELECT * FROM adis_periode_master WHERE status = 1 AND jenjangType = 1")->row_array();
		$totalreg = array();         
		if ($periode){       
		
			$qryReg = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (SUBSTR(bukaSmb, 1, 8) = '".$periode['kode']."' AND SUBSTR(A.bukaSmb,12,2) = '01')
				GROUP BY B.nama;";
			$totalReg = $this->db2->query($qryReg)->result_array();
			
			$qryTRF = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (SUBSTR(bukaSmb, 1, 8) = '".$periode['kode']."' AND SUBSTR(A.bukaSmb,12,2) = '02')
				GROUP BY B.nama;";
			$totalTRF = $this->db2->query($qryTRF)->result_array();
			
			$qryPSR = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (SUBSTR(bukaSmb, 1, 8) = '".$periode['kode']."' AND SUBSTR(A.bukaSmb,12,2) = 'PS')
				GROUP BY B.nama;";
			$totalPSR = $this->db2->query($qryPSR)->result_array();
			
			$qryBM = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (SUBSTR(bukaSmb, 1, 8) = '".$periode['kode']."' AND SUBSTR(A.bukaSmb,12,2) = 'BM')
				GROUP BY B.nama;";
			$totalBM = $this->db2->query($qryBM)->result_array();
			
			$qryPF = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				WHERE (SUBSTR(bukaSmb, 1, 8) = '".$periode['kode']."' AND SUBSTR(A.bukaSmb,12,2) = '03')
				GROUP BY B.nama;";
			$totalPF = $this->db2->query($qryPF)->result_array();
			
			$qryKPD3 = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				INNER JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				INNER JOIN adis_smb_usr_edu C ON C.smbUsr = A.kode  
				WHERE (SUBSTR(bukaSmb, 1, 13) = '".$periode['kode'].'.1.KP'."') 
				AND C.lulus_acc = 'D3'
				GROUP BY B.nama;";
			$totalKPD3 = $this->db2->query($qryKPD3)->result_array();
			
			
			$qryKPSMA = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				LEFT JOIN adis_smb_usr_edu C ON C.smbUsr = A.kode  
				WHERE (SUBSTR(bukaSmb, 1, 13) = '".$periode['kode'].'.1.KP'."' AND substr(bukaSmb, -4)) 
				AND C.lulus_acc = 'SMA'
				GROUP BY B.nama;";
			$totalKPSMA = $this->db2->query($qryKPSMA)->result_array();
			
			
			$qryJP = "SELECT B.singkatan as prodi, COUNT(A.kode) as totalMhs FROM adis_smb_form A
				LEFT JOIN adis_prodi B ON B.kode = SUBSTR(A.bukaSmb,-4)
				LEFT JOIN adis_smb_usr_edu C ON C.smbUsr = A.kode  
				WHERE (SUBSTR(bukaSmb, 1, 13) = '".$periode['kode'].'.1.JP'."' AND substr(bukaSmb, -4)) 
				GROUP BY B.nama;";
			$totalJP = $this->db2->query($qryJP)->result_array();
		
			$data[0] = array('PROGRAM STUDI','Mahasiswa');	
			
			// for($i = 0; $i < count($totalReg); $i++){
			for($i = 0; $i < count($QryProdi); $i++){
				$data[0] = array('Program Studi','REGULER', 'PARALEL D3', 'PARALEL SMA', "PSR", "FELLOWSHIP", "BIDIK MISI", "TRANSFER", "PRESTASI");
				$totalParD3 = 0;
				$totalParSMA = 0;						
				$totalPSRAll = 0;	
				$totalBMAll = 0;
				$totalPFAll = 0;
				$totalTRFAll = 0;
				$totalJPAll = 0;
				$totalRegAll = 0;
				
				for($j = 0; $j < count($totalReg); $j++){
					if ($totalReg[$j]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalRegAll = $totalReg[$j]['totalMhs'];
					}
					
				}
				for($j = 0; $j < count($totalJP); $j++){
					if ($totalJP[$j]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalJPAll = $totalJP[$j]['totalMhs'];
					}
					
				}
				
				for($j = 0; $j < count($totalKPD3); $j++){
					if ($totalKPD3[$j]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalParD3 = $totalKPD3[$j]['totalMhs'];
					}
					
				}
				
				for($k = 0; $k < count($totalKPSMA); $k++){
					if ($totalKPSMA[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalParSMA = $totalKPSMA[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalPSR); $k++){
					if ($totalPSR[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalPSRAll = $totalPSR[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalBM); $k++){
					if ($totalBM[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalBMAll = $totalBM[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalPF); $k++){
					if ($totalPF[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalPFAll = $totalPF[$k]['totalMhs'];
					}
				}
					
				for($k = 0; $k < count($totalTRF); $k++){
					if ($totalTRF[$k]['prodi'] == $QryProdi[$i]['singkatan']){
						$totalTRFAll = $totalTRF[$k]['totalMhs'];
					}
				}
				
				$data[$i+1] = array(
					$QryProdi[$i]['singkatan'],
					(int)$totalRegAll, 
					(int)$totalParD3, 
					(int)$totalParSMA,
					(int)$totalPSRAll,
					(int)$totalPFAll,
					(int)$totalBMAll,
					(int)$totalTRFAll,
					(int)$totalJPAll
					);
				
			}	
			$totalReg = json_encode($data);
			// echo $totalReg;exit;
			$this->smarty->assign('totalPeserta',$totalReg);
		}
	}
	
	function clickNotif(){
		$idActivity = $this->uri->segment(3);
		$kodeActivity = $this->uri->segment(4);
		
		$this->db2->where('kode', $kodeActivity);
		$this->db2->update('adis_activity',array('status_activity'=>1));
		
		if ($idActivity == '13.1'){
			redirect ('/smb/smbCalon', 'location');
		}else if ($idActivity == '13.2'){
			redirect ('/smb/smbCalon', 'location');
		}else if ($idActivity == '13.3'){
			redirect ('/smb/smbPay', 'location');
		}else if ($idActivity == '13.4'){
			redirect ('/smb/smbCalon', 'location');
		}else if ($idActivity == '13.5'){
			redirect ('/smb/smbSeleksi', 'location');
		}else if ($idActivity == '13.9'){
			redirect ('/smb/smbDaftarUlang', 'location');
		}
		
	}
}