<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

class Msmb extends CUTI_Model{

	function __construct(){
		parent::__construct();
		$this->log = $this->config->item('log_path');
	}

	function Msmb(){
		parent::__construct();

		$this->db2->query("SET lc_time_names = 'id_ID'");

	}

	function get_data_grid($type="", $p1="", $p2="", $p3=""){
		$this->load->library('lib');
		$where = "";
		switch($type){
			case 'masterKurikulum':
				$post = $this->input->post();
				$where = " where 1=1 ";
				if(!empty($post['method']) && $post['method'] == 'search'){
					$where .= " and concat(a.namaunit,a.idperiode,a.idkurikulum) like '%".$post['nama_cm']."%' ";
				}

				$sql = "select a.*, concat(b.nama,' S',b.jenjang) as namaunit, if(a.idjenjang=1,'S1','S2') as namajenjang,
					c.namasistemkuliah
					from smart_master_kurikulum a
					left join adis_prodi b on a.idunit = b.idBig
					left join (select * from smart_sistemkuliah_big group by idsistemkuliah) c on a.idsistemkuliah = c.idsistemkuliah and a.idjenjang = c.jenjang
				$where order by id asc";

				break;
			case 'masterRefJalur':
				$post = $this->input->post();
				$where = " where 1=1 ";
				if(!empty($post['method']) && $post['method'] == 'search'){
					$where .= " and namajalurpendaftaran like '%".$post['nama_cm']."%' ";
				}

				$sql = "select * from smart_lv_jalurpendaftaran $where order by id asc";

				break;
			case 'setting_tarif_kk':
				$method = $this->input->post('method');
				if(empty($method)){
					$periodeAka = "SELECT B.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '3' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and a.periode_kode = '".$periodeAka['idPeriode']."' ";

				}
					$sql = "SELECT a.*, b.singkatan as prodi
					FROM adis_pembayaran_kk a
					LEFT JOIN adis_prodi b ON a.prodi = b.kode
					where 1=1 $where ";
			break;
			case 'hasilSeleksiKK':
				$where = " where 1=1 ";
				$method = $this->input->post('method');
				if(empty($method)){
					$periodeAka = "SELECT A.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '3' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and SUBSTR(a.bukaSmb, 1, 10) = '".$periodeAka['idPeriode']."' ";

				}else{
					if($method == "search"){
						$term = $this->input->post('nama_cm');
						$where .= " and concat(f.nama,a.nomor,a.kode) like '%$term%' and d.jenjangType = 2 ";
					}
					if ($method == 'select'){
						$prodi = $this->input->post('prodi');
						$gelombang = $this->input->post('gelombang');
						$whereGel = "";
						$whereProdi = "";

						if($gelombang != 'all'){
							$whereGel = " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
						}

						if ($prodi != 'all'){
							$whereProdi = " AND substr(f.bukaSmb, -4) = '$prodi' ";
						}
						$where = " WHERE $whereGel $whereProdi";
					}
				}

				$sql = "select a.nomor, a.kode, d.singkatan, f.nama, a.stsResultConfirm, a.stsResultPass,
						f.rumahCell,  g.is_acc
						from adis_smb_form a
						INNER JOIN adis_periode_master c ON LEFT(a.bukaSmb,8) = c.kode
						left JOIN adis_prodi d ON RIGHT(a.bukaSmb, 4) = d.kode
						left JOIN adis_jalur_smb e ON SUBSTR(a.bukaSmb, 12, 2) = e.kode
						left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
						left JOIN smart_jadwal_tpa g ON a.kode = g.kode
						$where and g.is_acc = 1
						ORDER BY a.updateTime ASC
					";

			break;
			case 'modulPrivilege':
				$sql = "select a.*, `level_name`
				from `smart_menu_privilage` a
				left join tbl_usrm_level b on a.`level_user`=b.id ";
				break;
			case 'masterProdi':
				$post = $this->input->post();
				$where = " where 1=1 ";
				if(!empty($post['method']) && $post['method'] == 'search'){
					$where .= " and nama like '%".$post['nama_cm']."%' ";
				}

				$sql = "select * from adis_prodi $where order by jenjang asc,nama asc";

				break;
			case 'masterAgama':
				$sql = "select * from adis_type where kode like '02.%' order by idBig asc";
				break;
			case 'ver_dok_kk':
				$where = " where 1=1 ";
				$method = $this->input->post('method');
				if(empty($method)){
					$periodeAka = "SELECT A.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '3' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and SUBSTR(a.bukaSmb, 1, 10) = '".$periodeAka['idPeriode']."' ";

				}else{
					if($method == "search"){
						$term = $this->input->post('nama_cm');
						$where .= " and concat(f.nama,a.nomor,a.kode) like '%$term%' and d.jenjangType = 2 ";
					}
					if ($method == 'select'){
						$prodi = $this->input->post('prodi');
						$gelombang = $this->input->post('gelombang');
						$whereGel = "";
						$whereProdi = "";

						if($gelombang != 'all'){
							$whereGel = " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
						}

						if ($prodi != 'all'){
							$whereProdi = " AND substr(f.bukaSmb, -4) = '$prodi' ";
						}



						$where = " WHERE $whereGel $whereProdi";
					}
				}
				$sql = "select a.nomor, a.kode, d.singkatan, f.nama, g.tahunLulus, g.jurusan, g.lulusAsal, g.verifikasi_forlap,
					f.rumahCell, g.lulusankk
					from adis_smb_form a
					-- INNER JOIN adis_smb_usr b ON a.kode = b.kode
					INNER JOIN adis_periode_master c ON LEFT(a.bukaSmb,8) = c.kode
					left JOIN adis_prodi d ON RIGHT(a.bukaSmb, 4) = d.kode
					left JOIN adis_jalur_smb e ON SUBSTR(a.bukaSmb, 12, 2) = e.kode
					left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
					left JOIN adis_smb_usr_edu g ON a.kode = g.smbUsr
					$where -- and g.verifikasi_forlap != 1
					ORDER BY a.createTime DESC, g.verifikasi_forlap ASC
				";

			break;
			case 'man_edu':
				$where = " where 1=1 ";
				$method = $this->input->post('method');
				if($method == "search"){
					$term = $this->input->post('nama_cm');
					if($term){
						$where .= " and concat(username,fullname,kode) like '%$term%' ";
					}
				}

				$sql = "select * from adis_sys_usr $where and erased = 0 and id_level = 44 ";

				break;
			case 'mahasiswaS1':
				$where = " where 1=1 ";
				$method = $this->input->post('method');
				$gelombang = $this->input->post('gelombang');
				$prodi = $this->input->post('prodi');

				if(empty($method) || $gelombang == 'all'){
					$periodeAka = "SELECT B.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '1' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and SUBSTR(a.bukaSmb, 1, 8) = '".$periodeAka['idPeriode']."' ";

				}else{
					if($method == "search"){
						$term = $this->input->post('nama_cm');
						$where .= " and concat(f.nama,a.nomor,a.kode) like '%$term%' and d.jenjangType = 1 ";
					}
					if ($method == 'select'){
						if($gelombang != 'all'){
							$where .= " AND SUBSTR(a.bukaSmb, 1, 10) = '$gelombang' ";
						}

						if ($prodi != 'all'){
							$where .= " AND substr(a.bukaSmb, -4) = '$prodi' ";
						}
					}
				}
				$sql = "select a.nomor, a.kode, d.singkatan, f.nama, g.verifikasi_forlap,
					f.rumahCell, a.stsReapplyPaid, a.stsReapplyPaidConfirm, a.reapplyBankTransferAmount,
					b.buktiBayarDaftarUlang, a.reapplyMaxDate, a.nim, a.stsIntegrasikeBIG, a.stsMundurAfterReapply,
					d.nama as prodi
					from adis_smb_form a
					left JOIN adis_smb_usr_keu b ON a.kode = b.smbUsr
					INNER JOIN adis_periode_master c ON LEFT(a.bukaSmb,8) = c.kode
					left JOIN adis_prodi d ON RIGHT(a.bukaSmb, 4) = d.kode
					left JOIN adis_jalur_smb e ON SUBSTR(a.bukaSmb, 12, 2) = e.kode
					left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
					left JOIN adis_smb_usr_edu g ON a.kode = g.smbUsr
					$where and a.stsResultPass = 1 AND a.stsReapplyPaidConfirm = 1
					ORDER BY a.updateTime, a.stsApplyPaidConfirm ASC
				";


			break;
			case 'mahasiswaKK':
				$where = " where 1=1 ";
				$method = $this->input->post('method');
				$gelombang = $this->input->post('gelombang');

				if(empty($method) || $gelombang == 'all'){
					$periodeAka = "SELECT B.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '3' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and SUBSTR(a.bukaSmb, 1, 8) = '".$periodeAka['idPeriode']."' ";

				}else{
					if($method == "search"){
						$term = $this->input->post('nama_cm');
						$where .= " and concat(f.nama,a.nomor,a.kode) like '%$term%' and d.jenjangType = 3 ";
					}
					if ($method == 'select'){
						$prodi = $this->input->post('prodi');
						$whereGel = "";
						$whereProdi = "";

						if($gelombang != 'all'){
							$where .= " AND SUBSTR(a.bukaSmb, 1, 10) = '$gelombang' ";
						}

						if ($prodi != 'all'){
							$where .= " AND substr(a.bukaSmb, -4) = '$prodi' ";
						}
					}
				}
				$sql = "select a.nomor, a.kode, d.singkatan, f.nama, g.verifikasi_forlap,
					f.rumahCell, a.stsReapplyPaid, a.stsReapplyPaidConfirm, a.reapplyBankTransferAmount,
					b.buktiBayarDaftarUlang, a.reapplyMaxDate, a.nim, a.stsIntegrasikeBIG, a.stsMundurAfterReapply
					d.nama as prodi
					from adis_smb_form a
					left JOIN adis_smb_usr_keu b ON a.kode = b.smbUsr
					INNER JOIN adis_periode_master c ON LEFT(a.bukaSmb,8) = c.kode
					left JOIN adis_prodi d ON RIGHT(a.bukaSmb, 4) = d.kode
					left JOIN adis_jalur_smb e ON SUBSTR(a.bukaSmb, 12, 2) = e.kode
					left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
					left JOIN adis_smb_usr_edu g ON a.kode = g.smbUsr
					$where and a.stsResultPass = 1 AND a.stsReapplyPaidConfirm = 1
					ORDER BY a.updateTime, a.stsApplyPaidConfirm ASC
				";


			break;
			case 'mahasiswaS2':
				$where = " where 1=1 ";
				$method = $this->input->post('method');
				$prodi = $this->input->post('prodi');
				$gelombang = $this->input->post('gelombang');

				if(empty($method) || $gelombang == 'all'){
					$periodeAka = "SELECT B.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '2' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and SUBSTR(a.bukaSmb, 1, 8) = '".$periodeAka['idPeriode']."' ";

				}else{
					if($method == "search"){
						$term = $this->input->post('nama_cm');
						$where .= " and concat(f.nama,a.nomor,a.kode) like '%$term%' and d.jenjangType = 2 ";
					}
					if ($method == 'select'){

						if($gelombang != 'all'){
							$where .= " and SUBSTR(a.bukaSmb, 1, 10) = '$gelombang' ";
						}

						if ($prodi != 'all'){
							$where .= " and substr(a.bukaSmb, -4) = '$prodi' ";
						}

					}
				}
				$sql = "select a.nomor, a.kode, d.singkatan, f.nama, g.verifikasi_forlap,
					f.rumahCell, a.stsReapplyPaid, a.stsReapplyPaidConfirm, a.reapplyBankTransferAmount,
					b.buktiBayarDaftarUlang, a.reapplyMaxDate, a.nim, a.stsIntegrasikeBIG, a.stsMundurAfterReapply
					d.nama as prodi
					from adis_smb_form a
					left JOIN adis_smb_usr_keu b ON a.kode = b.smbUsr
					INNER JOIN adis_periode_master c ON LEFT(a.bukaSmb,8) = c.kode
					left JOIN adis_prodi d ON RIGHT(a.bukaSmb, 4) = d.kode
					left JOIN adis_jalur_smb e ON SUBSTR(a.bukaSmb, 12, 2) = e.kode
					left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
					left JOIN adis_smb_usr_edu g ON a.kode = g.smbUsr
					$where and a.stsResultPass = 1 AND a.stsReapplyPaidConfirm = 1
					ORDER BY a.updateTime, a.stsApplyPaidConfirm ASC
				";


			break;
			case 'daftarulangKK':
				$where = " where 1=1 ";
				$method = $this->input->post('method');
				if(empty($method)){
					$periodeAka = "SELECT A.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '3' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and SUBSTR(a.bukaSmb, 1, 10) = '".$periodeAka['idPeriode']."' ";

				}else{
					if($method == "search"){
						$term = $this->input->post('nama_cm');
						$where .= " and concat(f.nama,a.nomor,a.kode) like '%$term%' and d.jenjangType = 1 ";
					}
					if ($method == 'select'){
						$prodi = $this->input->post('prodi');
						$gelombang = $this->input->post('gelombang');
						$whereGel = "";
						$whereProdi = "";

						if($gelombang != 'all'){
							$whereGel = " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
						}

						if ($prodi != 'all'){
							$whereProdi = " AND substr(f.bukaSmb, -4) = '$prodi' ";
						}



						$where = " WHERE $whereGel $whereProdi";
					}
				}
				$sql = "select a.nomor, a.kode, d.singkatan, f.nama, g.verifikasi_forlap,
					f.rumahCell, a.stsReapplyPaid, a.stsReapplyPaidConfirm, a.reapplyBankTransferAmount,
					b.buktiBayarDaftarUlang, a.reapplyMaxDate
					from adis_smb_form a
					left JOIN adis_smb_usr_keu b ON a.kode = b.smbUsr
					INNER JOIN adis_periode_master c ON LEFT(a.bukaSmb,8) = c.kode
					left JOIN adis_prodi d ON RIGHT(a.bukaSmb, 4) = d.kode
					left JOIN adis_jalur_smb e ON SUBSTR(a.bukaSmb, 12, 2) = e.kode
					left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
					left JOIN adis_smb_usr_edu g ON a.kode = g.smbUsr
					$where and a.stsResultPass = 1
					ORDER BY a.updateTime, a.stsApplyPaidConfirm ASC
				";


			break;
			case 'daftarulangMag':
				$where = " where 1=1 ";
				$method = $this->input->post('method');
				if(empty($method)){
					$periodeAka = "SELECT A.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '2' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and SUBSTR(a.bukaSmb, 1, 10) = '".$periodeAka['idPeriode']."' ";

				}else{
					if($method == "search"){
						$term = $this->input->post('nama_cm');
						$where .= " and concat(f.nama,a.nomor,a.kode) like '%$term%' and d.jenjangType = 2 ";
					}
					if ($method == 'select'){
						$prodi = $this->input->post('prodi');
						$gelombang = $this->input->post('gelombang');
						$whereGel = "";
						$whereProdi = "";

						if($gelombang != 'all'){
							$whereGel = " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
						}

						if ($prodi != 'all'){
							$whereProdi = " AND substr(f.bukaSmb, -4) = '$prodi' ";
						}



						$where = " WHERE $whereGel $whereProdi";
					}
				}
				$sql = "select a.nomor, a.kode, d.singkatan, f.nama, g.verifikasi_forlap,
					f.rumahCell, a.stsReapplyPaid, a.stsReapplyPaidConfirm, a.reapplyBankTransferAmount,
					b.buktiBayarDaftarUlang, a.reapplyMaxDate
					from adis_smb_form a
					left JOIN adis_smb_usr_keu b ON a.kode = b.smbUsr
					INNER JOIN adis_periode_master c ON LEFT(a.bukaSmb,8) = c.kode
					left JOIN adis_prodi d ON RIGHT(a.bukaSmb, 4) = d.kode
					left JOIN adis_jalur_smb e ON SUBSTR(a.bukaSmb, 12, 2) = e.kode
					left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
					left JOIN adis_smb_usr_edu g ON a.kode = g.smbUsr
					$where and a.stsResultPass = 1
					ORDER BY a.updateTime, a.stsApplyPaidConfirm ASC
				";


			break;
			case 'beasiswaMag':
				$sql = "select * from smart_ref_beasiswa";
			break;
			case 'setPotonganMag':
				$method = $this->input->post('method');
				if(empty($method)){
					$periodeAka = "SELECT B.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '2' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and b.kodePeriode = '".$periodeAka['idPeriode']."' ";

				}
					$sql = "SELECT a.*, c.singkatan, d.nama_skema
					FROM smart_skema_biaya_beasiswa a
					inner join smart_skema_biaya_normal aa on a.skema_normal_id = aa.id
					inner join adis_pembayaran_s2 b on aa.pembayaran_kode = b.kode
					left JOIN adis_prodi c ON b.prodi = c.kode
					left JOIN smart_ref_skema_biaya d ON aa.ref_skema_id = d.id
					where 1=1 $where ";
			break;
			case 'skemaBiaya':
				$method = $this->input->post('method');
				if(empty($method)){
					$periodeAka = "SELECT B.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '2' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and b.kodePeriode = '".$periodeAka['idPeriode']."' ";

				}
					$sql = "SELECT a.*, c.singkatan, d.nama_skema, b.totalBiaya
					FROM smart_skema_biaya_normal a
					inner join adis_pembayaran_s2 b on a.pembayaran_kode = b.kode
					left JOIN adis_prodi c ON b.prodi = c.kode
					left JOIN smart_ref_skema_biaya d ON a.ref_skema_id = d.id
					where 1=1 $where ";
			break;
			case 'setting_tarif_kk':
				$method = $this->input->post('method');
				if(empty($method)){
					$periodeAka = "SELECT B.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '2' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and p.kodePeriode = '".$periodeAka['idPeriode']."' ";

				}
					$sql = "SELECT p.*, pr.singkatan as prodi
					FROM adis_pembayaran_s2 p
					LEFT JOIN adis_prodi pr ON p.prodi = pr.kode
					where 1=1 $where -- p.kodePeriode = '' ORDER BY kode";

					// echo $sql;exit;
			break;
			case 'setting_tarif_magister':
				$method = $this->input->post('method');
				if(empty($method)){
					$periodeAka = "SELECT B.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '2' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and p.kodePeriode = '".$periodeAka['idPeriode']."' ";

				}
					$sql = "SELECT p.*, pr.singkatan as prodi
					FROM adis_pembayaran_s2 p
					LEFT JOIN adis_prodi pr ON p.prodi = pr.kode
					where 1=1 $where -- p.kodePeriode = '' ORDER BY kode";

					// echo $sql;exit;
			break;
			case 'hasilSeleksi':
				$where = " where 1=1 ";
				$method = $this->input->post('method');
				if(empty($method)){
					$periodeAka = "SELECT A.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '2' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and SUBSTR(a.bukaSmb, 1, 10) = '".$periodeAka['idPeriode']."' ";

				}else{
					if($method == "search"){
						$term = $this->input->post('nama_cm');
						$where .= " and concat(f.nama,a.nomor,a.kode) like '%$term%' and d.jenjangType = 2 ";
					}
					if ($method == 'select'){
						$prodi = $this->input->post('prodi');
						$gelombang = $this->input->post('gelombang');
						$whereGel = "";
						$whereProdi = "";

						if($gelombang != 'all'){
							$whereGel = " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
						}

						if ($prodi != 'all'){
							$whereProdi = " AND substr(f.bukaSmb, -4) = '$prodi' ";
						}
						$where = " WHERE $whereGel $whereProdi";
					}
				}

				$sql = "select a.nomor, a.kode, d.singkatan, f.nama, a.stsResultConfirm, a.stsResultPass,
						f.rumahCell,  g.is_acc
						from adis_smb_form a
						INNER JOIN adis_periode_master c ON LEFT(a.bukaSmb,8) = c.kode
						left JOIN adis_prodi d ON RIGHT(a.bukaSmb, 4) = d.kode
						left JOIN adis_jalur_smb e ON SUBSTR(a.bukaSmb, 12, 2) = e.kode
						left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
						left JOIN smart_jadwal_test g ON a.kode = g.kode
						$where and g.is_acc = 1
						ORDER BY a.updateTime ASC
					";

			break;
			case 'tesTPA':
				$where = " where 1=1 ";
				$method = $this->input->post('method');
				if(empty($method)){
					$periodeAka = "SELECT A.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '3' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and SUBSTR(a.bukaSmb, 1, 10) = '".$periodeAka['idPeriode']."' ";

				}else{
					if($method == "search"){
						$term = $this->input->post('nama_cm');
						$where .= " and concat(f.nama,a.nomor,a.kode) like '%$term%' and d.jenjangType = 2 ";
					}
					if ($method == 'select'){
						$prodi = $this->input->post('prodi');
						$gelombang = $this->input->post('gelombang');
						$whereGel = "";
						$whereProdi = "";

						if($gelombang != 'all'){
							$whereGel = " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
						}

						if ($prodi != 'all'){
							$whereProdi = " AND substr(f.bukaSmb, -4) = '$prodi' ";
						}



						$where = " WHERE $whereGel $whereProdi";
					}
				}
				$sql = "select a.nomor, a.kode, d.singkatan, f.nama,
					f.rumahCell,
					case when g.is_acc = 1 then g.metode_acc else g.metode_purpose end metode,
					case when g.is_acc = 1 then g.tanggal_acc else g.tanggal_purpose end tanggal,
					case when g.is_acc = 1 then g.jam_acc else g.jam_purpose end jam,
					g.metode_purpose, g.tanggal_purpose,g.jam_purpose, g.metode_acc, g.tanggal_acc, g.jam_acc, g.is_acc,
					g.jam_end_purpose, g.jam_end_acc
					from adis_smb_form a
					INNER JOIN adis_periode_master c ON LEFT(a.bukaSmb,8) = c.kode
					left JOIN adis_prodi d ON RIGHT(a.bukaSmb, 4) = d.kode
					left JOIN adis_jalur_smb e ON SUBSTR(a.bukaSmb, 12, 2) = e.kode
					left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
					left JOIN smart_jadwal_tpa g ON a.kode = g.kode
					$where and a.stsApplyPaidConfirm = 1
					ORDER BY a.updateTime ASC
				";
			break;
			case 'tesWawancara':
				$where = " where 1=1 ";
				$method = $this->input->post('method');
				if(empty($method)){
					$periodeAka = "SELECT A.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '2' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and SUBSTR(a.bukaSmb, 1, 10) = '".$periodeAka['idPeriode']."' ";

				}else{
					if($method == "search"){
						$term = $this->input->post('nama_cm');
						$where .= " and concat(f.nama,a.nomor,a.kode) like '%$term%' and d.jenjangType = 2 ";
					}
					if ($method == 'select'){
						$prodi = $this->input->post('prodi');
						$gelombang = $this->input->post('gelombang');
						$whereGel = "";
						$whereProdi = "";

						if($gelombang != 'all'){
							$whereGel = " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
						}

						if ($prodi != 'all'){
							$whereProdi = " AND substr(f.bukaSmb, -4) = '$prodi' ";
						}



						$where = " WHERE $whereGel $whereProdi";
					}
				}
				$sql = "select a.nomor, a.kode, d.singkatan, f.nama,
					f.rumahCell,
					case when g.is_acc = 1 then g.metode_acc else g.metode_purpose end metode,
					case when g.is_acc = 1 then g.tanggal_acc else g.tanggal_purpose end tanggal,
					case when g.is_acc = 1 then g.jam_acc else g.jam_purpose end jam,
					g.metode_purpose, g.tanggal_purpose,g.jam_purpose, g.metode_acc, g.tanggal_acc, g.jam_acc, g.is_acc,
					g.fc_ijazah, g.fc_transkrip
					from adis_smb_form a
					INNER JOIN adis_periode_master c ON LEFT(a.bukaSmb,8) = c.kode
					left JOIN adis_prodi d ON RIGHT(a.bukaSmb, 4) = d.kode
					left JOIN adis_jalur_smb e ON SUBSTR(a.bukaSmb, 12, 2) = e.kode
					left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
					left JOIN smart_jadwal_test g ON a.kode = g.kode
					$where and a.stsApplyPaidConfirm = 1
					ORDER BY a.updateTime ASC
				";
			break;
			case 'payment_reg_kk':
				$where = " where 1=1 ";
				$method = $this->input->post('method');
				if(empty($method)){
					$periodeAka = "SELECT A.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '3' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and SUBSTR(a.bukaSmb, 1, 10) = '".$periodeAka['idPeriode']."' ";

				}else{
					if($method == "search"){
						$term = $this->input->post('nama_cm');
						$where .= " and concat(f.nama,a.nomor,a.kode) like '%$term%' and d.jenjangType = 3 ";
					}
					if ($method == 'select'){
						$prodi = $this->input->post('prodi');
						$gelombang = $this->input->post('gelombang');
						$whereGel = "";
						$whereProdi = "";

						if($gelombang != 'all'){
							$whereGel = " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
						}

						if ($prodi != 'all'){
							$whereProdi = " AND substr(f.bukaSmb, -4) = '$prodi' ";
						}



						$where = " WHERE $whereGel $whereProdi";
					}
				}
				$sql = "select a.nomor, a.kode, d.singkatan, f.nama, g.verifikasi_forlap,
					f.rumahCell, a.stsApplyPaid, a.stsApplyPaidConfirm, a.applyBankTransferAmount, b.buktiBayarPendaftaran
					from adis_smb_form a
					left JOIN adis_smb_usr_keu b ON a.kode = b.smbUsr
					INNER JOIN adis_periode_master c ON LEFT(a.bukaSmb,8) = c.kode
					left JOIN adis_prodi d ON RIGHT(a.bukaSmb, 4) = d.kode
					left JOIN adis_jalur_smb e ON SUBSTR(a.bukaSmb, 12, 2) = e.kode
					left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
					left JOIN adis_smb_usr_edu g ON a.kode = g.smbUsr
					$where and g.verifikasi_forlap = 1 -- and a.stsApplyPaidConfirm = 0
					ORDER BY a.updateTime, a.stsApplyPaidConfirm ASC
				";


			break;
			case 'payment_reg':
				$where = " where 1=1 ";
				$method = $this->input->post('method');
				if(empty($method)){
					$periodeAka = "SELECT A.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '2' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and SUBSTR(a.bukaSmb, 1, 10) = '".$periodeAka['idPeriode']."' ";

				}else{
					if($method == "search"){
						$term = $this->input->post('nama_cm');
						$where .= " and concat(f.nama,a.nomor,a.kode) like '%$term%' and d.jenjangType = 2 ";
					}
					if ($method == 'select'){
						$prodi = $this->input->post('prodi');
						$gelombang = $this->input->post('gelombang');
						$whereGel = "";
						$whereProdi = "";

						if($gelombang != 'all'){
							$whereGel = " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
						}

						if ($prodi != 'all'){
							$whereProdi = " AND substr(f.bukaSmb, -4) = '$prodi' ";
						}



						$where = " WHERE $whereGel $whereProdi";
					}
				}
				$sql = "select a.nomor, a.kode, d.singkatan, f.nama, g.verifikasi_forlap,
					f.rumahCell, a.stsApplyPaid, a.stsApplyPaidConfirm, a.applyBankTransferAmount, b.buktiBayarPendaftaran
					from adis_smb_form a
					left JOIN adis_smb_usr_keu b ON a.kode = b.smbUsr
					INNER JOIN adis_periode_master c ON LEFT(a.bukaSmb,8) = c.kode
					left JOIN adis_prodi d ON RIGHT(a.bukaSmb, 4) = d.kode
					left JOIN adis_jalur_smb e ON SUBSTR(a.bukaSmb, 12, 2) = e.kode
					left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
					left JOIN adis_smb_usr_edu g ON a.kode = g.smbUsr
					$where and g.verifikasi_forlap = 1 -- and a.stsApplyPaidConfirm = 0
					ORDER BY a.updateTime, a.stsApplyPaidConfirm ASC
				";


			break;
			case 'ver_dok':
				$where = " where 1=1 ";
				$method = $this->input->post('method');
				if(empty($method)){
					$periodeAka = "SELECT A.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '2' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where .= " and SUBSTR(a.bukaSmb, 1, 10) = '".$periodeAka['idPeriode']."' ";

				}else{
					if($method == "search"){
						$term = $this->input->post('nama_cm');
						$where .= " and concat(f.nama,a.nomor,a.kode) like '%$term%' and d.jenjangType = 2 ";
					}
					if ($method == 'select'){
						$prodi = $this->input->post('prodi');
						$gelombang = $this->input->post('gelombang');
						$whereGel = "";
						$whereProdi = "";

						if($gelombang != 'all'){
							$whereGel = " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
						}

						if ($prodi != 'all'){
							$whereProdi = " AND substr(f.bukaSmb, -4) = '$prodi' ";
						}



						$where = " WHERE $whereGel $whereProdi";
					}
				}
				$sql = "select a.nomor, a.kode, d.singkatan, f.nama, g.tahunLulus, g.jurusan, g.lulusAsal, g.verifikasi_forlap,
					f.rumahCell
					from adis_smb_form a
					-- INNER JOIN adis_smb_usr b ON a.kode = b.kode
					INNER JOIN adis_periode_master c ON LEFT(a.bukaSmb,8) = c.kode
					left JOIN adis_prodi d ON RIGHT(a.bukaSmb, 4) = d.kode
					left JOIN adis_jalur_smb e ON SUBSTR(a.bukaSmb, 12, 2) = e.kode
					left JOIN adis_smb_usr_pribadi f ON a.kode = f.kode
					left JOIN adis_smb_usr_edu g ON a.kode = g.smbUsr
					$where -- and g.verifikasi_forlap != 1
					ORDER BY a.createTime DESC, g.verifikasi_forlap ASC
				";

			break;
			case 'telemarketing_kk':
			case 'telemarketing':
				$method = $this->input->post('method');
				if ($method == 'search'){
					$srcNama = $this->input->post('nama_cm');
					if ($srcNama){
						$where .= " and up.nama LIKE '%$srcNama%' OR u.username LIKE '%$srcNama%' OR  f.kode LIKE '%$srcNama%' OR f.nomor LIKE '%$srcNama%'";
					}
				}
				if ($method == 'select'){
					$periode = $this->input->post('periode');
					$this->smarty->assign('periode', $periode);
					$prodi = $this->input->post('prodi');
					$jalur = $this->input->post('jalur');
					$gelombang = $this->input->post('gelombang');
					$wherePeriode = "";
					$whereGel = "";
					$whereJalur = "";
					$whereProdi = "";

					if ($jalur != 'all'){
						$wherePeriode = " AND SUBSTR(f.bukaSmb, 1, 8) = '$periode' ";
						$wherePeriode .= " AND u.jalur_penerimaan = '$jalur' ";
					}else{
						$wherePeriode = " AND SUBSTR(f.bukaSmb, 1, 8) = '$periode' ";
					}

					if($gelombang != 'all'){
						$whereGel = " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
					}

					if ($prodi != 'all'){
						$whereProdi = " AND substr(f.bukaSmb, -4) = '$prodi' ";
					}



					$where .= " $wherePeriode $whereGel $whereProdi";
				}else{

					if($p1){
						$where .= " AND SUBSTR(f.bukaSmb, 1, 8) = '$p1' ";
					}
					if($p2){
						$where .= " AND SUBSTR(f.bukaSmb, 1, 10) = '$p2' ";
					}
				}

				// $where .= empty($p1) ? '': " and "

				$sql ="SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, up.nama as nama_cm, up.statusSaudara, p.singkatan as progdi, j.nama as n_jalur, f.`event` as event, up.adaSaudara, up.confirmSaudara,
					f.stsEventInterviewPresent as hadirWwc, f.stsEventUsmPresent hadirUsm, up.rumahCell,
					f.resultUsm as hasilUsm, f.resultInterview as hasilWwc, f.resultPept, f.stsResultGrade as hasilAkhir,
					f.stsResultPass as lulusTidak, f.stsResultRecommended as recomended, f.stsResultKet as ket, f.stsResultConfirm as konfirm, j.jenjang, u.jalur_penerimaan,
					T.id, T.followup, T.edu, odsK.followup as ods, T.date_updated as tele_date_update,
					if(f.applyBankTransferType = '', '' , if(f.applyBankTransferType = '04.5', 'Voucher', 'Transfer')) as 'status_pay', f.createTime as tglDaftar,
					'' as cgts
					FROM adis_smb_form f
						JOIN adis_smb_usr u ON u.kode = f.kode
						JOIN adis_periode_master c ON LEFT(f.bukaSmb,8) = c.kode
						JOIN adis_prodi p ON RIGHT(f.bukaSmb, 4) = p.kode
						JOIN adis_jalur_smb j ON SUBSTR(f.bukaSmb, 12, 2) = j.kode
						JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
						LEFT JOIN smart_telemarketing T on f.kode = T.kode
						-- LEFT JOIN (select id, kode, ods, date_updated, followup, edu from smart_telemarketing ) as  T on f.kode = T.kode
						LEFT JOIN idx_followup odsK ON T.ods = odsK.id
					where 1=1 $where
					ORDER BY f.createTime DESC, T.date_updated DESC";

				// echo $sql;exit;
			break;
			case 'invent_kk':
				$method = $this->input->post('method');
				if ($method == 'search'){
					$srcNama = $this->input->post('nama_cm');
					if ($srcNama){
						$where = " and concat(A.nama_lengkap ,A.email, A.asal) LIKE '%$srcNama%' ";
					}
				}

					$sql = "select A.*, C.nama as prodi, if(B.kode IS NULL, 'Not Register', 'Registered') as sts_daftar, date_format(A.tanggal_daftar, '%d-%m-%Y') as createTime,
					D.status as status_contact, date_format(A.voucher_expired, '%d-%m-%Y') as voucher_expired,
					hp,sumber,edu, datediff(now(), send_date) as count_day, E.fullname
					from tbl_invent_kk A
					LEFT JOIN adis_smb_form B ON A.email = B.kode
					LEFT JOIN adis_prodi C ON RIGHT(B.bukaSmb,4) = C.kode
					left join idx_status_invent D on A.status = D.id
					left join adis_sys_usr E on A.edu = E.username
					where 1=1 $where
					order by id DESC";
					// echo $sql;exit;

			break;
			case 'invent_mm':
				$method = $this->input->post('method');
				if ($method == 'search'){
					$srcNama = $this->input->post('nama_cm');
					if ($srcNama){
						$where = " and concat(A.nama_lengkap ,A.email, A.nama_sekolah) LIKE '%$srcNama%' ";
					}
				}

					$sql = "select A.*, C.nama as prodi, if(B.kode IS NULL, 'Not Register', 'Registered') as sts_daftar, date_format(A.tanggal_daftar, '%d-%m-%Y') as createTime,
					D.status as status_contact, date_format(A.voucher_expired, '%d-%m-%Y') as voucher_expired,
					hp,sumber,edu, datediff(now(), send_date) as count_day, E.fullname
					from tbl_invent_mm A
					LEFT JOIN adis_smb_form B ON A.email = B.kode
					LEFT JOIN adis_prodi C ON RIGHT(B.bukaSmb,4) = C.kode
					left join idx_status_invent D on A.status = D.id
					inner join adis_sys_usr E on A.edu = E.username
					where 1=1 $where
					order by id DESC";
					// echo $sql;exit;

			break;
			case 'invent':
				$method = $this->input->post('method');
				if ($method == 'search'){
					$srcNama = $this->input->post('nama_cm');
					if ($srcNama){
						$where = " and concat(A.nama_lengkap ,A.email, A.nama_sekolah) LIKE '%$srcNama%' ";
					}
				}

					$sql = "select A.*, C.nama as prodi, if(B.kode IS NULL, 'Not Register', 'Registered') as sts_daftar, date_format(A.tanggal_daftar, '%d-%m-%Y') as createTime,
					D.status as status_contact, date_format(A.voucher_expired, '%d-%m-%Y') as voucher_expired,
					hp,sumber,edu, datediff(now(), send_date) as count_day
					from tbl_invent A
					LEFT JOIN adis_smb_form B ON A.email = B.kode
					LEFT JOIN adis_prodi C ON RIGHT(B.bukaSmb,4) = C.kode
					left join idx_status_invent D on A.status = D.id
					where 1=1 $where
					order by id DESC";
					// echo $sql;exit;

			break;
			case 'cmb' :
				$method = $this->input->post('method');
				if ($method == 'search'){
					$srcNama = $this->input->post('nama_cm');
					if ($srcNama){
						$where = " WHERE up.nama LIKE '%$srcNama%' OR u.username LIKE '%$srcNama%' OR  f.kode LIKE '%$srcNama%' OR f.nomor LIKE '%$srcNama%'";
					}
				}
				if ($method == 'select'){
					$periode = $this->input->post('periode');
					$this->smarty->assign('periode', $periode);
					$prodi = $this->input->post('prodi');
					$jalur = $this->input->post('jalur');
					$gelombang = $this->input->post('gelombang');
					$wherePeriode = "";
					$whereGel = "";
					$whereJalur = "";
					$whereProdi = "";

					if ($jalur != 'all'){
						$wherePeriode = " SUBSTR(f.bukaSmb, 1, 8) = '$periode' ";
						$wherePeriode .= " AND u.jalur_penerimaan = '$jalur' ";
					}else{
						$wherePeriode = " SUBSTR(f.bukaSmb, 1, 8) = '$periode' ";
					}

					if($gelombang != 'all'){
						$whereGel = " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
					}

					if ($prodi != 'all'){
						$whereProdi = " AND substr(f.bukaSmb, -4) = '$prodi' ";
					}



					$where = " WHERE $wherePeriode $whereGel $whereProdi";
				}

				if(empty($method)){
					$periodeAka = "SELECT A.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '1' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where = " WHERE SUBSTR(f.bukaSmb, 1, 10) = '".$periodeAka['idPeriode']."' ";

				}

				$sql = "SELECT '' AS 'No', f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, up.nama as nama_cm , p.nama as progdi,
					DAYNAME(u.createTime) as hari, DATE(u.createTime) as tanggal, p.singkatan as progdi_inisial,
					f.stsApplyPaid, f.stsApplyPaidConfirm, up.stsPribadiConfirm, f.stsEventConfirm, f.stsEventUsmPresent, up.foto,
					f.stsResultConfirm, f.stsMundurBeforeReapply,
					f.stsReapplyPaid, stsReapplyPaidConfirm, f.stsMundurAfterReapply, j.nama as n_jalur, u.jalur_penerimaan,
					concat(
								u.validation_status, '-' ,  IFNULL(e.upload_rapor,0), '-',  f.stsApplyPaid,
								'-' , f.stsEventConfirm,  '-' ,  f.stsResultConfirm, '-' , f.stsResultConfirm,
								'-', f.stsReapplyPaid, '-' ,  up.statusKeluarga, '-' , IFNULL(ber.is_done, 0)
							) as status
					FROM adis_smb_form f
					INNER JOIN adis_smb_usr u ON f.kode = u.kode
					INNER JOIN adis_smb_usr_edu e ON f.kode = e.smbUsr
					INNER JOIN adis_periode_master c ON LEFT(f.bukaSmb,8) = c.kode
					INNER JOIN adis_smb_usr_pribadi up ON f.kode = up.kode
					INNER JOIN adis_prodi p ON RIGHT(f.bukaSmb, 4) = p.kode
					LEFT JOIN adis_jalur_smb j ON SUBSTR(f.bukaSmb, 12, 2) = j.kode
					LEFT JOIN tbl_smb_berkas ber ON f.kode = ber.kode
					$where
					ORDER BY u.createTime DESC";

					// echo $sql;exit;

			break;
			case 'cmbs2' :
				$method = $this->input->post('method');
				if ($method == 'search'){
					$srcNama = $this->input->post('nama_cm');
					if ($srcNama){
						$where = " WHERE up.nama LIKE '%$srcNama%' OR u.username LIKE '%$srcNama%' OR  f.kode LIKE '%$srcNama%' OR f.nomor LIKE '%$srcNama%'";
					}
				}
				if ($method == 'select'){
					$periode = $this->input->post('periode');
					$this->smarty->assign('periode', $periode);
					$prodi = $this->input->post('prodi');
					$jalur = $this->input->post('jalur');
					$gelombang = $this->input->post('gelombang');
					$wherePeriode = "";
					$whereGel = "";
					$whereJalur = "";
					$whereProdi = "";

					if ($jalur != 'all'){
						$wherePeriode = " SUBSTR(f.bukaSmb, 1, 8) = '$periode' ";
						$wherePeriode .= " AND u.jalur_penerimaan = '$jalur' ";
					}else{
						$wherePeriode = " SUBSTR(f.bukaSmb, 1, 8) = '$periode' ";
					}

					if($gelombang != 'all'){
						$whereGel = " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
					}

					if ($prodi != 'all'){
						$whereProdi = " AND substr(f.bukaSmb, -4) = '$prodi' ";
					}



					$where = " WHERE $wherePeriode $whereGel $whereProdi";
				}

				if(empty($method)){
					$periodeAka = "SELECT A.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '2' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where = " WHERE SUBSTR(f.bukaSmb, 1, 10) = '".$periodeAka['idPeriode']."' ";

				}

				$sql = "SELECT '' AS 'No', f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, up.nama as nama_cm , p.nama as progdi,
					DAYNAME(u.createTime) as hari, DATE(u.createTime) as tanggal, p.singkatan as progdi_inisial,
					f.stsApplyPaid, f.stsApplyPaidConfirm, up.stsPribadiConfirm, f.stsEventConfirm, f.stsEventUsmPresent, up.foto,
					f.stsResultConfirm, f.stsMundurBeforeReapply,
					f.stsReapplyPaid, stsReapplyPaidConfirm, f.stsMundurAfterReapply, j.nama as n_jalur, u.jalur_penerimaan,
					concat(
								u.validation_status, '-' ,  IFNULL(e.verifikasi_forlap,0), '-',  f.stsApplyPaid,
								'-' , f.stsEventConfirm,  '-' ,  f.stsResultConfirm, '-' , f.stsResultConfirm,
								'-', f.stsReapplyPaid, '-' ,  up.statusKeluarga, '-' , IFNULL(ber.is_done, 0)
							) as status
					FROM adis_smb_form f
					INNER JOIN adis_smb_usr u ON f.kode = u.kode
					INNER JOIN adis_smb_usr_edu e ON f.kode = e.smbUsr
					INNER JOIN adis_periode_master c ON LEFT(f.bukaSmb,8) = c.kode
					INNER JOIN adis_smb_usr_pribadi up ON f.kode = up.kode
					INNER JOIN adis_prodi p ON RIGHT(f.bukaSmb, 4) = p.kode
					LEFT JOIN adis_jalur_smb j ON SUBSTR(f.bukaSmb, 12, 2) = j.kode
					LEFT JOIN tbl_smb_berkas ber ON f.kode = ber.kode
					$where
					ORDER BY u.createTime DESC";

					// echo $sql;exit;

			break;
			case 'cmbkk' :
				$method = $this->input->post('method');
				if ($method == 'search'){
					$srcNama = $this->input->post('nama_cm');
					if ($srcNama){
						$where = " WHERE up.nama LIKE '%$srcNama%' OR u.username LIKE '%$srcNama%' OR  f.kode LIKE '%$srcNama%' OR f.nomor LIKE '%$srcNama%'";
					}
				}
				if ($method == 'select'){
					$periode = $this->input->post('periode');
					$this->smarty->assign('periode', $periode);
					$prodi = $this->input->post('prodi');
					$jalur = $this->input->post('jalur');
					$gelombang = $this->input->post('gelombang');
					$wherePeriode = "";
					$whereGel = "";
					$whereJalur = "";
					$whereProdi = "";

					if ($jalur != 'all'){
						$wherePeriode = " SUBSTR(f.bukaSmb, 1, 8) = '$periode' ";
						$wherePeriode .= " AND u.jalur_penerimaan = '$jalur' ";
					}else{
						$wherePeriode = " SUBSTR(f.bukaSmb, 1, 8) = '$periode' ";
					}

					if($gelombang != 'all'){
						$whereGel = " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' ";
					}

					if ($prodi != 'all'){
						$whereProdi = " AND substr(f.bukaSmb, -4) = '$prodi' ";
					}



					$where = " WHERE $wherePeriode $whereGel $whereProdi";
				}

				if(empty($method)){
					$periodeAka = "SELECT A.kode as idPeriode FROM adis_periode A
							LEFT JOIN  adis_periode_master B ON B.kode = A.idPeriodeMaster
							WHERE A.erased = 0 AND B.jenjangType = '3' AND A.status = 1 ORDER BY A.kode DESC";
					$periodeAka = $this->db2->query($periodeAka)->row_array();
					$where = " WHERE SUBSTR(f.bukaSmb, 1, 10) = '".$periodeAka['idPeriode']."' ";

				}

				$sql = "SELECT '' AS 'No', f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, up.nama as nama_cm , p.nama as progdi,
					DAYNAME(u.createTime) as hari, DATE(u.createTime) as tanggal, p.singkatan as progdi_inisial,
					f.stsApplyPaid, f.stsApplyPaidConfirm, up.stsPribadiConfirm, f.stsEventConfirm, f.stsEventUsmPresent, up.foto,
					f.stsResultConfirm, f.stsMundurBeforeReapply,
					f.stsReapplyPaid, stsReapplyPaidConfirm, f.stsMundurAfterReapply, j.nama as n_jalur, u.jalur_penerimaan,
					concat(
								u.validation_status, '-' ,  IFNULL(e.verifikasi_forlap,0), '-',  f.stsApplyPaid,
								'-' , f.stsEventConfirm,  '-' ,  f.stsResultConfirm, '-' , f.stsResultConfirm,
								'-', f.stsReapplyPaid, '-' ,  up.statusKeluarga, '-' , IFNULL(ber.is_done, 0)
							) as status
					FROM adis_smb_form f
					left JOIN adis_smb_usr u ON f.kode = u.kode
					left JOIN adis_smb_usr_edu e ON f.kode = e.smbUsr
					left JOIN adis_periode_master c ON LEFT(f.bukaSmb,8) = c.kode
					left JOIN adis_smb_usr_pribadi up ON f.kode = up.kode
					left JOIN adis_prodi p ON RIGHT(f.bukaSmb, 4) = p.kode
					LEFT JOIN adis_jalur_smb j ON SUBSTR(f.bukaSmb, 12, 2) = j.kode
					LEFT JOIN tbl_smb_berkas ber ON f.kode = ber.kode
					$where
					ORDER BY u.createTime DESC";

					// echo $sql;exit;

			break;
		}

		return $this->lib->jsondata($sql, $type);
	}

	function simpandata($table,$data,$sts_crud){ //$sts_crud --> STATUS NYEE INSERT, UPDATE, DELETE

		$this->load->library('lib');
		$this->db->trans_begin();
		$array_where = array();
		$id_table = "id";

		if(isset($data['id'])){
			$id = $data['id'];
			unset($data['id']);
		}

		if($sts_crud == "add"){
			unset($data['id']);
        }

		$action_tbl = $table;
        $data_temp = array();
        switch($table){
			case 'mundur_mhs':
				$table = 'adis_resign_smb';
				$data['createTime'] = date("Y-m-d H:i:s");
				$id_table = 'kode';
				$data['smbUsr'] = $id;
				$data['createUser'] = $this->auth['name'];

				$this->db2->where("kode", $id);
				$this->db2->update("adis_smb_form", array(
						"stsMundurAfterReapply"=>1
					));

				$jumlahBayar = $data["jumlahBayar"];
				$jumlahBayar = str_replace('.', '', $jumlahBayar);
				$jumlahBayar = str_replace(' ', '', $jumlahBayar);

				$data['amountRegistration'] = $jumlahBayar;

				unset($data['jumlahBayar']);
			break;
			case 'smart_menu_privilage_add':

				$table = 'smart_menu_privilage';
				$menuDb = $this->db->get_where('smart_menu_privilage', array('level_user'=>$data['level_user']))->row_array();
				if($menuDb){
					$sts_crud == "edit";
					$menuDbPriv = json_decode($menuDb['privilage'],true);
					foreach($menuDbPriv as $val){
						$tmpSubMenu = '';
						$tmpParent = '';

						if($val != $data['parent_menu']){
							$tmpParent = $data['parent_menu'];
						}
						if(!empty($data['sub_menu'])){
							if($val != $data['sub_menu']){
								$tmpSubMenu = $data['sub_menu'];
							}
						}

						if(!in_array($tmpSubMenu, $menuDbPriv)){
							if(!empty($tmpSubMenu)){
								$menuDbPriv[] = $tmpSubMenu;
							}
						}
						if(!in_array($tmpParent, $menuDbPriv)){
							if(!empty($tmpParent)){
								$menuDbPriv[] = $tmpParent;
							}
						}
					}
					$data['privilage'] = json_encode($menuDbPriv);

				}else{
					$sts_crud == "add";
					$menuList[] = $data['parent_menu'];
					if(!empty($data['sub_menu'])){
						$menuList[] = $data['sub_menu'];
					}
					$data['privilage'] = json_encode($menuList);
				}


				unset($data['parent_menu']);
				unset($data['sub_menu']);

				break;
			case 'smart_menu_privilage':
				if($data['action'] == 'rem_modul'){
					$menuDb = $this->db->get_where('smart_menu_privilage', array('id'=>$id))->row_array();
					$menuDbPriv = json_decode($menuDb['privilage'],true);
					$privData = array();
					foreach($menuDbPriv as $val){
						if($val != $data['idMenu']){
							$privData[] = $val;
						}
					}

					$data['privilage'] = empty($privData) ? json_encode($menuDbPriv) : json_encode($privData);
					unset($data['idMenu']);
					unset($data['action']);
				}
				break;
			case 'adis_prodi':
				$id_table = 'kode';
				if($sts_crud == 'add'){
					$cekKode = $this->db->get_where($table,array('kode'=>$data['kode']))->num_rows();
					if($cekKode > 0){
						return 'Kode program studi sudah digunakan!';
					}
				}
				if($sts_crud != 'delete'){
				$data['jenjangType']  = $data['jenjang'] == 1 ? '01.S1' : '02.S2';
				}
				break;
			case 'adis_type':
				$id_table = 'kode';
				if($sts_crud == 'add'){
					$cekKode = $this->db->get_where($table,array('kode'=>$data['kode']))->num_rows();
					if($cekKode > 0){
						return 'Kode agama sudah digunakan!';
					}
				}
				break;
			case 'dataEduCons':
				$table = 'adis_sys_usr';
				$id_table = 'kode';

				$this->load->library('encrypt');
				if($sts_crud == 'add'){

					$data['fullname'] = ucwords($data['fullname']);
					$data['password'] = $this->encrypt->encode($data['password']);
					$check = $this->db->get_where('adis_sys_usr', array('username'=>strtolower($data['username'])));
					if($check->num_rows() > 0){
						die('Username sudah pernah digunakan!');
					}
					$randomChar =substr(str_shuffle("abcdefghijklmnopqrstvwxyz"), 0, 3);
					$data['kode'] = $randomChar.'_'.strtolower($data['username']);
					$data['id_level'] = '44';
					$data['aktif_edu'] = 1;$data['is_reg'] = 1;$data['is_mag'] = 1;$data['is_kk'] = 1;
					$data['createTime']=date('Y-m-d H:i:s');
					$data['createUser']=$this->auth['name'];
				}else if($sts_crud == 'edit'){
					$data['fullname'] = ucwords($data['fullname']);
					$data['password'] = $this->encrypt->encode($data['password']);
					$data['updateTime']=date('Y-m-d H:i:s');
					$data['updateUser']=$this->auth['name'];
				}else if($sts_crud == 'delete'){
					$sts_crud = 'edit';
					$data['erased'] = 1;
					$data['aktif_edu'] = 0;
					$data['updateTime']=date('Y-m-d H:i:s');
					$data['updateUser']=$this->auth['name'];
				}
				break;
			case 'adis_sys_usr':
				$id_table = 'kode';
				$data['updateTime']=date('Y-m-d H:i:s');
				$data['updateUser']=$this->auth['name'];
				break;
			case 'smart_telemarketing':
				$table = 'smart_telemarketing';
				$id_table = 'kode';
				break;
			case 'approval_dukk':
				if($data['approval'] == '0'){

					$this->db->where('smbUsr', $id);
					$this->db->update('adis_smb_usr_keu', array(
						'tolakDU'=>1,
						'metodBayarDaftarUlang'=>'',
						'skema_id'=>'',
						'pesanTolakDU'=>$data['pesan_tolak']
					));

					$data['stsReapplyPaid'] = 0;
					$id_table = "kode";
					$table = "adis_smb_form";

					// $this->sendEmailMultiple('tolakPaymentFormulir', $id, $data['pesan_tolak']);

				}elseif($data['approval'] == '1'){

					$data['stsReapplyPaidConfirm'] = 1;
					$data['updateUser']=$this->auth['name'];
					$data['updateTime']=date('Y-m-d H:i:s');
					$id_table = "kode";
					$table = "adis_smb_form";

					$this->generateNimKK($id);

					// $this->sendEmailMultiple('confirmPaymentFormulir', $id, $data['pesan_tolak']);

				}

				unset($data['approval']);
				unset($data['pesan_tolak']);
			break;
			case 'approval_du':
				if($data['approval'] == '0'){

					$this->db->where('smbUsr', $id);
					$this->db->update('adis_smb_usr_keu', array(
						'tolakDU'=>1,
						'metodBayarDaftarUlang'=>'',
						'skema_id'=>'',
						'pesanTolakDU'=>$data['pesan_tolak']
					));

					$data['stsReapplyPaid'] = 0;
					$id_table = "kode";
					$table = "adis_smb_form";

					// $this->sendEmailMultiple('tolakPaymentFormulir', $id, $data['pesan_tolak']);

				}elseif($data['approval'] == '1'){

					$data['stsReapplyPaidConfirm'] = 1;
					$data['updateUser']=$this->auth['name'];
					$data['updateTime']=date('Y-m-d H:i:s');
					$id_table = "kode";
					$table = "adis_smb_form";

					$this->generateNimMagister($id);

					// $this->sendEmailMultiple('confirmPaymentFormulir', $id, $data['pesan_tolak']);

				}

				unset($data['approval']);
				unset($data['pesan_tolak']);
			break;
			case 'set_hasil_seleksi':
				$table = 'adis_smb_form';
				$id_table = 'kode';

				$this->db2->where('smbUsr', $id);
				$this->db2->update('adis_smb_usr_keu', array(
					'kode_potongan'=>$data['kode_potongan']
				));

				$data['stsResultConfirm'] = 1;
				$data['reapplyMaxDate'] = date('Y-m-d', strtotime(date('Y-m-d'). ' + 7 days'));

				unset($data['kode_potongan']);

			break;
			case 'smart_ref_beasiswa':
				if($sts_crud == 'edit'){
					unset($data['kode']);
				}else if($sts_crud == 'add'){
					$ckDb = $this->db->get_where($table, array('kode'=>$data['kode']))->num_rows();
					if($ckDb > 0){
						die('Kode sudah digunakan di database!');
					}
				}

			break;
			case 'simpanSkemaBea':
				$table = 'smart_skema_biaya_beasiswa';
				if($sts_crud == 'edit'){
					unset($data['skema_normal_id']);
				}else{
					$ckDb = $this->db->get_where($table, array('skema_normal_id'=>$data['skema_normal_id']))->num_rows();
					if($ckDb > 0){
						die('Setting skema beasiswa sudah tersedia di database!');
					}
				}

				$data['total'] = preg_replace( '/[^0-9,"."]/', '', $data['total'] );
				$data['tagihan_1'] = preg_replace( '/[^0-9,"."]/', '', $data['tagihan_1'] );
				$data['tagihan_angsuran'] = preg_replace( '/[^0-9,"."]/', '', $data['tagihan_angsuran'] );
				$data['potongan'] = preg_replace( '/[^0-9,"."]/', '', $data['potongan'] );

			break;
			case 'simpanSkemaNormal':
				$id_table = 'id';
				$table = 'smart_skema_biaya_normal';
				if($sts_crud == 'edit'){
					unset($data['pembayaran_kode']);
					unset($data['ref_skema_id']);
				}else{
					$ckDb = $this->db->get_where($table, array('pembayaran_kode'=>$data['pembayaran_kode'], 'ref_skema_id'=>$data['ref_skema_id']))->num_rows();
					if($ckDb > 0){
						die('Setting tarif dengan periode dan prodi sudah tersedia di database!');
					}
				}

				$data['tagihan_pertama'] = preg_replace( '/[^0-9,"."]/', '', $data['tagihan_pertama'] );
				$data['tagihan_angsuran'] = preg_replace( '/[^0-9,"."]/', '', $data['tagihan_angsuran'] );
				$data['potongan'] = preg_replace( '/[^0-9,"."]/', '', $data['potongan'] );

			break;
			case 'simpanTarifKK':
				$id_table = "kode";
				if($sts_crud == 'add'){
					$data['kode'] = implode (".",array($data['periode_kode'], $data['prodi'],'1'));
					$ckDb = $this->db->get_where('adis_pembayaran_kk', array('kode'=>$data['kode']))->num_rows();
					if($ckDb > 0){
						die('Setting tarif dengan periode dan prodi sudah tersedia di database!');
					}
				}else if($sts_crud == 'edit'){
					unset($data['periode_kode']);
					unset($data['prodi']);
				}

				$data['almamater'] = preg_replace( '/[^0-9,"."]/', '', $data['almamater'] );
				$data['registrasi'] = preg_replace( '/[^0-9,"."]/', '', $data['registrasi'] );
				$data['uangPangkal'] = preg_replace( '/[^0-9,"."]/', '', $data['uangPangkal'] );
				$data['matrikulasi'] = preg_replace( '/[^0-9,"."]/', '', $data['matrikulasi'] );
				$data['bpp'] = preg_replace( '/[^0-9,"."]/', '', $data['bpp'] );
				$data['sks'] = preg_replace( '/[^0-9,"."]/', '', $data['sks'] );
				$data['formulir'] = preg_replace( '/[^0-9,"."]/', '', $data['formulir'] );

				$data['totalBiaya'] = $data['almamater']+$data['registrasi']+$data['uangPangkal']+$data['matrikulasi']+$data['bpp']+$data['sks']+$data['formulir'];

				$table = "adis_pembayaran_kk";
			break;
			case 'simpanTarifMagister':
				$id_table = "kode";
				if($sts_crud == 'add'){
					$data['kode'] = implode (".",array($data['kodePeriode'], $data['prodi'],'1'));
					$ckDb = $this->db->get_where('adis_pembayaran_s2', array('kode'=>$data['kode']))->num_rows();
					if($ckDb > 0){
						die('Setting tarif dengan periode dan prodi sudah tersedia di database!');
					}
				}else if($sts_crud == 'edit'){
					unset($data['kodePeriode']);
					unset($data['prodi']);
				}
				$data['biayaRegistrasi'] = preg_replace( '/[^0-9,"."]/', '', $data['biayaRegistrasi'] );
				$data['uangMasuk'] = preg_replace( '/[^0-9,"."]/', '', $data['uangMasuk'] );
				$data['biayaSpp'] = preg_replace( '/[^0-9,"."]/', '', $data['biayaSpp'] );
				$data['biayaSks'] = preg_replace( '/[^0-9,"."]/', '', $data['biayaSks'] );
				$data['biayaAlmamater'] = preg_replace( '/[^0-9,"."]/', '', $data['biayaAlmamater'] );
				$data['biayaTesis'] = preg_replace( '/[^0-9,"."]/', '', $data['biayaTesis'] );

				$data['totalBiaya'] = $data['biayaRegistrasi']+$data['uangMasuk']+$data['biayaSpp']+$data['biayaSks']+$data['biayaAlmamater']+$data['biayaTesis'];

				$table = "adis_pembayaran_s2";
			break;
			case 'approval_jadwal':
				$id_table = "kode";
				$data['is_acc'] = 1;
				$table = "smart_jadwal_test";
			break;
			case 'approval_jadwal_tpa':
				$id_table = "kode";
				$data['is_acc'] = 1;
				$table = "smart_jadwal_tpa";
			break;
			case 'approval_payment':
				if($data['approval'] == '0'){

					$this->db->where('smbUsr', $id);
					$this->db->update('adis_smb_usr_keu', array(
						'tolakPendaftaran'=>1,
						'metodBayarDaftarUlang'=>'',
						'pesanTolakP'=>$data['pesan_tolak']
					));

					$data['stsApplyPaid'] = 0;
					$id_table = "kode";
					$table = "adis_smb_form";

					// $this->sendEmailMultiple('tolakPaymentFormulir', $id, $data['pesan_tolak']);

				}elseif($data['approval'] == '1'){

					$data['stsApplyPaidConfirm'] = 1;
					$data['updateUser']=$this->auth['name'];
					$data['stsEventConfirm']=1;
					$id_table = "kode";
					$table = "adis_smb_form";

					// $this->sendEmailMultiple('confirmPaymentFormulir', $id, $data['pesan_tolak']);

				}

				unset($data['approval']);
				unset($data['pesan_tolak']);
			break;
			case 'set_biaya_formulir':
				$id_table = "kode";
				$table = "adis_smb_form";
			break;
			case 'ver_forlap':
				$id_table = "smbUsr";
				$table = "adis_smb_usr_edu";
            break;
		}

        switch ($sts_crud){
			case "add":
				$insert = $this->db->insert($table,$data);
                $id = $this->db->insert_id();
			break;
			case "edit":
                $update = $this->db->update($table, $data, array($id_table => $id) );
			break;
            case "delete":
                $this->db->delete($table, array($id_table => $id));
			break;
		}

		if($this->db->trans_status() == false){
			$this->db->trans_rollback();
			return 'gagal';
		}else{
			 return $this->db->trans_commit();
		}

	}

	function generateNimKK($kode){
		$datas = $this->db->query("SELECT right(bukaSmb,4) as kode_prodi, nomor FROM adis_smb_form WHERE kode = '$kode';")->row();
		$val = $datas->nomor;

		$where_prodi = " AND RIGHT(bukaSmb,4)=@_prodi ";

		if($datas->kode_prodi == '1004'){
			$where_prodi = " AND ( RIGHT(bukaSmb,4)=@_prodi or RIGHT(bukaSmb,4)='1008') ";
		}

		$sql_p= "SET @_periode:=(SELECT LEFT(bukaSmb,8) FROM adis_smb_form WHERE nomor = '$val');";
		$sql  = "set @_bukaSmb:=(SELECT bukaSmb FROM adis_smb_form WHERE nomor = '$val');";
		$sql2 = "set @_tahun:=left(@_bukasmb,4);";
		$sql3 = "set @_tahun_yy:=right(@_tahun,2);";
		$sql4 = "set @_jenjang:=1;";
		$sql5 = "set @_periode:=left(@_bukasmb,8);";
		$sql6 = "set @_sesi:=right(left(@_bukasmb,8),1);";
		$sql7 = "set @_prodi:=if(right(@_bukasmb,4)='1008','1004', right(@_bukasmb,4));";
		$sql8 = "set @_nimOrder:=(select max(nimOrder) from adis_smb_form
						where LEFT(bukaSmb,8)=@_periode $where_prodi );";
		// $sql9 = "set @_nim:=concat(@_jenjang,@_tahun_yy,@_sesi,right(@_prodi,2),LPAD((@_nimOrder+1),3,0));";
		$sql9 = "set @_nim:=concat(@_jenjang,@_tahun_yy,@_prodi,LPAD((@_nimOrder+1),3,0));";
		$sql10= "update adis_smb_form set nimOrder = (@_nimOrder+1) where nomor='$val';";

		$this->db2->query($sql_p);
		$this->db2->query($sql);
		$this->db2->query($sql2);
		$this->db2->query($sql3);
		$this->db2->query($sql4);
		$this->db2->query($sql5);
		$this->db2->query($sql6);
		$this->db2->query($sql7);
		$this->db2->query($sql8);
		$this->db2->query($sql9);
		$this->db2->query($sql10);

		$qry = "UPDATE adis_smb_form SET nim  = @_nim WHERE nomor='$val'";
		$this->db2->query($qry);
	}

	function generateNimMagister($kode){
		$datas = $this->db->query("SELECT right(bukaSmb,4) as kode_prodi, nomor FROM adis_smb_form WHERE kode = '$kode';")->row();
		$val = $datas->nomor;

		$where_prodi = " AND RIGHT(bukaSmb,4)=right(@_bukasmb,4) ";

		$sql_p= "SET @_periode:=(SELECT LEFT(bukaSmb,8) FROM adis_smb_form WHERE nomor = '$val');";
		$sql  = "set @_bukaSmb:=(SELECT bukaSmb FROM adis_smb_form WHERE nomor = '$val');";
		$sql2 = "set @_tahun:=left(@_bukasmb,4);";
		$sql3 = "set @_tahun_yy:=right(@_tahun,2);";
		$sql4 = "set @_jenjang:=right(left(@_bukasmb,6),1);";
		$sql5 = "set @_periode:=left(@_bukasmb,8);";
		$sql6 = "set @_sesi:=right(left(@_bukasmb,8),1);";
		$sql7 = "set @_prodi:= right(@_bukasmb,4);";
		$sq71 = "set @_prodi:=if(@_sesi = '2', right(@_bukasmb,4), insert(right(@_bukasmb,4), 3, 1, '0')) ";
		$sql8 = "set @_nimOrder:=(select max(nimOrder) from adis_smb_form
						where LEFT(bukaSmb,8)=@_periode $where_prodi );";
		// $sql9 = "set @_nim:=concat(@_jenjang,@_tahun_yy,@_sesi,right(@_prodi,2),LPAD((@_nimOrder+1),3,0));";
		$sql9 = "set @_nim:=concat(@_jenjang,@_tahun_yy,@_prodi,LPAD((@_nimOrder+1),3,0));";
		$sql10= "update adis_smb_form set nimOrder = (@_nimOrder+1) where nomor='$val';";

		$this->db2->query($sql_p);
		$this->db2->query($sql);
		$this->db2->query($sql2);
		$this->db2->query($sql3);
		$this->db2->query($sql4);
		$this->db2->query($sql5);
		$this->db2->query($sql6);
		$this->db2->query($sql7);
		$this->db2->query($sq71);
		$this->db2->query($sql8);
		$this->db2->query($sql9);
		$this->db2->query($sql10);

		$qry = "UPDATE adis_smb_form SET nim  = @_nim WHERE nomor='$val'";
		$this->db2->query($qry);
	}

	function sendEmailMultiple($type, $email, $p1 = '', $p2 = ''){
		$cmb = $this->db->query("select a.username as nama, b.nomor from adis_smb_usr a inner join adis_smb_form b on a.kode=b.kode
			where kode ='$email' ")->row_array();
		switch($type){
			case 'confirmPaymentFormulir':
				$this->db->where(array('kode_mhs'=>$email, 'nomor_tagihan'=>$cmb['nomor']));
				$this->db->update('tbl_tagihan_cmb', array('status_bayar'=>1));

				$konten = array( 'konten' =>"
					Kepada ".$cmb['nama']."
					<br>
					<p>Bukti bayar Pendaftaran anda telah diterima. Silahkan melanjutkan proses selanjutnya pada aplikasi SMART.
					<br>
					<br>
					<br>
					<br>
					<br>Mohon untuk tidak membalas email ini. Jika ada sanggahan/pertanyaan, silahkan hubungi bagian Admisi Universitas Bakrie
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>Universitas Bakrie, Kawasan Rasuna Epicentrum
					<br>Jl.H.R Rasuna Said Kav C-22, Kuningan Jakarta
					<br>Jakarta 12920 Indonesia
					<br>Office Ph : (021) 526 1448
					<br>E-mail : usm@bakrie.ac.id
					");

				$subject = "Konfirmasi Bukti Bayar Pendaftaran Diterima - ".$cmb['nama']."";
				$this->emailNotifikasi($email, $subject, $konten);
			break;
			case 'tolakPaymentFormulir':
				$konten = array( 'konten' =>"
					Kepada ".$cmb['nama']."
					<br>
					<p>Mohon Maaf, Bukti Bayar Pendaftaran Belum Dapat di Verifikasi,
					<br>Pesan Keuangan : $p1
					<br>
					<br>Silahkan untuk dapat mengirimkan kembali bukti bayar anda.
					<br>
					<br>
					<br>Mohon untuk tidak membalas email ini. Jika ada sanggahan/pertanyaan, silahkan hubungi bagian Admisi Universitas Bakrie
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>Universitas Bakrie, Kawasan Rasuna Epicentrum
					<br>Jl.H.R Rasuna Said Kav C-22, Kuningan Jakarta
					<br>Jakarta 12920 Indonesia
					<br>Office Ph : (021) 526 1448
					<br>E-mail : usm@bakrie.ac.id
				");

				$subject = "Konfirmasi Bukti Bayar Pendaftaran Ditolak - ".$cmb['nama']."";
				$this->emailNotifikasi($email, $subject, $konten);
			break;
		}
	}

	function validasiTagihan($kode , $nomor_tagihan){
		$this->db2->trans_start();
		$this->db2->where(array('kode_mhs'=>$kode, 'nomor_tagihan'=>$nomor_tagihan));
		$this->db2->update('tbl_tagihan_cmb', array('status_bayar'=>1));
		if ($this->db2->trans_status() === FALSE){
			$this->db2->trans_rollback();
			return 0;
		}else{
			$this->db2->trans_commit();
			return 1;
		}
	}

	function modelCalon($periode = ''){

			// $QWhere = '';
			// if ($prodi != 'all'){ $QWhere = " AND p.kode = '$prodi'"; }
			// if ($jalur != 'all'){ $QWhere = " AND j.kode = '$'"; }

			$this->db2->query("SET @num:=0;");
			$sql = "SELECT @num:=@num+1 AS 'No', u.username as 'Nama Lengkap',  f.bukaSmb, f.nomor as 'No Registrasi',
			p.nama as 'Program Studi', up.rumahCell as 'No HP', DATE(u.createTime) as 'Tanggal Daftar', j.nama as 'Jalur Ujian',
			if (f.stsApplyPaid = 1 ,'Sudah', 'Belum') as 'Bayar Pendaftaran',if (f.stsApplyPaidConfirm = 1 ,'Sudah', 'Belum') AS 'Confirm BP',
			if (up.stsPribadiConfirm = 1 ,'Sudah', 'Belum') AS 'Melengkapi Formulir',
			if (f.stsEventConfirm=1 ,'Sudah', 'Belum') AS 'Jadwal Ujian',
			if (f.stsEventUsmPresent= 1 ,'Hadir', 'Belum') AS Kehadiran,
			if (f.stsResultConfirm = 1, 'Sudah', 'Belum') AS 'Hasil Ujian',
			if (f.stsReapplyPaid = 1, 'Sudah', 'Belum') AS 'Pembayaran Daftar Ulang',
			if (stsReapplyPaidConfirm = 1 ,'Sudah', 'Belum') AS 'Konfirm DU',
			if (f.stsMundurAfterReapply = 1, 'Mundur', 'Tidak') AS Mundur, f.kode as 'Email',
			e.nama as 'Nama Sekolah', w.nama as 'Kab/Kota', w2.nama as 'Provinsi'
			FROM adis_smb_form f
			INNER JOIN adis_smb_usr u ON u.kode = f.kode
			INNER JOIN adis_buka_smb b ON b.kode = f.bukaSmb
			INNER JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
			INNER JOIN adis_prodi p ON p.kode = b.prodi
			INNER JOIN adis_jalur_smb j ON j.kode = b.jalur
			INNER JOIN adis_smb_usr_edu e ON e.smbUsr = f.kode
			LEFT JOIN adis_wil w ON w.kode = e.kabKota
			LEFT JOIN adis_wil w2 ON w2.kode = e.prop
			WHERE SUBSTR(f.bukaSmb, 1, 10) = '$periode'
			ORDER BY 'Tanggal Daftar' DESC";


			$sql = $this->db2->query($sql);
			return $sql;

	}

	function exportCsv($periode = '', $prodi = '', $jalur = '', $gelombang = ''){

		$QWhere = '';
		if ($prodi != 'all'){ $QWhere .= " AND p.kode = '$prodi'"; }
		if ($jalur != 'all'){ $QWhere .= " AND u.jalur_penerimaan = '$jalur'"; }
		if ($gelombang != 'all'){ $QWhere .= " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' "; }

	   $this->db2->query("SET @num:=0;");
	   $sql = "SELECT
				   @num :=@num + 1 AS 'No',
				   DATE(u.createTime) AS 'Tanggal Daftar',
				   f.nomor AS 'No Registrasi',
				up.nomorKtp as 'No NIK KTP'
			   FROM
				   adis_smb_form f
	   LEFT JOIN adis_smb_usr u ON u.kode = f.kode
	   LEFT JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
	   LEFT JOIN adis_prodi p ON p.kode = RIGHT(f.bukaSmb, 4)
	   WHERE SUBSTR(f.bukaSmb, 1, 8) = '$periode' $QWhere
	   ORDER BY 'Tanggal Daftar' DESC";

	   // echo $sql;exit;
	   $sql = $this->db2->query($sql);
	   return $sql;

	}


	function exportCalon($periode = '', $prodi = '', $jalur = '', $gelombang = ''){

			 $QWhere = '';
			 if ($prodi != 'all'){ $QWhere .= " AND p.kode = '$prodi'"; }
			 if ($jalur != 'all'){ $QWhere .= " AND u.jalur_penerimaan = '$jalur'"; }
			 if ($gelombang != 'all'){ $QWhere .= " AND SUBSTR(f.bukaSmb, 1, 10) = '$gelombang' "; }

			$this->db2->query("SET @num:=0;");
			$sql = "SELECT
						@num :=@num + 1 AS 'No',
						DATE(u.createTime) AS 'Tanggal Daftar',
						f.nomor AS 'No Registrasi',
						u.jalur_penerimaan AS 'Jalur seleksi',
						e.edit_ke as 'Rapor Diedit ke-',

					IF (
						f.stsApplyPaid = 1,
						'Sudah',
						'Belum'
					) AS 'Bayar Pendaftaran',
					up.rumahCell AS 'No HP',
					u.username AS 'Nama Lengkap',
					f.kode AS 'Email',

					IF (
						f.applyBankTransferType = '04.5',
						f.applyBankAccountType,
						''
					) AS 'Kode Voucher',
					x.nama AS Gelombang,
					p.nama AS 'Program Studi ',
					f.bukaSmb,
					u.jurusan AS 'Jurusan Sekolah',
					p2.nama AS 'PILIHAN 2',
					e.rata_rata_rapor AS 'Nilai rata-rata rapor',
					if(e.upload_rapor = '1', 'Sudah Upload', 'Belum Upload') as 'Upload Rapor',
					IF (
						f.stsApplyPaidConfirm = 1,
						'Sudah',
						'Belum'
					) AS 'Confirm BP',

					IF (
						up.stsPribadiConfirm = 1,
						'Sudah',
						'Belum'
					) AS 'Melengkapi Formulir',

					IF (
						f.stsEventConfirm = 1,
						'Sudah',
						'Belum'
					) AS 'Jadwal Ujian',

					IF (
						f.stsEventUsmPresent = 1,
						'Hadir',
						'Belum'
					) AS Kehadiran,

					IF (
						f.stsResultConfirm = 1,
						'Sudah',
						'Belum'
					) AS 'Hasil Ujian',

					IF (
						f.stsReapplyPaid = 1,
						'Sudah',
						'Belum'
					) AS 'Pembayaran Daftar Ulang',

					IF (
						stsReapplyPaidConfirm = 1,
						'Sudah',
						'Belum'
					) AS 'Konfirm DU',

					IF (
						f.stsMundurAfterReapply = 1,
						'Mundur',
						'Tidak'
					) AS Mundur,
					e.tahunLulus as 'Tahun Lulus',
					e.nama AS 'Nama Sekolah',
					w.nama AS 'Kab/Kota',
					w2.nama AS 'Provinsi',
					up.nama_panggilan as 'Nama Panggilan',
					up.nomorKtp as 'No NIK KTP',
					t.nama as 'Agama',
					up.rumahTel as 'Telepon',
					up.rumahCell as 'Hp',
					up.tempatLahir as 'Tempat Lahir',
					up.tanggalLahir as 'Tanggal Lahir',
					tG.nama as 'Jenis Kelamin',
					up.rumahAlamat as 'Alamat Rumah',
					wil_rumah.nama as 'Propinsi',
					kab_rumah.nama as 'Kabupaten/Kota',
					kp.kodePos as 'Kode Pos',
					up.ibuNama as 'Nama Ibu Kandung',
					IFNULL(up.ayahNama, up.waliNama) as 'Nama Ayah/Wali',
					up.pekerjaan as Pekerjaan,
					up.pendidikan as Pendidikan,
					IFNULL(up.ayahAlamat, up.waliAlamat) as 'Alamat',
					IFNULL(prop_ayah.nama, prop_wali.nama) as 'Propinsi',
					IFNULL(kab_ayah.nama, kab_wali.nama) as 'Kabupaten/Kota',
					IFNULL(kp_ayah.kodePos, kp_wali.kodePos) as 'Kode Pos',
					f.nim as NIM, up.foto, k.kode_potongan as 'Kode Potongan',
					ts.nama as followup, ST.edu, ixo.followup as ods, ST.ket_ods, ixoh.followup as oh, ST.ket_oh, ixp.attempt as pelunasan, ST.ket_pelunasan,
					ST.date_updated as 'Update Terakhir Telemarketing'
					FROM
						adis_smb_form f
			LEFT JOIN adis_smb_usr u ON u.kode = f.kode
			LEFT JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
			LEFT JOIN adis_smb_usr_keu k ON f.kode = k.smbUsr
			LEFT JOIN adis_prodi p ON p.kode = RIGHT(f.bukaSmb, 4)
			LEFT JOIN adis_prodi p2 ON u.pilihan_prodi_2 = p2.kode
			LEFT JOIN adis_jalur_smb j ON j.kode = SUBSTR(f.bukaSmb, 12,2 )
			LEFT JOIN adis_smb_usr_edu e ON e.smbUsr = f.kode
			LEFT JOIN adis_wil w ON w.kode = e.kabKota
			LEFT JOIN adis_wil w2 ON w2.kode = e.prop
			LEFT JOIN adis_periode x ON SUBSTR(f.bukaSmb, 1, 10) = x.kode
			LEFT JOIN adis_type t on up.agamaType = t.kode
			LEFT JOIN adis_wil wil_rumah ON up.rumahProp = wil_rumah.kode
			LEFT JOIN adis_wil kab_rumah ON up.rumahKabKota = kab_rumah.kode
			LEFT JOIN adis_kodepos kp on up.rumahKodePos = kp.kode
			LEFT JOIN adis_wil prop_ayah ON up.ayahProp = prop_ayah.kode
			LEFT JOIN adis_wil kab_ayah ON up.ayahKabKota = kab_ayah.kode
			LEFT JOIN adis_kodepos kp_ayah on up.ayahKodePos = kp_ayah.kode
			LEFT JOIN adis_wil prop_wali ON up.waliProp = prop_wali.kode
			LEFT JOIN adis_wil kab_wali ON up.waliKabKota = kab_wali.kode
			LEFT JOIN adis_kodepos kp_wali on up.waliKodePos = kp_wali.kode
			LEFT JOIN adis_type tG ON up.genderType = tG.kode
			LEFT JOIN smart_telemarketing ST ON f.kode = ST.kode
			LEFT JOIN tbl_sales ts on ST.followup = ts.id
			LEFT JOIN idx_followup ixo on ST.ods = ixo.id
			LEFT JOIN idx_followup ixoh on ST.oh = ixoh.id
			LEFT JOIN idx_attempt ixp on ST.pelunasan = ixp.id
			WHERE SUBSTR(f.bukaSmb, 1, 8) = '$periode' $QWhere
			ORDER BY 'Tanggal Daftar' DESC";

			// echo $sql;exit;
			$sql = $this->db2->query($sql);
			return $sql;

	}


	function mMundur($kode){
		$row = $this->db2->query("SELECT kode from adis_smb_form WHERE nomor='$kode'")->row();
		$kode = $row->kode;
		$date = date("Y-m-d H:i:s");

		$this->db2->where("kode", $kode);
		$this->db2->update("adis_smb_form", array(
				"stsMundurAfterReapply"=>1
			));
		$jumlahBayar = $this->input->post("jumlahBayar");
		$jumlahBayar = str_replace('.', '', $jumlahBayar);
		$jumlahBayar = str_replace(' ', '', $jumlahBayar);

		$this->db2->insert("adis_resign_smb", array(
				"kode"=>$kode,
				"smbUsr"=>$kode,
				"createTime"=>$date,
				"createUser"=>$this->auth['name'],
				"reason"=>$this->input->post('alasan'),
				"otherReason"=>$this->input->post('alasanLain'),
				"paymentMethod"=>$this->input->post("typeTrans"),
				"amountRegistration"=>$jumlahBayar,
				"bankName"=>$this->input->post("bank"),
				"bankAccount"=>$this->input->post("kcp"),
				"bankBranch"=>$this->input->post("noRek"),
				"bankAccountName"=>$this->input->post("an")
			));
	}

	function saveCmbNotReg($bukaSmb, $noReg){

		$date = date("Y-m-d H:i:s");
		$tanggalLahir = date("Y-m-d", strtotime($this->input->post("tanggalLahir")));
		$nomor = uniqid();
		$jenjang = $this->input->post('jenjang');

		$rank = $this->input->post('rank');
		$bayar_met = $this->input->post('lunas');
		$jalur = $this->input->post('jalur');
		$prodi = $this->input->post('prodi');
		$lulusan = $this->input->post('lulusan');
		$alumni = 0;
		$earlybird = 0;
		$keluargacivitas = 0;
		$keluargadlb = 0;
		$miai = 0;

		$grade = '1';
		if ($jalur == 'CI'){ $grade = '5';}
		if ($rank != '0' || $rank != ''){
			$grade = $rank;
		}

		switch($jenjang){
			case '1':
				if($jalur == 'KP'){
					if ($bayar_met == '0'){
						if ($lulusan){$metodBayar = implode (".", array($prodi,'69', 'KP', $lulusan));}
						else{$metodBayar = implode (".", array($prodi,'69', 'KP'));}
					}else{
						if ($lulusan){$metodBayar = implode (".", array($prodi,'1', 'KP', $lulusan));}
						else{$metodBayar = implode (".", array($prodi,'1', 'KP'));}
					}
				}else if ($jalur == '02' || $jalur == '01'){
					if ($bayar_met == '0'){
						$metodBayar = implode (".", array($prodi,'69', '01'));
					}else{
						$metodBayar = implode (".", array($prodi,'1', '01'));
					}
				}else{
					$metodBayar = implode (".", array($prodi,'1', '01'));
				}
			break;
			case '2':

				$alumni = $this->input->post('alumni');
				$miai = $this->input->post('miai');
				$earlybird = $this->input->post('earlybird');
				$keluargacivitas = $this->input->post('keluargacivitas');
				$keluargadlb = $this->input->post('keluargadlb');
				$grade = '0';
				$periode = $this->input->post('periode');
				$metodBayar = implode (".", array($periode, $prodi,'1'));
				$grade = '0';
				// if ($jalur == '10'){
					// if ($bayar_met == '0'){
						// $metodBayar = implode (".", array($prodi,'69', '10'));
					// }else{
						// $metodBayar = implode (".", array($prodi,'1', '10'));
					// }
				// }else{
					// $metodBayar = implode (".", array($prodi,'69', '10'));
				// }
			break;
		}







		$this->db2->insert("adis_smb_usr", array(
				"kode"=>$nomor,
				"username"=>$this->input->post('name'),
				"email"=>$this->input->post('email'),
				"createUser"=>'Admin Admisi',
				"createTime"=>$date
			));

		$this->db2->insert("adis_smb_form", array(
				"kode"=>$nomor,
				"bukaSmb"=>$bukaSmb,
				"nomor"=>$noReg,
				"stsResultPass"=>'1',
				"stsResultGrade"=>$grade,
				"createTime"=>$date,
				"createUser"=>'Admin Admisi'
			));

		$this->db2->insert("adis_smb_usr_keu", array(
				"kode"=>$noReg,
				"smbusr"=>$nomor,
				"metodBayarDaftarUlang"=>$metodBayar,
				"metodePembayaran"=>$bayar_met,
				"sks_acc"=>$this->input->post('sks_acc'),
				"createTime"=>$date,
				"createUser"=>'Admin Admisi',
				"alumni_s1"=>$alumni,
				"earlybird"=>$earlybird,
				"keluargacivitas"=>$keluargacivitas,
				"keluargadlb"=>$keluargadlb,
				"miaiPindahan"=>$miai
			));

		$this->db2->insert("adis_smb_usr_edu", array(
				"kode"=>$noReg,
				"smbUsr"=>$nomor,
				"nama"=>$this->input->post("namaSekolah"),
				"jurusan"=>$this->input->post("jurusanSMA"),
				"tahunLulus"=>$this->input->post("tahunLulus"),
				'sekolahType'=> $this->input->post("pendType"),
				'sekolahOwnerType'=> $this->input->post("sekType"),
				'sekolahAlamat'=> $this->input->post("alamatSekPT"),
				'prop'=> $this->input->post("propSek"),
				'kabKota'=> $this->input->post("kabSek"),
				'kodePos'=> $this->input->post("kposSek"),
				'nilai'=> $this->input->post("nilai"),
				'nisn_nim'=> $this->input->post("nisnnim"),
				"createUser"=>'Admin Admisi',
				"createTime"=>$date
			));

		$this->db2->insert("adis_smb_usr_pribadi", array(
				'kode'=>$nomor,
				'nama'=>$this->input->post('nameFull'),
				'genderType'=>$this->input->post('sex'),
				'tempatLahir'=>$this->input->post('tempatLahir'),
				'tanggalLahir'=>$tanggalLahir,
				'rumahAlamat'=>$this->input->post('occupation'),
				'rumahCell'=>$this->input->post('no_hp'),
				'stsPribadi'=>0,
				'stsPribadiConfirm'=>0,
				'nomorKtp' => $this->input->post('no_id'),
				'agamaType' => $this->input->post('agama'),
				'rumahProp' => $this->input->post('propinsi'),
				'rumahKabKota' => $this->input->post('kabkota'),
				'rumahKodePos' => $this->input->post('kodepos'),
				'rumahTel' => $this->input->post('telRumah'),
				'rumahFax' => $this->input->post('fax'),
				'ayahNama'=>$this->input->post('namaAyah'),
				'ayahAlamat'=>$this->input->post('alamatAyah'),
				'ayahKabKota'=>$this->input->post('kabAyah'),
				'ayahProp'=>$this->input->post('propAyah'),
				'ayahKodePos'=>$this->input->post('kposAyah'),
				'ayahTel'=>$this->input->post('telAyah'),
				'ayahCell'=>$this->input->post('hpAyah'),
				'ayahEmail'=>$this->input->post('emailAyah'),
				'ibuNama'=>$this->input->post('namaIbu'),
				'ibuAlamat'=>$this->input->post('alamatIbu'),
				'ibuKabKota'=>$this->input->post('kabIbu'),
				'ibuProp'=>$this->input->post('propIbu'),
				'ibuKodePos'=>$this->input->post('kposIbu'),
				'ibuTel'=>$this->input->post('telIbu'),
				'ibuCell'=>$this->input->post('hpIbu'),
				'ibuEmail'=>$this->input->post('emailIbu'),
				'waliNama'=>$this->input->post('namaWali'),
				'waliAlamat'=>$this->input->post('alamatWali'),
				'waliKabKota'=>$this->input->post('kabWali'),
				'waliProp'=>$this->input->post('propWali'),
				'waliKodePos'=>$this->input->post('kposWali'),
				'waliTel'=>$this->input->post('telWali'),
				'waliCell'=>$this->input->post('hpWali'),
				'waliEmail'=>$this->input->post('emailWali'),
				'createUser'=>'Admin Admisi',
				'createTime'=>$date,
                                'nama_donor'=>$this->input->post('nama_donor')
			));

		if ($this->input->post('saudara') == '1'){
			$this->db2->insert("adis_smb_usr_kel", array(
					"kode"=>$nomor,
					"smbusr"=>$nomor,
					"status"=>1,
					"confirmed"=>1,
					"createTime"=>$date,
					"createUser"=>'Admin Admisi'
				));
		}


	}

	function mProfil($kode, $asu=''){

		if (is_numeric($kode)){
			$wer = "WHERE f.nomor = '$kode'";
		}else{
			$wer ="WHERE f.kode = '$kode'";
		}
		// echo '<script>alert("Kode Tipe :'.$kode.'");</script>';
		$sql = "SELECT f.kode as kode_smb, f.bukaSmb, f.nomor as no_smb, up.nama as nama_cm , f.nim,
				p.nama as progdi, p.kode as proKode,
				DAYNAME(u.createTime) as hari, DATE(u.createTime) as tanggal, up.agamaType,
				DAY(u.createTime) as cDay, MONTHNAME(u.createTime) as cMonth, YEAR(u.createTime) as cYear,
				m.jenjangType, up.rumahCell, up.genderType, up.tempatLahir, up.tanggalLahir, DAYNAME(up.tanggalLahir) as hLahir,
				DAY(up.tanggalLahir) as tLahir, MONTHNAME(up.tanggalLahir) as bLahir, YEAR(up.tanggalLahir) as yLahir,
				up.nomorKtp, up.rumahEmail, up.suratAlamat, tA.nama as suratProp, tD.kodepos as suratKodPos,
				up.suratTel, up.suratFax,
				up.rumahAlamat, t2.nama as propNama, t3.nama as kabKotaNama, t4.kodepos as kodePos, up.rumahTel, up.rumahFax,
				up.ayahNama, up.ayahAlamat, up.ayahCell, up.ayahEmail,
				up.ibuNama, up.ibuAlamat, up.ibuCell, up.ibuEmail,
				up.waliNama, up.waliAlamat, up.waliCell, up.waliEmail,
				t.nama as gender, t.kode as kodeGender, t1.nama as agamaName,f.stsResultGrade,
				up.statusAlamat, up.statusKeluarga,  m.nama as namaPeriode,
				e.status as statusPend, e.nama as namaEdu, e.jurusan,
				up.statusPrestasi, up.stsPribadiConfirm, up.statusSaudara, up.foto,
				f.stsApplyPaid, f.stsApplyPaidConfirm, up.stsPribadiConfirm, f.stsEventConfirm, f.stsResultConfirm, f.stsMundurBeforeReapply,
				f.stsReapplyPaid, stsReapplyPaidConfirm, f.stsMundurAfterReapply, j.nama as n_jalur, f.stsResultPass, f.stsReapplyPaidConfirm,
				m.tahun, m.semester, SUBSTR(f.bukaSmb, 12, 2) as kode_jalur, f.stsResultKet
				, e.file_rapor_10_1, e.file_rapor_10_2, e.file_rapor_11_1, e.file_rapor_11_2
				FROM adis_smb_form f
				LEFT JOIN adis_smb_usr u ON u.kode = f.kode
				LEFT JOIN adis_periode_master m ON m.kode = LEFT(f.bukaSmb,8)
				LEFT JOIN adis_smb_usr_pribadi up ON up.kode = f.kode
				LEFT JOIN adis_smb_usr_edu e ON e.smbUsr = f.kode
				LEFT JOIN adis_prodi p ON p.kode = RIGHT(f.bukaSmb,4)
				LEFT JOIN adis_jalur_smb j ON j.kode = SUBSTR(f.bukaSmb, 12, 2)
				LEFT JOIN adis_type t ON t.kode = up.genderType
				LEFT JOIN adis_type t1 ON t1.kode = up.agamaType
				LEFT JOIN adis_wil t2 ON t2.kode = up.rumahProp
				LEFT JOIN adis_wil t3 ON t3.kode = up.rumahKabKota
				LEFT JOIN adis_kodepos t4 ON t4.kode = up.rumahKodePos
				LEFT JOIN adis_wil tA ON tA.kode = up.suratProp
				LEFT JOIN adis_kodepos tD ON tD.kode = up.suratKodePos ".
				$wer;

		$sql = $this->db2->query($sql)->row();
		if ($asu){
			$this->smarty->assign("cmb", $sql);
		}else{
			$this->smarty->assign("prof", $sql);
		}
	}



    function sendMail($email,$index, $konten){


	}

	function autoMail($email, $konten, $subject, $kodeusr){

	}

	function emailNotifikasi($address, $subject, $konten){

		// PHPMailer object
		$response = false;
		$this->load->library("phpmailer_library");
        $mail = $this->phpmailer_library->load();

		$this->load->library('parser');


	   // SMTP configuration
	   $mail->isSMTP();
	   $mail->Host     = 'smtp.gmail.com'; //sesuaikan sesuai nama domain hosting/server yang digunakan
	   $mail->SMTPAuth = true;
	   $mail->Username = 'noreply-admisi@bakrie.ac.id'; // user email
	   $mail->Password = 'noname.12920'; // password email
	   $mail->SMTPSecure = 'tls';
	   $mail->Port     = 587;

	   $mail->setFrom('noreply-admisi@bakrie.ac.id', ''); // user email
	   $mail->addReplyTo('noreply-admisi@bakrie.ac.id', ''); //user email

	   // Add a recipient
	   $mail->addAddress($address); //email tujuan pengiriman email

	   if (strpos($subject, 'Nilai Rapor') !== false) {
			$recipientArr = array('sukmalevi@gmail.com','smart.bakrie@gmail.com', 'bakrie.promosi@gmail.com', 'fadjar.handoyo@bakrie.ac.id', 'linggar.putra@bakrie.ac.id');
			foreach($recipientArr as $val){
				$mail->AddCC($val);
			}
		}else if (strpos($subject, 'Bayar') !== false) {
			$mail->AddCC("keuangan@bakrie.ac.id");
		}else{
			$mail->AddCC("smart.bakrie@gmail.com");
		}

	   // Email subject
	   $mail->Subject = $subject; //subject email

	   // Set email format to HTML
	   $mail->isHTML(true);
	   $htmlMessage = $this->parser->parse('email_template.html', $konten, true);
	   $mail->msgHTML($htmlMessage);

	   // Send email
	   if(!$mail->send()){
		   return 0;
	   }else{
		   return 1;
	   }
	}
}
