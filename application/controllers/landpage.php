<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Landpage extends CI_Controller{
	function __construct(){
		parent::__construct();		
		date_default_timezone_set("Asia/Jakarta");
		$this->host	= $this->config->item('base_url');
		$this->db2 = $this->load->database('second', TRUE); 
		
		$this->smarty->assign('host',$this->host);
		$host = $this->host;
    }
    
    function index() {
        $src_campign = "";
        $get_uri = $_GET;
        if(!empty($get_uri)){
            $src_campign = json_encode($get_uri);
        }else{
            $src_campign = "Direct Link";
        }
        $this->smarty->assign('campign', $src_campign);
		$this->smarty->display('lp/index.html');
    }

    function magister(){
        $src_campign = "";
        $get_uri = $_GET;
        if(!empty($get_uri)){
            $src_campign = json_encode($get_uri);
        }else{
            $src_campign = "Direct Link";
        }
        $this->smarty->assign('campign', $src_campign);
		$this->smarty->display('lp/s2/index.html');
    }

    function karyawan(){
        $src_campign = "";
        $get_uri = $_GET;
        if(!empty($get_uri)){
            $src_campign = json_encode($get_uri);
        }else{
            $src_campign = "Direct Link";
        }
        $this->smarty->assign('campign', $src_campign);
		$this->smarty->display('lp/kk/index.html');
    }

    function trims(){
		$this->smarty->display('lp/terimakasih.html');
    }

    function submit_form_mm(){
        $post = $this->input->post();
        $code = 1;
        $field = "";
        if($post){
            if (empty($post['nama']))
                die(json_encode(array('code'=>0, 'msg'=>"Nama lengkap tidak boleh kosong!")));
            if (empty($post['email']))
                die(json_encode(array('code'=>0, 'msg'=>"Email tidak boleh kosong!")));
            if (empty($post['lulusan']))
                die(json_encode(array('code'=>0, 'msg'=>"Lulusan tidak boleh kosong!")));
            if (empty($post['wa']))
                die(json_encode(array('code'=>0, 'msg'=>"No WhatsApp tidak boleh kosong!")));
            if (empty($post['sumber']))
                die(json_encode(array('code'=>0, 'msg'=>"Sumber iklan tidak boleh kosong!")));

            if(!empty($post['email'])){
                if ( !$this->checkEmail($post['email']) ) {
                    die(json_encode(array('code'=>0, 'msg'=>"Format email salah!")));
                }
            }

            $wa_no = $this->convert_number($post['wa']);

            $isDaftar = $this->db->query('select * from tbl_invent_mm where email = "'.$post['email'].'" or  hp = "'.$wa_no.'" ;');
            if($isDaftar->num_rows() > 0){
                die(json_encode(array('code'=>0, 'msg'=>"Email atau No WhatsApp sudah digunakan!")));
            }

            $this->load->model('madmisi');
            $curdate = date('Y-m-d');

            $edu_checked = $this->cekEdu($post['email'], 's2');

            $this->db->insert('tbl_invent_mm', array(
                'email'=>$post['email'],
                'nama_lengkap'=>ucwords($post['nama']),
                'lulusan'=>ucwords($post['lulusan']),
                'tanggal_daftar'=>date('Y-m-d'),
                'hp'=>$this->convert_number($post['wa']),
                'sumber'=>$post['sumber'],
                'edu'=>$edu_checked,
                'noted'=>'Mendaftar dari site form invert'
            ));

            if ($this->db2->trans_status() === FALSE)
            {
                $this->db2->trans_rollback();
                die(json_encode(array('code'=>0, 'msg'=>"Gagal menyimpan data!")));
            }
            else
            {
                $this->db2->trans_commit();
                die(json_encode(array('code'=>1, 'msg'=>"Terima kasih telah mengajukan permohonan bebas biaya pendaftaran, kode voucher akan segera dikirimkan lewat whatsapp maksimal 2x24 jam. <br>Silakan menghubungi Edu Consultant kami jika ada pertanyaan terkait pendaftaran. "
                )));
            }




        }
        
        echo json_encode(array('code'=>$code, 'msg'=>$field));
    }
    
    function submit_form(){
        $post = $this->input->post();
        $code = 1;
        $field = "";
        if($post){
            if (empty($post['nama']))
                die(json_encode(array('code'=>0, 'msg'=>"Nama lengkap tidak boleh kosong!")));
            if (empty($post['email']))
                die(json_encode(array('code'=>0, 'msg'=>"Email tidak boleh kosong!")));
            if (empty($post['kota']))
                die(json_encode(array('code'=>0, 'msg'=>"Kota asal tidak boleh kosong!")));
            if (empty($post['wa']))
                die(json_encode(array('code'=>0, 'msg'=>"No WhatsApp tidak boleh kosong!")));
            if (empty($post['sumber']))
                die(json_encode(array('code'=>0, 'msg'=>"Sumber iklan tidak boleh kosong!")));

            if(!empty($post['email'])){
                if ( !$this->checkEmail($post['email']) ) {
                    die(json_encode(array('code'=>0, 'msg'=>"Format email salah!")));
                }
            }

            $wa_no = $this->convert_number($post['wa']);

            $isDaftar = $this->db->query('select * from tbl_invent where email = "'.$post['email'].'" or  hp = "'.$wa_no.'" ;');
            if($isDaftar->num_rows() > 0){
                die(json_encode(array('code'=>0, 'msg'=>"Email atau No WhatsApp sudah digunakan!")));
            }

            $this->load->model('madmisi');
            $curdate = date('Y-m-d');
            $curVoMonthDate = "LP".date('md');
            $vouRan = $this->madmisi->generateRandomString(6);
            $issuedVoucher = $curVoMonthDate.$vouRan;

            $this->db2->trans_start();

            $this->db2->insert('tbl_voucher', array(
                'date_created'=>date('Y-m-d H:m:s'),
                'created_by'=>'Form Invert',
                'kode_voucher'=>$issuedVoucher,
                'tanggal_expired'=>date('Y-m-d',strtotime('+30 days',strtotime($curdate))),
                'isUsed'=>'N'
            ));

            $edu_checked = $this->cekEdu($post['email'], 's1');

            $this->db->insert('tbl_invent', array(
                'email'=>$post['email'],
                'nama_lengkap'=>$post['nama'],
                'alamat_asal'=>$post['kota'],
                'tanggal_daftar'=>date('Y-m-d'),
                'hp'=>$this->convert_number($post['wa']),
                'sumber'=>$post['sumber'],
                'edu'=>$edu_checked,
                'kode_voucher'=>$issuedVoucher,
                'voucher_expired'=>date('Y-m-d',strtotime('+30 days',strtotime($curdate))),
                'noted'=>'Mendaftar dari site form invert'
            ));

            $exp_voucher = date('Y-m-d',strtotime('+30 days',strtotime($curdate)));

            if ($this->db2->trans_status() === FALSE)
            {
                $this->db2->trans_rollback();
                die(json_encode(array('code'=>0, 'msg'=>"Gagal menyimpan data!")));
            }
            else
            {
                $this->db2->trans_commit();
                die(json_encode(array('code'=>1, 'msg'=>"Terima kasih telah mengajukan permohonan bebas biaya pendaftaran, kode voucher akan segera dikirimkan lewat whatsapp maksimal 2x24 jam. <br>Silakan menghubungi Edu Consultant kami jika ada pertanyaan terkait pendaftaran. "
                )));
            }




        }
        
        echo json_encode(array('code'=>$code, 'msg'=>$field));
    }

    // function setEduAll(){
    //     $data = $this->db->get_where('tbl_invent', array('edu'=>''))->result_array();
    //     foreach($data as $val){
    //         $edu_next = "";
    //         $edu_next = $this->cekEdu($val['email']);
    //         if($edu_next != ""){
    //             echo $edu_next;
    //             $this->db->where('email', $val['email']);
    //             $this->db->update('tbl_invent', array(
    //                 'edu'=>$edu_next
    //             ));
    //         }
    //     }
    //     // echo "<pre>";
    //     // print_r($data);
    // }

    function convert_number($no){
        if(!preg_match('/[^+0-9]/',trim($no))){
            // cek apakah no hp karakter 1 adalah +
            if(substr(trim($no), 0, 1)=='+'){
                $hp = str_replace('+', '', trim($no));
            }
            elseif(substr(trim($no), 0, 2)=='62'){
                $hp = trim($no);
            }
            // cek apakah no hp karakter 1 adalah 0
            elseif(substr(trim($no), 0, 1)=='0'){
                $hp = '62'.substr(trim($no), 1);
            }else{
                die(json_encode(array('code'=>0, 'msg'=>'Format nomor hp yang dimasukkan salah!')));
            }
        }
        return $hp;
    }

    function cekEdu($kode, $type = ""){
        $tbl_invent = 'tbl_invent';
        $where_cek = " where 1=1 and id_level = 44 and aktif_edu = 1 and erased = 0 ";
        if($type == 's1'){
            $where_cek .= " and is_reg = 1 ";
        } else if($type == 's2'){
            $where_cek .= " and is_mag = 1 ";
            $tbl_invent = 'tbl_invent_mm';
        }else if($type == 'kk'){
            $where_cek .= " and is_kk = 1 ";
        }
        $edu = $this->db->query("select * from adis_sys_usr $where_cek ")->result_array();
        $query_max_edu = $this->db->query("
                select * from $tbl_invent a 
                inner join (select max(id) as max_id from $tbl_invent where edu != '') as b on a.id = b.max_id");
        $sizeEdu = count($edu);
        if($query_max_edu->num_rows() > 0){
            $query_max_edu = $query_max_edu->row_array();
            $next_edu = '';
            foreach($edu as $k => $val){
                if($query_max_edu['edu'] == $val['username']){
                    if(($sizeEdu-1) == $k){
                        $next_edu = $edu[0]['username'];
                        break;
                    }else{
                        $next_edu = $edu[$k+1]['username'];
                        break;
                    }
                }
            }

            return $next_edu;
        }else{
            $next_edu = empty($edu[0]['username']) ? '' : $edu[0]['username'];
            return $next_edu;
        }
    }

    function checkEmail($email) {
        $find1 = strpos($email, '@');
        $find2 = strpos($email, '.');
        return ($find1 !== false && $find2 !== false);
     }
     
    
}