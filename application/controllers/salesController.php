<?php
// application/controllers/SalesController.php

class SalesController extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('sales_model');
    }

    public function index() {
        $data['sales'] = $this->sales_model->getSalesData();
        $this->load->library('smarty');
        $this->smarty->assign('data', $data);
        $this->smarty->display('template/sales_template.html');
    }

    public function detail($kode) {
        // Fetch sales details and associated new students based on $fullname
        $salesDetails = $this->sales_model->getSalesDetails($kode);
        $students = $this->sales_model->getStudentsBySales($kode);

        $data['salesDetails'] = $salesDetails;
        $data['students'] = $students;

        $this->load->library('smarty');
        $this->smarty->assign('data', $data);
        $this->smarty->display('template/sales_detil.html');
    }
 
}


?>