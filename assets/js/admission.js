function getClientHeight(){
	var theHeight;
	if (window.innerHeight)
		theHeight=window.innerHeight;
	else if (document.documentElement && document.documentElement.clientHeight) 
		theHeight=document.documentElement.clientHeight;
	else if (document.body) 
		theHeight=document.body.clientHeight;
	
	return theHeight;
}
function getClientWidth(){
	var theWidth;
	if (window.innerWidth) 
		theWidth=window.innerWidth;
	else if (document.documentElement && document.documentElement.clientWidth) 
		theWidth=document.documentElement.clientWidth;
	else if (document.body) 
		theWidth=document.body.clientWidth;

	return theWidth;
}

function fillCmb(url, SelID, value, value2, value3, value4){
	if (value == undefined) value = "";
	if (value2 == undefined) value2 = "";
	if (value3 == undefined) value3 = "";
	if (value4 == undefined) value4 = "";
	
	$('#'+SelID).empty();
	$.post(url, {"v": value, "v2": value2, "v3": value3, "v4": value4},function(data){
		$('#'+SelID).append(data);
	});
}

function kumpulAction(type, p1, p2, p3, p4, p5){
	var param = {};
	switch(type){
		case "hasilSeleksiKK":
			var row = $("#hasilSeleksiKK").datagrid('getSelected');
			if(row){
				$.LoadingOverlay("show");
				param['editstatus'] = 'edit';
				param['id'] = row.kode;
				$.post(hostir+'smb/getDisplayView/formHasilSeleksiKK', param, function(resp){
					$('.modal-title').html("<b>Set Hasil Seleksi</b>");
					$('#form_load').html(resp);
					$('#form_invent').modal('show');
					$.LoadingOverlay("hide", true);
				});
			}else{
				$.messager.alert('Smart', "Please Select Data First!",'error');
			}
			break;
		case 'mod_priv':
			if(p1){
				if(p1=='add'){
					$.LoadingOverlay("show");
					param['editstatus'] = 'add';
					$.post(hostir+'smb/getDisplayView/form_mod_priv', param, function(resp){
						$('.modal-title').html("<b>Tambah Modul Privilage</b>");
						$('#form_load').html(resp);
						$('#form_invent').modal('show');
						$.LoadingOverlay("hide", true);
					});

				}else if(p1 == 'edit'){
					var row = $("#modulPrivilege").datagrid('getSelected');
					if(row){
						$.LoadingOverlay("show");
						param['editstatus'] = 'edit';
						param['id'] = row.id;
						$.post(hostir+'smb/getDisplayView/form_mod_priv', param, function(resp){
							$('.modal-title').html("<b>Edit Modul Privilage</b>");
							$('#form_load').html(resp);
							$('#form_invent').modal('show');
							$.LoadingOverlay("hide", true);
						});
					}else{
						$.messager.alert('Smart', "Please Select Data First!",'error');
					}

				}else if(p1 == 'delete'){
					var row = $("#modulPrivilege").datagrid('getSelected');
					if(row){
						
						param['editstatus'] = 'delete';
						param['id'] = row.kode;
						$.post(hostir+'smb/simpandata/smart_menu_privilage', param, function(resp){
							if(resp == 1){
								$.messager.alert('Smart - Bakrie','Data berhasil dihapus','info'); 
								$('#modulPrivilege').datagrid('reload');
							}else{
								$.messager.alert('Smart - Bakrie','Data Gagal DIHAPUS - '+resp,'info'); 
							}
							$.LoadingOverlay("hide", true);
						});
					}else{
						$.messager.alert('Smart', "Please Select Data First!",'error');
					}

				}

			}else{
				$.messager.alert('Smart - Bakrie','Invalid Action!','info'); 
			}
			break;
		case 'manage_prodi':
			if(p1){
				if(p1=='add'){
					$.LoadingOverlay("show");
					param['editstatus'] = 'add';
					$.post(hostir+'smb/getDisplayView/form_manage_prodi', param, function(resp){
						$('.modal-title').html("<b>Tambah Master Program Studi</b>");
						$('#form_load').html(resp);
						$('#form_invent').modal('show');
						$.LoadingOverlay("hide", true);
					});

				}else if(p1 == 'edit'){
					var row = $("#masterProdi").datagrid('getSelected');
					if(row){
						$.LoadingOverlay("show");
						param['editstatus'] = 'edit';
						param['id'] = row.kode;
						$.post(hostir+'smb/getDisplayView/form_manage_prodi', param, function(resp){
							$('.modal-title').html("<b>Edit Master Prodi</b>");
							$('#form_load').html(resp);
							$('#form_invent').modal('show');
							$.LoadingOverlay("hide", true);
						});
					}else{
						$.messager.alert('Smart', "Please Select Data First!",'error');
					}

				}else if(p1 == 'delete'){
					var row = $("#masterProdi").datagrid('getSelected');
					if(row){
						
						param['editstatus'] = 'delete';
						param['id'] = row.kode;
						$.post(hostir+'smb/simpandata/adis_prodi', param, function(resp){
							if(resp == 1){
								$.messager.alert('Smart - Bakrie','Data berhasil dihapus','info'); 
								$('#masterProdi').datagrid('reload');
							}else{
								$.messager.alert('Smart - Bakrie','Data Gagal DIHAPUS - '+resp,'info'); 
							}
							$.LoadingOverlay("hide", true);
						});
					}else{
						$.messager.alert('Smart', "Please Select Data First!",'error');
					}

				}

			}else{
				$.messager.alert('Smart - Bakrie','Invalid Action!','info'); 
			}
			break;
		case 'manage_agama':
			if(p1){
				if(p1=='add'){
					$.LoadingOverlay("show");
					param['editstatus'] = 'add';
					$.post(hostir+'smb/getDisplayView/form_manage_agama', param, function(resp){
						$('.modal-title').html("<b>Tambah Master Agama</b>");
						$('#form_load').html(resp);
						$('#form_invent').modal('show');
						$.LoadingOverlay("hide", true);
					});

				}else if(p1 == 'edit'){
					var row = $("#masterAgama").datagrid('getSelected');
					if(row){
						$.LoadingOverlay("show");
						param['editstatus'] = 'edit';
						param['id'] = row.kode;
						$.post(hostir+'smb/getDisplayView/form_manage_agama', param, function(resp){
							$('.modal-title').html("<b>Edit Master Agama</b>");
							$('#form_load').html(resp);
							$('#form_invent').modal('show');
							$.LoadingOverlay("hide", true);
						});
					}else{
						$.messager.alert('Smart', "Please Select Data First!",'error');
					}

				}else if(p1 == 'delete'){
					var row = $("#masterAgama").datagrid('getSelected');
					if(row){
						
						param['editstatus'] = 'delete';
						param['id'] = row.kode;
						$.post(hostir+'smb/simpandata/adis_type', param, function(resp){
							if(resp == 1){
								$.messager.alert('Smart - Bakrie','Data berhasil dihapus','info'); 
								$('#masterAgama').datagrid('reload');
							}else{
								$.messager.alert('Smart - Bakrie','Data Gagal DIHAPUS - '+resp,'info'); 
							}
							$.LoadingOverlay("hide", true);
						});
					}else{
						$.messager.alert('Smart', "Please Select Data First!",'error');
					}

				}

			}else{
				$.messager.alert('Smart - Bakrie','Invalid Action!','info'); 
			}
			break;
		case "manage_edu":
			if(p1){
				if(p1=='add'){
					$.LoadingOverlay("show");
					param['editstatus'] = 'add';
					$.post(hostir+'smb/getDisplayView/form_manage_edu', param, function(resp){
						$('.modal-title').html("<b>Tambah Edu Consultant</b>");
						$('#form_load').html(resp);
						$('#form_invent').modal('show');
						$.LoadingOverlay("hide", true);
					});

				}else if(p1 == 'edit'){
					var row = $("#man_edu").datagrid('getSelected');
					if(row){
						$.LoadingOverlay("show");
						param['editstatus'] = 'edit';
						param['id'] = row.kode;
						$.post(hostir+'smb/getDisplayView/form_manage_edu', param, function(resp){
							$('.modal-title').html("<b>Edit Edu Consultant</b>");
							$('#form_load').html(resp);
							$('#form_invent').modal('show');
							$.LoadingOverlay("hide", true);
						});
					}else{
						$.messager.alert('Smart', "Please Select Data First!",'error');
					}

				}else if(p1 == 'delete'){
					var row = $("#man_edu").datagrid('getSelected');
					if(row){
						
						param['editstatus'] = 'delete';
						param['id'] = row.kode;
						$.post(hostir+'smb/simpandata/dataEduCons', param, function(resp){
							if(resp == 1){
								$.messager.alert('Smart - Bakrie','Data berhasil dihapus','info'); 
								$('#man_edu').datagrid('reload');
							}else{
								$.messager.alert('Smart - Bakrie','Data Gagal DIHAPUS - '+resp,'info'); 
							}
							$.LoadingOverlay("hide", true);
						});
					}else{
						$.messager.alert('Smart', "Please Select Data First!",'error');
					}

				}

			}else{
				$.messager.alert('Smart - Bakrie','Invalid Action!','info'); 
			}
		break;
		case "set_edu":
			if(p1){
				if(p2){
						param['editstatus'] = 'edit';
						param['id'] = p1;
						param[p2] = p3;
						$.LoadingOverlay("show");
						$.post(hostir+'smb/simpandata/adis_sys_usr', param, function(resp){
							$.LoadingOverlay("hide", true);
							if(resp == 1){
								$.messager.alert('Smart - Bakrie','Data berhasil diperbaharui','info'); 
								$('#man_edu').datagrid('reload');
							}else{
								$.messager.alert('Smart - Bakrie','Data Gagal diperbaharui! - '+resp,'info'); 
							}
						});
				}
			}
			break;
		case "edit_invent_kk":
			var row = $("#invent_kk").datagrid('getSelected');
			if(row){
				$.LoadingOverlay("show");
				if(row.sts_daftar == 'Registered'){
					$.LoadingOverlay("hide", true);
					$.messager.alert('Smart', "Invent sudah bersatus Register!",'error');
				}else{
					param['editstatus'] = 'edit';
					param['id'] = row.id;
					$.post(hostir+'smb/getDisplay/form_invent_kk', param, function(resp){
						$('.modal-title').html("<b>Edit Data Invent</b>");
						$('#form_load').html(resp);
						$('#form_invent').modal('show');
						$.LoadingOverlay("hide", true);
					});

				}
			}else{
				$.messager.alert('Smart', "Please Select Data First!",'error');
			}
		break;
		case "edit_invent_mm":
			var row = $("#invent_mm").datagrid('getSelected');
			if(row){
				$.LoadingOverlay("show");
				if(row.sts_daftar == 'Registered'){
					$.LoadingOverlay("hide", true);
					$.messager.alert('Smart', "Invent sudah bersatus Register!",'error');
				}else{
					param['editstatus'] = 'edit';
					param['id'] = row.id;
					$.post(hostir+'smb/getDisplay/form_invent_mm', param, function(resp){
						$('.modal-title').html("<b>Edit Data Invent</b>");
						$('#form_load').html(resp);
						$('#form_invent').modal('show');
						$.LoadingOverlay("hide", true);
					});

				}
			}else{
				$.messager.alert('Smart', "Please Select Data First!",'error');
			}
		break;
		case "detil_tele":
			var row = $("#telemarketing").datagrid('getSelected');
			if(row){
				
				$.LoadingOverlay("show");
				param['editstatus'] = 'edit';
				param['id'] = row.kode_smb;
				$.post(hostir+'smb/getDisplayView/detil_tele/', param, function(resp){
					$('.modal-title').html("<b>Detil Telemarketing</b>");
					$('#form_load').html(resp);
					$('#form_invent').modal('show');
					$.LoadingOverlay("hide", true);
				}); 
			}else{
				$.messager.alert('Smart', "Please Select Data First!",'error');
			}
		break;
		case 'payment_du_kk':
			var row = $("#daftarulangKK").datagrid('getSelected');
			if(row){
				$.LoadingOverlay("show");
				param['editstatus'] = 'edit';
				param['id'] = row.kode;
				$.post(hostir+'smb/getDisplayView/payment_du_kk/'+p1, param, function(resp){
					$('.modal-title').html("<b>Set biaya formulir</b>");
					$('#form_load').html(resp);
					$('#form_invent').modal('show');
					$.LoadingOverlay("hide", true);
				});
			}else{
				$.messager.alert('Smart', "Please Select Data First!",'error');
			}
		break;
		case 'payment_du':
			var row = $("#daftarulangMag").datagrid('getSelected');
			if(row){
				$.LoadingOverlay("show");
				param['editstatus'] = 'edit';
				param['id'] = row.kode;
				$.post(hostir+'smb/getDisplayView/payment_du/'+p1, param, function(resp){
					$('.modal-title').html("<b>Set biaya formulir</b>");
					$('#form_load').html(resp);
					$('#form_invent').modal('show');
					$.LoadingOverlay("hide", true);
				});
			}else{
				$.messager.alert('Smart', "Please Select Data First!",'error');
			}
		break;
		case "formbeasiswaMag":
			if(p1 == 'edit' || p1 == 'delete'){
				var row = $("#beasiswaMag").datagrid('getSelected');
				if(row){
					$.LoadingOverlay("show");
					param['editstatus'] = p1;
					param['id'] = row.id;
					if(p1 == 'delete'){
						$.post(hostir+'smb/simpandata/smart_ref_beasiswa', param, function(resp){
							if(resp == 1){
								$.messager.alert('Smart - Bakrie','Data berhasil dihapus','info'); 
								$('#beasiswaMag').datagrid('reload');
							}else{
								$.messager.alert('Smart - Bakrie','Data Gagal DIHAPUS - '+resp,'info'); 
							}
							$.LoadingOverlay("hide", true);
						});

					}else{
						$.post(hostir+'smb/getDisplayView/formbeasiswaMag', param, function(resp){
							$('.modal-title').html("<b>Setting Beasiswa magister</b>");
							$('#form_load').html(resp);
							$('#form_invent').modal('show');
							$.LoadingOverlay("hide", true);
						});
					}
				}else{
					$.messager.alert('Smart', "Please Select Data First!",'error');
				}
			}else if(p1 == 'add'){
				$.LoadingOverlay("show");
				param['editstatus'] = 'add';
				$.post(hostir+'smb/getDisplayView/formbeasiswaMag', param, function(resp){
					$('.modal-title').html("<b>Setting Beasiswa magister</b>");
					$('#form_load').html(resp);
					$('#form_invent').modal('show');
					$.LoadingOverlay("hide", true);
				});
			}
			break;
		case "setPotonganMag":
			if(p1 == 'edit' || p1 == 'delete'){
				var row = $("#setPotonganMag").datagrid('getSelected');
				if(row){
					$.LoadingOverlay("show");
					param['editstatus'] = p1;
					param['id'] = row.id;
					if(p1 == 'delete'){
						$.post(hostir+'smb/simpandata/smart_skema_biaya_beasiswa', param, function(resp){
							if(resp == 1){
								$.messager.alert('Smart - Bakrie','Data berhasil dihapus','info'); 
								$('#setPotonganMag').datagrid('reload');
							}else{
								$.messager.alert('Smart - Bakrie','Data Gagal DIHAPUS - '+resp,'info'); 
							}
							$.LoadingOverlay("hide", true);
						});

					}else{
						$.post(hostir+'smb/getDisplayView/setPotonganMag', param, function(resp){
							$('.modal-title').html("<b>Setting Potongan magister</b>");
							$('#form_load').html(resp);
							$('#form_invent').modal('show');
							$.LoadingOverlay("hide", true);
						});
					}
				}else{
					$.messager.alert('Smart', "Please Select Data First!",'error');
				}
			}else if(p1 == 'add'){
				$.LoadingOverlay("show");
				param['editstatus'] = 'add';
				$.post(hostir+'smb/getDisplayView/setPotonganMag', param, function(resp){
					$('.modal-title').html("<b>Setting tarif magister</b>");
					$('#form_load').html(resp);
					$('#form_invent').modal('show');
					$.LoadingOverlay("hide", true);
				});
			}
			break;
			case "skemaBiayaForm":
				if(p1 == 'edit' || p1 == 'delete'){
					var row = $("#skemaBiaya").datagrid('getSelected');
					if(row){
						$.LoadingOverlay("show");
						param['editstatus'] = p1;
						param['id'] = row.id;

						if(p1 == 'delete'){
							$.post(hostir+'smb/simpandata/smart_skema_biaya_normal', param, function(resp){
								if(resp == 1){
									$.messager.alert('Smart - Bakrie','Data berhasil dihapus','info'); 
									$('#skemaBiaya').datagrid('reload');
								}else{
									$.messager.alert('Smart - Bakrie','Data Gagal DIHAPUS - '+resp,'info'); 
								}
								$.LoadingOverlay("hide", true);
							});
						}else{
							$.post(hostir+'smb/getDisplayView/skemaBiayaForm', param, function(resp){
								$('.modal-title').html("<b>Setting Skema Biaya Magister</b>");
								$('#form_load').html(resp);
								$('#form_invent').modal('show');
								$.LoadingOverlay("hide", true);
							});
						}
					}else{
						$.messager.alert('Smart', "Please Select Data First!",'error');
					}
				}else if(p1 == 'add'){
					$.LoadingOverlay("show");
					param['editstatus'] = 'add';
					$.post(hostir+'smb/getDisplayView/skemaBiayaForm', param, function(resp){
						$('.modal-title').html("<b>Setting Skema Biaya Magiste</b>");
						$('#form_load').html(resp);
						$('#form_invent').modal('show');
						$.LoadingOverlay("hide", true);
					});
				}
				break;
		case "formSettingTarifkk":
			if(p1 == 'edit'){
				var row = $("#setting_tarif_kk").datagrid('getSelected');
				if(row){
					$.LoadingOverlay("show");
					param['editstatus'] = 'edit';
					param['id'] = row.kode;
					$.post(hostir+'smb/getDisplayView/formSettingTarifkk', param, function(resp){
						$('.modal-title').html("<b>Setting tarif Kelas Karyawan</b>");
						$('#form_load').html(resp);
						$('#form_invent').modal('show');
						$.LoadingOverlay("hide", true);
					});
				}else{
					$.messager.alert('Smart', "Please Select Data First!",'error');
				}
			}else if(p1 == 'add'){
				$.LoadingOverlay("show");
				param['editstatus'] = 'add';
				$.post(hostir+'smb/getDisplayView/formSettingTarifkk', param, function(resp){
					$('.modal-title').html("<b>Setting tarif Kelas Karyawan</b>");
					$('#form_load').html(resp);
					$('#form_invent').modal('show');
					$.LoadingOverlay("hide", true);
				});
			}
			break;
		case "formSettingTarif":
			if(p1 == 'edit'){
				var row = $("#setting_tarif_magister").datagrid('getSelected');
				if(row){
					$.LoadingOverlay("show");
					param['editstatus'] = 'edit';
					param['id'] = row.kode;
					$.post(hostir+'smb/getDisplayView/formSettingTarif', param, function(resp){
						$('.modal-title').html("<b>Setting tarif magister</b>");
						$('#form_load').html(resp);
						$('#form_invent').modal('show');
						$.LoadingOverlay("hide", true);
					});
				}else{
					$.messager.alert('Smart', "Please Select Data First!",'error');
				}
			}else if(p1 == 'add'){
				$.LoadingOverlay("show");
				param['editstatus'] = 'add';
				$.post(hostir+'smb/getDisplayView/formSettingTarif', param, function(resp){
					$('.modal-title').html("<b>Setting tarif magister</b>");
					$('#form_load').html(resp);
					$('#form_invent').modal('show');
					$.LoadingOverlay("hide", true);
				});
			}
			break;
		case "hasilSeleksi":
			var row = $("#hasilSeleksi").datagrid('getSelected');
			if(row){
				$.LoadingOverlay("show");
				param['editstatus'] = 'edit';
				param['id'] = row.kode;
				$.post(hostir+'smb/getDisplayView/formHasilSeleksi', param, function(resp){
					$('.modal-title').html("<b>Set Hasil Seleksi</b>");
					$('#form_load').html(resp);
					$('#form_invent').modal('show');
					$.LoadingOverlay("hide", true);
				});
			}else{
				$.messager.alert('Smart', "Please Select Data First!",'error');
			}
			break;
		case "tesTpaAction":
			var row = $("#tesTPA").datagrid('getSelected');
			if(row){
				if(p1 == 'detil'){
					$.LoadingOverlay("show");
					param['editstatus'] = 'edit';
					param['id'] = row.kode;
					$.post(hostir+'smb/getDisplayView/formTesTPA', param, function(resp){
						$('.modal-title').html("<b>Form Informasi Jadwal TPA</b>");
						$('#form_load').html(resp);
						$('#form_invent').modal('show');
						$.LoadingOverlay("hide", true);
					});
				}else if(p1 == 'confirm'){
					
					param['editstatus'] = 'edit';
					param['metode_acc'] = row.metode_purpose;
					param['tanggal_acc'] = row.tanggal_purpose;
					param['jam_acc'] = row.jam_purpose;
					param['jam_end_acc'] = row.jam_end_purpose;
					param['editstatus'] = 'edit';
					param['id'] = row.kode;
					$.post(hostir+'smb/simpandata/approval_jadwal_tpa', param, function(resp){
						if(resp == 1){
							$("#tesTPA").datagrid('reload');
							$.messager.alert('Smart', "Berhasil!");
						}else{
							$.messager.alert('Smart', "Gagal menyimpan data! "+ resp,'error');
						}
						$.LoadingOverlay("hide", true);
					});
				}
			}else{
				$.messager.alert('Smart', "Please Select Data First!",'error');
			}
		break;
		case "tesWawancaraAction":
			var row = $("#tesWawancara").datagrid('getSelected');
			if(row){
				if(p1 == 'detil'){
					$.LoadingOverlay("show");
					param['editstatus'] = 'edit';
					param['id'] = row.kode;
					$.post(hostir+'smb/getDisplayView/formTesWawancara', param, function(resp){
						$('.modal-title').html("<b>Form Informasi Jadwal</b>");
						$('#form_load').html(resp);
						$('#form_invent').modal('show');
						$.LoadingOverlay("hide", true);
					});
				}else if(p1 == 'confirm'){
					
					param['editstatus'] = 'edit';
					param['metode_acc'] = row.metode_purpose;
					param['tanggal_acc'] = row.tanggal_purpose;
					param['jam_acc'] = row.jam_purpose;
					param['editstatus'] = 'edit';
					param['id'] = row.kode;
					$.post(hostir+'smb/simpandata/approval_jadwal', param, function(resp){
						if(resp == 1){
							$("#tesWawancara").datagrid('reload');
							$.messager.alert('Smart', "Berhasil!");
						}else{
							$.messager.alert('Smart', "Gagal menyimpan data! "+ resp,'error');
						}
						$.LoadingOverlay("hide", true);
					});
				}
			}else{
				$.messager.alert('Smart', "Please Select Data First!",'error');
			}
			break;
		case "payment_reg_kk":
			var row = $("#payment_reg_kk").datagrid('getSelected');
			if(row){
				if(row.stsApplyPaid != 1 || p1 == 'detil'){
					$.LoadingOverlay("show");
					param['editstatus'] = 'edit';
					param['id'] = row.kode;
					$.post(hostir+'smb/getDisplayView/payment_reg_kk/'+p1, param, function(resp){
						$('.modal-title').html("<b>Set biaya formulir</b>");
						$('#form_load').html(resp);
						$('#form_invent').modal('show');
						$.LoadingOverlay("hide", true);
					});
				}else{
					$.messager.alert('Smart', "Peserta sudah melakukan pembayaran!",'error');
				}
			}else{
				$.messager.alert('Smart', "Please Select Data First!",'error');
			}
			break;
		case "payment_reg":
			var row = $("#payment_reg").datagrid('getSelected');
			if(row){
				if(row.stsApplyPaid != 1 || p1 == 'detil'){
					$.LoadingOverlay("show");
					param['editstatus'] = 'edit';
					param['id'] = row.kode;
					$.post(hostir+'smb/getDisplayView/payment_reg/'+p1, param, function(resp){
						$('.modal-title').html("<b>Set biaya formulir</b>");
						$('#form_load').html(resp);
						$('#form_invent').modal('show');
						$.LoadingOverlay("hide", true);
					});
				}else{
					$.messager.alert('Smart', "Peserta sudah melakukan pembayaran!",'error');
				}
			}else{
				$.messager.alert('Smart', "Please Select Data First!",'error');
			}
			break;
		
		case "ver_forlap_kk":
			var row = $("#ver_dok_kk").datagrid('getSelected');
			if(row){
				$.LoadingOverlay("show");
				if(p1){
					if(p1 == '2'){
						$.LoadingOverlay("show");
						param['editstatus'] = 'edit';
						param['id'] = row.kode;
						$.post(hostir+'smb/getDisplayView/tolak_forlap_kk/'+p1, param, function(resp){
							$('.modal-title').html("<b>Tolak Verifikasi Ijazah</b>");
							$('#form_load').html(resp);
							$('#form_invent').modal('show');
							$.LoadingOverlay("hide", true);
						});
					}else{
						param['editstatus'] = 'edit';
						param['id'] = row.kode;
						param['verifikasi_forlap'] = p1;
						$.post(hostir+'smb/simpandata/ver_forlap', param, function(resp){
							if(resp == 1){
								$("#ver_dok_kk").datagrid('reload');
								$.messager.alert('Smart', "Berhasil!");
							}else{
								$.messager.alert('Smart', "Gagal menyimpan data! "+ resp,'error');
							}
							$.LoadingOverlay("hide", true);
						});
					}
				}else{
					$.messager.alert('Smart', "Invalid action! ",'error');
				}
			}else{
				$.messager.alert('Smart', "Please Select Data First!",'error');
			}
			break;
		case "ver_forlap":
			var row = $("#ver_dok").datagrid('getSelected');
			if(row){
				$.LoadingOverlay("show");
				if(p1){
					if(p1 == '2'){
						$.LoadingOverlay("show");
						param['editstatus'] = 'edit';
						param['id'] = row.kode;
						$.post(hostir+'smb/getDisplayView/tolak_forlap/'+p1, param, function(resp){
							$('.modal-title').html("<b>Tolak Verifikasi Ijazah</b>");
							$('#form_load').html(resp);
							$('#form_invent').modal('show');
							$.LoadingOverlay("hide", true);
						});
					}else{
						param['editstatus'] = 'edit';
						param['id'] = row.kode;
						param['verifikasi_forlap'] = p1;
						$.post(hostir+'smb/simpandata/ver_forlap', param, function(resp){
							if(resp == 1){
								$("#ver_dok").datagrid('reload');
								$.messager.alert('Smart', "Berhasil!");
							}else{
								$.messager.alert('Smart', "Gagal menyimpan data! "+ resp,'error');
							}
							$.LoadingOverlay("hide", true);
						});
					}
				}else{
					$.messager.alert('Smart', "Invalid action! ",'error');
				}
			}else{
				$.messager.alert('Smart', "Please Select Data First!",'error');
			}
			break;
		case "edit_invent":
			var row = $("#invent").datagrid('getSelected');
			if(row){
				// $.LoadingOverlay("show");
				if(row.sts_daftar == 'Registered'){
					$.messager.alert('Smart', "Invent sudah bersatus Register!",'error');
				}else{
					param['editstatus'] = 'edit';
					param['id'] = row.id;
					$.post(hostir+'smb/getDisplay/form_invent', param, function(resp){
						$('.modal-title').html("<b>Edit Data Invent</b>");
						$('#form_load').html(resp);
						$('#form_invent').modal('show');
						// $.LoadingOverlay("hide", true);
					});

				}
			}else{
				$.messager.alert('Smart', "Please Select Data First!",'error');
			}
		break;
		case 'search_invent':
			$('#'+p1).datagrid('load',{
				nama_cm: $('#nama').val(),
				method: "search",
			});
			break;

	}
}


function loadUrl(urls,func,parentMenu){	
	$("#tMain").html("").addClass("loading");
	
	// if(parentMenu){
	// 	parentMenu = parentMenu.replace("/", "");
	// }
	
    $('li').removeClass("active");
	$('#menu_'+func).addClass("active");
	$('#menu_'+parentMenu).addClass("active");
	
	$.get(urls,function (html){
	    $("#tMain").html(html).removeClass("loading");
    }).done(function(){
       // func;
    });
	//*/
}

function sendwa_mm(name, phone, voucher, exp_date, edu, sent_wa){
	var isMobile = {
		Android: function() {
		return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
		return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
		return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
		return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
		return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};

	text ="Kepada sdr/i  *"+name+"* "
		+" \r\nTerimakasih telah mengisi buku tamu kami, Perkenalkan saya "+edu+" Edu Consultant  Program Magister Manajemen Universitas Bakrie."
		+" \r\n\r\*MM-UB telah Terakreditasi A BAN-PT, Mampu mencetak para pelaku bisnis dan Top Management yang handal, inovatif, dan berwawasan global."
		+" \r\n\r\n*Program MM-UB di dukungan oleh Kelompok Usaha Bakrie dengan lebih dari 250 unit usaha telah membuktikan eksistensinya sejak tahun 1942."
		+" \r\n\r\n*Universitas Bakrie mempunyai program potongan 50% biaya Formulir Pendaftaran dan potongan biaya pertama kuliah sebesar 1.000.000, Program MM-UB juga mempunyai Skema angsuran biaya kuliah yg dapat dicicil setiap bulan.*"
		+" \r\n\r\nUntuk tata cara pendaftaran di aplikasi smart, kamu dapat melihatnya di bit.ly/tutorialsmartbakrie"
		+" \r\n\r\nBila membutuhkan informasi lebih lanjut, silahkan menghubungi saya di Nomer ini." 
		+" \r\nSalam," 
		+" \r\n\r\*"+edu+"*"
		+" \r\n\r\Edu Consultant MM-UB.";

	var message = encodeURIComponent(text);

	if( isMobile.any() ) {
		//mobile device
		var whatsapp_API_url = "whatsapp://send";
		whatsapp_API_url = whatsapp_API_url+'?phone=' + phone + '&text=' + message
		// $(this).attr( 'href', whatsapp_API_url+'?phone=' + phone + '&text=' + message );
	} else {
		//desktop
		// var whatsapp_API_url = "https://web.whatsapp.com/send";
		// whatsapp_API_url = whatsapp_API_url+'?phone='+ phone + '&text=' + message 
		var whatsapp_API_url = "https://wa.me/";
		whatsapp_API_url = whatsapp_API_url+ phone + '?text=' + message 
		// $(this).attr( 'href', whatsapp_API_url+'?phone=' + phone + '&text=' + message );
	}
	counted_sent = Number(sent_wa) + 1;
	$.post(hostir+'smb/sendWaMagister', {'hp':phone, 'count_wa':counted_sent}, function(resp){
		$('#invent_mm').datagrid('reload');
	});
	window.open(whatsapp_API_url);
}

function sendwa(name, phone, voucher, exp_date, edu, sent_wa){
	var isMobile = {
		Android: function() {
		return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
		return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
		return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
		return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
		return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};

	text ="Terima kasih *"+name+"* telah mengajukan permohonan bebas biaya pendaftaran di Universitas Bakrie "
		+" untuk tahun ajaran 2021/2022,"
		+" \r\n\r\nPerkenalkan saya dengan *"+edu+"* dari Educonsultant Universitas Bakrie, siap membantu pendaftaran kamu."
		+" \r\n\r\nberikut kode vouchernya *"+voucher+"*. Masukan voucher pada"
		+" pendaftaran online di smart.bakrie.ac.id"
		+" \r\n\r\nVoucher berlaku sampai *"+exp_date+"*."
		+" \r\n\r\nUntuk tata cara pendaftaran di aplikasi smart, kamu dapat melihatnya di bit.ly/tutorialsmartbakrie"
		+" \r\n\r\nMohon menyimpan nomor whatsapp ini." 
		+" \r\nSegala kendala terkait pendaftaran dapat diinfokan melalui nomor ini ya." 
		+" \r\n\r\nTerima kasih.";

	var message = encodeURIComponent(text);

	if( isMobile.any() ) {
		//mobile device
		var whatsapp_API_url = "whatsapp://send";
		whatsapp_API_url = whatsapp_API_url+'?phone=' + phone + '&text=' + message
		// $(this).attr( 'href', whatsapp_API_url+'?phone=' + phone + '&text=' + message );
	} else {
		//desktop
		// var whatsapp_API_url = "https://web.whatsapp.com/send";
		// whatsapp_API_url = whatsapp_API_url+'?phone='+ phone + '&text=' + message 
		var whatsapp_API_url = "https://wa.me/";
		whatsapp_API_url = whatsapp_API_url+ phone + '?text=' + message 
		// $(this).attr( 'href', whatsapp_API_url+'?phone=' + phone + '&text=' + message );
	}
	counted_sent = Number(sent_wa) + 1;
	$.post(hostir+'smb/sendWa', {'hp':phone, 'count_wa':counted_sent}, function(resp){
		$('#invent').datagrid('reload');
	});
	window.open(whatsapp_API_url);
}

function genGrid(modnya, lebarnya, tingginya, p1, p2){
	if(lebarnya == undefined){
		lebarnya = getClientWidth-230;
	}
	if(tingginya == undefined){
		tingginya = getClientHeight-350
	}

	var kolom ={};
	var frozen ={};
	var judulnya;
	var param={};
	var urlnya;
	var urlglobal="";
	var fitnya;
	var pagesizeboy = 10;
	
	switch(modnya){
		case "hasilSeleksiKK":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'nomor', title:'<b>Nomor Registrasi</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'nama', title:'<b>Nama lengkap</b>', halign:'left', width:300,align:'left',sortable:true},
				{field:'singkatan', title:'<b>Program Studi</b>', halign:'left', width:200,align:'left',sortable:true},
				{field:'stsResultConfirm', title:'<b>Hasil Seleksi</b>', halign:'left', width:450,align:'left',sortable:true, 
					formatter: function(val, row, i){
						if(row.stsResultConfirm == '1'){
							if(row.stsResultPass == '1'){
								return "<span class='btn btn-xs btn-success'><i class='icon-check'></i> Lulus Seleksi</span>";
							}else{
								return "<span class='btn btn-xs btn-danger'><i class='icon-remove-sign'></i> Tidak Lulus</span>";
							}
						}else{
							return "<span class='btn btn-xs btn-warning'><i class='icon-time'></i> Belum diperiksa</span>";
						}
					}
				}
			];
			break;
		case 'modulPrivilege':
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'level_name', title:'<b>User Level</b>', halign:'left', width:200,align:'left',sortable:true},
				{field:'modul', title:'<b>Module Access</b>', halign:'left', width:600,align:'left',sortable:true},
			];
			break
		case 'masterProdi':
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'kode', title:'<b>Kode </b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'idBig', title:'<b>ID/Kode BIG </b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'singkatan', title:'<b>Singkatan</b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'nama', title:'<b>Nama Program Studi</b>', halign:'left',width:300, align:'left',sortable:true},
				{field:'jenjang', title:'<b>Jenjang</b>', halign:'left', align:'left',width:100,sortable:true},
				{field:'lulusanType', title:'<b>Tipe Lulusan</b>', halign:'left',width:100, align:'left',sortable:true},
				{field:'kaprodi', title:'<b>Ketua Prodi</b>', halign:'left',width:250, align:'left',sortable:true},
			];
			break;
		case 'masterAgama':
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'kode', title:'<b>Kode </b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'idBig', title:'<b>ID/Kode BIG </b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'nama', title:'<b>Nama Agama</b>', halign:'left', align:'left',sortable:true},
			];
			break;
		case "invent_kk":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 50;
			kolom[modnya] = [
				{field:'count_day', title:'<b>Count</b>', halign:'center', valign:'center', width:50,align:'center',
					formatter: function(value, row, i){
						if(Number(row.count_day) >= 1){
							return '+'+row.count_day;
						}else{
							return '0';
						}
					}
				},
				{field:'createTime', title:'<b>Tanggal Daftar</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'sts_daftar', title:'<b>Smart</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'status_contact', title:'<b>Status Contact</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'hp', title:'<b>No HP</b>', halign:'left', width:150, align:'left',sortable:false, 
					formatter: function(value, row, i){
						var html = row.hp;
						if(row.send_wa >= 1){
							html += ' <img width="24px" style="margin-left:8px;" src="'+hostir+'assets/img/social/wa-don.png" onclick="sendwa_mm(\''+row.nama_lengkap+'\', \''+row.hp+'\', \''+row.kode_voucher+'\', \''+row.voucher_expired+'\', \''+row.fullname+'\', \''+row.send_wa+'\')">'
						}else{
							html += ' <img width="25px" style="margin-left:8px;" src="'+hostir+'assets/img/social/wa-non.png" onclick="sendwa_mm(\''+row.nama_lengkap+'\', \''+row.hp+'\', \''+row.kode_voucher+'\', \''+row.voucher_expired+'\', \''+row.fullname+'\', \'0\')">'
						}
						return html;
					}
				},
				{field:'fullname', title:'<b>EC</b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'nama_lengkap', title:'<b>Nama Lengkap</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'email', title:'<b>Email</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'lulusan', title:'<b>Lulusan</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'asal', title:'<b>Asal PT</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'sumber', title:'<b>Sumber</b>', halign:'center', width:150,align:'left',sortable:true,
					formatter: function(value, row, i){
						if(value != 'Direct Link'){
							var _src = JSON.parse(value);
							html = "<small>utm_source : "+_src.utm_source;
							html += ", utm_medium : "+_src.utm_medium;
							html += "<br>utm_campaign : "+_src.utm_campaign;
							html += ", gclid : "+_src.gclid+'</small>';

							return html;
						}else{
							return value;
						}
					}
				},
				// {field:'nama_sekolah', title:'<b>Nama Sekolah</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'prodi', title:'<b>Prodi</b>', halign:'center', width:150,align:'left',sortable:true},
				// {field:'cgts', title:'<b>CGTS</b>', halign:'center', width:150,align:'left',sortable:true},
			];
		break;
		case "cmbkk":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 50;
			kolom[modnya] = [
				{field:'foto', title:'<b>DATA PESERTA</b>', halign:'center', width:150,align:'center',sortable:true,
					formatter: function(value,row,index){
							return  '<div class="tiles">'+
									'	<div class="tile image">'+
									'		<a  style="color:black" class="accordion-toggle" target="_blank" href="'+hosturi+'smb/profil/'+row.no_smb+'">'+
									'		<div class="tile-body">'+				            
									'			<img class="img-responsive" alt="" src="'+hosturi+'assets/upload/foto/'+value+'">'+
									'		</div>'+
									'		<div class="tile-object" style="background-color:#006895">'+
									'			<div class="name">'+row.no_smb+'</div>'+
									'		</div>'+
									'		</a>'+
									'	</div>'+
									'</div>';
					}
				},
				{field:'no_smb',title:'<b>NOMOR REG</b>',width:150, halign:'left',align:'left', hidden:true },
				{field:'nama_cm',title:'<b>NAMA</b>', halign:'left',width:275, align:'left',resizable:true, sortable:true, 
					formatter: function(val, row, idx){
							return '<a href="#" ><h4><b>' + val + '</b></h4></a>'+
									'<a  class="btn btn-xs red"  href="#" onclick="deleteMhs(\'del\',\''+ row.no_smb +'\')"><i class="icon-remove-sign"></i> Delete</a>';
					}},
				{field:'progdi_inisial',title:'<b>PRODI</b>', halign:'left',width:75, align:'left',sortable:true, 
					formatter: function(val, row, idx){ return '<h4>' + val + '</h4>';}},
				{field:'jalur_penerimaan',title:'<b>JALUR</b>', halign:'left',width:150, align:'left',sortable:true, 
					formatter: function(val, row, idx){ return '<h4>' + val + '</h4>';}},
				{field:'tanggal',title:'<b>TANGGAL DAFTAR</b>', halign:'left',width:150, align:'left',sortable:true, 
					formatter: function(val, row, idx){ return '<h4>' + val + '</h4>';}},
				{field:'status',title:'<b>STATUS</b>', halign:'left',width:400, align:'left',
					formatter: function(value,row,index){
						var status = new Array();
						status = value.split('-');
						
						var stsTahap = new Array();
						var icon = new Array();
						
						for(var i = 0; i < status.length; i++){
							if (status[i] == 1){
								stsTahap[i] = "primary";
								icon[i] = ' <i class="icon-ok-sign"></i>';
							}else{
								stsTahap[i] = 'default';
								icon[i] = ' <i class="icon-remove-sign"></i>';
							}
							
						}
						
						var step = [	"Mendaftar ", 
										"Verifikasi Ijazah", 
										"Membayar Formulir Pendaftaran", 
										"Jadwal Ujian & Wawancara", 
										"Hasil Ujian ", 
										"Pengumuman ", 
										"Membayar Biaya Daftar Ulang ", 
										"Melengkapi Form Pendaftaran ", 
										"Mendapat Jadwal Perkuliahan " 
									]
						html = "";
						for (var i = 0; i < step.length; i++){
							html += '<span class="label label-' + stsTahap[i] +' "><i> ' + step[i] + icon[i] + '</i> </span><br>';
						}
						return html;
					}						
				},
			];                
		break;
		case "man_edu":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'kode', title:'<b>Kode Edu</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'fullname', title:'<b>Nama Lengkap</b>', halign:'left', width:250,align:'left',sortable:true},
				{field:'username', title:'<b>Username </b>', halign:'left', width:200,align:'left',sortable:true},
				{field:'aktif_edu', title:'<b>Status</b>', halign:'center', width:100,align:'center',sortable:true,
					formatter: function(val, row, i){
						html = '<i class="icon-check" onclick="kumpulAction(\'set_edu\', \''+row.kode+'\', \'aktif_edu\', \'0\')">';
						if(val != '1'){
							html = '<i class="icon-minus" onclick="kumpulAction(\'set_edu\', \''+row.kode+'\', \'aktif_edu\', \'1\')">';
						}
						return html;
					}
				},
				{field:'is_reg', title:'<b>S1</b>', halign:'left', width:50,align:'left',sortable:true,
				formatter: function(val, row, i){
					html = '<i class="icon-check" onclick="kumpulAction(\'set_edu\', \''+row.kode+'\', \'is_reg\', \'0\')">';
					if(val != '1'){
						html = '<i class="icon-check-empty" onclick="kumpulAction(\'set_edu\', \''+row.kode+'\', \'is_reg\', \'1\')">';
					}
					return html;
				}},
				{field:'is_mag', title:'<b>S2</b>', halign:'left', width:50,align:'left',sortable:true,
				formatter: function(val, row, i){
					html = '<i class="icon-check" onclick="kumpulAction(\'set_edu\', \''+row.kode+'\', \'is_mag\', \'0\')">';
					if(val != '1'){
						html = '<i class="icon-check-empty" onclick="kumpulAction(\'set_edu\', \''+row.kode+'\', \'is_mag\', \'1\')">';
					}
					return html;
				}},
				{field:'is_kk', title:'<b>KK</b>', halign:'left', width:50,align:'left',sortable:true,
				formatter: function(val, row, i){
					html = '<i class="icon-check" onclick="kumpulAction(\'set_edu\', \''+row.kode+'\', \'is_kk\', \'0\')">';
					if(val != '1'){
						html = '<i class="icon-check-empty" onclick="kumpulAction(\'set_edu\', \''+row.kode+'\', \'is_kk\', \'1\')">';
					}
					return html;
				}},
			];
			break;
		case "daftarulangKK":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'nomor', title:'<b>Nomor Reg.</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'nama', title:'<b>Nama </b>', halign:'left', width:300,align:'left',sortable:true},
				{field:'singkatan', title:'<b>Prodi</b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'stsReapplyPaid', title:'<b>Status</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(val, row, i){
						if(row.stsReapplyPaidConfirm == '1'){
							return "<span class='btn btn-xs btn-success'><i class='icon-check'></i> Sudah Dikonfirmasi</span>";
						}else{
							if(val == '1'){
								return "<span class='btn btn-xs btn-warning'><i class='icon-time'></i> Menunggu Konfirmasi</span>";
							}else{
								return "<span class='btn btn-xs btn-danger'><i class='icon-remove-sign'></i> Belum Bayar</span>";
							}
						}
					}
				},
				{field:'reapplyBankTransferAmount', title:'<b>Tagihan</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(val, row, i){
						return "Rp "+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
					}
				},
				{field:'buktiBayarDaftarUlang', title:'<b>Bukti Bayar</b>', halign:'left', width:250,align:'left',
					formatter: function(val, row, i){
						if(val){
							return "<a class='btn btn-xs btn-info' target='_blank' href='"+hostir+"assets/upload/kk/"+row.nomor+"/"+val+"'><i class='icon-picture'></i> "+val+"</a>";
						}else{
							return '';
						}
					}
				},
			];
			break;
		case "daftarulangMag":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'nomor', title:'<b>Nomor Reg.</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'nama', title:'<b>Nama </b>', halign:'left', width:300,align:'left',sortable:true},
				{field:'singkatan', title:'<b>Prodi</b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'reapplyMaxDate', title:'<b>Batas Bayar</b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'stsReapplyPaid', title:'<b>Status</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(val, row, i){
						if(row.stsReapplyPaidConfirm == '1'){
							return "<span class='btn btn-xs btn-success'><i class='icon-check'></i> Sudah Dikonfirmasi</span>";
						}else{
							if(val == '1'){
								return "<span class='btn btn-xs btn-warning'><i class='icon-time'></i> Menunggu Konfirmasi</span>";
							}else{
								return "<span class='btn btn-xs btn-danger'><i class='icon-remove-sign'></i> Belum Bayar</span>";
							}
						}
					}
				},
				{field:'reapplyBankTransferAmount', title:'<b>Tagihan</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(val, row, i){
						return "Rp "+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
					}
				},
				{field:'buktiBayarDaftarUlang', title:'<b>Bukti Bayar</b>', halign:'left', width:250,align:'left',
					formatter: function(val, row, i){
						if(val){
							return "<a class='btn btn-xs btn-info' target='_blank' href='"+hostir+"assets/upload/magister/"+row.nomor+"/"+val+"'><i class='icon-picture'></i> "+val+"</a>";
						}else{
							return '';
						}
					}
				},
			];
			break;
		case "mahasiswaKK":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'nama', title:'<b>Nama</b>', halign:'left', width:300,align:'left',sortable:true},
				{field:'nim', title:'<b>NIM</b>', halign:'left', width:250,align:'left',sortable:true},
				{field:'singkatan', title:'<b>Program Studi</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'stsMundurAfterReapply', title:'<b>Mundur</b>', halign:'left', width:150,align:'left',sortable:true,
					formatter: function(val, row, i){
						if(val == 1){
							return "<a class='btn btn-xs btn-danger' href='javascript:;'> Mengundurkan Diri</a>";
						}else{
							return '';
						}
					}
				},
				{field:'stsIntegrasikeBIG', title:'<b>Status POST </b>', halign:'left', width:150,align:'left',sortable:true,
					formatter: function(val, row, i){
						if(val == 1){
							return "<a class='btn btn-xs btn-success' href='javascript:;'><i class='icon-check'></i> Berhasil</a>";
						}else{
							return '';
						}
					}
				}
			];
			break;
		case "mahasiswaS2":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'nama', title:'<b>Nama</b>', halign:'left', width:300,align:'left',sortable:true},
				{field:'nim', title:'<b>NIM</b>', halign:'left', width:250,align:'left',sortable:true},
				{field:'singkatan', title:'<b>Program Studi</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'stsMundurAfterReapply', title:'<b>Mundur</b>', halign:'left', width:150,align:'left',sortable:true,
					formatter: function(val, row, i){
						if(val == 1){
							return "<a class='btn btn-xs btn-danger' href='javascript:;'> Mengundurkan Diri</a>";
						}else{
							return '';
						}
					}
				},
				{field:'stsIntegrasikeBIG', title:'<b>Status POST </b>', halign:'left', width:150,align:'left',sortable:true,
					formatter: function(val, row, i){
						if(val == 1){
							return "<a class='btn btn-xs btn-success' href='javascript:;'><i class='icon-check'></i> Berhasil</a>";
						}else{
							return '';
						}
					}
				}
			];
			break;
		case "beasiswaMag":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'kode', title:'<b>Kode</b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'deskripsi', title:'<b>Nama Beasiswa</b>', halign:'left', width:300,align:'left',sortable:true},
				{field:'potongan_percent', title:'<b>Potongan (%) </b>', halign:'left', width:100,align:'left',sortable:true}
			];
			break;
		case "setPotonganMag":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'singkatan', title:'<b>Program Studi</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'nama_skema', title:'<b>Skema Pembayaran</b>', halign:'left', width:250,align:'left',sortable:true},
				{field:'nilai_potongan_percent', title:'<b>Potongan (%) </b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'total', title:'<b>Total Biaya</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}
				},
				{field:'tagihan_1', title:'<b>Tagihan Pertama</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}},
				{field:'tagihan_angsuran', title:'<b>Tagihan N</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}}
			];
			break;
		case "skemaBiaya":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'singkatan', title:'<b>Program Studi</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'nama_skema', title:'<b>Skema Pembayaran</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'deskripsi', title:'<b>Keterangan</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'totalBiaya', title:'<b>Total Biaya</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}
				},
				{field:'tagihan_pertama', title:'<b>Tagihan Pertama</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}},
				{field:'tagihan_angsuran', title:'<b>Tagihan N</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}},
				{field:'potongan', title:'<b>Potongan</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}}
			];
			break;
		case "setting_tarif_kk":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'prodi', title:'<b>PROGRAM STUDI</b>', halign:'left', width:200,align:'left',sortable:true},
				{field:'almamater', title:'<b>JAKET ALMAMATER</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}},
				{field:'registrasi', title:'<b>BIAYA REGISTRASI</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}
				},
				{field:'uangPangkal', title:'<b>UANG PANGKAL</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}},
				{field:'matrikulasi', title:'<b>MATRIKULASI</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}},
				{field:'totalBiaya', title:'<b>TOTAL BIAYA</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}},
			];
			break;
		case "setting_tarif_magister":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'prodi', title:'<b>PROGRAM STUDI</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'biayaRegistrasi', title:'<b>BIAYA REGISTRASI</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}
				},
				{field:'biayaAlmamater', title:'<b>JAKET ALMAMATER</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}},
				{field:'uangMasuk', title:'<b>DAFTAR ULANG</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}},
				{field:'biayaSpp', title:'<b>BPP TOTAL</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}},
				{field:'biayaSks', title:'<b>SKS TOTAL</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}},
				{field:'biayaTesis', title:'<b>BIAYA TESIS</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}},
				{field:'totalBiaya', title:'<b>TOTAL BIAYA</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(v, r, i){
						return v ? "Rp "+v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : '';
					}},
			];
			break;
		case "hasilSeleksi":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'nomor', title:'<b>Nomor Registrasi</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'nama', title:'<b>Nama lengkap</b>', halign:'left', width:300,align:'left',sortable:true},
				{field:'singkatan', title:'<b>Program Studi</b>', halign:'left', width:200,align:'left',sortable:true},
				{field:'stsResultConfirm', title:'<b>Hasil Seleksi</b>', halign:'left', width:450,align:'left',sortable:true, 
					formatter: function(val, row, i){
						if(row.stsResultConfirm == '1'){
							if(row.stsResultPass == '1'){
								return "<span class='btn btn-xs btn-success'><i class='icon-check'></i> Lulus Seleksi</span>";
							}else{
								return "<span class='btn btn-xs btn-danger'><i class='icon-remove-sign'></i> Tidak Lulus</span>";
							}
						}else{
							return "<span class='btn btn-xs btn-warning'><i class='icon-time'></i> Belum diperiksa</span>";
						}
					}
				}
			];
			break;
		case "tesTPA":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'nomor', title:'<b>Nomor Registrasi</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'nama', title:'<b>Nama lengkap</b>', halign:'left', width:300,align:'left',sortable:true},
				{field:'singkatan', title:'<b>Program Studi</b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'is_acc', title:'<b>Status</b>', halign:'left', width:200,align:'left',sortable:true, 
					formatter: function(val, row, i){
						if(row.tanggal){
							if(row.is_acc == '1'){
								return "<span class='btn btn-xs btn-success'><i class='icon-check'></i> Dikonfirasi</span>";
							}else{
								return "<span class='btn btn-xs btn-warning'><i class='icon-time'></i> Menunggu Konfirmasi</span>";
							}
						}else{
							return "<span class='btn btn-xs btn-danger'><i class='icon-exclamation-sign'></i> Belum disetting</span>";
						}
					}
				},
				{field:'metode', title:'<b>Metode</b>', halign:'left', width:250,align:'left',sortable:true, 
					formatter: function(val, row, i){
						if(val){
							return val == 'ONSITE' ? 'Hadir ke Kampus 1 (Plaza Festival)' : 'Online Melalui Aplikasi Zoom';
						}
						return val;
					}
				},
				{field:'tanggal', title:'<b>Tanggal</b>', halign:'left', width:150,align:'left',
					formatter: function(val, row, i){
						return val;
					}
				},
				{field:'jam_purpose', title:'<b>Jam</b>', halign:'left', width:150,align:'left',
					formatter: function(val, row, i){
						if(row.is_acc == '1'){
							return row.jam_acc+' - '+row.jam_end_acc;
						}else{
							return row.jam_purpose+' - '+row.jam_end_purpose;
						}
					}
				}
			];
			break;
		case "tesWawancara":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'nomor', title:'<b>Nomor Registrasi</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'nama', title:'<b>Nama lengkap</b>', halign:'left', width:300,align:'left',sortable:true},
				{field:'singkatan', title:'<b>Program Studi</b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'is_acc', title:'<b>Status</b>', halign:'left', width:200,align:'left',sortable:true, 
					formatter: function(val, row, i){
						if(row.tanggal){
							if(row.is_acc == '1'){
								return "<span class='btn btn-xs btn-success'><i class='icon-check'></i> Dikonfirasi</span>";
							}else{
								return "<span class='btn btn-xs btn-warning'><i class='icon-time'></i> Menunggu Konfirmasi</span>";
							}
						}else{
							return "<span class='btn btn-xs btn-danger'><i class='icon-exclamation-sign'></i> Belum disetting</span>";
						}
					}
				},
				{field:'metode', title:'<b>Metode</b>', halign:'left', width:250,align:'left',sortable:true, 
					formatter: function(val, row, i){
						if(val){
							return val == 'ONSITE' ? 'Hadir ke Kampus 1 (Plaza Festival)' : 'Online Melalui Aplikasi Zoom';
						}
						return val;
					}
				},
				{field:'tanggal', title:'<b>Tanggal</b>', halign:'left', width:150,align:'left',
					formatter: function(val, row, i){
						return val;
					}
				},
				{field:'jam', title:'<b>Jam</b>', halign:'left', width:150,align:'left',
					formatter: function(val, row, i){
						if(val){
							if(val == "1"){
								return "Pagi 09-12 WIB";
							}else if(val == "2"){
								return "Siang 12-15 WIB";
							}else if(val == "3"){
								return "Siang 15-17 WIB";
							}
						}
						return val;
					}
				},
				{field:'fc_ijazah', title:'<b>File</b>', halign:'left', width:250,align:'left',
					formatter: function(val, row, i){
						if(val){
							html = " <a class='btn btn-xs btn-default' target='_blank' href='"+hostir+"assets/upload/magister/"+row.nomor+"/"+val+"'><i class='icon-picture'></i> File Ijazah</a>"
							html += " <a class='btn btn-xs btn-default' target='_blank' href='"+hostir+"assets/upload/magister/"+row.nomor+"/"+val+"'><i class='icon-file'></i> Transkrip nilai</a>"
							return html;
						}
						return val;
					}
				},
			];
			break;
		case "payment_reg_kk":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'nomor', title:'<b>Nomor Registrasi</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'nama', title:'<b>Nama lengkap</b>', halign:'left', width:300,align:'left',sortable:true},
				{field:'singkatan', title:'<b>Program Studi</b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'stsApplyPaid', title:'<b>Status</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(val, row, i){
						if(row.stsApplyPaidConfirm == '1'){
							return "<span class='btn btn-xs btn-success'><i class='icon-check'></i> Sudah Dikonfirmasi</span>";
						}else{
							if(val == '1'){
								return "<span class='btn btn-xs btn-warning'><i class='icon-time'></i> Menunggu Konfirmasi</span>";
							}else{
								return "<span class='btn btn-xs btn-danger'><i class='icon-remove-sign'></i> Belum Bayar</span>";
							}
						}
					}
				},
				{field:'applyBankTransferAmount', title:'<b>Biaya</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(val, row, i){
						return "Rp "+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
					}
				},
				{field:'buktiBayarPendaftaran', title:'<b>Bukti Bayar</b>', halign:'left', width:250,align:'left',
					formatter: function(val, row, i){
						if(val){
							return "<a class='btn btn-xs btn-info' target='_blank' href='"+hostir+"assets/upload/bukti_bayar/"+val+"'><i class='icon-picture'></i> "+val+"</a>";
						}else{
							return '';
						}
					}
				},
			];
			break;
		case "payment_reg":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'nomor', title:'<b>Nomor Registrasi</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'nama', title:'<b>Nama lengkap</b>', halign:'left', width:300,align:'left',sortable:true},
				{field:'singkatan', title:'<b>Program Studi</b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'stsApplyPaid', title:'<b>Status</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(val, row, i){
						if(row.stsApplyPaidConfirm == '1'){
							return "<span class='btn btn-xs btn-success'><i class='icon-check'></i> Sudah Dikonfirmasi</span>";
						}else{
							if(val == '1'){
								return "<span class='btn btn-xs btn-warning'><i class='icon-time'></i> Menunggu Konfirmasi</span>";
							}else{
								return "<span class='btn btn-xs btn-danger'><i class='icon-remove-sign'></i> Belum Bayar</span>";
							}
						}
					}
				},
				{field:'applyBankTransferAmount', title:'<b>Biaya</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(val, row, i){
						return "Rp "+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
					}
				},
				{field:'buktiBayarPendaftaran', title:'<b>Bukti Bayar</b>', halign:'left', width:250,align:'left',
					formatter: function(val, row, i){
						if(val){
							return "<a class='btn btn-xs btn-info' target='_blank' href='"+hostir+"assets/upload/bukti_bayar/"+val+"'><i class='icon-picture'></i> "+val+"</a>";
						}else{
							return '';
						}
					}
				},
			];
			break;
		case 'ver_dok_kk':
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'nomor', title:'<b>Nomor Registrasi</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'rumahCell', title:'<b>Nomor HP</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'nama', title:'<b>Nama lengkap</b>', halign:'left', width:300,align:'left',sortable:true},
				{field:'verifikasi_forlap', title:'<b>Status</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(val, row, i){
						if(val == '1'){
							return "<span class='btn btn-xs btn-success'><i class='icon-check'></i> Terverifikasi</span>";
						}else if(val == '2'){
							return "<span class='btn btn-xs btn-danger'><i class='icon-remove-sign'></i> Ditolak</span>";
						}else{
							return "<span class='btn btn-xs btn-warning'><i class='icon-time'></i> Pending</span>";
						}
					}
				},
				{field:'lulusankk', title:'<b>Lulusan</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'singkatan', title:'<b>Program Studi</b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'lulusAsal', title:'<b>PTN Asal</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'jurusan', title:'<b>Prodi Asal</b>', halign:'left', width:150,align:'left',sortable:true},
			];
			break;
		case "ver_dok":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'nomor', title:'<b>Nomor Registrasi</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'rumahCell', title:'<b>Nomor HP</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'nama', title:'<b>Nama lengkap</b>', halign:'left', width:300,align:'left',sortable:true},
				{field:'verifikasi_forlap', title:'<b>Status</b>', halign:'left', width:150,align:'left',sortable:true, 
					formatter: function(val, row, i){
						if(val == '1'){
							return "<span class='btn btn-xs btn-success'><i class='icon-check'></i> Terverifikasi</span>";
						}else if(val == '2'){
							return "<span class='btn btn-xs btn-danger'><i class='icon-remove-sign'></i> Ditolak</span>";
						}else{
							return "<span class='btn btn-xs btn-warning'><i class='icon-time'></i> Pending</span>";
						}
					}
				},
				{field:'singkatan', title:'<b>Program Studi</b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'lulusAsal', title:'<b>PTN Asal</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'jurusan', title:'<b>Prodi Asal</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'tahunLulus', title:'<b>Tahun lulus</b>', halign:'left', width:150,align:'left',sortable:true},
			];
			break;
		case "telemarketing_kk":
		case "telemarketing":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 30;
			kolom[modnya] = [
				{field:'tele_date_update', title:'<b>Tanggal Diubah</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'tglDaftar', title:'<b>Tanggal Daftar</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'nama_cm', title:'<b>NAMA CMB</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'rumahCell', title:'<b>NO. TELEPON</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'progdi', title:'<b>PRODI</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'edu', title:'<b>EDU</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'ods', title:'<b>ODS</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'status_pay', title:'<b>STATUS</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'kode_smb', title:'<b>ACTION</b>', halign:'center', width:150,align:'center',sortable:true},
			];
			break;
		case "invent_mm":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 50;
			kolom[modnya] = [
				{field:'count_day', title:'<b>Count</b>', halign:'center', valign:'center', width:50,align:'center',
					formatter: function(value, row, i){
						if(Number(row.count_day) >= 1){
							return '+'+row.count_day;
						}else{
							return '0';
						}
					}
				},
				{field:'createTime', title:'<b>Tanggal Daftar</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'sts_daftar', title:'<b>Smart</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'status_contact', title:'<b>Status Contact</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'hp', title:'<b>No HP</b>', halign:'left', width:150, align:'left',sortable:false, 
					formatter: function(value, row, i){
						var html = row.hp;
						if(row.send_wa >= 1){
							html += ' <img width="24px" style="margin-left:8px;" src="'+hostir+'assets/img/social/wa-don.png" onclick="sendwa_mm(\''+row.nama_lengkap+'\', \''+row.hp+'\', \''+row.kode_voucher+'\', \''+row.voucher_expired+'\', \''+row.fullname+'\', \''+row.send_wa+'\')">'
						}else{
							html += ' <img width="25px" style="margin-left:8px;" src="'+hostir+'assets/img/social/wa-non.png" onclick="sendwa_mm(\''+row.nama_lengkap+'\', \''+row.hp+'\', \''+row.kode_voucher+'\', \''+row.voucher_expired+'\', \''+row.fullname+'\', \'0\')">'
						}
						return html;
					}
				},
				{field:'fullname', title:'<b>EC</b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'nama_lengkap', title:'<b>Nama Lengkap</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'email', title:'<b>Email</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'lulusan', title:'<b>Asal Lulusan</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'sumber', title:'<b>Sumber</b>', halign:'center', width:150,align:'left',sortable:true,
					formatter: function(value, row, i){
						if(value != 'Direct Link'){
							var _src = JSON.parse(value);
							html = "<small>utm_source : "+_src.utm_source;
							html += ", utm_medium : "+_src.utm_medium;
							html += "<br>utm_campaign : "+_src.utm_campaign;
							html += ", gclid : "+_src.gclid+'</small>';

							return html;
						}else{
							return value;
						}
					}
				},
				// {field:'nama_sekolah', title:'<b>Nama Sekolah</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'prodi', title:'<b>Prodi</b>', halign:'center', width:150,align:'left',sortable:true},
				// {field:'cgts', title:'<b>CGTS</b>', halign:'center', width:150,align:'left',sortable:true},
			];
		break;
		case "invent":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 50;
			kolom[modnya] = [
				{field:'count_day', title:'<b>Count</b>', halign:'center', valign:'center', width:50,align:'center',
					formatter: function(value, row, i){
						if(Number(row.count_day) >= 1){
							return '+'+row.count_day;
						}else{
							return '0';
						}
					}
				},
				{field:'createTime', title:'<b>Tanggal Daftar</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'sts_daftar', title:'<b>Smart</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'status_contact', title:'<b>Status Contact</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'hp', title:'<b>No HP</b>', halign:'left', width:150, align:'left',sortable:false, 
					formatter: function(value, row, i){
						var html = row.hp;
						if(row.send_wa >= 1){
							html += ' <img width="24px" style="margin-left:8px;" src="'+hostir+'assets/img/social/wa-don.png" onclick="sendwa(\''+row.nama_lengkap+'\', \''+row.hp+'\', \''+row.kode_voucher+'\', \''+row.voucher_expired+'\', \''+row.edu+'\', \''+row.send_wa+'\')">'
						}else{
							html += ' <img width="25px" style="margin-left:8px;" src="'+hostir+'assets/img/social/wa-non.png" onclick="sendwa(\''+row.nama_lengkap+'\', \''+row.hp+'\', \''+row.kode_voucher+'\', \''+row.voucher_expired+'\', \''+row.edu+'\', \'0\')">'
						}
						return html;
					}
				},
				{field:'edu', title:'<b>EC</b>', halign:'left', width:100,align:'left',sortable:true},
				{field:'nama_lengkap', title:'<b>Nama Lengkap</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'email', title:'<b>Email</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'kode_voucher', title:'<b>Kode Voucher</b>', halign:'left', width:150,align:'left',sortable:true},
				{field:'voucher_expired', title:'<b>Exp</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'sumber', title:'<b>Sumber</b>', halign:'center', width:150,align:'left',sortable:true,
					formatter: function(value, row, i){
						if(value != 'Direct Link'){
							var _src = JSON.parse(value);
							html = "<small>utm_source : "+_src.utm_source;
							html += ", utm_medium : "+_src.utm_medium;
							html += "<br>utm_campaign : "+_src.utm_campaign;
							html += ", gclid : "+_src.gclid+'</small>';

							return html;
						}else{
							return value;
						}
					}
				},
				// {field:'nama_sekolah', title:'<b>Nama Sekolah</b>', halign:'center', width:150,align:'left',sortable:true},
				{field:'prodi', title:'<b>Prodi</b>', halign:'center', width:150,align:'left',sortable:true},
				// {field:'cgts', title:'<b>CGTS</b>', halign:'center', width:150,align:'left',sortable:true},
			];
		break;
		case "cmbs2":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 50;
			kolom[modnya] = [
				{field:'foto', title:'<b>DATA PESERTA</b>', halign:'center', width:150,align:'center',sortable:true,
					formatter: function(value,row,index){
							return  '<div class="tiles">'+
									'	<div class="tile image">'+
									'		<a  style="color:black" class="accordion-toggle" href="'+hosturi+'smb/profil/'+row.no_smb+'" target="_blank" >'+
									'		<div class="tile-body">'+				            
									'			<img class="img-responsive" alt="" src="'+hosturi+'assets/upload/foto/'+value+'">'+
									'		</div>'+
									'		<div class="tile-object" style="background-color:#006895">'+
									'			<div class="name">'+row.no_smb+'</div>'+
									'		</div>'+
									'		</a>'+
									'	</div>'+
									'</div>';
					}
				},
				{field:'no_smb',title:'<b>NOMOR REG</b>',width:150, halign:'left',align:'left', hidden:true },
				{field:'nama_cm',title:'<b>NAMA</b>', halign:'left',width:275, align:'left',resizable:true, sortable:true, 
					formatter: function(val, row, idx){
							return '<a href="#" ><h4><b>' + val + '</b></h4></a>'+
									'<a  class="btn btn-xs red"  href="#" onclick="deleteMhs(\'del\',\''+ row.no_smb +'\')"><i class="icon-remove-sign"></i> Delete</a>';
					}},
				{field:'progdi_inisial',title:'<b>PRODI</b>', halign:'left',width:75, align:'left',sortable:true, 
					formatter: function(val, row, idx){ return '<h4>' + val + '</h4>';}},
				{field:'jalur_penerimaan',title:'<b>JALUR</b>', halign:'left',width:150, align:'left',sortable:true, 
					formatter: function(val, row, idx){ return '<h4>' + val + '</h4>';}},
				{field:'tanggal',title:'<b>TANGGAL DAFTAR</b>', halign:'left',width:150, align:'left',sortable:true, 
					formatter: function(val, row, idx){ return '<h4>' + val + '</h4>';}},
				{field:'status',title:'<b>STATUS</b>', halign:'left',width:400, align:'left',
					formatter: function(value,row,index){
						var status = new Array();
						status = value.split('-');
						
						var stsTahap = new Array();
						var icon = new Array();
						
						for(var i = 0; i < status.length; i++){
							if (status[i] == 1){
								stsTahap[i] = "primary";
								icon[i] = ' <i class="icon-ok-sign"></i>';
							}else{
								stsTahap[i] = 'default';
								icon[i] = ' <i class="icon-remove-sign"></i>';
							}
							
						}
						
						var step = [	"Mendaftar ", 
										"Verifikasi Ijazah", 
										"Membayar Formulir Pendaftaran", 
										"Jadwal Ujian & Wawancara", 
										"Hasil Ujian ", 
										"Pengumuman ", 
										"Membayar Biaya Daftar Ulang ", 
										"Melengkapi Form Pendaftaran ", 
										"Mendapat Jadwal Perkuliahan " 
									]
						html = "";
						for (var i = 0; i < step.length; i++){
							html += '<span class="label label-' + stsTahap[i] +' "><i> ' + step[i] + icon[i] + '</i> </span><br>';
						}
						return html;
					}						
				},
			];                
		break;
		case "cmb":
			judulnya = "";
			fitnya = true;
			pagesizeboy = 50;
			kolom[modnya] = [
				{field:'foto', title:'<b>DATA PESERTA</b>', halign:'center', width:150,align:'center',sortable:true,
					formatter: function(value,row,index){
							return  '<div class="tiles">'+
									'	<div class="tile image">'+
									'		<a  style="color:black" class="accordion-toggle" href="'+hosturi+'smb/profil/'+row.no_smb+'" target="_blank" >'+
									'		<div class="tile-body">'+				            
									'			<img class="img-responsive" alt="" src="'+hosturi+'assets/upload/foto/'+value+'">'+
									'		</div>'+
									'		<div class="tile-object" style="background-color:#006895">'+
									'			<div class="name">'+row.no_smb+'</div>'+
									'		</div>'+
									'		</a>'+
									'	</div>'+
									'</div>';
					}
				},
				{field:'no_smb',title:'<b>NOMOR REG</b>',width:150, halign:'left',align:'left', hidden:true },
				{field:'nama_cm',title:'<b>NAMA</b>', halign:'left',width:275, align:'left',resizable:true, sortable:true, 
					formatter: function(val, row, idx){
							return '<a href="#" ><h4><b>' + val + '</b></h4></a>'+
									'<a  class="btn btn-xs red"  href="#" onclick="deleteMhs(\'del\',\''+ row.no_smb +'\')"><i class="icon-remove-sign"></i> Delete</a>';
					}},
				{field:'progdi_inisial',title:'<b>PRODI</b>', halign:'left',width:75, align:'left',sortable:true, 
					formatter: function(val, row, idx){ return '<h4>' + val + '</h4>';}},
				{field:'jalur_penerimaan',title:'<b>JALUR</b>', halign:'left',width:150, align:'left',sortable:true, 
					formatter: function(val, row, idx){ return '<h4>' + val + '</h4>';}},
				{field:'tanggal',title:'<b>TANGGAL DAFTAR</b>', halign:'left',width:150, align:'left',sortable:true, 
					formatter: function(val, row, idx){ return '<h4>' + val + '</h4>';}},
				{field:'status',title:'<b>STATUS</b>', halign:'left',width:400, align:'left',
					formatter: function(value,row,index){
						var status = new Array();
						status = value.split('-');
						
						var stsTahap = new Array();
						var icon = new Array();
						
						for(var i = 0; i < status.length; i++){
							if (status[i] == 1){
								stsTahap[i] = "primary";
								icon[i] = ' <i class="icon-ok-sign"></i>';
							}else{
								stsTahap[i] = 'default';
								icon[i] = ' <i class="icon-remove-sign"></i>';
							}
							
						}
						
						var step = [	"Mendaftar ", 
										"Isi Nilai Rapor", 
										"Membayar Formulir Pendaftaran", 
										"Jadwal Ujian ", 
										"Hasil Ujian ", 
										"Pengumuman ", 
										"Membayar Biaya Daftar Ulang ", 
										"Melengkapi Form Pendaftaran ", 
										"Mendapat Jadwal Perkuliahan " 
									]
						html = "";
						for (var i = 0; i < step.length; i++){
							html += '<span class="label label-' + stsTahap[i] +' "><i> ' + step[i] + icon[i] + '</i> </span><br>';
						}
						return html;
					}						
				},
			];                
		break;
	}
		
	$("#"+modnya).datagrid({
	    title:judulnya,
        height:tingginya,
        width:lebarnya,
        rownumbers:true,
        iconCls:'',
        fit:fitnya,
        striped:true,
        pagination:true,
        remoteSort: false,
        url: hostir+'smb/getdatagrid/'+modnya+(p1 != null ? ('/'+p1) : '') + (p2 != null ? ('/'+p2) : ''),
        nowrap: true,
        singleSelect:true,
        pageSize:pagesizeboy,
        pageList:[10,20,30,40,50,75,100,200],
        queryParams:param,
        columns:[
            kolom[modnya]
        ],
        toolbar: '#toolbar_'+modnya,
	});

	$('.pagination-page-list').css({'display':'inline'});
}

function doSearch(modul, $p2, $p2){
	$('#'+modul).datagrid('load',{
		nama_cm: $('#nama').val(),
		method: "search",
	});
}

function doSelect(modul, $p2, $p2){
	$('#'+modul).datagrid('load',{
		periode: $('#periode').val(),
		jalur: $('#jalur').val(),
		prodi: $('#prodi').val(),
		gelombang: $('#gelombang').val(),
		method: "select",
	});
	$html = '<a href="'+hostir+'smb/exportToExcel/calonSmb/'+$('#periode').val()+'/'+$('#prodi').val()+'/'+$('#jalur').val()+'/'+$('#gelombang').val()+'" class="btn default  black" style="margin-bottom: 12px;"><i class="icon-download-alt"></i> Download Laporan</a>';
	$('#btnDownload').html($html);
}


function deleteMhs(method, p1){
    if (confirm("Apakah Anda Ingin Menghapus Calon Mahasiswa Ini?")) { 
        $.post(hostir + "smb/deleteMhs", {
            'id': p1,
            'method': method
        }, function(resp) {
        if (resp == 1){
            alert("Mahasiswa Berhasil Dihapus.");
            location.reload();
        }else{
            alert(resp);
        }
    });
    }else{
        return true;
    }
}

function tableCMB(){
	var table = $('#myDataTable').DataTable( {
        processing: true,
        serverSide: true,
        ajax: {
            "url": hosturi+"smb/calonTable",
            "type": "POST"
        },
        columns: [
			{data:"f.nomor","visible": false},
            { data: "$.foto",
				"render": function(data, type, row) {					
					return '<div class="tiles">'+
								'<div class="tile image">'+
									'<a  style="color:black" class="accordion-toggle" href="'+hosturi+'smb/profil/'+row.f.nomor+'">'+
									'<div class="tile-body">'+				            
										'<img class="img-responsive" alt="" src="'+hosturi+'assets/upload/foto/'+data+'">'+
									'</div>'+
							'		<div class="tile-object" style="background-color:#006895">'+
							'			<div class="name">'+row.f.nomor+'</div>'+
							'		</div>'+
							'		</a>'+
							'	</div>'+
							'</div>';
				} 
			},
            { data : "u.username", "render": function(data, type, row){ 
				return '<a href="' + hosturi + 'smb/formulir/' + row.f.nomor + '" /> <h4>' + data + '</h4> </a>'; }},
            { data : "p.singkatan", "render":function(data, type, row){ return '<h4>' + data + '</h4>';  }},
            { data: "j.nama", "render":function(data, type, row){ return '<h4>' + data + '</h4>';  }  },
            { data: "$.status", "render":function(data, type, row){ 	
				var status = new Array();
				status = data.split('-');
				
				var stsTahap = new Array();
				var icon = new Array();
				
				for(var i = 0; i < status.length; i++){
					if (status[i] == 1){
						stsTahap[i] = "info";
						icon[i] = '<i class="icon-ok-sign"></i>';
					}else{
						stsTahap[i] = 'danger';
						icon[i] = '<i class="icon-remove-sign"></i>';
					}
					
				}
				
				var step = [	"Mendaftar ", "Melengkapi Form Pendaftaran ", "Jadwal Ujian ", 
								"Menghadiri Ujian ", "Menerima Hasil Ujian ", "Membayar Biaya Daftar Ulang ", 
								"Mendapat NIM " 
							]
				html = "";
				for (var i = 0; i < step.length; i++){
					html += '<p><span class="label label-' + stsTahap[i] +' "> ' + step[i] + icon[i] + '</span></p>';
				}
				return html; 
				
				
				
			}
			}
        ]
    });
	
	table.columns( '.select-filter' ).every( function () {
		var that = this;
	 
		// Create the select list and search operation
		var select = $('<select />')
			.appendTo(
				this.header()
			)
			.on( 'change', function () {
				that
					.search( $(this).val() )
					.draw();
			} );
	 
		// Get the search data for the first column and add to the select list
		this
			.cache( 'search' )
			.sort()
			.unique()
			.each( function ( d ) {
				select.append( $('<option value="'+d+'">'+d+'</option>') );
			} );
	} );
}