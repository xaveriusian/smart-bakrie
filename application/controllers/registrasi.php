<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrasi extends CUTI_Controller{
	function __construct(){
		parent::__construct();	
		setlocale (LC_TIME, 'INDONESIA');

		//$this->auth = unserialize(base64_decode($this->session->userdata('calon_mah')));
		$this->host	= $this->config->item('base_url');
		
		$this->db2 = $this->load->database('second', TRUE); 
		
		$modul = "pendaftaran/";
		$this->smarty->assign('modul',$modul);
		$this->smarty->assign('host',$this->host);
		$this->load->model('mregistrasi');
		$this->db2->query("SET lc_time_names = 'id_ID'");
		

	}
	
	function index() {
		$this->registrasiForm();
	}

	
		
	function faq(){
		$title = "FAQ";
		$page = "faq";
		$this->smarty->assign('title', $title);
		$this->smarty->assign('page', $page);
		$this->smarty->display("pendaftaran/index.html");		
	}

	function regKaryawan(){
		$prodi = $this->db2->get_where('adis_prodi', array('erased'=>0, 'jenjang'=>1))->result_array();
		$this->smarty->assign('prodi', $prodi);

		$title = "Form Pendaftaran S1 Kelas Karyawan";
		$page = "formRegistrationKK";
		$this->smarty->assign('title', $title);
		$this->smarty->assign('page', $page);
		$this->smarty->display("pendaftaran/index.html");

	}

	function registrasiForm(){
		$title = "Form Pendaftaran";
		$page = "formPendaftaran";

		$this->smarty->assign('title', $title);
		$this->smarty->assign('page', $page);
		$this->smarty->display("pendaftaran/index.html");
	}

	function registration(){
		$title = "Form Pendaftaran";
		$page = "formRegistration";
		$this->smarty->assign('title', $title);
		$this->smarty->assign('page', $page);
		$this->smarty->display("pendaftaran/index.html");
	}
	
	function selectProdi(){
		$jenjang = $this->input->post("jenjang");
		$jurusan = $this->input->post("jurusan");
		
		$where = "";
		
		if($jurusan == 'IPS'){
			$where = " AND lulusanType = '$jurusan' ";
		}
		
		$prodi ="SELECT * FROM adis_prodi WHERE erased = 0 AND jenjang = '$jenjang' $where ";
		$prodi = $this->db2->query($prodi)->result();				
		$view = '<option value="">Select..</option>
				{foreach name=lope from=$prodi item=row}
					<option value="{$row->kode}">{$row->nama}</option>
				{/foreach}';
					
		//INISIASI PERIODE YANG SEDANG AKTIF Berdasarkan Jenjang
		$periode = $this->db2->query("SELECT p.kode as id, m.jenjangType 
		from adis_periode p
		LEFT JOIN adis_periode_master m ON m.kode = p.idPeriodeMaster
		WHERE m.jenjangType = '$jenjang' AND p.status = 1 AND p.erased = 0")->row();
		$periode = $periode->id;
		$this->session->set_userdata('periode', $periode);
		
		//$periode = $this->db2->query("SELECT * from adis_periode WHERE jenjangType = '$jenjang' AND `status` = 1 AND erased = 0")->row();
		//$periode = $periode->kode;
		//$this->session->set_userdata('periode', $periode);
		
		////////////////////////////////////////////////////END
		
		
		$this->smarty->assign('prodi',$prodi);
		$this->smarty->display('string:'.$view);
	}
	
	function selectProdiS2(){
		$jenjang = 2;
		$prodi = $this->db2->query("SELECT * FROM adis_prodi WHERE erased = 0 AND jenjang = '$jenjang' ")->result();			
		$view = '<option value="">Select..</option>
				{foreach name=lope from=$prodi item=row}
					<option value="{$row->kode}">{$row->nama}</option>
				{/foreach}';
					
		//INISIASI PERIODE YANG SEDANG AKTIF Berdasarkan Jenjang
		$periode = $this->db2->query("SELECT p.kode as id, m.jenjangType 
		from adis_periode p
		LEFT JOIN adis_periode_master m ON m.kode = p.idPeriodeMaster
		WHERE m.jenjangType = '$jenjang' AND p.status = 1 AND p.erased = 0")->row();
		$periode = $periode->id;
		$this->session->set_userdata('periode', $periode);
		
		$this->smarty->assign('prodi',$prodi);
		$this->smarty->display('string:'.$view);
	}
	
	
	function selectJalurPenerimaan(){
		
		$tahun = $this->input->post("tahun");		
		$tahun_array = array();

		$per_aktif = $this->db->get_where('adis_periode_master',array('status'=>1))->row_array();
		
		$year = $per_aktif['tahun']+1;
		
		$minYearLulus = $year - 3;
		$harga = 0;

		// echo $year."<br>";
		// echo $minYearLulus;

		
		if($tahun < $minYearLulus){
			echo 0;exit; 
		}else{

			$tahun_array[0] = array('kode'=>'RAPOR', 'nama'=>'Jalur Rapor');
			#disable karena tidak ada jalur lain
			// if(($year-3) == $tahun || ($year-2) == $tahun){
			// if(($year-2) == $tahun){
			// 	$tahun_array[0] = array('kode'=>'RAPOR', 'nama'=>'Jalur Rapor');	
			// 	// $tahun_array[0] = array('kode'=>'TULIS', 'nama'=>'Jalur Tes Tulis');
			// 	// $tahun_array[0] = array('kode'=>'ODS', 'nama'=>'One Day Service Online');	
			// }else
			// if(($year-1) == $tahun || $year == $tahun || ($year-2) == $tahun){
			// 	$tahun_array[0] = array('kode'=>'RAPOR', 'nama'=>'Jalur Rapor');	
			// 	// $tahun_array[1] = array('kode'=>'TULIS', 'nama'=>'Jalur Tes Tulis - Mengikuti Ujian Tes Tulis di Kampus Universitas Bakrie Jakarta');		
			// 	// $tahun_array[0] = array('kode'=>'ODS', 'nama'=>'One Day Service Online');		
			// }
		}
		
		$view = '<option value="">Select..</option>
				{foreach name=lope from=$jalur_array item=row}
					<option value="{$row.kode}">{$row.nama}</option>
				{/foreach}';
		$this->smarty->assign('jalur_array',$tahun_array);
		$this->smarty->display('string:'.$view);
	}
	
	
	function selectJalur(){
		
		$prodi = $this->input->post("prodi");
		// $periode = $this->session->userdata('periode');
		
		$jalur ="SELECT kode FROM adis_periode 
				WHERE erased = 0 AND status = 1";
		$jalur = $this->db2->query($jalur)->row_array();	
		
		$kode_periode = $jalur['kode']."."."01".".".$prodi;
		echo $kode_periode;exit;
		$this->smarty->assign('kode_periode', $kode_periode);
	}
	
	
	function selectJalurOld(){
		
		$prodi = $this->input->post("prodi");
		// $periode = $this->session->userdata('periode');
		
		$jalur ="SELECT b.kode as kodesmb, j.nama as namajalur, p.kode , b.jalur, j.kode as kodejalur 
				,b.periode 
				FROM adis_buka_smb b
				INNER JOIN adis_prodi p ON b.prodi = p.kode
				INNER JOIN adis_jalur_smb j ON b.jalur = j.kode
				INNER JOIN adis_periode m ON m.kode = b.periode
				WHERE b.prodi = '$prodi' AND stsBuka = 1 AND b.erased = 0 AND m.`status` = 1";
		$jalur = $this->db2->query($jalur)->result();	
		
		$view = '<option value="">Select..</option>
				{foreach name=lope from=$jalur item=row}
					<option value="{$row->kodesmb}">{$row->namajalur}</option>
				{/foreach}';
				
		print_r($jalur);exit;
		$this->smarty->assign('jalur',$jalur);
		$this->smarty->display('string:'.$view);
	}
	
	function cekPeriodeAktif(){		
		$prodi = $this->input->post("prodi");
		$jalur ="SELECT b.kode as kodesmb, j.nama as namajalur, p.kode , b.jalur, j.kode as kodejalur 
				,b.periode 
				FROM adis_buka_smb b
				INNER JOIN adis_prodi p ON b.prodi = p.kode
				INNER JOIN adis_jalur_smb j ON b.jalur = j.kode
				INNER JOIN adis_periode m ON m.kode = b.periode
				WHERE b.prodi = '$prodi' AND stsBuka = 1 AND b.erased = 0 AND m.`status` = 1";
		$jalur = $this->db2->query($jalur)->num_rows();	
		echo($jalur);
		
	}

	function submitFormKK(){
		$post = $this->input->post();
		if (!$post['prodi'] || !$post['lulusan'] || !$post['name']  || !$post['no_hp'] || !$post['ptn_asal']|| !$post['prodi_asal']){
			echo '<script>alert("Lengkapi kolom form yang masih kosong! ");history.go(-1);</script>';
			exit;
		}
		
		$email = $post["email"];
		$password = $post["password"];
		$count = $this->db2->query("SELECT * FROM adis_smb_usr WHERE kode = '$email'")->num_rows();
		$encrMail = base64_encode($email);
		$gradeUniv = 'S1 - Kelas Karyawan';

		if ($count > 0){
			echo '<script>alert("Email sudah terdaftar, silahkan daftar dengan akun email yang lain. Atau hubungi Admin!");history.go(-1);</script>';
		}else{
			$validation_key = md5(uniqid(rand()));
			if($this->mregistrasi->mRegKelasKaryawan($validation_key) != 0){
				$konten = array( "konten" =>
					"Kepada ".$post['name'].""
					  . "<br>"
					  . "<br>"
					  . "<br>Terima kasih telah mendaftar pada Sistem Penerimaan Mahasiswa Baru Universitas Bakrie (SMART)."
					  . "<br>"
					  . "<br>Anda dapat melanjutkan log in setelah melakukan validasi akun dengan meng-klik tautan dibawah ini:"
					  . "<br>"
					  . "<br>Nama : ".$post['name'].""
					  . "<br>Email : $email"
					  . "<br>Password : $password"
					  . "<br>"
					  . "<br>Link Aktifasi : <a href='".$this->host."registrasi/validasi/$validation_key/$encrMail' class='btn' "
					  . "style='border-radius: 6px;font-family: Arial;color: #ffffff;font-size: 9px;padding: 10px 20px 10px 20px;
							text-decoration: none;background: #6B1314;'>Klik aktifasi disini</a>"
					  . "<br>"
					  . "<br>URL aktivasi : <a href='".$this->host."registrasi/validasi/$validation_key/$encrMail' >".$this->host."registrasi/validasi/$validation_key/$encrMail"."</a>"
					  . "<br>"
					  . "<br>"
					  . "<br>Mohon untuk tidak membalas email ini. Jika ada sanggahan/pertanyaan, silahkan hubungi bagian Admisi Universitas Bakrie."
					  . "<br>"
					  . "<br>"
					  . "<br>"
					  . "<br>"
					  . "<br>"
					  . "<br>"
					  . "<br>Universitas Bakrie, Kawasan Rasuna Epicentrum"
					  . "<br>Jl.H.R Rasuna Said Kav C-22, Kuningan Jakarta "
					  . "<br>Jakarta 12920 Indonesia"
					  . "<br>Office Ph : (021) 526 1448 "
					  . "<br>E-mail : usm@bakrie.ac.id"
				  );

				$subject = "Konfirmasi Pendaftaran Admisi Universitas Bakrie - $gradeUniv - ".$post['name']." "; 
				$mailRespon = ""; 
				// $mailRespon = $this->mregistrasi->emailNotifikasi($email, $subject, $konten);
				if($mailRespon == 1){
					echo '<script>alert("Verifikasi Telah di kirimkan kepada alamat email anda, 
						\\nbila anda tidak menemukan email verifikasi cek pada folder SPAM anda. 
						\\nAtau Silahkan Hubungi Admisi Universitas Bakrie di No 021 5261448
						\\natau Whatsapp ke 0857 777 2010");</script>';
				}else{
					echo '<script>alert("Email validasi gagal dikirim, silahkan menghubungi Admin. ");</script>';
				}	

				//Memanggil fungsi notification
				$idActivity = "13.1";
				$this->activity($email, $idActivity, $email);
				
				$status = "notvalid";
				$this->session->set_userdata('status', $status);
				
				redirect ('/kelaskaryawan', 'refresh');
			}else{
				echo '<script>alert("Mohon maaf, terjadi kesalahan saat melakukan penyimpanan data, silahkan dicoba lagi!");history.go(-1);</script>';
			}

		}

	}

	function submitFormMagister(){
		$post = $this->input->post();
		$rowProdi = $this->db2->query("SELECT * FROM adis_prodi WHERE kode = '".$post['prodi']."'")->row_array();

		if($rowProdi['jenjang'] == 1){
			if (!$post['prodi'] || !$post['tahun_lulus'] || !$post['name']  || !$post['no_hp'] || !$post['ptn_asal']|| !$post['prodi_asal']){
				echo '<script>alert("Lengkapi kolom form yang masih kosong!");history.go(-1);</script>';
				exit;
			}
		}

		
		$email = $post["email"];
		$password = $post["password"];
		$count = $this->db2->query("SELECT * FROM adis_smb_usr WHERE kode = '$email'")->num_rows();
		$encrMail = base64_encode($email);
		$gradeUniv = "S2";
		
		if ($count > 0){
			echo '<script>alert("Email sudah terdaftar, silahkan daftar dengan akun email yang lain. Atau hubungi Admin!");history.go(-1);</script>';
		}else{
			$validation_key = md5(uniqid(rand()));
			if($this->mregistrasi->mRegMegister($validation_key) != 0){
				$konten = array( "konten" =>
					"Kepada ".$post['name'].""
					  . "<br>"
					  . "<br>"
					  . "<br>Terima kasih telah mendaftar pada Sistem Penerimaan Mahasiswa Baru Universitas Bakrie (SMART)."
					  . "<br>"
					  . "<br>Anda dapat melanjutkan log in setelah melakukan validasi akun dengan meng-klik tautan dibawah ini:"
					  . "<br>"
					  . "<br>Nama : ".$post['name'].""
					  . "<br>Email : $email"
					  . "<br>Password : $password"
					  . "<br>"
					  . "<br>Link Aktifasi : <a href='".$this->host."registrasi/validasi/$validation_key/$encrMail' class='btn' "
					  . "style='border-radius: 6px;font-family: Arial;color: #ffffff;font-size: 9px;padding: 10px 20px 10px 20px;
							text-decoration: none;background: #6B1314;'>Klik aktifasi disini</a>"
					  . "<br>"
					  . "<br>URL aktivasi : <a href='".$this->host."registrasi/validasi/$validation_key/$encrMail' >".$this->host."registrasi/validasi/$validation_key/$encrMail"."</a>"
					  . "<br>"
					  . "<br>"
					  . "<br>Mohon untuk tidak membalas email ini. Jika ada sanggahan/pertanyaan, silahkan hubungi bagian Admisi Universitas Bakrie."
					  . "<br>"
					  . "<br>"
					  . "<br>"
					  . "<br>"
					  . "<br>"
					  . "<br>"
					  . "<br>Universitas Bakrie, Kawasan Rasuna Epicentrum"
					  . "<br>Jl.H.R Rasuna Said Kav C-22, Kuningan Jakarta "
					  . "<br>Jakarta 12920 Indonesia"
					  . "<br>Office Ph : (021) 526 1448 "
					  . "<br>E-mail : usm@bakrie.ac.id"
				  );
				  
				  
				  
				$subject = "Konfirmasi Pendaftaran Admisi Universitas Bakrie - $gradeUniv - ".$post['name']." "; 
				$mailRespon = ""; 
				// $mailRespon = $this->mregistrasi->emailNotifikasi($email, $subject, $konten);
				if($mailRespon == 1){
					echo '<script>alert("Verifikasi Telah di kirimkan kepada alamat email anda, 
						\\nbila anda tidak menemukan email verifikasi cek pada folder SPAM anda. 
						\\nAtau Silahkan Hubungi Admisi Universitas Bakrie di No 021 5261448
						\\natau Whatsapp ke 0857 777 2010");</script>';
				}else{
					echo '<script>alert("Email validasi gagal dikirim, silahkan menghubungi Admin. ");</script>';
				}	

				//Memanggil fungsi notification
				$idActivity = "13.1";
				$this->activity($email, $idActivity, $email);
				
				$status = "notvalid";
				$this->session->set_userdata('status', $status);
				
				redirect ('/magister', 'refresh');

			}else{
				echo '<script>alert("Mohon maaf, terjadi kesalahan saat melakukan penyimpanan data, silahkan dicoba lagi!");history.go(-1);</script>';
			}

		}

	}
	
	function formRegistrasi(){
		
		$post = $this->input->post();

		
		$tahun = $post["tahun_lulus"];
		$per_aktif = $this->db->get_where('adis_periode_master',array('status'=>1))->row_array();
		$year = $per_aktif['tahun']+1;
		$minYearLulus = $year - 3;
		
		if($tahun < $minYearLulus){
			echo '<script>alert("Tahun kelulusan yang Anda masukkan tidak memenuhi syarat!");history.go(-1);</script>';exit; 
		}
		
		
		$rowProdi = $this->db2->query("SELECT * FROM adis_prodi WHERE kode = '".$post['prodi']."'")->row_array();
		if($rowProdi['jenjang'] == 1){
			if (!$post['prodi'] || !$post['jalur_penerimaan'] || !$post['prodi2'] || !$post['asal_sma'] || !$post['name']  || !$post['no_hp']){
				echo '<script>alert("Kolom Program Studi & Jalur Seleksi Tidak Boleh Kosong!");history.go(-1);</script>';
				exit;
			}
		}

		if(empty($post['kode_periode'])){
			echo '<script>alert("Tidak dapat melakukan pendaftaran, data salah!");history.go(-1);</script>';
			exit;
		}
		$gradeUniv = ( $rowProdi['jenjang'] == 1 ) ? 'S1' : '' ;
		// $gradeUniv = ( $rowProdi['jenjang'] == 1 ) ? 'S1' : 'S2' ;
		/* 
		if (!$this->validate_captcha()) {
			// What happens when the CAPTCHA was entered incorrectly
			echo '<script>alert("Please check the the captcha form!");history.go(-1);</script>';
			
		} else {*/
		
			// Your code here to handle a successful verification	
		$email = $post["email"];
		$password = $post["password"];
		$count = $this->db2->query("SELECT * FROM adis_smb_usr WHERE kode = '$email'")->num_rows();
		$encrMail = base64_encode($email);
		
		if ($count > 0){
			echo '<script>alert("Email sudah terdaftar, silahkan daftar dengan akun email yang lain. Atau hubungi Admin!");history.go(-1);</script>';
		}else{						
			$validation_key = md5(uniqid(rand()));
			
			if ($this->mregistrasi->mAddCalonMahasiswa($validation_key, $rowProdi['jenjang']) != 0){

				$konten = array( "konten" =>
					"Kepada ".$post['name'].""
					  . "<br>"
					  . "<br>"
					  . "<br>Terima kasih telah mendaftar pada Sistem Penerimaan Mahasiswa Baru Universitas Bakrie (SMART)."
					  . "<br>"
					  . "<br>Anda dapat melanjutkan log in setelah melakukan validasi akun dengan meng-klik tautan dibawah ini:"
					  . "<br>"
					  . "<br>Nama : ".$post['name'].""
					  . "<br>Email : $email"
					  . "<br>Password : $password"
					  . "<br>"
					  . "<br>Link Aktifasi : <a href='".$this->host."registrasi/validasi/$validation_key/$encrMail' class='btn' "
					  . "style='border-radius: 6px;font-family: Arial;color: #ffffff;font-size: 9px;padding: 10px 20px 10px 20px;
							text-decoration: none;background: #6B1314;'>Klik aktifasi disini</a>"
					  . "<br>"
					  . "<br>URL aktivasi : <a href='".$this->host."registrasi/validasi/$validation_key/$encrMail' >".$this->host."registrasi/validasi/$validation_key/$encrMail"."</a>"
					  . "<br>"
					  . "<br>"
					  . "<br>Mohon untuk tidak membalas email ini. Jika ada sanggahan/pertanyaan, silahkan hubungi bagian Admisi Universitas Bakrie."
					  . "<br>"
					  . "<br>"
					  . "<br>"
					  . "<br>"
					  . "<br>"
					  . "<br>"
					  . "<br>Universitas Bakrie, Kawasan Rasuna Epicentrum"
					  . "<br>Jl.H.R Rasuna Said Kav C-22, Kuningan Jakarta "
					  . "<br>Jakarta 12920 Indonesia"
					  . "<br>Office Ph : (021) 526 1448 "
					  . "<br>E-mail : usm@bakrie.ac.id"
				  );
				  
				  
				  
				$subject = "Konfirmasi Pendaftaran Admisi Universitas Bakrie - $gradeUniv - ".$post['name']." "; 
				$mailRespon = ""; 
				// $mailRespon = $this->mregistrasi->emailNotifikasi($email, $subject, $konten);
				if($mailRespon == 1){
					echo '<script>alert("Verifikasi Telah di kirimkan kepada alamat email anda, 
						\\nbila anda tidak menemukan email verifikasi cek pada folder SPAM anda. 
						\\nAtau Silahkan Hubungi Admisi Universitas Bakrie di No 021 5261448
						\\natau Whatsapp ke 0857 777 2010");</script>';
				}else{
					echo '<script>alert("Email validasi gagal dikirim, silahkan menghubungi Admin. ");</script>';
				}	

				//Memanggil fungsi notification
				$idActivity = "13.1";
				$this->activity($email, $idActivity, $email);
				
				$status = "notvalid";
				$this->session->set_userdata('status', $status);
				
				redirect ('/site', 'refresh');
			}else{
				echo '<script>alert("Mohon maaf, terjadi kesalahan saat melakukan penyimpanan data, silahkan dicoba lagi!");history.go(-1);</script>';
			}
		// }
		}

		
	}
	
	function validate_captcha() {
        $recaptcha = trim($this->input->post('g-recaptcha-response'));
        $userIp= $this->input->ip_address();
        $secret='6LcA7TcUAAAAAALakEJqnNXZRBLZPcFZVgIN19NP';
        $data = array(
            'secret' => "$secret",
            'response' => "$recaptcha",
            'remoteip' =>"$userIp"
        );

        $verify = curl_init();
        curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($verify, CURLOPT_POST, true);
        curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($verify);
        $status= json_decode($response, true);
        if(empty($status['success'])){
            return FALSE;
        }else{
            return TRUE;
        }
    }
	
	function sendMail($email = '', $nama = '', $kode_booking = '')
	{
		$config = Array(
		  'protocol' => 'smtp',
		  'smtp_host' => 'ssl://smtp.gmail.com',
		//   'smtp_crypto' => 'ssl',
		  'smtp_port' => 465,
		  'smtp_user' => 'noreply@bakrie.ac.id', // change it to yours
		  'smtp_pass' => 'noname.12920', // change it to yours
		  'mailtype' => 'html',
		  'charset' => 'iso-8859-1',
		  'wordwrap' => TRUE
		);

		// $config = Array(
		// 	'protocol' => 'smtp',
		// 	'smtp_host' => 'ssl://smtp.googlemail.com',
		// 	'smtp_port' => 465,
		// 	'smtp_user' => 'xxx',
		// 	'smtp_pass' => 'xxx',
		// 	'mailtype'  => 'html', 
		// 	'charset'   => 'iso-8859-1'
		// );
		$message = '<html><body> <p>Dear Saudara Kamu</p>';
		$message .= '<p>Anda telah terdaftar pada kegiatan Rakornas BPP Kementrian Dalam Negeri, </p>';
		$message .= '<p><b>Kode Pendaftaran Anda: </b></p>';
		$message .= '<p><b>Print Tiket : <a href="http://inovasi.otda.go.id/rakornas/home/print_ticket/" >http://inovasi.otda.go.id/rakornas/home/print_ticket/'.$kode_booking.'</a></b></p>';
		$message .= '<p>Terima Kasih</p></body></html>';
		
		$this->load->library('email');
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->from('smart.bakrie@gmail.com'); // change it to yours
		$this->email->to('syalevi@gmail');// change it to yours
		$this->email->subject('Informasi Pendaftaran Online Rakornas BPP Kementrian Dalam Negeri');
		$this->email->message($message);
		// show_error($this->email->print_debugger());
		$res = $this->email->send();
		echo $res;
		// if($this->email->send()){
		// 	 echo 1;
		// }else{
		// 	 echo 0;
		// }

	}
	
	function info(){
		phpinfo();
	}
        
    function testMail(){
		
            
		}

    function setEduAll(){
        $data = $this->db->get_where('smart_telemarketing', array('edu'=>''))->result_array();
        foreach($data as $val){
            $edu_next = "";
            $edu_next = $this->cekEdu($val['kode']);
            if($edu_next != ""){
                echo $edu_next."<br>";
                $this->db->where('kode', $val['kode']);
                $this->db->update('smart_telemarketing', array(
                    'edu'=>$edu_next
                ));
            }
        }
        // echo "<pre>";
        // print_r($data);
	}
	
	function cekEdu($kode){

		if($kode){

			$edu = $this->db->get_where('adis_sys_usr', array('id_level'=>44, 'erased'=>0, 'username !='=> 'Rekomendasi'))->result_array();
			$query_max_edu = $this->db->query("
					select * from smart_telemarketing a 
					inner join (select max(id) as max_id from smart_telemarketing where edu != '') as b on a.id = b.max_id 
					inner join adis_sys_usr c on c.username = a.edu and c.erased = 0 ");
			$sizeEdu = count($edu);
			// print_r($edu);exit;

			if($query_max_edu->num_rows() > 0){
				$query_max_edu = $query_max_edu->row_array();
				$next_edu = '';
				foreach($edu as $k => $val){
					if($query_max_edu['edu'] == $val['username']){
						if(($sizeEdu-1) == $k){
							$next_edu = $edu[0]['username'];
							break;
						}else{
							$next_edu = $edu[$k+1]['username'];
							break;
						}
					}
				}
		
				// echo "next".$next_edu;

				return $next_edu;
				
			}else{
				$next_edu = $edu[0]['username'];
				// echo "next 2".$next_edu;
				return $next_edu;
			}

		}
	}
		
	function setEdu(){
		$kode = $this->input->post('kode');

		if($kode){

			$edu = $this->db->get_where('adis_sys_usr', array('id_level'=>44, 'erased'=>0, 'username !='=> 'Rekomendasi'))->result_array();
			$query_max_edu = $this->db->query("
					select * from smart_telemarketing a 
					inner join (select max(id) as max_id from smart_telemarketing where edu != '') as b on a.id = b.max_id 
					inner join adis_sys_usr c on c.username = a.edu and c.erased = 0 ");
			$sizeEdu = count($edu);

			if($query_max_edu->num_rows() > 0){
				$query_max_edu = $query_max_edu->row_array();
				$next_edu = '';
				foreach($edu as $k => $val){
					if($query_max_edu['edu'] == $val['username']){
						if(($sizeEdu-1) == $k){
							$next_edu = $edu[0]['username'];
							break;
						}else{
							$next_edu = $edu[$k+1]['username'];
							break;
						}
					}
				}

				// echo "next : ".$next_edu;exit;

				$onTele = $this->db->get_where('smart_telemarketing',array('kode'=>$kode));
				if($onTele->num_rows() > 0){
					$this->db->where('kode', $kode);
					$this->db->update('smart_telemarketing', array(
						'kode'=>$kode,
						'edu'=>$next_edu,
						'date_created'=>date('Y-m-d H:i:s')
					));
				}else{
					$this->db->insert('smart_telemarketing', array(
						'kode'=>$kode,
						'edu'=>$next_edu,
						'date_created'=>date('Y-m-d H:i:s')
					));
				}
				// }

				
				// header("Location: " .  $this->host.'/smb/telemarketing');
				
			}else{
				$next_edu = $edu[0]['username'];
				// echo "next : ".$next_edu;exit;
				$onTele = $this->db->get_where('smart_telemarketing',array('kode'=>$kode));
				if($onTele->num_rows() > 0){
					$this->db->where('kode', $kode);
					$this->db->update('smart_telemarketing', array(
						'kode'=>$kode,
						'edu'=>$next_edu,
						'date_created'=>date('Y-m-d H:i:s')
					));
				}else{
					$this->db->insert('smart_telemarketing', array(
						'kode'=>$kode,
						'edu'=>$next_edu,
						'date_created'=>date('Y-m-d H:i:s')
					));
				}
				// }
				
				// header("Location: " .  $this->host.'/smb/telemarketing');
			}

		}
	}
	
	function validasi($validation_key = ""){
		$redirect_link =  $this->host;
		$validation_key = $this->uri->segment(3);
		$kode = base64_decode($this->uri->segment(4));

		$usr = $this->db2->query("SELECT * FROM adis_smb_usr WHERE validation_Key = '$validation_key' AND kode = '$kode'")->row();
		// echo $usr->kode;exit;
		
		if($usr){
			if($usr->validation_status != 1){

				$maba = $this->db2->query("select substr(bukaSmb, 6, 1) as jenjang from adis_smb_form where kode='$kode';")->row_array();
				$tbl_invent = 'tbl_invent';
				if($maba['jenjang'] == 2){
					$tbl_invent = 'tbl_invent_mm';
					$redirect_link .= "magister"; 
				}elseif($maba['jenjang'] == 3){
					$tbl_invent = 'tbl_invent_kk';
					$redirect_link .= "kelaskaryawan"; 
				}
		
				$this->db2->where("kode",$kode);
				$this->db2->update("adis_smb_usr", array("validation_status"=>1));
				
				$noRegistrasi = $this->mregistrasi->mGenNoPendafataran($kode);
				
				
				$onTele = $this->db->get_where('smart_telemarketing',array('kode'=>$kode));
				if($onTele->num_rows() <= 0){
					if($noRegistrasi != 0){

						$edu_invent = $this->db->get_where($tbl_invent, array('email'=>$kode));
						if($edu_invent->num_rows() > 0){
							$edu_invent = $edu_invent->row_array();
							if($edu_invent['edu']){
								$this->db->insert('smart_telemarketing', array(
									'kode'=>$kode,
									'edu'=>$edu_invent['edu'],
									'date_created'=>date('Y-m-d H:i:s')
								));
							}
						}else{

							$where_cek = " where 1=1 and id_level = 44 and aktif_edu = 1 and erased = 0 ";
							if($maba['jenjang'] == 1){
								$where_cek .= " and is_reg = 1 ";
							} else if($maba['jenjang'] == 2){
								$where_cek .= " and is_mag = 1 ";
							}else if($maba['jenjang'] == 3){
								$where_cek .= " and is_kk = 1 ";
							}

							$edu = $this->db->query("select * from adis_sys_usr $where_cek ")->result_array();
							// $edu = $this->db->get_where('adis_sys_usr', array('id_level'=>44, 'erased'=>0, 'username !='=> 'Rekomendasi'))->result_array();
							$query_max_edu = $this->db->query("
								select * from smart_telemarketing a 
								inner join (select max(id) as max_id from smart_telemarketing where edu != '') as b on a.id = b.max_id 
								inner join adis_sys_usr c on c.username = a.edu and c.erased = 0 ");
							$sizeEdu = count($edu);
							if($query_max_edu->num_rows() > 0){
								$query_max_edu = $query_max_edu->row_array();
								$next_edu = '';
								foreach($edu as $k => $val){
									if($query_max_edu['edu'] == $val['username']){
										if(($sizeEdu-1) == $k){
											$next_edu = $edu[0]['username'];
											break;
										}else{
											$next_edu = $edu[$k+1]['username'];
											break;
										}
									}
								}

								if(empty($next_edu)){
									$next_edu = $edu[0]['username'];
								}

								$this->db->insert('smart_telemarketing', array(
									'kode'=>$kode,
									'edu'=>$next_edu,
									'date_created'=>date('Y-m-d H:i:s')
								));

								// echo $next_edu;
							}else{
								$next_edu = $edu[0]['username'];
								$this->db->insert('smart_telemarketing', array(
									'kode'=>$kode,
									'edu'=>$next_edu,
									'date_created'=>date('Y-m-d H:i:s')
								));
							}
						}
					
					}
				}
			}else{
				echo "<script>alert('Validasi gagal, akun Anda telah aktif. Silahkan login menggunakan akun Anda!');</script>";
				exit;				
			}
			
			

		}else{
			echo "<script>alert('Validation failed, make sure you have correct url!');</script>";
			exit;
		}
		
		$status = "valid";
		$this->session->set_userdata('status', $status);
		header("Location: " .  $redirect_link);
	}
	
	
	
	function integrasi_data(){
		$this->load->model('mapi');
		
		$query = "SELECT A.nomor, A.bukaSmb, A.kode FROM adis_smb_form A 
			WHERE A.kode = 'syalevi@gmail.com' "; //AND RIGHT(A.bukaSmb,4) = '2141' ";
			
		$result = $this->db->query($query)->result_array();
		
		// foreach($result as  $val){
			// echo $val['nomor']."<br>";
		// }
		// echo count($result);exit;
		
		foreach($result as $val){
			$integrasi[] = $this->mapi->addMahasiswa($val['kode'], 'Add');
			// $integrasi[] = $this->mapi->tagihan_reg($val['kode'], 'createTagihan');
			// $integrasi[] = $this->mapi->altoPay($val['kode'], 'cek');
			// $integrasi[] = $this->mapi->tagihan_daftar_ulang($val['kode'], 'addTagihan');
			// $integrasi[] = $this->mapi->tagihan_daftar_ulang($val['kode'], 'updateTagihan');
		}
		
		echo "<pre>";
		print_r($integrasi);
		echo "<br>";
		
	}
	
	function activity($email, $idActivity, $user){
		$kode = uniqid();
		$date = date("Y-m-d H:i:s");
		$activity = $this->db2->query("SELECT nama, kode FROM adis_type WHERE kode = '$idActivity'")->row();
		$this->db2->insert('adis_activity', array('kode'=>$kode,
								'id_activity'=>$activity->kode,
								'created_date'=>$date,
								'created_user'=>$user,
								'nama_activity'=>$activity->nama,
								'status_activity'=>0,
								'id_cmb'=>$email
								));
	}
}
