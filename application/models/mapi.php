<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

class Mapi extends CUTI_Model{

	function __construct(){
		parent::__construct();


		// $this->host_asik = "http://admission.paramadina.ac.id/site/";

		// $this->username = 'admission';
		// $this->password = 'P@r4m4d1n@!';
	}

	function Mapi(){
		parent::__construct();

		$this->db2->query("SET lc_time_names = 'id_ID'");

	}

	function insert_temp_big($nim, $user = ""){
		$this->sb = $this->load->database('big', TRUE);
		$mhs= $this->sb->get_where('smart.calon_mahasiswa', array('nim'=>$nim));
		if($mhs->num_rows() > 0){
			$this->db->insert('smart_integrasi_big', array(
				'nim'=>$nim,
				'query'=>"",
				'respon'=>'Failed, Students has inserted to BIG!',
				'datetime'=>date('Y-m-d H:i:s'),
				'user'=>$user
			));
			return "Gagal, data mahasiswa sudah tersedia di big!";
		}else{
			$queryinsert = $this->getMahasiswaData($nim);
			if($queryinsert != '0'){
				$this->sb->query($queryinsert);

				$this->db->where(array('nim'=>$nim));
				$this->db->update('adis_smb_form', array(
					'validasiDataFinal'=>1,
					'stsIntegrasikeBIG'=>1,
					'tglIntegrasi'=>date('Y-m-d H:i:s')
					));

				$this->db->insert('smart_integrasi_big', array(
					'nim'=>$nim,
					'query'=>$queryinsert,
					'respon'=>'Success, Data mahasiswa berhasil diintegrasikan ke BIG 2.0',
					'datetime'=>date('Y-m-d H:i:s'),
					'user'=>$user
				));

				return 1;
			}

		}

	}

	function postBIG_($nim, $user = ""){
		$this->dbbig = $this->load->database('big', TRUE);

		$mhs= $this->dbbig->get_where('akademik.ak_mahasiswa', array('nim'=>$nim));
		if($mhs->num_rows() > 0){

			$this->db->insert('smart_integrasi_big', array(
				'nim'=>$nim,
				'query'=>"",
				'respon'=>'Failed, Students has inserted to BIG!',
				'datetime'=>date('Y-m-d H:i:s'),
				'user'=>$user
			));
			return "Failed, Students has inserted to BIG!";
		}else{
			$queryinsert = $this->getMahasiswaData($nim);
			// echo $queryinsert;exit;
			if($queryinsert != '0'){
				// return $queryinsert;

				$this->dbbig->query($queryinsert);
				// // $this->dbbig->insert('akademik.ak_mahasiswa', $queryinsert);

				$this->db->where(array('nim'=>$nim));
				$this->db->update('adis_smb_form', array(
					'validasiDataFinal'=>1,
					'stsIntegrasikeBIG'=>1,
					'tglIntegrasi'=>date('Y-m-d H:i:s')
					));

				$this->db->insert('smart_integrasi_big', array(
					'nim'=>$nim,
					'query'=>$queryinsert,
					'respon'=>'Success, Data mahasiswa berhasil diintegrasikan ke BIG 2.0',
					'datetime'=>date('Y-m-d H:i:s'),
					'user'=>$user
				));
				return 'Success, Data mahasiswa berhasil diintegrasikan ke BIG 2.0';
			}else{

				$this->db->insert('smart_integrasi_big', array(
					'nim'=>$nim,
					'query'=>$queryinsert,
					'respon'=>'Failed, Periode not existed!',
					'datetime'=>date('Y-m-d H:i:s'),
					'user'=>$user
				));

				return 'Failed, Periode not existed!';
			}
		}

	}

	function getMahasiswaData($nim=''){
		$sql = "
			select a.kode, a.nim, a.nomor, a.nomor as no_registrasi,  b.nama, concat(left(a.bukaSmb,4), substring(a.bukaSmb, 8,1)) as idperiode,
				'' idsistemkuliah, '' idjalurpendaftaran, substring(a.bukaSmb, 10,1) as idgelombang, c.idBig as idunit,
				c.`nama` as prodi, if(d.lulusankk='TRANSFER', 1, '0') istransfer,
				d.lulusAsal iduniversitasasal, d.nisn_nim nimlama, d.nilai ipkasal, '' as idkurikulum, e.`nama` as agama,
				e.`idBig` as idagama,  'IDN' as idnegara, 'A' idstatusmhs ,b.rumahAlamat as alamat, b.`rumahTel` telepon, b.rumahCell hp,
				b.`tempatLahir` tmplahir, b.tanggalLahir tgllahir, f.`kodepos`,b.genderType, if(b.genderType='03.P','L','P') as jk, 'g' goldarah, '' statusnikah,
				a.kode as email, b.nomorKtp nik, '' nokk, b.rumahRT rt, b.rumahRW rw, b.rumahKecamatan namakecamatan, b.rumahKecamatanKode as idkecamatan,
				b.rumahKelurahan desa, b.rumahKabKota as idkota, g.nama as namakota, b.rumahProp as idpropinsimu, h.nama as namapropinsi,
				date_format(a.createTime, '%Y-%m-%d') as tgldaftar, b.ayahNama as namaayah, b.ayahAlamat alamatayah, b.ayahCell telpayah, '' tglLahirAyah, '' pendAyah, '' kerjaAyah,
				'' penghasilanAyah, b.ibuNama namaibu, b.ibuAlamat alamatibu, b.ibuCell telpibu, b.tglLahirIbu, '' pendIbu, '' pekIbu, '' pengIbu,
				b.waliNama namawali, b.waliAlamat alamatwali, b.waliCell telpwali, '' tgllahirWali, '' pendWali, '' pekWali, '' pengWali, '' periodeTransfer, '' tglTransfer, '' sksAsal, '' jenjangpendasal,
				b.ibuEmail as emailortu, '' nisn, '' npsn, i.kode_potongan, SUBSTRING(a.bukaSmb, 6, 1) as tagjenjang, b.`agamaType`,
				d.lulusankk, b.rumahProp, b.rumahKabKota, b.rumahKodePos, b.foto
				from adis_smb_form a
				left join `adis_smb_usr_pribadi` b on a.kode = b.kode
				left join adis_prodi c on right(a.bukaSmb,4) =. c.kode
				left join adis_smb_usr_edu d on a.kode = d.smbUsr
				left join adis_type e on b.`agamaType` = e.kode
				left join adis_kodepos f on b.rumahKodePos = f.kode
				left join adis_wil g on b.rumahKabKota = g.kode
				left join adis_wil h on b.rumahProp = h.kode
				left join adis_smb_usr_keu i on a.kode = i.smbUsr
				left join adis_wil j on b.rumahKecamatanKode = j.kode
			where nim = '$nim' ";


		$val = $this->db->query($sql)->row_array();
		// $periode = $this->dbbig->get_where('ref.ms_periode', array('idperiode'=>$val['idperiode']));

		// if($periode->num_rows() <= 0){
		// 	return 0;
		// }

		// $this->dbbig = $this->load->database('big', TRUE);

		$jenjangMaba = $val['tagjenjang'];
		$kode = $val['kode'];

		$berkas = $this->db2->query("SELECT * FROM `tbl_master_berkas` A
				left JOIN tbl_kelengkapan_berkas B ON A.id_berkas = B.id_berkas AND B.kode_cmb = '$kode'
				WHERE  A.jenjang in (".$jenjangMaba.",0);");

		$stsBerkas = $this->db2->query("select * from tbl_kelengkapan_berkas WHERE kode_cmb = '$kode' ");


		if ($stsBerkas->num_rows == 0 ){
			$berkas = $this->db2->query("SELECT A.*, '' as status FROM `tbl_master_berkas` A  where jenjang in ('".$jenjangMaba."',0) ")->result_array();
		}else{
			$berkas = $berkas->result_array();
		}

		$satusberkas = "";
		$dokumen = array();
		$berkas_test = array();
		if($jenjangMaba ==  1){
			$satusberkas = $this->db2->query("SELECT * FROM `tbl_smb_berkas` WHERE kode = '$kode';");
			$dokumen = $this->db2->query("SELECT * FROM tbl_smb_berkas WHERE kode = '$kode' ")->row_array();
		}else if($jenjangMaba == 2){
			$satusberkas = $this->db2->query("SELECT * FROM `tbl_smb_berkas_mm` WHERE kode = '$kode';");
			$dokumen = $this->db2->query("SELECT * FROM tbl_smb_berkas_mm WHERE kode = '$kode' ")->row_array();

			$berkas_test = $this->db->query("Select fc_ijazah, fc_transkrip from smart_jadwal_test where kode='$kode'")->row_array();
			$this->smarty->assign('berkas_test', $berkas_test);

		}else if($jenjangMaba == 3){
			$satusberkas = $this->db2->query("SELECT * FROM `tbl_smb_berkas_kk` WHERE kode = '$kode';");
			$dokumen = $this->db2->query("SELECT * FROM tbl_smb_berkas_kk WHERE kode = '$kode' ")->row_array();

			$berkas_test = $this->db->query("Select fc_ijazah, fc_transkrip from smart_jadwal_tpa where kode='$kode'")->row_array();
			$this->smarty->assign('berkas_test', $berkas_test);
		}

		$all_doc = array();
		$sts_doc = array();
		$url_ = "";
		if($val['foto']){
			$url_ = $this->host.'assets/upload/foto/'.$val['foto'];
		}
		$all_doc[] = array('nama'=>'foto', 'desc'=>'Foto', 'url'=>$url_);

		foreach($berkas as $val_){
			$url = "";
			if ($dokumen[$val_['field']]){
				$sts_doc[$val_['field']] = 1;
				$url = $this->host.'assets/upload/pemberkasan/'.$val['nomor'].'/'.$dokumen[$val_['field']];
				$all_doc[] = array('nama'=>$val_['field'],'desc'=>$val_['nama_berkas'],'url'=>$url);
			}else{
				$sts_doc[$val_['field']] = 0;
				$all_doc[] = array('nama'=>$val_['field'],'desc'=>$val_['nama_berkas'],'url'=>$url);
			}
		}

		foreach($berkas_test as $k =>$val_){
			$url = "";
			$nama_berkas = $k == 'fc_ijazah' ? 'Foto Copy Ijazah' : ($k == 'fc_transkrip' ? 'Foto Copy Transkrip Nilai' : '');
			if ($val_){
				$sts_doc[$k] = 1;
				if($val['tagjenjang'] == 2){
					$url = $this->host.'assets/upload/magister/'.$val['nomor'].'/'.$val_;
				}elseif($val['tagjenjang'] == 3){
					$url = $this->host.'assets/upload/kk/'.$val['nomor'].'/'.$val_;
				}
				$all_doc[] = array('nama'=>$k,'desc'=>$nama_berkas,'url'=>$url);
			}else{
				$sts_doc[$k] = 0;
				$all_doc[] = array('nama'=>$k,'desc'=>$nama_berkas,'url'=>$url);
			}


		}

		$json_doc = json_encode($all_doc);
		$base64_doc = base64_encode(json_encode($all_doc));


		if($val['tagjenjang'] == '3'){
				$val['idjalurpendaftaran'] = 1;
				$val['namajalurpendaftaran'] = 'Reguler';
			}elseif($val['tagjenjang'] == '2'){
				if(!empty($data['kode_potongan'])){
					$beasiswa = $this->db->get_where('smart_ref_beasiswa', array('kode'=>$val['kode_potongan']))->row_array();
					$val['idjalurpendaftaran'] = empty($beasiswa['idjalurpendaftaran']) ? '' : $beasiswa['idjalurpendaftaran'];

					$jalurpendfataran = $this->db->get_where('smart_lv_jalurpendaftaran', array('idjalurpendaftaran'=>$val['idjalurpendaftaran']))->row_array();
					$val['namajalurpendaftaran'] = empty($jalurpendfataran['namajalurpendaftaran']) ? '' : $jalurpendfataran['namajalurpendaftaran'];
				}else{
					$val['idjalurpendaftaran'] = 1;
					$val['namajalurpendaftaran'] = 'Reguler';
				}
			}elseif($val['tagjenjang'] == '1'){
				if(!empty($val['kode_potongan'])){
					$beasiswa = $this->db->get_where('tbl_master_potongan', array('kode_potongan'=>$val['kode_potongan']))->row_array();
					$val['idjalurpendaftaran'] = empty($beasiswa['idjalurpendaftaran']) ? '' : $beasiswa['idjalurpendaftaran'];

					$jalurpendfataran = $this->db->get_where('smart_lv_jalurpendaftaran', array('idjalurpendaftaran'=>$val['idjalurpendaftaran']))->row_array();
					$val['namajalurpendaftaran'] = empty($jalurpendfataran['namajalurpendaftaran']) ? '' : $jalurpendfataran['namajalurpendaftaran'];
				}else{
					$val['idjalurpendaftaran'] = 1;
					$val['namajalurpendaftaran'] = 'Reguler';
				}
			}

			##idsistemkuliah, didapat dari filter jenjang dan tipe lulusan untuk Kelas Karyawan. tabel nya ke smart_sistemkuliah_big
			if($val['tagjenjang'] == '3'){
				$sistemkul = $this->db->get_where('smart_sistemkuliah_big', array('jenjang'=>1, 'jalurmasuk'=>$val['lulusankk']))->row_array();
				$val['idsistemkuliah'] = empty($sistemkul['idsistemkuliah'])?'':$sistemkul['idsistemkuliah'];
				$val['namasistemkuliah'] = empty($sistemkul['namasistemkuliah'])?'':$sistemkul['namasistemkuliah'];
			}elseif($val['tagjenjang'] == '2'){
				$sistemkul = $this->db->get_where('smart_sistemkuliah_big', array('jenjang'=>2, 'idsistemkuliah'=>2))->row_array();
				$val['idsistemkuliah'] = empty($sistemkul['idsistemkuliah'])?'':$sistemkul['idsistemkuliah'];
				$val['namasistemkuliah'] = empty($sistemkul['namasistemkuliah'])?'':$sistemkul['namasistemkuliah'];
			}elseif($val['tagjenjang'] == '1'){
				$sistemkul = $this->db->get_where('smart_sistemkuliah_big', array('jenjang'=>1, 'jalurmasuk is null'=>null))->row_array();
				$val['idsistemkuliah'] = empty($sistemkul['idsistemkuliah'])?'':$sistemkul['idsistemkuliah'];
				$val['namasistemkuliah'] = empty($sistemkul['namasistemkuliah'])?'':$sistemkul['namasistemkuliah'];
			}

			##idkurikulum, tabel nya smart_master_kurikulum. query idperiode, idunit, idsistemkuliah
			$kurikulum = $this->db->get_where('smart_master_kurikulum', array('idunit'=>$val['idunit'], 'idsistemkuliah'=>$val['idsistemkuliah'], 'idperiode'=>$val['idperiode']))->row_array();
			$val['idkurikulum'] = empty($kurikulum['idkurikulum']) ? '' : $kurikulum['idkurikulum'];

			$insertId = array('no_registrasi', 'nim', 'nama', 'idperiode', 'idsistemkuliah', 'idjalurpendaftaran', 'idgelombang', 'idunit',
					'istransfer', 'idkurikulum', 'idagama', 'idnegara', 'idstatusmhs', 'alamat', 'telepon', 'hp', 'tmplahir', 'tgllahir',
					'jk', 'email', 'nik', 'rt', 'rw', 'desa','idkecamatan','namakecamatan', 'idkota', 'namakota', 'namapropinsi','idpropinsimu',
					'tgldaftar', 'namaayah', 'alamatayah', 'telpayah', 'namaibu', 'alamatibu', 'telpibu', 'namawali', 'alamatwali', 'telpwali', 'kodepos'
				);
			$valInsert = array();
			foreach($insertId as $v){
				// array_push($valInsert, "'".trim($val[$v])."'");
				array_push($valInsert, "'".trim(str_replace("'", "''", $val[$v]))."'");
			}
			$valInsert[] = "'".$json_doc."'";
			$insertId[] = 'docsurl';
			$valInsert[] = "'".$base64_doc."'";
			$insertId[] = 'docsurl_base64';

			$fieldInsert = implode(',', $insertId);
			$valueInsert = implode(',', $valInsert);

			$insQry = "insert into smart.calon_mahasiswa ($fieldInsert) values ($valueInsert);";

			return $insQry;

			$insert_array = array();
			foreach($insertId as $v){
				$insert_array[$v] = $val[$v];
			}

			return $insert_array;
	}

	function testMasukAccount($kode = ''){
		// $token = '0d5bd0a6a31d14f384ed4c5f40383791';
		$token = '03a135507c9d2a19f44f4593c545c2c4';
		$domainName = 'https://usm.bakrie.ac.id';
		$serverUrl = "";
		$error = "";

		$serverUrl = $domainName . '/webservice/rest/server.php' . '?wstoken=' . $token;

		$qryMhs = "SELECT A.kode, A.nomor, B.nama, C.password, D.nama as prodi FROM adis_smb_form A
			LEFT JOIN adis_smb_usr_pribadi B ON B.kode = A.kode
			LEFT JOIN adis_smb_usr C ON C.kode = A.kode
			LEFT JOIN adis_prodi D ON D.kode = RIGHT(A.bukaSmb, 4)
			WHERE A.kode = '$kode';";
		$mhs = $this->db2->query($qryMhs)->row_array();
		$this->load->library('encrypt');
		$password = $this->encrypt->decode($mhs['password']);
		if(strlen($password)<6){
			$password = '1234567';
		}

		$fullName = $this->split_name($mhs['nama']);
		if($fullName[1] == ''){
			$fullName[1] = $fullName[0];
		}


		$functionName = 'core_user_create_users';

		$user1 = new stdClass();
        $user1->username = strtolower($kode);
        $user1->password = $password ;
        $user1->firstname = $fullName[0];
        $user1->lastname = $fullName[1];
        $user1->email = $kode;
        $user1->auth = 'manual';
        $user1->idnumber = '';
        $user1->lang = 'en';
        $user1->timezone = 'Asia/Jakarta';
        $user1->mailformat = 0;
        $user1->description = '';
        $user1->city = 'Jakarta';
        $user1->country = 'ID';     //list of abrevations is in yourmoodle/lang/en/countries
        // $preferencename1 = 'auth_forcepasswordchange';
        // $user1->preferences = array(
            // array('type' => $preferencename1, 'value' => 'false')
            // );


		$users = array((array)$user1);
		$params = array('users' => (array)$users);
		/// REST CALL
		$restformat = "json";
		$serverurl = $serverUrl . '&wsfunction=' . $functionName. '&moodlewsrestformat=' . $restformat;
		require_once (APPPATH."libraries/curl.php");
		$curl = new curl();

		echo "<pre>";
		print_r($params);
		// exit;

		$resp = $curl->post($serverurl, $params);

		print_r($resp);

	}

	function split_name($name) {
		$name = trim($name);
		$last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
		$first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
		return array($first_name, $last_name);
	}



}
